<div class="center-title">![Readme Screenshot](./screenshots/Readme.png)</div>

<div class="center-title">

# Field Agent Companion

</div>

<div class="container">

<div class="topleft-column">![Project Screenshot 1](./screenshots/image1.png)</div>

<div class="topcenter-column">

# Project Description

The Field Agent Companion app before you is a powerful tool designed to assist field agents in conducting successful interviews and presentations. With this app, agents can create customized surveys and presentations that can be easily accessed and shared with clients or potential customers. The app also features advanced analytics capabilities, allowing agents to gather valuable insights and track their performance in real-time. In addition, this app is fully integrated with popular CRM and sales automation tools, streamlining the sales process and boosting productivity. With its user-friendly interface and powerful features, the Field Agent Companion app is an indispensable tool for any field agent looking to maximize their performance and drive business growth.


</div>

<div class="topright-column">

# My Tech Stack:

*   Kotlin, Java
*   AndroidX
*   Material Design
*   Jetpack Compose
*   Coroutines & Flow
*   Android Architecture Components
*   Dagger Hilt
*   Gradle
*   GitLab
*   JUnit
*   Jupiter
*   Mockito
*   MockK
*   Espresso
*   Web Service Integration (REST, JSON, XML, GSON)
*   Firebase
*   Firestore
*   Room DB
*   REST
*   RESTful APIs
*   Retrofit2
*   GraphQL (Apollo)
*   Glide
*   Glide for Jetpack Compose
*   Coil for Jetpack Compose
*   Google Pay
*   Google Location
*   MVI
*   MVVM
*   MVC
*   Service Locator
*   Clean Architecture
*   Dependency Injection
*   CI/CD
*   Clean Code Principles
*   Agile Software Development Practices
*   Test Driven Development

</div>

</div>

<div class="container">

<div class="left-column">

# Target Customer Persona

The target user of the Field Agent Companion app would be a field agent or sales representative who conducts interviews and presentations as part of their job. This user would typically be tech-savvy and comfortable with using mobile applications to increase their efficiency and productivity. They may work for a variety of industries, such as market research, advertising, or sales, and would need to conduct interviews and presentations in various settings, including offices, retail stores, and customer homes. They would benefit from the app's real-time analytics, which would allow them to track their performance and adjust their approach as needed. Overall, the Field Agent Companion app is an ideal solution for any field agent looking to streamline their workflow and increase their success in conducting interviews and presentations.


</div>

<div class="right-column">![Project Screenshot 2](./screenshots/image2.png)</div>

</div>

<div class="container">

<div class="left-column">![Project Screenshot 3](./screenshots/image3.png)</div>

<div class="right-column">

# Problem Statement

	1. Inefficient workflows: The app streamlines the sales process by integrating with popular CRM and sales automation tools, reducing the need for manual data entry and increasing productivity.
	2. Limited insights: The app provides advanced analytics capabilities, enabling field agents to gather valuable insights and track their performance in real-time, leading to more informed decision-making and improved results.
	3. Difficulty in accessing materials: The app allows field agents to access and share presentations and surveys with clients and potential customers, even when offline, eliminating the need for physical materials and reducing the risk of lost or damaged documents.
	4. Poor user experience: The app's user-friendly interface and intuitive design make it easy for field agents to navigate and use, resulting in a better overall user experience and increased adoption.


</div>

</div>

<div class="container">

<div class="right-column">

# Functionalities

	1. Offline access: The app enables field agents to access and share their surveys and presentations even when offline, ensuring they can always be prepared for meetings and presentations.
	2. Real-time analytics: The app provides advanced analytics capabilities, allowing field agents to track their performance and gather valuable insights in real-time.
	3. Integration with CRM and sales automation tools: The app seamlessly integrates with company's CRM and sales automation tools, streamlining the sales process and reducing manual data entry.
	4. Secure data storage: The app stores all data securely in the cloud, ensuring that sensitive information remains protected and accessible only to authorized users.
	5. User-friendly interface: The app's user-friendly interface and intuitive design make it easy for field agents to navigate and use, resulting in a better overall user experience and increased adoption.
	6. Collaborative features: The app allows field agents to collaborate with team members and share materials, leading to more effective teamwork and better results.


</div>

<div class="left-column">![Project Screenshot 4](./screenshots/image4.png)</div>

</div>

<div class="container">

<div class="right-column">

# Application's Screens

	1. Home screen: The app's main screen, which displays a summary of recent activity and allows users to access all other app features.
	2. Surveys screen: A screen that displays a list of all surveys created by the user, allowing them to view, edit, and share surveys with others.
	3. Presentations screen: A screen that displays a list of all presentations created by the user, allowing them to view, edit, and share presentations with others.
	4. Analytics screen: A screen that displays real-time analytics and insights, including performance metrics and survey results, allowing users to track their progress and adjust their approach as needed.
	5. CRM Integration screen: A screen that allows users to integrate the app with company's CRM and sales automation tools, enabling them to streamline their sales process and reduce manual data entry.
	6. Settings screen: A screen that allows users to customize the app's settings, such as language preferences, notification settings, and security options.
	7. Collaboration screen: A screen that allows users to collaborate with team members, share materials, and communicate with each other, enabling more effective teamwork and better results.
	8. Help & Support screen: A screen that provides users with access to help and support resources, including user guides, FAQs, and contact information for customer support.


</div>

</div>

<div class="container">

<div class="right-column">

# Disclaimer

In order to provide their clients with protection of trade secrets, protection of user data, legal protection and competitive advantage all commissions of projects of this Developer are by default accompanied by an appropriate Non Disclosure Agreement (NDA). For the same reason all presented: project description, problem statement, functionalities and screen descriptions are for presentation purposes only and may or may not represent an entirety of project.

Due to NDA and obvious copyright and security issues, this GitHub project does not contain entire code of described application.

All included code snippets are for presentation purposes only and should not be used for commercial purposes as they are intellectual property of this application's Developer. In compliance with an appropriate NDA, all presented screenshots of working application have been made after removing any and all graphics that may be considered third party's intellectual property. As such they do not represent a final end product.

The developer does not claim any ownership or affiliation with any of the third-party libraries, technologies, or services used in the project.

While every effort has been made to ensure the accuracy, completeness, and reliability of the presented code, the developer cannot be held responsible for any errors, omissions, or damages that may arise from it's use.

In compliance with an appropriate NDA and in order to provide their clients with all presented: description, target customer persona, problem statement, functionalities, screen descriptions and other items are for presentation purposes only and do not consist an entirety of commisioned project.

Protection of trade secrets, protection of user data, legal protection and protection of competitive advantage of a Client is of utmost importance to the Developer.

</div>

</div>

<div class="container">

<div class="right-column">

# License

Any use of this code is prohibited except of for recruitment purposes only of the Developer who wrote it.

</div>

</div>