package com.aps.catemplateapp

import com.aps.catemplateapp.core.framework.datasources.cachedatasources.room.abstraction.*
import com.aps.catemplateapp.core.framework.datasources.networkdatasources.firebase.abstraction.UserFirestoreServiceTest
import com.aps.catemplateapp.feature01.framework.presentation.activity01.end_to_end.LoginExistingUserFeatureTest
import com.aps.catemplateapp.feature01.framework.presentation.activity01.end_to_end.RegisterUserFeatureTest
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.InternalCoroutinesApi
import org.junit.runner.RunWith
import org.junit.runners.Suite

@FlowPreview
@ExperimentalCoroutinesApi
@InternalCoroutinesApi
@RunWith(Suite::class)
@Suite.SuiteClasses(
    SportsGameDaoServiceTest::class,
    SportsTeamPlayerDaoServiceTest::class,
    Entity3DaoServiceTest::class,
    SportsNewsMessageDaoServiceTest::class,
    UserDaoServiceTest::class,
    UserFirestoreServiceTest::class,
    LoginExistingUserFeatureTest::class,
    RegisterUserFeatureTest::class
)
class InstrumentationTestSuite