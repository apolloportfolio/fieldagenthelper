package com.aps.catemplateapp.core.framework.datasources

import android.app.Application
import com.aps.catemplateapp.core.business.domain.model.entities.ProjectUser
import com.google.common.reflect.TypeToken
import com.google.gson.Gson
import javax.inject.Inject

private const val TAG = "EntityDataFactoryUser"
private const val LOG_ME = true

class EntityDataFactoryUser
@Inject
constructor(
    override val application: Application
): EntityDataFactory<ProjectUser>(application) {
//    override val testClassLoader: ClassLoader,
//    override val fileNameWithTestData: String): com.aps.catemplateapp.core.framework.datasources.EntityDataFactory<ProjectUser>(testClassLoader, fileNameWithTestData) {


    override fun produceListOfEntities(): List<ProjectUser> {
        val entitiesInString: String? = getEntitiesFromFile("user_test_list.json")
        if(entitiesInString != null) {
            return Gson()
                .fromJson(
                    entitiesInString,
                    object : TypeToken<List<ProjectUser>>() {}.type
                )
        } else {
            throw NullPointerException("EntityDataFactoryUser.produceListOfEntities: entitiesInString == null")
        }
    }
}