package com.aps.catemplateapp.core.framework.datasources.networkdatasources.firebase.abstraction

import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import com.aps.catemplateapp.BaseInstrumentedTest
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.core.framework.datasources.EntityDataFactoryEntity1
import com.aps.catemplateapp.core.util.Entity1
import com.aps.catemplateapp.core.util.Entity1Factory
import com.aps.catemplateapp.core.util.Entity1FirestoreMapper
import com.aps.catemplateapp.core.util.Entity1FirestoreService
import com.aps.catemplateapp.core.util.Entity1FirestoreServiceImpl
import com.aps.catemplateapp.core.util.FIRESTORE_TEST_EMAIL
import com.aps.catemplateapp.core.util.FIRESTORE_TEST_PASSWORD
import com.google.firebase.auth.FirebaseAuth
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.tasks.await
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import java.util.*
import javax.inject.Inject
import kotlin.random.Random
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertTrue

/*
    LEGEND:
    1. CBS = "Confirm by searching"

    Test cases:
    1. insert a single entity, CBS
    2. update a random entity, CBS
    3. insert a list of entities, CBS
    4. delete a single entity, CBS
    5. insert a deleted entity into "deletes" node, CBS
    6. insert a list of deleted entities into "deletes" node, CBS
    7. delete a 'deleted entity' (entity from "deletes" node). CBS

 */
@ExperimentalCoroutinesApi
@FlowPreview
@RunWith(AndroidJUnit4ClassRunner::class)
class SportsGameFirestoreServiceTest: BaseInstrumentedTest() {


    // system in test
    private lateinit var entityFirestoreService: Entity1FirestoreService


    // dependencies
    @Inject
    lateinit var entityDataFactory: EntityDataFactoryEntity1

    @Inject
    lateinit var entityFactory: Entity1Factory

    @Inject
    lateinit var entity1NetworkMapper: Entity1FirestoreMapper

    init {
        injectTest()
        signIn()
        insertTestData()
    }

    override fun injectTest() {
        //(application.appComponent as TestAppComponent).inject(this)
    }

    fun insertTestData() {
        this.javaClass.classLoader?.let { classLoader ->
            entityDataFactory = EntityDataFactoryEntity1(application)
        }
        val entityList = entity1NetworkMapper.mapToEntityList(
            entityDataFactory.produceListOfEntities()
        )
        for(entity in entityList){
            entity.id?.let {
                firestore
                    .collection(Entity1FirestoreServiceImpl.COLLECTION_NAME)
                    .document(it.firestoreDocumentID)
                    .set(entity)
            }
        }
    }

    private fun signIn() = runBlocking{
        FirebaseAuth.getInstance().signInWithEmailAndPassword(
            FIRESTORE_TEST_EMAIL,
            FIRESTORE_TEST_PASSWORD
        ).await()
    }

    @Before
    fun before(){
        entityFirestoreService = Entity1FirestoreServiceImpl(
//            firebaseAuth = FirebaseAuth.getInstance(),
//            firestore = firestore,
            entity1NetworkMapper = entity1NetworkMapper,
            dateUtil = dateUtil,
        )
    }

    @Test
    fun insertSingleEntity_CBS() = runBlocking{
        val entity = entityFactory.generateEmpty()
        entity.id = UniqueID(UUID.randomUUID().toString())

        entityFirestoreService.insertOrUpdateEntity(entity)

        val searchResult = entityFirestoreService.searchEntity(entity)

        assertEquals(entity, searchResult)
    }


    @Test
    fun updateSingleEntity_CBS() = runBlocking{

        val searchResults = entityFirestoreService.getAllEntities()

        // choose a random entity from list to update
        val randomEntity = searchResults.get(Random.nextInt(0,searchResults.size-1) + 1)
        val UPDATED_CREATION_DATE = UUID.randomUUID().toString()
        val UPDATED_UPDATE_DATE = UUID.randomUUID().toString()
        var updatedEntity = entityFactory.generateEmpty()
        updatedEntity.id = randomEntity.id
        updatedEntity.created_at = UPDATED_CREATION_DATE
        updatedEntity.updated_at = UPDATED_UPDATE_DATE

        // make the update
        entityFirestoreService.insertOrUpdateEntity(updatedEntity)

        // query the entity after update
        updatedEntity = entityFirestoreService.searchEntity(updatedEntity)!!

        assertEquals(UPDATED_CREATION_DATE, updatedEntity.created_at)
        assertEquals(UPDATED_UPDATE_DATE, updatedEntity.updated_at)
    }

    @Test
    fun insertEntityList_CBS() = runBlocking {
        val list = entityDataFactory.produceListOfEntities()

        entityFirestoreService.insertOrUpdateEntities(list)

        val searchResults = entityFirestoreService.getAllEntities()

        assertTrue { searchResults.containsAll(list) }
    }

    @Test
    fun deleteSingleEntity_CBS() = runBlocking {
        val entityList = entityFirestoreService.getAllEntities()

        // choose one at random to delete
        val entityToDelete = entityList.get(Random.nextInt(0, entityList.size - 1) + 1)

        entityFirestoreService.deleteEntity(entityToDelete.id)

        // confirm it no longer exists in firestore
        val searchResults = entityFirestoreService.getAllEntities()

        assertFalse { searchResults.contains(entityToDelete) }
    }

    @Test
    fun insertIntoDeletesNode_CBS() = runBlocking {
        val entityList = entityFirestoreService.getAllEntities()

        // choose one at random to insert into "deletes" node
        val entityToDelete = entityList.get(Random.nextInt(0, entityList.size - 1) + 1)

        entityFirestoreService.insertDeletedEntity(entityToDelete)

        // confirm it is now in the "deletes" node
        val searchResults = entityFirestoreService.getAllDeletedEntities()

        assertTrue { searchResults.contains(entityToDelete) }
    }

    @Test
    fun insertListIntoDeletesNode_CBS() = runBlocking {
        val entityList = ArrayList(entityFirestoreService.getAllEntities())

        // choose some random entities to add to "deletes" node
        val entitiesToDelete: ArrayList<Entity1> = ArrayList()

        // 1st
        var entityToDelete = entityList.get(Random.nextInt(0, entityList.size - 1) + 1)
        entityList.remove(entityToDelete)
        entitiesToDelete.add(entityToDelete)

        // 2nd
        entityToDelete = entityList.get(Random.nextInt(0, entityList.size - 1) + 1)
        entityList.remove(entityToDelete)
        entitiesToDelete.add(entityToDelete)

        // 3rd
        entityToDelete = entityList.get(Random.nextInt(0, entityList.size - 1) + 1)
        entityList.remove(entityToDelete)
        entitiesToDelete.add(entityToDelete)

        // 4th
        entityToDelete = entityList.get(Random.nextInt(0, entityList.size - 1) + 1)
        entityList.remove(entityToDelete)
        entitiesToDelete.add(entityToDelete)

        // insert into "deletes" node
        entityFirestoreService
            .insertDeletedEntities(entitiesToDelete)

        // confirm the entities are in "deletes" node
        val searchResults = entityFirestoreService.getAllDeletedEntities()

        assertTrue { searchResults.containsAll(entitiesToDelete) }
    }

    @Test
    fun deleteDeletedEntity_CBS() = runBlocking {
        val entity = entityFactory.generateEmpty()
        entity.id = UniqueID(UUID.randomUUID().toString())

        // insert into "deletes" node
        entityFirestoreService.insertDeletedEntity(entity)

        // confirm entity is in "deletes" node
        var searchResults = entityFirestoreService.getAllDeletedEntities()

        assertTrue { searchResults.contains(entity) }

        // delete from "deletes" node
        entityFirestoreService.deleteDeletedEntity(entity)

        // confirm entity is deleted from "deletes" node
        searchResults = entityFirestoreService.getAllDeletedEntities()

        assertFalse { searchResults.contains(entity) }
    }

}