package com.aps.catemplateapp.core.util

class RandomStringGenerator {
    companion object {
        private const val DEFAULT_STRING_LENGTH = 10;
        private const val ALPHANUMERIC_REGEX = "[a-zA-Z0-9]+";
        private val defaultCharPool : List<Char> = ('a'..'z') + ('A'..'Z') + ('0'..'9')

        // https://www.baeldung.com/kotlin/random-alphanumeric-string
        @JvmStatic
        fun generateRandomString(
            stringLength : Int = DEFAULT_STRING_LENGTH,
            charPool : List<Char> = defaultCharPool
        ) : String {
            return (1..stringLength)
                .map { i -> kotlin.random.Random.nextInt(0, charPool.size) }
                .map(charPool::get)
                .joinToString("");
        }
    }
}