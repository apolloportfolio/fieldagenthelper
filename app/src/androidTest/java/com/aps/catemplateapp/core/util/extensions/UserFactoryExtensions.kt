package com.aps.catemplateapp.core.util.extensions


import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.core.business.domain.model.entities.ProjectUser
import com.aps.catemplateapp.core.business.domain.model.factories.UserFactory
import com.aps.catemplateapp.core.util.*

private const val TAG = "UserFactoryExtensions"
private const val LOG_ME = true

fun UserFactory.generateTestUserWithIncompleteProfile() : ProjectUser {
    val user: ProjectUser = generateEmpty()
//    user.id = getTestUserUniqueID()       // id should be set after registering user so that Firebase rules will allow access tot hat user.
    user.emailAddress = TEST_USER_REAL_EMAIL_ADDRESS
    user.password = TEST_USER_REAL_PASSWORD
    user.name = TEST_USER_NAME
    user.surname = TEST_USER_SURNAME
    user.city = TEST_USER_CITY
    user.description = TEST_USER_DESCRIPTION
    return user
}

fun UserFactory.getTestUserUniqueID() : UserUniqueID {
    return UserUniqueID(
        TEST_USER_REAL_ID,
        TEST_USER_REAL_FIRESTORE_DOCUMENT_ID
    )
}

fun UserFactory.generateTestUserWithCompleteProfile() : ProjectUser {
    val user: ProjectUser = generateTestUserWithIncompleteProfile()
    user.apply {
        emailAddressVerified = true
        phoneNumberVerified = true
    }
    return user
}

fun UserFactory.generateTestUnreliableUserWithCompleteProfile() : ProjectUser {
    val unreliableUser: ProjectUser = generateTestUserWithIncompleteProfile()
    unreliableUser.apply {
        emailAddressVerified = true
        phoneNumberVerified = true
    }
    unreliableUser.id = UserUniqueID(
        TEST_USER_2_REAL_ID,
        TEST_USER_2_REAL_FIRESTORE_DOCUMENT_ID,
    )
    unreliableUser.name = "Zbigniew"
    unreliableUser.surname = "Zatęchły"
    unreliableUser.description = "User who throws their words to the wind."
    return unreliableUser
}

fun UserFactory.generateTestEntity1OwnerWithCompleteProfile() : ProjectUser {
    val entity1Owner: ProjectUser = generateTestUserWithIncompleteProfile()
    entity1Owner.apply {
        emailAddressVerified = true
        phoneNumberVerified = true
    }
    entity1Owner.id = UserUniqueID(
        TEST_USER_3_REAL_ID,
        TEST_USER_3_REAL_FIRESTORE_DOCUMENT_ID,
    )
    entity1Owner.name = "Marzena"
    entity1Owner.surname = "Prawicka"
    entity1Owner.description = "Owner of something."
    return entity1Owner
}

fun UserFactory.generateAnotherTestEntity1OwnerWithCompleteProfile() : ProjectUser {
    val entity1Owner: ProjectUser = generateTestUserWithIncompleteProfile()
    entity1Owner.apply {
        emailAddressVerified = true
        phoneNumberVerified = true
    }
    entity1Owner.id = UserUniqueID(
        TEST_USER_4_REAL_ID,
        TEST_USER_4_REAL_FIRESTORE_DOCUMENT_ID,
    )
    entity1Owner.name = "Bonifacy"
    entity1Owner.surname = "Gąbka"
    entity1Owner.description = "Another owner of something."
    return entity1Owner
}