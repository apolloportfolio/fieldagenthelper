package com.aps.catemplateapp.core.util

class ActivationCodeGeneratorEmail {

    companion object {
        private var lastActivationCode : String? = null

        fun getLastActivationCode() = lastActivationCode

        fun generateActivationCode() : String {
            lastActivationCode = RandomStringGenerator.generateRandomString(ProjectConstants.EMAIL_VERIFICATION_CODE_LENGTH)
            return lastActivationCode!!
        }
    }
}