package com.aps.catemplateapp.feature02.framework.presentation.activity01

import androidx.fragment.app.FragmentFactory
import androidx.lifecycle.ViewModelProvider
import com.aps.catemplateapp.common.framework.presentation.UIController
import com.aps.catemplateapp.common.util.DateUtil
import com.aps.catemplateapp.fah.framework.presentation.activity01.fragments.ActivityHomeScreenFragment1
import com.aps.catemplateapp.fah.framework.presentation.activity01.fragments.ActivityHomeScreenFragment2
import com.aps.catemplateapp.fah.framework.presentation.activity01.fragments.ActivityHomeScreenFragment3
import com.aps.catemplateapp.fah.framework.presentation.activity01.fragments.ActivityHomeScreenFragment4
import com.aps.catemplateapp.fah.framework.presentation.activity01.fragments.ActivityHomeScreenFragment5
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import javax.inject.Inject
import javax.inject.Singleton

@ExperimentalCoroutinesApi
@FlowPreview
@Singleton
class TestActivityHomeScreenFragmentFactory
@Inject
constructor(
    private val viewModelFactory: ViewModelProvider.Factory,
    private val dateUtil: DateUtil
): FragmentFactory(){
    lateinit var uiController: UIController
    override fun instantiate(classLoader: ClassLoader, className: String) =
        when(className){
            ActivityHomeScreenFragment1::class.java.name -> {
                val fragment = ActivityHomeScreenFragment1(viewModelFactory, dateUtil)
                if(::uiController.isInitialized){
                    fragment.setUIController(uiController)
                }
                fragment
            }
            ActivityHomeScreenFragment2::class.java.name -> {
                val fragment = ActivityHomeScreenFragment2(viewModelFactory, dateUtil)
                if(::uiController.isInitialized){
                    fragment.setUIController(uiController)
                }
                fragment
            }
            ActivityHomeScreenFragment3::class.java.name -> {
                val fragment = ActivityHomeScreenFragment3(viewModelFactory, dateUtil)
                if(::uiController.isInitialized){
                    fragment.setUIController(uiController)
                }
                fragment
            }
            ActivityHomeScreenFragment4::class.java.name -> {
                val fragment = ActivityHomeScreenFragment4(viewModelFactory, dateUtil)
                if(::uiController.isInitialized){
                    fragment.setUIController(uiController)
                }
                fragment
            }
            ActivityHomeScreenFragment5::class.java.name -> {
                val fragment = ActivityHomeScreenFragment5(viewModelFactory, dateUtil)
                if(::uiController.isInitialized){
                    fragment.setUIController(uiController)
                }
                fragment
            }

            else -> {
                super.instantiate(classLoader, className)
            }
        }
}