package com.aps.catemplateapp.feature02.framework.presentation.activity01.end_to_end

import android.content.Context
import android.content.Intent
import android.os.Parcelable
import android.util.Log
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.semantics.getOrNull
import androidx.compose.ui.test.ExperimentalTestApi
import androidx.compose.ui.test.assert
import androidx.compose.ui.test.assertContentDescriptionContains
import androidx.compose.ui.test.assertIsDisplayed
import androidx.compose.ui.test.assertIsNotDisplayed
import androidx.compose.ui.test.assertIsOff
import androidx.compose.ui.test.assertTextContains
import androidx.compose.ui.test.assertTextEquals
import androidx.compose.ui.test.click
import androidx.compose.ui.test.hasTestTag
import androidx.compose.ui.test.hasText
import androidx.compose.ui.test.isOff
import androidx.compose.ui.test.isOn
import androidx.compose.ui.test.isToggleable
import androidx.compose.ui.test.junit4.createAndroidComposeRule
import androidx.compose.ui.test.onNodeWithContentDescription
import androidx.compose.ui.test.onNodeWithTag
import androidx.compose.ui.test.onNodeWithText
import androidx.compose.ui.test.performClick
import androidx.compose.ui.test.performScrollTo
import androidx.compose.ui.test.performTextReplacement
import androidx.compose.ui.test.performTouchInput
import androidx.test.espresso.Espresso
import androidx.test.espresso.IdlingPolicies
import androidx.test.espresso.action.ViewActions.click
import androidx.test.filters.MediumTest
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.rule.ActivityTestRule
import com.aps.catemplateapp.BaseInstrumentedTest
import com.aps.catemplateapp.R
import com.aps.catemplateapp.common.framework.presentation.IntentExtras
import com.aps.catemplateapp.common.framework.presentation.views.DRAWER_CONTENT_DESCRIPTION_TAG
import com.aps.catemplateapp.common.framework.presentation.views.PeekingClosableTwoButtonBottomSheetsRowCloseButtonTestTag
import com.aps.catemplateapp.common.framework.presentation.views.PeekingClosableTwoButtonBottomSheetsRowLeftButtonTestTag
import com.aps.catemplateapp.common.framework.presentation.views.PeekingClosableTwoButtonBottomSheetsRowRightButtonTestTag
import com.aps.catemplateapp.common.framework.presentation.views.RangeRowRangeMaxTestTag
import com.aps.catemplateapp.common.framework.presentation.views.RangeRowRangeMinTestTag
import com.aps.catemplateapp.common.framework.presentation.views.SWITCH_TEST_TAG
import com.aps.catemplateapp.common.util.ALog
import com.aps.catemplateapp.core.di.NetworkDataSourcesModule
import com.aps.catemplateapp.core.di.ProductionModule
import com.aps.catemplateapp.core.di.RoomModule
import com.aps.catemplateapp.core.util.EspressoIdlingResourceRule
import com.aps.catemplateapp.core.util.extensions.*
import com.aps.catemplateapp.fah.framework.presentation.activity01.ActivityHomeScreen
import com.aps.catemplateapp.fah.framework.presentation.activity01.ActivityHomeScreenComposable
import com.aps.catemplateapp.fah.framework.presentation.activity01.composables.impl.fragments.HomeScreenCompDetailsScreen1ActionButtonTestTag
import com.aps.catemplateapp.fah.framework.presentation.activity01.composables.impl.fragments.HomeScreenCompDetailsScreen1TestTag
import com.aps.catemplateapp.fah.framework.presentation.activity01.composables.impl.fragments.HomeScreenCompFragment1BottomSheetBackgroundTestTag
import com.aps.catemplateapp.fah.framework.presentation.activity01.composables.impl.fragments.HomeScreenCompoDetailsScreen1CityTestTag
import com.aps.catemplateapp.fah.framework.presentation.activity01.composables.impl.fragments.HomeScreenComposableFragment1TestTag
import com.aps.catemplateapp.fah.framework.presentation.activity01.composables.impl.values.Dimens
import com.aps.catemplateapp.fah.framework.presentation.activity01.composables.impl.views.HomeScreenCard1SearchFiltersColumnTestTag
import com.aps.catemplateapp.fah.framework.presentation.activity01.composables.impl.views.HomeScreenCard1SearchFiltersRange1TestTag
import com.aps.catemplateapp.fah.framework.presentation.activity01.composables.impl.views.HomeScreenCard1SearchFiltersRange2TestTag
import com.aps.catemplateapp.fah.framework.presentation.activity01.composables.impl.views.HomeScreenCard1SearchFiltersRangeSlider1TestTag
import com.aps.catemplateapp.fah.framework.presentation.activity01.composables.impl.views.HomeScreenCard1SearchFiltersRangeSlider2TestTag
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import dagger.hilt.android.testing.UninstallModules
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.tasks.await
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import java.util.concurrent.TimeUnit
import kotlin.test.assertNotEquals

private const val TAG = "SearchEntities1ComposableFeatureTest"
private const val LOG_ME = true
/*
    Test case scenarios:
    1. User decided to complete their profile by clicking on profileStatusCompleteProfileButton.
    2. User searches for entities without setting any filters or search query (search around user).
    3. User searches for entities without setting any filters but inputs search query (city).
    4. User searches for entities after setting filters without search query (search around user).
    5. User searches for entities after setting filters with search query (city).
    6. User sets filters and then clears them with removeFiltersButton.
    7. User types search query but decides to delete it with searchBackButton.
    8. User has completed their profile, check if profile completion button is invisible.
    9. User decided to view the first entity in the list. User has incomplete profile.
    10. User decided to view the first entity in the list. User has complete profile.
 */

@MediumTest
@ExperimentalCoroutinesApi
@FlowPreview
@RunWith(AndroidJUnit4ClassRunner::class)
@HiltAndroidTest
@UninstallModules(
    RoomModule::class,
    NetworkDataSourcesModule::class,
    ProductionModule::class,
)
class SearchDishesComposableFeatureTest: BaseInstrumentedTest() {
    
    @get:Rule(order = 0)
    override var hiltRule = HiltAndroidRule(this)

    @get: Rule(order = 1)
    override val espressoIdlingResourceRule = EspressoIdlingResourceRule()
    
    @get:Rule(order = 2)       // Necessary for launching activity with extras
    open var mActivityRuleComposable = ActivityTestRule(
        ActivityHomeScreenComposable::class.java, false, false
    )

//    @get:Rule       // Necessary for launching activity with extras
//    override var mActivityRule = ActivityTestRule(
//        ActivityHomeScreen::class.java, false, false
//    )

    override lateinit var context: Context


    override val firestore = FirebaseFirestore.getInstance()

    @Before
    override fun runBeforeEveryTest() {
        if(LOG_ME)ALog.d(TAG, "runBeforeEveryTest(): " +
                                "")
        IdlingPolicies.setMasterPolicyTimeout(2, TimeUnit.MINUTES)
        IdlingPolicies.setIdlingResourceTimeout(2, TimeUnit.MINUTES)
        hiltRule.inject()
        injectTest()
        context = InstrumentationRegistry.getInstrumentation().targetContext
    }

    @After
    override fun runAfterEveryTest() {
        enableInternetConnection()
    }

    override fun startTestActivityWithUserWhoHasIncompleteProfile() : ActivityHomeScreen {
//        val scenario = launchActivity<ActivityHomeScreen>()       // Launches activity without extras
        if(user == null)user = userFactory.generateTestUserWithIncompleteProfile()
        val intent = Intent()
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        intent.putExtra(IntentExtras.APP_USER, user as Parcelable)
        return mActivityRule.launchActivity(intent)
    }

    private fun startComposableTestActivityWithUserWhoHasIncompleteProfile() : ActivityHomeScreenComposable {
//        val scenario = launchActivity<ActivityHomeScreen>()       // Launches activity without extras
        if(user == null)user = userFactory.generateTestUserWithIncompleteProfile()
        val intent = Intent()
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        intent.putExtra(IntentExtras.APP_USER, user as Parcelable)
        return mActivityRuleComposable.launchActivity(intent)
    }

    override fun startTestActivityWithUserWhoHasCompleteProfile() : ActivityHomeScreen {
//        val scenario = launchActivity<ActivityHomeScreen>()       // Launches activity without extras
        if(user == null)user = userFactory.generateTestUserWithCompleteProfile()
        val intent = Intent()
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        intent.putExtra(IntentExtras.APP_USER, user as Parcelable)
        return mActivityRule.launchActivity(intent)
    }

    private fun startComposableTestActivityWithUserWhoHasCompleteProfile() : ActivityHomeScreenComposable {
//        val scenario = launchActivity<ActivityHomeScreen>()       // Launches activity without extras
        if(user == null)user = userFactory.generateTestUserWithCompleteProfile()
        val intent = Intent()
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        intent.putExtra(IntentExtras.APP_USER, user as Parcelable)
        return mActivityRuleComposable.launchActivity(intent)
    }

    override fun injectTest() {
//        (application.appComponent as TestAppComponent).inject(this)
    }

    @Test
    fun test0() = runBlocking {
        val methodName = "test0"
        if (LOG_ME) ALog.d(TAG, ".$methodName(): Method start: $methodName")
//        ALog.d(TAG, "Deleting all entities in userDao")
//        userDao.deleteAllEntities()
//        ALog.d(TAG, "Deleting all entities in userNetworkDataSource")
//        userNetworkDataSource.deleteAllEntities()
//        ALog.d(TAG, "Creating new user entity")
//        val user : User = userFactory.generateTestUserWithIncompleteProfile()
//        ALog.d(TAG, "Deleting old user account")
//        userNetworkDataSource.deleteAccount(user)
        ALog.d(TAG, "Inserting new user entity to userNetworkDataSource")
//        userNetworkDataSource.registerNewUser(user)

        ALog.d(TAG, "Logging user in.")
//        userNetworkDataSource.loginUser(user)

        // Checking if Firestore allows creation of new documents
        ALog.d(TAG, "Checking Firestore availability.")
        testFirestoreAvailability()

        ALog.d(TAG, "Firestore is available for usage.")
        if (LOG_ME) ALog.d(TAG, ".$methodName(): Method end: $methodName")
    }

    private suspend fun testFirestoreAvailability() {
        val methodName = "testFirestoreAvailability"
        Log.d(TAG, "Method start: $methodName")
        try {
            val testEntity = hashMapOf(
                "test" to "Yay!"
            )

            if(FirebaseAuth.getInstance().currentUser == null) {
                Log.d(TAG, "$methodName(): User is not logged in")
            } else {
                Log.d(TAG, "$methodName(): User is logged in")
            }

            firestore.collection("test")
                .add(testEntity)
                .addOnSuccessListener { documentReference ->
                    Log.d(
                        TAG,
                        "$methodName(): DocumentSnapshot written with ID: ${documentReference.id}"
                    )
                }
                .addOnFailureListener { e ->
                    Log.w(TAG, "$methodName(): Error adding document: $e")
                }
                .await()
            Log.d(TAG, "Method end: $methodName")
        } catch (e: java.lang.Exception) {
            Log.e(TAG, methodName, e)
            return
        } finally {
            Log.d(TAG, "Method end: $methodName")
        }
    }

    @get:Rule(order = 1)
    val composeTestRule = createAndroidComposeRule<ActivityHomeScreenComposable>()

    //    1. User decided to complete their profile by clicking on profileStatusCompleteProfileButton.
    @Test
    fun test1() = runBlocking {
        val methodName = "test1"
        if (LOG_ME) ALog.d(TAG, ".$methodName(): Method start: $methodName")

        // Prepare data set
        prepareDataSetWithUserWhoHasIncompleteProfile()

        // Launch scenario
        startComposableTestActivityWithUserWhoHasIncompleteProfile()

        // Use composeTestRule to interact with composables
//        composeTestRule.setContent {
//            // Render the composables you want to test
//            HomeScreenContent()
//        }

        // Wait for ActivityHomeScreenFragment1 to appear
        if (LOG_ME) ALog.d(TAG, ".$methodName(): Wait for ActivityHomeScreenFragment1 to appear")
        composeTestRule.onNodeWithContentDescription(
            context.getString(R.string.search_query_edittext_content_description)
        ).assertIsDisplayed()

        // Confirm profile completion button is in view
        if (LOG_ME) ALog.d(TAG, ".$methodName(): Confirm profile completion button is in view")
        composeTestRule.onNodeWithContentDescription(
            context.getString(R.string.profile_status_complete_profile_button_content_description)
        ).assertIsDisplayed()


        // Click on profile completion button
        if (LOG_ME) ALog.d(TAG, ".$methodName(): Click on profile completion button")
        composeTestRule.onNodeWithContentDescription(
            context.getString(R.string.profile_status_complete_profile_button_content_description)
        ).performClick()

        // TODO: Create Fragment 5 UI before proceeding with test.
        if(LOG_ME)ALog.w(TAG, ".$methodName(): " +
                                "TODO: Create Fragment 5 UI before proceeding with test.")
        return@runBlocking

        // Wait for ActivityHomeScreenFragment5 to come into view
        if (LOG_ME) ALog.d(TAG, ".$methodName(): Wait for ActivityHomeScreenFragment5 to come into view")
        composeTestRule.onNodeWithContentDescription(
            context.getString(R.string.users_name_lbl_content_description)
        ).assertIsDisplayed()

        // Confirm ActivityHomeScreenFragment5 is in view
        if (LOG_ME) ALog.d(TAG, ".$methodName(): Confirm ActivityHomeScreenFragment5 is in view")
        composeTestRule.onNodeWithContentDescription(
            context.getString(R.string.users_name_lbl_content_description)
        ).assertIsDisplayed()

        // Wait for ActivityHomeScreenFragment5 to come into view
        if (LOG_ME) ALog.d(TAG, ".$methodName(): Wait for ActivityHomeScreenFragment5 to come into view")
        composeTestRule.onNodeWithContentDescription(
            context.getString(R.string.users_name_lbl_content_description)
        ).assertIsDisplayed()

        if (LOG_ME) ALog.d(TAG, ".$methodName(): Method end: $methodName")
//        pauseTestHere(TAG, methodName, composeTestRule)
    }


    //    2. User searches for entities without setting any filters or search query (search around user).
    @OptIn(ExperimentalTestApi::class)
    @Test
    fun test2() {
        val methodName = "test2"
        if(LOG_ME) ALog.d(TAG, ".$methodName(): Method start: $methodName")

        // Prepare data set
        prepareDataSetWithUserWhoHasIncompleteProfile()

        // Launch scenario
        startComposableTestActivityWithUserWhoHasIncompleteProfile()

        // Wait for ActivityHomeScreenFragment1 to appear
        if (LOG_ME) ALog.d(TAG, ".$methodName(): Wait for HomeScreenComposableFragment1 to appear")
        composeTestRule.waitUntilAtLeastOneExists(
            matcher = hasTestTag(HomeScreenComposableFragment1TestTag),
        )

        // Confirm HomeScreenComposableFragment1 is in view
        if(LOG_ME) ALog.d(TAG, ".$methodName(): Confirm HomeScreenComposableFragment1 is in view")
        composeTestRule.onNodeWithTag(HomeScreenComposableFragment1TestTag).assertIsDisplayed()

        // Wait until data has loaded into lazy column.
        if (LOG_ME) ALog.d(TAG, ".$methodName(): Wait until data has loaded into lazy column.")
        composeTestRule.waitForDataToLoadIntoLazyColumn(
            "Wait until data has loaded into lazy column.",
            TAG,
            LOG_ME,
            methodName,
            "0",
        )

        // Click on search button
        if(LOG_ME) ALog.d(TAG, ".$methodName(): Click on search button")
        composeTestRule.onNodeWithContentDescription(
            context.getString(R.string.search_button_content_description),
            ignoreCase = true,
        ).performClick()

        // Click on 4th item in search results
        val listItemInTest = 3
        composeTestRule.clickOnNthItemInLazyColumn(
            "Click on 4th item in search results",
            TAG,
            LOG_ME,
            methodName,
            listItemInTestNumber = listItemInTest,
            lazyColumnTag = "LazyColumnWithPullToRefresh",
            firstListItemTag = "0",
        )

        // Wait for entity details screen to come into view
        if(LOG_ME) ALog.d(TAG, ".$methodName(): Wait for entity details screen to come into view")
        composeTestRule.waitUntilAtLeastOneExists(
            matcher = hasTestTag(HomeScreenCompDetailsScreen1TestTag),
        )

        // Check that entity details screen is in view
        if(LOG_ME) ALog.d(TAG, ".$methodName(): Check that entity details screen is in view")
        composeTestRule.onNodeWithTag(HomeScreenCompDetailsScreen1TestTag).assertIsDisplayed()

        if(LOG_ME) ALog.d(TAG, ".$methodName(): Method end: $methodName")
//        pauseTestHere(TAG, methodName, composeTestRule)
    }

    //    3. User searches for entities without setting any filters but inputs search query (city).
    @OptIn(ExperimentalTestApi::class)
    @Test
    fun test3() {
        val methodName = "test3"
        if(LOG_ME) ALog.d(TAG, ".$methodName(): Method start: $methodName")

        // Prepare data set
        prepareDataSetWithUserWhoHasIncompleteProfile()

        // Launch scenario
        startComposableTestActivityWithUserWhoHasIncompleteProfile()

        // Wait for ActivityHomeScreenFragment1 to appear
        if (LOG_ME) ALog.d(TAG, ".$methodName(): Wait for HomeScreenComposableFragment1 to appear")
        composeTestRule.waitUntilAtLeastOneExists(
            matcher = hasTestTag(HomeScreenComposableFragment1TestTag),
        )

        // Confirm HomeScreenComposableFragment1 is in view
        if(LOG_ME) ALog.d(TAG, ".$methodName(): Confirm HomeScreenComposableFragment1 is in view")
        composeTestRule.onNodeWithTag(HomeScreenComposableFragment1TestTag).assertIsDisplayed()

        // Wait until data has loaded into lazy column.
        if (LOG_ME) ALog.d(TAG, ".$methodName(): Wait until data has loaded into lazy column.")
        composeTestRule.waitForDataToLoadIntoLazyColumn(
            "Wait until data has loaded into lazy column.",
            TAG,
            LOG_ME,
            methodName,
            "0",
        )

        // Click on search query text field
        if(LOG_ME) ALog.d(TAG, ".$methodName(): Click on search query text field")
        composeTestRule.onNodeWithContentDescription(
            context.getString(R.string.search_query_edittext_content_description),
            ignoreCase = true,
        ).performClick()

        // Type search query to search query text field
        val searchQueryString = "Warsaw"
        composeTestRule.onNodeWithContentDescription(
            context.getString(R.string.search_query_edittext_content_description),
            ignoreCase = true,
        ).performTextReplacement(searchQueryString)

        // Click on search button
        if(LOG_ME) ALog.d(TAG, ".$methodName(): Click on search button")
        composeTestRule.onNodeWithContentDescription(
            context.getString(R.string.search_button_content_description),
            ignoreCase = true,
        ).performClick()

        // Wait until data has loaded into lazy column.
        if (LOG_ME) ALog.d(TAG, ".$methodName(): Wait until data has loaded into lazy column.")
        composeTestRule.waitForDataToLoadIntoLazyColumn(
            "Wait until data has loaded into lazy column.",
            TAG,
            LOG_ME,
            methodName,
            "0",
        )

        // Click on 4th item in search results
        val listItemInTest = 3
        composeTestRule.clickOnNthItemInLazyColumn(
            "Click on 4th item in search results",
            TAG,
            LOG_ME,
            methodName,
            listItemInTestNumber = listItemInTest,
            lazyColumnTag = "LazyColumnWithPullToRefresh",
            firstListItemTag = "0",
        )

        // Wait for entity details screen to come into view
        if(LOG_ME) ALog.d(TAG, ".$methodName(): Wait for entity details screen to come into view")
        composeTestRule.waitUntilAtLeastOneExists(
            matcher = hasTestTag(HomeScreenCompDetailsScreen1TestTag),
        )

        // Check that entity details screen is in view
        if(LOG_ME) ALog.d(TAG, ".$methodName(): Check that entity details screen is in view")
        composeTestRule.onNodeWithTag(HomeScreenCompDetailsScreen1TestTag).assertIsDisplayed()

        // Scroll to entity's city label
        if(LOG_ME) ALog.d(TAG, ".$methodName(): Scroll to entity's city label")
        composeTestRule.onNodeWithContentDescription(
            context.getString(R.string.city)
        ).performScrollTo()

        // Check that searched entity is from the provided city
        if(LOG_ME) ALog.d(
            TAG,".$methodName(): Check that searched entity is from the provided city"
        )
        composeTestRule.onNodeWithText(
            text = searchQueryString,
            ignoreCase = true,
        ).assertIsDisplayed()

        if(LOG_ME) ALog.d(TAG, ".$methodName(): Method end: $methodName")
//        pauseTestHere(TAG, methodName, composeTestRule)
    }

    //    4. User searches for entities after setting filters without search query (search around user).
    @OptIn(ExperimentalTestApi::class)
    @Test
    fun test4() {
        val methodName = "test4"
        if(LOG_ME) ALog.d(TAG, ".$methodName(): Method start: $methodName")

        // Prepare data set
        prepareDataSetWithUserWhoHasIncompleteProfile()

        // Launch scenario
        startComposableTestActivityWithUserWhoHasIncompleteProfile()

        // Wait for ActivityHomeScreenFragment1 to appear
        if (LOG_ME) ALog.d(TAG, ".$methodName(): Wait for HomeScreenComposableFragment1 to appear")
        composeTestRule.waitUntilAtLeastOneExists(
            matcher = hasTestTag(HomeScreenComposableFragment1TestTag),
        )

        // Confirm HomeScreenComposableFragment1 is in view
        if(LOG_ME) ALog.d(TAG, ".$methodName(): Confirm HomeScreenComposableFragment1 is in view")
        composeTestRule.onNodeWithTag(HomeScreenComposableFragment1TestTag).assertIsDisplayed()

        // Wait until data has loaded into lazy column.
        if (LOG_ME) ALog.d(TAG, ".$methodName(): Wait until data has loaded into lazy column.")
        composeTestRule.waitForDataToLoadIntoLazyColumn(
            "Wait until data has loaded into lazy column.",
            TAG,
            LOG_ME,
            methodName,
            "0",
        )
        // Click on filters button
        composeTestRule.onNodeWithTag(
            PeekingClosableTwoButtonBottomSheetsRowLeftButtonTestTag
        ).performClick()

        // Wait for bottom sheet to appear
        if(LOG_ME) ALog.d(TAG, ".$methodName(): Wait for bottom sheet to appear")
        composeTestRule.waitUntilAtLeastOneExists(
            hasTestTag(HomeScreenCard1SearchFiltersColumnTestTag)
        )

        // Confirm that bottom sheet with search filters appeared
        if(LOG_ME) ALog.d(
            TAG,
            ".$methodName(): Confirm that bottom sheet with search filters appeared"
        )
        composeTestRule.onNodeWithTag(HomeScreenCard1SearchFiltersColumnTestTag).assertIsDisplayed()

        // Set test filters
        if(LOG_ME) ALog.d(TAG, ".$methodName(): Set test filters")
        setTestFilters(methodName)

        // Click on bottom sheet close button
        if(LOG_ME) ALog.d(TAG, ".$methodName(): Click on bottom sheet close button")
        composeTestRule.onNodeWithTag(
            PeekingClosableTwoButtonBottomSheetsRowCloseButtonTestTag
        ).performClick()

        // Confirm that bottom sheet is collapsed
        if(LOG_ME) ALog.d(TAG, ".$methodName(): Confirm that bottom sheet is collapsed")
        composeTestRule.onNodeWithText(
            context.getString(R.string.switch_1)
        ).assertIsNotDisplayed()

        // Click on search button
        if(LOG_ME) ALog.d(TAG, ".$methodName(): Click on search button")
        composeTestRule.onNodeWithContentDescription(
            context.getString(R.string.search_button_content_description),
            ignoreCase = true,
        ).performClick()

        // Click on 1st item in search results
        val listItemInTest = 0
        composeTestRule.clickOnNthItemInLazyColumn(
            "Click on 1st item in search results",
            TAG,
            LOG_ME,
            methodName,
            listItemInTestNumber = listItemInTest,
            lazyColumnTag = "LazyColumnWithPullToRefresh",
            firstListItemTag = "0",
        )

        // Wait for entity details screen to come into view
        if(LOG_ME) ALog.d(TAG, ".$methodName(): Wait for entity details screen to come into view")
        composeTestRule.waitUntilAtLeastOneExists(
            matcher = hasTestTag(HomeScreenCompDetailsScreen1TestTag),
        )

        // Check that entity details screen is in view
        if(LOG_ME) ALog.d(TAG, ".$methodName(): Check that entity details screen is in view")
        composeTestRule.onNodeWithTag(HomeScreenCompDetailsScreen1TestTag).assertIsDisplayed()

        if(LOG_ME) ALog.d(TAG, ".$methodName(): Method end: $methodName")
//        pauseTestHere(TAG, methodName, composeTestRule)
    }

    //    5. User searches for entities after setting filters with search query (city).
    @OptIn(ExperimentalTestApi::class)
    @Test
    fun test5() {
        val methodName = "test5"
        if(LOG_ME) ALog.d(TAG, ".$methodName(): Method start: $methodName")

        // Prepare data set
        prepareDataSetWithUserWhoHasIncompleteProfile()

        // Launch scenario
        startComposableTestActivityWithUserWhoHasIncompleteProfile()

        // Wait for ActivityHomeScreenFragment1 to appear
        if (LOG_ME) ALog.d(TAG, ".$methodName(): Wait for HomeScreenComposableFragment1 to appear")
        composeTestRule.waitUntilAtLeastOneExists(
            matcher = hasTestTag(HomeScreenComposableFragment1TestTag),
        )

        // Confirm HomeScreenComposableFragment1 is in view
        if(LOG_ME) ALog.d(TAG, ".$methodName(): Confirm HomeScreenComposableFragment1 is in view")
        composeTestRule.onNodeWithTag(HomeScreenComposableFragment1TestTag).assertIsDisplayed()

        // Wait until data has loaded into lazy column.
        if (LOG_ME) ALog.d(TAG, ".$methodName(): Wait until data has loaded into lazy column.")
        composeTestRule.waitForDataToLoadIntoLazyColumn(
            "Wait until data has loaded into lazy column.",
            TAG,
            LOG_ME,
            methodName,
            "0",
        )

        // Click on search query text field
        if(LOG_ME) ALog.d(TAG, ".$methodName(): Click on search query text field")
        composeTestRule.onNodeWithContentDescription(
            context.getString(R.string.search_query_edittext_content_description),
            ignoreCase = true,
        ).performClick()

        // Type search query to search query text field
        val searchQueryString = "lublin"
        composeTestRule.onNodeWithContentDescription(
            context.getString(R.string.search_query_edittext_content_description),
            ignoreCase = true,
        ).performTextReplacement(searchQueryString)

        // Click on filters button
        composeTestRule.onNodeWithTag(
            PeekingClosableTwoButtonBottomSheetsRowLeftButtonTestTag
        ).performClick()

        // Wait for bottom sheet to appear
        if(LOG_ME) ALog.d(TAG, ".$methodName(): Wait for bottom sheet to appear")
        composeTestRule.waitUntilAtLeastOneExists(
            hasTestTag(HomeScreenCard1SearchFiltersColumnTestTag)
        )

        // Confirm that bottom sheet with search filters appeared
        if(LOG_ME) ALog.d(
            TAG,
            ".$methodName(): Confirm that bottom sheet with search filters appeared"
        )
        composeTestRule.onNodeWithTag(HomeScreenCard1SearchFiltersColumnTestTag).assertIsDisplayed()

        // Set test filters
        if(LOG_ME) ALog.d(TAG, ".$methodName(): Set test filters")
        setTestFilters(methodName)
//        pauseTestHere(TAG, methodName, composeTestRule)

        // Click on bottom sheet close button
        if(LOG_ME) ALog.d(TAG, ".$methodName(): Click on bottom sheet close button")
        composeTestRule.onNodeWithTag(
            PeekingClosableTwoButtonBottomSheetsRowCloseButtonTestTag
        ).performClick()

        // Confirm that bottom sheet is collapsed
        if(LOG_ME) ALog.d(TAG, ".$methodName(): Confirm that bottom sheet is collapsed")
        composeTestRule.onNodeWithText(
            context.getString(R.string.switch_1)
        ).assertIsNotDisplayed()

        // Click on search button
        if(LOG_ME) ALog.d(TAG, ".$methodName(): Click on search button")
        composeTestRule.onNodeWithContentDescription(
            context.getString(R.string.search_button_content_description),
            ignoreCase = true,
        ).performClick()
        // Click on 1st item in search results
        val listItemInTest = 0
        composeTestRule.clickOnNthItemInLazyColumn(
            "Click on 1st item in search results",
            TAG,
            LOG_ME,
            methodName,
            listItemInTestNumber = listItemInTest,
            lazyColumnTag = "LazyColumnWithPullToRefresh",
            firstListItemTag = "0",
        )

        // Wait for entity details screen to come into view
        if(LOG_ME) ALog.d(TAG, ".$methodName(): Wait for entity details screen to come into view")
        composeTestRule.waitUntilAtLeastOneExists(
            matcher = hasTestTag(HomeScreenCompDetailsScreen1TestTag),
        )

        // Check that entity details screen is in view
        if(LOG_ME) ALog.d(TAG, ".$methodName(): Check that entity details screen is in view")
        composeTestRule.onNodeWithTag(HomeScreenCompDetailsScreen1TestTag).assertIsDisplayed()

        // Scroll to entity's city label
        if(LOG_ME) ALog.d(TAG, ".$methodName(): Scroll to entity's city label")
        composeTestRule.onNodeWithContentDescription(
            context.getString(R.string.city)
        ).performScrollTo()

        // Check that searched entity is from the provided city
        if(LOG_ME) ALog.d(TAG,".$methodName(): " +
                "Check that searched entity is from the provided city: $searchQueryString")
        composeTestRule.onNodeWithText(
            text = searchQueryString,
            ignoreCase = true,
        ).assertIsDisplayed()

        if(LOG_ME) ALog.d(TAG, ".$methodName(): Method end: $methodName")
//        pauseTestHere(TAG, methodName, composeTestRule)
    }

    private fun setTestFilters(methodName : String) {
        val nodesTopLeftCornerPosition = Offset(0F, 0F)
        val newMinimalRange1ValueForTextView = "50"
        val newMaximumRange1ValueForTextView = "100"
        val newMinimalRange2ValueForTextView = "30"
        val newMaximumRange2ValueForTextView = "90"
        val bottomSheetBackground = hasTestTag(HomeScreenCompFragment1BottomSheetBackgroundTestTag)
        val minimalRange1EditText = hasTestTag(
            HomeScreenCard1SearchFiltersRange1TestTag+RangeRowRangeMinTestTag
        )
        val range1RangeSlider = hasTestTag(HomeScreenCard1SearchFiltersRangeSlider1TestTag)
        val maximumRange1EditText = hasTestTag(
            HomeScreenCard1SearchFiltersRange1TestTag+RangeRowRangeMaxTestTag
        )
        val minimalRange2EditText = hasTestTag(
            HomeScreenCard1SearchFiltersRange2TestTag+RangeRowRangeMinTestTag
        )
        val range2RangeSlider = hasTestTag(HomeScreenCard1SearchFiltersRangeSlider2TestTag)
        val maximumRange2EditText = hasTestTag(
            HomeScreenCard1SearchFiltersRange2TestTag+RangeRowRangeMaxTestTag
        )
        val switch1 = hasTestTag(SWITCH_TEST_TAG+context.getString(R.string.switch_1))
        val switch2 = hasTestTag(SWITCH_TEST_TAG+context.getString(R.string.switch_2))
        val switch3 = hasTestTag(SWITCH_TEST_TAG+context.getString(R.string.switch_3))
        val switch4 = hasTestTag(SWITCH_TEST_TAG+context.getString(R.string.switch_4))
        val switch5 = hasTestTag(SWITCH_TEST_TAG+context.getString(R.string.switch_5))
        val switch6 = hasTestTag(SWITCH_TEST_TAG+context.getString(R.string.switch_6))
        val switch7 = hasTestTag(SWITCH_TEST_TAG+context.getString(R.string.switch_7))

        // Check if every composable is displayed
        if(LOG_ME) ALog.d(TAG, ".$methodName(): Check if every composable is displayed")
        composeTestRule.waitForIdle()
        composeTestRule.onNode(bottomSheetBackground).assertIsDisplayed()
        composeTestRule.onNode(minimalRange1EditText).assertIsDisplayed()
        composeTestRule.onNode(range1RangeSlider).assertIsDisplayed()
        composeTestRule.onNode(maximumRange1EditText).assertIsDisplayed()
        composeTestRule.onNode(minimalRange2EditText).assertIsDisplayed()
        composeTestRule.onNode(range2RangeSlider).assertIsDisplayed()
        composeTestRule.onNode(maximumRange2EditText).assertIsDisplayed()
        composeTestRule.onNode(switch1).assertIsDisplayed()
        composeTestRule.onNode(switch2).assertIsDisplayed()
        composeTestRule.onNode(switch3).assertIsDisplayed()
        composeTestRule.onNode(switch4).assertIsDisplayed()
        composeTestRule.onNode(switch5).assertIsDisplayed()
        composeTestRule.onNode(switch6).assertIsDisplayed()
        composeTestRule.onNode(switch7).assertIsDisplayed()

        // Type minimal range 1 value
        if(LOG_ME) ALog.d(TAG, ".$methodName(): Type minimal range 1 value")
        composeTestRule.onNode(minimalRange1EditText).performScrollTo()
        composeTestRule.waitForIdle()
        composeTestRule.onNode(minimalRange1EditText)
            .performClick()
        Espresso.closeSoftKeyboard()
        composeTestRule.onNode(minimalRange1EditText)
            .performTextReplacement(newMinimalRange1ValueForTextView)
        composeTestRule.waitForIdle()

        // Check if range 1 value range slider changed it's value[0]
        if(LOG_ME) ALog.d(
            TAG,
            ".$methodName(): Check if range 1 value range slider changed it's valueFrom"
        )
        composeTestRule.onNode(bottomSheetBackground).performTouchInput {
            click(position = nodesTopLeftCornerPosition)
        }
        composeTestRule.onNode(range1RangeSlider)
            .assertContentDescriptionContains(
                value = newMinimalRange1ValueForTextView,
                substring = true,
                ignoreCase = true,
            )

        // Type maximum range 1 value
        if(LOG_ME) ALog.d(TAG, ".$methodName(): Type maximum range 1 value")
        composeTestRule.onNode(maximumRange1EditText).performScrollTo()
        composeTestRule.waitForIdle()
        composeTestRule.onNode(maximumRange1EditText)
            .performClick()
        Espresso.closeSoftKeyboard()
        composeTestRule.onNode(maximumRange1EditText)
            .performTextReplacement(newMaximumRange1ValueForTextView)
        composeTestRule.waitForIdle()

        // Check if range 1 value range slider changed it's value[1]
        // Reference: https://stackoverflow.com/questions/65390086/androidx-how-to-test-slider-in-ui-tests-espresso
        if(LOG_ME) ALog.d(
            TAG,
            ".$methodName(): Check if range 1 value range slider changed it's value[1]"
        )
        composeTestRule.onNode(bottomSheetBackground).performTouchInput {
            click(position = nodesTopLeftCornerPosition)
        }
        composeTestRule.onNode(range1RangeSlider)
            .assertContentDescriptionContains(
                value = newMaximumRange1ValueForTextView,
                substring = true,
                ignoreCase = true,
            )



        // Range 2 RangeSlider
        // Type minimal range 2 value
        if(LOG_ME) ALog.d(TAG, ".$methodName(): Type minimal range 2 value")
        composeTestRule.onNode(minimalRange2EditText).performScrollTo()
        composeTestRule.waitForIdle()
        composeTestRule.onNode(minimalRange2EditText)
            .performClick()
        Espresso.closeSoftKeyboard()
        composeTestRule.onNode(minimalRange2EditText)
            .performTextReplacement(newMinimalRange2ValueForTextView)
        composeTestRule.waitForIdle()

        // Check if range 2 value range slider changed it's value[0]
        if(LOG_ME) ALog.d(
            TAG,
            ".$methodName(): Check if range 2 value range slider changed it's value[0]"
        )
        composeTestRule.onNode(bottomSheetBackground).performTouchInput {
            click(position = nodesTopLeftCornerPosition)
        }
        composeTestRule.onNode(range2RangeSlider)
            .assertContentDescriptionContains(
                value = newMinimalRange2ValueForTextView,
                substring = true,
                ignoreCase = true,
            )

        // Type maximal range 2 value
        if(LOG_ME) ALog.d(TAG, ".$methodName(): Type maximal range 2 value")
        composeTestRule.onNode(maximumRange2EditText).performScrollTo()
        composeTestRule.waitForIdle()
        composeTestRule.onNode(maximumRange2EditText)
            .performClick()
        Espresso.closeSoftKeyboard()
        composeTestRule.onNode(maximumRange2EditText)
            .performTextReplacement(newMaximumRange2ValueForTextView)
        composeTestRule.waitForIdle()

        // Check if range 2 value range slider changed it's value[1]
        if(LOG_ME) ALog.d(
            TAG,
            ".$methodName(): Check if range 2 value range slider changed it's value[1]"
        )
        composeTestRule.onNode(bottomSheetBackground)
        composeTestRule.onNode(bottomSheetBackground).performTouchInput {
            click(position = nodesTopLeftCornerPosition)
        }
        composeTestRule.onNode(range2RangeSlider)
            .assertContentDescriptionContains(
                value = newMaximumRange2ValueForTextView,
                substring = true,
                ignoreCase = true,
            )



        // Tick all switches
        if(LOG_ME) ALog.d(TAG, ".$methodName(): Flip switch 1")
        composeTestRule.onNode(switch1)
            .performScrollTo()
            .performClick()
            .assert(isToggleable())      // Pass dummy {} onCheckedChange to Switch to prevent exceptions in case it has none
            .assert(isOn())
        if(LOG_ME) ALog.d(TAG, ".$methodName(): Flip switch 2")
        composeTestRule.onNode(switch2)
            .performScrollTo()
            .performClick()
            .assert(isToggleable())      // Pass dummy {} onCheckedChange to Switch to prevent exceptions in case it has none
            .assert(isOn())
        if(LOG_ME) ALog.d(TAG, ".$methodName(): Flip switch 3")
        composeTestRule.onNode(switch3)
            .performScrollTo()
            .performClick()
            .assert(isToggleable())      // Pass dummy {} onCheckedChange to Switch to prevent exceptions in case it has none
            .assert(isOn())
        if(LOG_ME) ALog.d(TAG, ".$methodName(): Flip switch 4")
        composeTestRule.onNode(switch4)
            .performScrollTo()
            .performClick()
            .assert(isToggleable())      // Pass dummy {} onCheckedChange to Switch to prevent exceptions in case it has none
            .assert(isOn())
        if(LOG_ME) ALog.d(TAG, ".$methodName(): Flip switch 5")
        composeTestRule.onNode(switch5)
            .performScrollTo()
            .performClick()
            .assert(isToggleable())      // Pass dummy {} onCheckedChange to Switch to prevent exceptions in case it has none
            .assert(isOn())
        if(LOG_ME) ALog.d(TAG, ".$methodName(): Flip switch 6")
        composeTestRule.onNode(switch6)
            .performScrollTo()
            .performClick()
            .assert(isToggleable())      // Pass dummy {} onCheckedChange to Switch to prevent exceptions in case it has none
            .assert(isOn())
        if(LOG_ME) ALog.d(TAG, ".$methodName(): Flip switch 7")
        composeTestRule.onNode(switch7)
            .performScrollTo()
            .performClick()
            .assert(isToggleable())      // Pass dummy {} onCheckedChange to Switch to prevent exceptions in case it has none
            .assert(isOn())
    }

    //    6. User sets filters and then clears them with removeFiltersButton.
    @OptIn(ExperimentalTestApi::class)
    @Test
    fun test6() {
        val methodName = "test6"
        if(LOG_ME) ALog.d(TAG, ".$methodName(): Method start: $methodName")

        // Prepare data set
        prepareDataSetWithUserWhoHasIncompleteProfile()

        // Launch scenario
        startComposableTestActivityWithUserWhoHasIncompleteProfile()

        // Wait for ActivityHomeScreenFragment1 to appear
        if (LOG_ME) ALog.d(TAG, ".$methodName(): Wait for HomeScreenComposableFragment1 to appear")
        composeTestRule.waitUntilAtLeastOneExists(
            matcher = hasTestTag(HomeScreenComposableFragment1TestTag),
        )

        // Confirm HomeScreenComposableFragment1 is in view
        if(LOG_ME) ALog.d(TAG, ".$methodName(): Confirm HomeScreenComposableFragment1 is in view")
        composeTestRule.onNodeWithTag(HomeScreenComposableFragment1TestTag).assertIsDisplayed()

        // Wait until data has loaded into lazy column.
        if (LOG_ME) ALog.d(TAG, ".$methodName(): Wait until data has loaded into lazy column.")
        composeTestRule.waitForDataToLoadIntoLazyColumn(
            "Wait until data has loaded into lazy column.",
            TAG,
            LOG_ME,
            methodName,
            "0",
        )

        // Click on search query text field
        if(LOG_ME) ALog.d(TAG, ".$methodName(): Click on search query text field")
        composeTestRule.onNodeWithContentDescription(
            context.getString(R.string.search_query_edittext_content_description),
            ignoreCase = true,
        ).performClick()

        // Type search query to search query text field
        val searchQueryString = "Lublin"
        composeTestRule.onNodeWithContentDescription(
            context.getString(R.string.search_query_edittext_content_description),
            ignoreCase = true,
        ).performTextReplacement(searchQueryString)

        // Click on filters button
        composeTestRule.onNodeWithTag(
            PeekingClosableTwoButtonBottomSheetsRowLeftButtonTestTag
        ).performClick()

        // Wait for bottom sheet to appear
        if(LOG_ME) ALog.d(TAG, ".$methodName(): Wait for bottom sheet to appear")
        composeTestRule.waitUntilAtLeastOneExists(
            hasTestTag(HomeScreenCard1SearchFiltersColumnTestTag)
        )

        // Confirm that bottom sheet with search filters appeared
        if(LOG_ME) ALog.d(
            TAG,
            ".$methodName(): Confirm that bottom sheet with search filters appeared"
        )
        composeTestRule.onNodeWithTag(HomeScreenCard1SearchFiltersColumnTestTag).assertIsDisplayed()

        // Set test filters
        if(LOG_ME) ALog.d(TAG, ".$methodName(): Set test filters")
        setTestFilters(methodName)

        // Click on remove filters button
        if(LOG_ME) ALog.d(TAG, ".$methodName(): Click on remove filters button")
        composeTestRule.onNodeWithTag(
            PeekingClosableTwoButtonBottomSheetsRowRightButtonTestTag
        ).performClick()

        // Confirm that bottom sheet is collapsed
        if(LOG_ME) ALog.d(TAG, ".$methodName(): Confirm that bottom sheet is collapsed")
        composeTestRule.onNodeWithText(
            context.getString(R.string.switch_1)
        ).assertIsNotDisplayed()

        // Click on filters button
        composeTestRule.onNodeWithTag(
            PeekingClosableTwoButtonBottomSheetsRowLeftButtonTestTag
        ).performClick()

        // Wait for bottom sheet to appear
        if(LOG_ME) ALog.d(TAG, ".$methodName(): Wait for bottom sheet to appear")
        composeTestRule.waitUntilAtLeastOneExists(
            hasTestTag(HomeScreenCard1SearchFiltersColumnTestTag)
        )

        // Confirm that bottom sheet with search filters appeared
        if(LOG_ME) ALog.d(
            TAG,
            ".$methodName(): Confirm that bottom sheet with search filters appeared"
        )
        composeTestRule.onNodeWithTag(HomeScreenCard1SearchFiltersColumnTestTag).assertIsDisplayed()

        // Confirm that filters are cleared
        if(LOG_ME) ALog.d(TAG, ".$methodName(): Confirm that filters are cleared.")
        confirmFiltersAreCleared()

        // Click on search button
        if(LOG_ME) ALog.d(TAG, ".$methodName(): Click on search button")
        composeTestRule.onNodeWithContentDescription(
            context.getString(R.string.search_button_content_description),
            ignoreCase = true,
        ).performClick()

        // Click on 4th item in search results
        val listItemInTest = 3
        composeTestRule.clickOnNthItemInLazyColumn(
            "Click on 4th item in search results",
            TAG,
            LOG_ME,
            methodName,
            listItemInTestNumber = listItemInTest,
            lazyColumnTag = "LazyColumnWithPullToRefresh",
            firstListItemTag = "0",
        )

        // Wait for entity details screen to come into view
        if(LOG_ME) ALog.d(TAG, ".$methodName(): Wait for entity details screen to come into view")
        composeTestRule.waitUntilAtLeastOneExists(
            matcher = hasTestTag(HomeScreenCompDetailsScreen1TestTag),
        )

        // Check that entity details screen is in view
        if(LOG_ME) ALog.d(TAG, ".$methodName(): Check that entity details screen is in view")
        composeTestRule.onNodeWithTag(HomeScreenCompDetailsScreen1TestTag).assertIsDisplayed()

        if(LOG_ME) ALog.d(TAG, ".$methodName(): Method end: $methodName")
//        pauseTestHere(TAG, methodName, composeTestRule)
    }

    private fun confirmFiltersAreCleared(){
        val minimalRange1EditText = hasTestTag(
            HomeScreenCard1SearchFiltersRange1TestTag+RangeRowRangeMinTestTag
        )
        val maximumRange1EditText = hasTestTag(
            HomeScreenCard1SearchFiltersRange1TestTag+RangeRowRangeMaxTestTag
        )
        val minimalRange2EditText = hasTestTag(
            HomeScreenCard1SearchFiltersRange2TestTag+RangeRowRangeMinTestTag
        )
        val maximumRange2EditText = hasTestTag(
            HomeScreenCard1SearchFiltersRange2TestTag+RangeRowRangeMaxTestTag
        )
        val switch1 = hasTestTag(SWITCH_TEST_TAG+context.getString(R.string.switch_1))
        val switch2 = hasTestTag(SWITCH_TEST_TAG+context.getString(R.string.switch_2))
        val switch3 = hasTestTag(SWITCH_TEST_TAG+context.getString(R.string.switch_3))
        val switch4 = hasTestTag(SWITCH_TEST_TAG+context.getString(R.string.switch_4))
        val switch5 = hasTestTag(SWITCH_TEST_TAG+context.getString(R.string.switch_5))
        val switch6 = hasTestTag(SWITCH_TEST_TAG+context.getString(R.string.switch_6))


        composeTestRule.onNode(minimalRange1EditText)
            .performScrollTo()
            .assertTextEquals("")

        composeTestRule.onNode(maximumRange1EditText)
            .performScrollTo()
            .assertTextEquals("")

        composeTestRule.onNode(minimalRange2EditText)
            .performScrollTo()
            .assertTextEquals("")

        composeTestRule.onNode(maximumRange2EditText)
            .performScrollTo()
            .assertTextEquals("")


        composeTestRule.onNode(switch1)
            .performScrollTo()
            .assert(isToggleable())      // Pass dummy {} onCheckedChange to Switch to prevent exceptions in case it has none
            .assertIsOff()

        composeTestRule.onNode(switch2)
            .performScrollTo()
            .assert(isToggleable())      // Pass dummy {} onCheckedChange to Switch to prevent exceptions in case it has none
            .assert(isOff())

        composeTestRule.onNode(switch3)
            .performScrollTo()
            .assert(isToggleable())      // Pass dummy {} onCheckedChange to Switch to prevent exceptions in case it has none
            .assert(isOff())

        composeTestRule.onNode(switch4)
            .performScrollTo()
            .assert(isToggleable())      // Pass dummy {} onCheckedChange to Switch to prevent exceptions in case it has none
            .assert(isOff())

        composeTestRule.onNode(switch5)
            .performScrollTo()
            .assert(isToggleable())      // Pass dummy {} onCheckedChange to Switch to prevent exceptions in case it has none
            .assert(isOff())

        composeTestRule.onNode(switch6)
            .performScrollTo()
            .assert(isToggleable())      // Pass dummy {} onCheckedChange to Switch to prevent exceptions in case it has none
            .assert(isOff())
//        pauseTestHere(TAG, "confirmFiltersAreCleared", composeTestRule)
    }

    //    7. User types search query but decides to delete it with searchBackButton.
    @OptIn(ExperimentalTestApi::class)
    @Test
    fun test7() {
        val methodName = "test7"
        if(LOG_ME) ALog.d(TAG, ".$methodName(): Method start: $methodName")

        // Prepare data set
        prepareDataSetWithUserWhoHasIncompleteProfile()

        // Launch scenario
        startComposableTestActivityWithUserWhoHasIncompleteProfile()

        // Wait for ActivityHomeScreenFragment1 to appear
        if (LOG_ME) ALog.d(TAG, ".$methodName(): Wait for HomeScreenComposableFragment1 to appear")
        composeTestRule.waitUntilAtLeastOneExists(
            matcher = hasTestTag(HomeScreenComposableFragment1TestTag),
        )

        // Confirm HomeScreenComposableFragment1 is in view
        if(LOG_ME) ALog.d(TAG, ".$methodName(): Confirm HomeScreenComposableFragment1 is in view")
        composeTestRule.onNodeWithTag(HomeScreenComposableFragment1TestTag).assertIsDisplayed()

        // Wait until data has loaded into lazy column.
        if (LOG_ME) ALog.d(TAG, ".$methodName(): Wait until data has loaded into lazy column.")
        composeTestRule.waitForDataToLoadIntoLazyColumn(
            "Wait until data has loaded into lazy column.",
            TAG,
            LOG_ME,
            methodName,
            "0",
        )

        // Click on search query text field
        if(LOG_ME) ALog.d(TAG, ".$methodName(): Click on search query text field")
        composeTestRule.onNodeWithContentDescription(
            context.getString(R.string.search_query_edittext_content_description),
            ignoreCase = true,
        ).performClick()

        // Type search query to search query text field
        val searchQueryString = "Lublin"
        composeTestRule.onNodeWithContentDescription(
            context.getString(R.string.search_query_edittext_content_description),
            ignoreCase = true,
        ).performTextReplacement(searchQueryString)

        // Click on search back button
        if(LOG_ME) ALog.d(TAG, ".$methodName(): Click on search back button")
        composeTestRule.onNodeWithContentDescription(
            context.getString(R.string.search_back_button_content_description),
            ignoreCase = true,
        ).performClick()
//        pauseTestHere(TAG, methodName, composeTestRule)
        // Confirm that search query edittext is empty
        if(LOG_ME) ALog.d(TAG, ".$methodName(): Confirm that search query edittext is empty")
        composeTestRule.onNodeWithContentDescription(
            context.getString(R.string.search_query_edittext_content_description),
            ignoreCase = true,
        ).assertTextContains(
                value = context.getString(R.string.search),
                substring = true,
                ignoreCase = true,
            )

        // Click on search button
        if(LOG_ME) ALog.d(TAG, ".$methodName(): Click on search button")
        composeTestRule.onNodeWithContentDescription(
            context.getString(R.string.search_button_content_description),
            ignoreCase = true,
        ).performClick()

        // Click on 4th item in search results
        val listItemInTest = 3
        composeTestRule.clickOnNthItemInLazyColumn(
            "Click on 4th item in search results",
            TAG,
            LOG_ME,
            methodName,
            listItemInTestNumber = listItemInTest,
            lazyColumnTag = "LazyColumnWithPullToRefresh",
            firstListItemTag = "0",
        )

        // Wait for entity details screen to come into view
        if(LOG_ME) ALog.d(TAG, ".$methodName(): Wait for entity details screen to come into view")
        composeTestRule.waitUntilAtLeastOneExists(
            matcher = hasTestTag(HomeScreenCompDetailsScreen1TestTag),
        )

        // Check that entity details screen is in view
        if(LOG_ME) ALog.d(TAG, ".$methodName(): Check that entity details screen is in view")
        composeTestRule.onNodeWithTag(HomeScreenCompDetailsScreen1TestTag).assertIsDisplayed()

        // Scroll to entity's city label
        if(LOG_ME) ALog.d(TAG, ".$methodName(): Scroll to entity's city label")
        composeTestRule.onNodeWithContentDescription(
            context.getString(R.string.city)
        ).performScrollTo()

        // Check that searched entity is not from the provided city.
        if(LOG_ME) ALog.d(TAG, ".$methodName(): " +
                "Check that searched entity is not from the provided city."
        )
        composeTestRule.onNodeWithText(searchQueryString).assertDoesNotExist()

        // Check that searched entity is not from the provided city (alternative method).
        val city = composeTestRule.onNodeWithTag(
            HomeScreenCompoDetailsScreen1CityTestTag
        ).fetchSemanticsNode().config.getOrNull(
            androidx.compose.ui.semantics.SemanticsProperties.Text
        )?.get(0) ?: "_null_"
        if(LOG_ME)ALog.d(TAG, ".$methodName(): " +
                "Check that searched entity is not from the provided city (alternative method)." +
                "\nsearchQueryString == $searchQueryString" +
                "\ncity == $city")
        assertNotEquals(city, "_null_")
        assertNotEquals(city, searchQueryString)

        if(LOG_ME) ALog.d(TAG, ".$methodName(): Method end: $methodName")
//        pauseTestHere(TAG, methodName, composeTestRule)
    }

    //    8. User has completed their profile, check if profile completion button is invisible.
    @Test
    fun test8() {
        val methodName = "test8"
        if(LOG_ME) ALog.d(TAG, ".$methodName(): Method start: $methodName")

        // Prepare data set
        prepareDataSetWithUserWhoHasCompleteProfile()

        // Launch scenario
        startComposableTestActivityWithUserWhoHasCompleteProfile()

        // Wait for ActivityHomeScreenFragment1 to appear
        if (LOG_ME) ALog.d(TAG, ".$methodName(): Wait for ActivityHomeScreenFragment1 to appear")
        composeTestRule.onNodeWithContentDescription(
            context.getString(R.string.search_query_edittext_content_description)
        ).assertIsDisplayed()

        // Confirm profile completion button is in not view
        if (LOG_ME) ALog.d(TAG, ".$methodName(): Confirm profile completion button is not in view")
        composeTestRule.onNodeWithContentDescription(
            context.getString(R.string.profile_status_complete_profile_button_content_description)
        ).assertDoesNotExist()

        if(LOG_ME) ALog.d(TAG, ".$methodName(): Method end: $methodName")
//        pauseTestHere(TAG, methodName, composeTestRule)
    }

    @OptIn(ExperimentalTestApi::class)
    @Test
    fun test9() {
        val methodName = "test9"
        val testDescription = "9. User decided to view the first entity in the list. User has incomplete profile."
        if(LOG_ME) ALog.d(TAG, ".$methodName(): Method start: $methodName \n $testDescription")

        // Prepare data set
        prepareDataSetWithUserWhoHasIncompleteProfile()

        // Launch scenario
        startComposableTestActivityWithUserWhoHasIncompleteProfile()

        // Wait for ActivityHomeScreenFragment1 to appear
        if (LOG_ME) ALog.d(TAG, ".$methodName(): Wait for HomeScreenComposableFragment1 to appear")
        composeTestRule.waitUntilAtLeastOneExists(
            matcher = hasTestTag(HomeScreenComposableFragment1TestTag),
        )

        // Confirm HomeScreenComposableFragment1 is in view
        if(LOG_ME) ALog.d(TAG, ".$methodName(): Confirm HomeScreenComposableFragment1 is in view")
        composeTestRule.onNodeWithTag(HomeScreenComposableFragment1TestTag).assertIsDisplayed()

        // Wait until data has loaded into lazy column.
        if (LOG_ME) ALog.d(TAG, ".$methodName(): Wait until data has loaded into lazy column.")
        composeTestRule.waitForDataToLoadIntoLazyColumn(
            "Wait until data has loaded into lazy column.",
            TAG,
            LOG_ME,
            methodName,
            "0",
        )

        // Click on 1st item in search results
        val listItemInTest = 0
        composeTestRule.clickOnNthItemInLazyColumn(
            "Click on 1st item in search results",
            TAG,
            LOG_ME,
            methodName,
            listItemInTestNumber = listItemInTest,
            lazyColumnTag = "LazyColumnWithPullToRefresh",
            firstListItemTag = "0",
        )

        // Wait for entity details screen to come into view
        if(LOG_ME) ALog.d(TAG, ".$methodName(): Wait for entity details screen to come into view")
        composeTestRule.waitUntilAtLeastOneExists(
            matcher = hasTestTag(HomeScreenCompDetailsScreen1TestTag),
        )

        // Check that entity details screen is in view
        if(LOG_ME) ALog.d(TAG, ".$methodName(): Check that entity details screen is in view")
        composeTestRule.onNodeWithTag(HomeScreenCompDetailsScreen1TestTag).assertIsDisplayed()
        // Click the action button
        if(LOG_ME) ALog.d(TAG, ".$methodName(): Click the action button")
        composeTestRule.onNodeWithTag(
            HomeScreenCompDetailsScreen1ActionButtonTestTag
        ).performClick()

        // Confirm that a dialog appeared saying that profile must be completed to make action on entity
        if(LOG_ME) ALog.d(
            TAG, ".$methodName(): " +
                    "Confirm that a dialog appeared saying that profile must be completed " +
                    "to make action on entity"
        )
        composeTestRule.waitUntilAtLeastOneExists(
            hasText(
                context.getString(R.string.only_fully_verified_users_can_act_on_entity1),
                ignoreCase = true,
            )
        )
        composeTestRule.onNodeWithText(
            context.getString(R.string.only_fully_verified_users_can_act_on_entity1)
        ).assertIsDisplayed()

        if(LOG_ME) ALog.d(TAG, ".$methodName(): Method end: $methodName")
//        pauseTestHere(TAG, methodName, composeTestRule)
    }

    @Test
    fun test10() {
        val methodName = "test10"
        val testDescription =
            "10. User with complete profile navigates around using swipes and navigational drawer."
        if (LOG_ME) ALog.d(TAG, ".$methodName(): Method start: $methodName \n $testDescription")

        // Prepare data set
        prepareDataSetWithUserWhoHasCompleteProfile()

        // Launch scenario
        startComposableTestActivityWithUserWhoHasCompleteProfile()

        // Wait for ActivityHomeScreenFragment1 to appear
        if (LOG_ME) ALog.d(TAG, ".$methodName(): Wait for HomeScreenComposableFragment1 to appear")
        composeTestRule.onNodeWithContentDescription(
            context.getString(R.string.search_query_edittext_content_description)
        ).assertIsDisplayed()


        // Navigate to card 2 via swipe left
        composeTestRule.performSwipeLeft(
            actionDescription = "Navigate to card 2 via swipe left",
            TAG,
            LOG_ME,
            methodName,
            node = composeTestRule.onNodeWithContentDescription(
                context.getString(R.string.home_screen_card1_content_description)
            ),
            sideThresholdFraction = 2* Dimens.fragmentsSwipingLeftSideThresholdFraction
        )


        // Check card 2 is visible
        if (LOG_ME) ALog.d(TAG, ".$methodName(): Check card 2 is visible")
        composeTestRule.onNodeWithContentDescription(
            context.getString(R.string.home_screen_floating_action_button_content_description_card_2),
            ignoreCase = true,
        ).assertIsDisplayed()

        // Navigate to card 1 via swipe right
        composeTestRule.performSwipeRight(
            actionDescription = "Navigate to card 1 via swipe right",
            TAG,
            LOG_ME,
            methodName,
            node = composeTestRule.onNodeWithContentDescription(
                context.getString(R.string.home_screen_card2_content_description)
            ),
            sideThresholdFraction = 2* Dimens.fragmentsSwipingLeftSideThresholdFraction
        )

        // Check card 1 is visible
        if (LOG_ME) ALog.d(TAG, ".$methodName(): Check card 1 is visible")
        composeTestRule.onNodeWithContentDescription(
            context.getString(R.string.search_query_edittext_content_description)
        ).assertIsDisplayed()

        // Navigate to card 3 via bottom nav bar
        composeTestRule.onNodeWithContentDescription(
            context.getString(R.string.home_screen_bottom_nav_menu_3_content_description),
            ignoreCase = true,
        ).performClick()

        // Check card 3 is visible
        if (LOG_ME) ALog.d(TAG, ".$methodName(): Check card 3 is visible")
        composeTestRule.onNodeWithContentDescription(
            context.getString(R.string.home_screen_floating_action_button_content_description_card_3)
        ).assertIsDisplayed()

        if(LOG_ME) ALog.d(TAG, ".$methodName(): Method end: $methodName")
//        pauseTestHere(TAG, methodName, composeTestRule)
    }

    @Test
    fun test11() {
        val methodName = "test11"
        val testDescription =
            "11. User with complete profile opens navigational drawer to navigate."
        if (LOG_ME) ALog.d(TAG, ".$methodName(): Method start: $methodName \n $testDescription")

        // Prepare data set
        prepareDataSetWithUserWhoHasCompleteProfile()

        // Launch scenario
        startComposableTestActivityWithUserWhoHasCompleteProfile()

        // Wait for ActivityHomeScreenFragment1 to appear
        if (LOG_ME) ALog.d(TAG, ".$methodName(): Wait for HomeScreenComposableFragment1 to appear")
        composeTestRule.onNodeWithContentDescription(
            context.getString(R.string.search_query_edittext_content_description)
        ).assertIsDisplayed()


        // Open navigation drawer using swipe right from screen side
        composeTestRule.performSwipeRight(
            actionDescription = "Open navigation drawer using swipe right from screen side",
            TAG,
            LOG_ME,
            methodName,
            node = composeTestRule.onNodeWithContentDescription(
                context.getString(R.string.home_screen_card1_content_description)
            ),
            sideThresholdFraction = 0.5f* Dimens.fragmentsSwipingLeftSideThresholdFraction,
        )

        // Check navigation drawer is visible
        if (LOG_ME) ALog.d(TAG, ".$methodName(): Check navigation drawer is visible")
        composeTestRule.onNodeWithContentDescription(
            context.getString(R.string.homescreen_drawer)
        ).assertIsDisplayed()


        // Navigate to card 4 via navigation drawer
        val itemContentDescription =
            "$DRAWER_CONTENT_DESCRIPTION_TAG ${context.getString(R.string.home_screen_bottom_nav_menu_4_content_description)}"
        composeTestRule.onNodeWithContentDescription(
            itemContentDescription
        ).performClick()


        // Check card 4 is visible
        if (LOG_ME) ALog.d(TAG, ".$methodName(): Check card 4 is visible")
        composeTestRule.onNodeWithContentDescription(
            context.getString(R.string.home_screen_floating_action_button_content_description_card_4)
        ).assertIsDisplayed()

        if(LOG_ME) ALog.d(TAG, ".$methodName(): Method end: $methodName")
//        pauseTestHere(TAG, methodName, composeTestRule)
    }
}