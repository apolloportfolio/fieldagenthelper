package com.aps.catemplateapp


import android.app.Application
import android.app.Service
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Build
import android.view.View
import android.widget.DatePicker
import androidx.compose.ui.test.junit4.AndroidComposeTestRule
import androidx.compose.ui.test.onRoot
import androidx.compose.ui.test.printToLog
import androidx.test.InstrumentationRegistry.getTargetContext
import androidx.test.core.app.ApplicationProvider
import androidx.test.espresso.Espresso
import androidx.test.espresso.IdlingPolicies
import androidx.test.espresso.IdlingRegistry
import androidx.test.espresso.IdlingResource
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.rule.ActivityTestRule
import androidx.test.rule.GrantPermissionRule
import com.aps.catemplateapp.common.framework.presentation.IntentExtras
import com.aps.catemplateapp.common.util.ALog
import com.aps.catemplateapp.common.util.DateUtil
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.core.business.data.cache.abstraction.*
import com.aps.catemplateapp.core.business.data.network.abstraction.*
import com.aps.catemplateapp.core.business.domain.model.entities.ProjectUser
import com.aps.catemplateapp.core.business.domain.model.factories.*
import com.aps.catemplateapp.core.framework.datasources.*
import com.aps.catemplateapp.core.framework.datasources.cachedatasources.room.daos.*
import com.aps.catemplateapp.core.framework.datasources.cachedatasources.room.mappers.*
import com.aps.catemplateapp.core.util.Entity1CacheDataSource
import com.aps.catemplateapp.core.util.Entity1CacheMapper
import com.aps.catemplateapp.core.util.Entity1Dao
import com.aps.catemplateapp.core.util.Entity1Factory
import com.aps.catemplateapp.core.util.Entity1NetworkDataSource
import com.aps.catemplateapp.core.util.Entity2CacheDataSource
import com.aps.catemplateapp.core.util.Entity2CacheMapper
import com.aps.catemplateapp.core.util.Entity2Dao
import com.aps.catemplateapp.core.util.Entity2Factory
import com.aps.catemplateapp.core.util.Entity2NetworkDataSource
import com.aps.catemplateapp.core.util.Entity3CacheDataSource
import com.aps.catemplateapp.core.util.Entity3CacheMapper
import com.aps.catemplateapp.core.util.Entity3Dao
import com.aps.catemplateapp.core.util.Entity3Factory
import com.aps.catemplateapp.core.util.Entity3NetworkDataSource
import com.aps.catemplateapp.core.util.Entity4CacheDataSource
import com.aps.catemplateapp.core.util.Entity4CacheMapper
import com.aps.catemplateapp.core.util.Entity4Dao
import com.aps.catemplateapp.core.util.Entity4Factory
import com.aps.catemplateapp.core.util.Entity4NetworkDataSource
import com.aps.catemplateapp.core.util.EspressoIdlingResourceRule
import com.aps.catemplateapp.core.util.ViewShownIdlingResource
import com.aps.catemplateapp.core.util.extensions.generateAnotherTestEntity1OwnerWithCompleteProfile
import com.aps.catemplateapp.core.util.extensions.generateTestUserWithCompleteProfile
import com.aps.catemplateapp.core.util.extensions.generateTestUserWithIncompleteProfile
import com.aps.catemplateapp.fah.framework.presentation.activity01.ActivityHomeScreen
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.firestore.FirebaseFirestore
import dagger.hilt.android.testing.HiltAndroidRule
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.runBlocking
import org.hamcrest.Matcher
import org.hamcrest.Matchers
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.rules.TestRule
import java.io.IOException
import java.net.InetSocketAddress
import java.net.Socket
import java.util.concurrent.TimeUnit
import javax.inject.Inject


private const val TAG = "BaseInstrumentedTest"
private const val LOG_ME = true

@ExperimentalCoroutinesApi
@FlowPreview
abstract class BaseInstrumentedTest {

    @Inject
    open lateinit var dateUtil : DateUtil

    //    @Inject
//    lateinit var firestore: FirebaseFirestore       // Injecting firestore with hilt causes stream closure
    open val firestore = FirebaseFirestore.getInstance()

    open lateinit var context: Context
    val application: Application
            = ApplicationProvider.getApplicationContext<Context>() as Application

    var user : ProjectUser? = null
    var entity1Owner : ProjectUser? = null


    abstract fun injectTest()


    @get:Rule(order = 0)
    open var hiltRule = HiltAndroidRule(this)

    @get: Rule(order = 1)
    open val espressoIdlingResourceRule = EspressoIdlingResourceRule()
    
    @get:Rule(order = 2)       // Necessary for launching activity with extras
    open var mActivityRule = ActivityTestRule(
        ActivityHomeScreen::class.java, false, false
    )

    @get:Rule(order = 3)
    var permissionRule_ACCESS_FINE_LOCATION = GrantPermissionRule.grant(
        android.Manifest.permission.ACCESS_FINE_LOCATION
    )

    @get:Rule(order = 4)
    var permissionRule_READ_EXTERNAL_STORAGE  = GrantPermissionRule.grant(
        android.Manifest.permission.READ_EXTERNAL_STORAGE
    )

//    @get:Rule(order = 4)
//    var permissionRule_MANAGE_DOCUMENTS = GrantPermissionRule.grant(
//        android.Manifest.permission.MANAGE_DOCUMENTS
//    )


    // User
    @Inject
    open lateinit var userDao: UserDao
    @Inject
    open lateinit var userCacheMapper: UserCacheMapper
    @Inject
    open lateinit var userFactory: UserFactory
    @Inject
    open lateinit var userDataFactory: EntityDataFactoryUser
    @Inject
    open lateinit var userNetworkDataSource: UserNetworkDataSource
    @Inject
    open lateinit var userCacheDataSource: UserCacheDataSource


    // Entity1
    @Inject
    open lateinit var entity1Dao: Entity1Dao
    @Inject
    open lateinit var entity1CacheMapper: Entity1CacheMapper
    @Inject
    open lateinit var entity1Factory: Entity1Factory
    @Inject
    open lateinit var entity1DataFactory: EntityDataFactoryEntity1
    @Inject
    open lateinit var entity1NetworkDataSource: Entity1NetworkDataSource
    @Inject
    open lateinit var entity1CacheDataSource: Entity1CacheDataSource


    // Entity2
    @Inject
    open lateinit var entity2Dao: Entity2Dao
    @Inject
    open lateinit var entity2CacheMapper: Entity2CacheMapper
    @Inject
    open lateinit var entity2Factory: Entity2Factory
    @Inject
    open lateinit var entity2DataFactory: EntityDataFactoryEntity2
    @Inject
    open lateinit var entity2NetworkDataSource: Entity2NetworkDataSource
    @Inject
    open lateinit var entity2CacheDataSource: Entity2CacheDataSource


    // Entity3
    @Inject
    open lateinit var entity3Dao: Entity3Dao
    @Inject
    open lateinit var entity3CacheMapper: Entity3CacheMapper
    @Inject
    open lateinit var entity3Factory: Entity3Factory
    @Inject
    open lateinit var entity3DataFactory: EntityDataFactoryEntity3
    @Inject
    open lateinit var entity3NetworkDataSource: Entity3NetworkDataSource
    @Inject
    open lateinit var entity3CacheDataSource: Entity3CacheDataSource

    // Entity4
    @Inject
    lateinit var entity4Dao: Entity4Dao
    @Inject
    lateinit var entity4CacheMapper: Entity4CacheMapper
    @Inject
    lateinit var entity4Factory: Entity4Factory
    @Inject
    lateinit var entity4DataFactory: EntityDataFactoryEntity4
    @Inject
    lateinit var entity4NetworkDataSource: Entity4NetworkDataSource
    @Inject
    lateinit var entity4CacheDataSource: Entity4CacheDataSource

    init {

    }

    // wait for a certain view to be shown.
    fun waitViewShown(matcher: Matcher<View>) {
        val idlingResource: IdlingResource = ViewShownIdlingResource(matcher,
            ViewMatchers.isDisplayed()
        )
        try {
            IdlingRegistry.getInstance().register(idlingResource)
            Espresso.onView(ViewMatchers.withId(0)).check(ViewAssertions.doesNotExist())
        } finally {
            IdlingRegistry.getInstance().unregister(idlingResource)
        }
    }

    // wait for a date picker view to be shown.
    fun waitViewShownDatePicker() {
        waitViewShown(ViewMatchers.withClassName(Matchers.equalTo(DatePicker::class.java.name)))
    }

    @Before
    open fun runBeforeEveryTest() {
        if(LOG_ME)ALog.d(TAG, "runBeforeEveryTest(): " +
                "")
        IdlingPolicies.setMasterPolicyTimeout(2, TimeUnit.MINUTES);
        IdlingPolicies.setIdlingResourceTimeout(2, TimeUnit.MINUTES);
        hiltRule.inject()
        injectTest()
        context = InstrumentationRegistry.getInstrumentation().targetContext
        grantLocationPermission()
        grantOpenDocumentPermission()
    }

    @After
    open fun runAfterEveryTest() {
        enableInternetConnection()
        user = null
    }

    open fun prepareDataSetWithUserWhoHasIncompleteProfile() = runBlocking {
        val methodName = "prepareDataSetWithUserWhoHasIncompleteProfile()"
        ALog.d(TAG, "Method start: $methodName")
        try {
            ALog.d(TAG, "Creating new user entity")
            user = userFactory.generateTestUserWithIncompleteProfile()
            entity1Owner = userFactory.generateAnotherTestEntity1OwnerWithCompleteProfile()

            prepareDataSetWithUser(user!!, entity1Owner!!)
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
            ALog.d(TAG, "Method end: $methodName")
        }
    }

    open fun prepareDataSetWithUserWhoHasCompleteProfile() = runBlocking {
        val methodName = "prepareDataSetWithUserWhoHasCompleteProfile()"
        ALog.d(TAG, "Method start: $methodName")
        try {
            ALog.d(TAG, "Creating new user entity")
            user = userFactory.generateTestUserWithCompleteProfile()
            entity1Owner = userFactory.generateAnotherTestEntity1OwnerWithCompleteProfile()

            if(LOG_ME)ALog.d(
                TAG, ".prepareDataSetWithUserWhoHasCompleteProfile(): \n" +
                                    "Before register: user == $user\n" +
                    "Before register: entity1Owner == $entity1Owner")
            prepareDataSetWithUser(user!!, entity1Owner!!)
            if(LOG_ME)ALog.d(
                TAG, ".prepareDataSetWithUserWhoHasCompleteProfile(): \n" +
                    "After register: user == $user\n" +
                    "After register: entity1Owner == $entity1Owner")
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
            ALog.d(TAG, "Method end: $methodName")
        }
    }

    private suspend fun prepareDataSetWithUser(
        user: ProjectUser,
        entity1Owner: ProjectUser,
    ) {
        val methodName: String = "prepareDataSetWithUser"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            ALog.d(TAG, "Checking Internet connection.")
            checkInternetConnection()
            if(LOG_ME)ALog.d(TAG, "$methodName(): Registering entity1 owner in Firebase")
            val registeredEntity1Owner = registerAndLoginUserInFirebase(entity1Owner)
            if(LOG_ME)ALog.d(TAG, "$methodName(): Inserting entity1 owner into databases.")
            insertUserIntoDatabases(registeredEntity1Owner)
            if(LOG_ME)ALog.d(TAG, "$methodName(): Logging entity1 owner out.")
            this.entity1Owner = registeredEntity1Owner
            logoutUser(entity1Owner)

            if(LOG_ME)ALog.d(TAG, "$methodName(): Registering user in Firebase.")
            val registeredUser = registerAndLoginUserInFirebase(user)
//        clearUsersInDatabases()         // Exceptions here with Firestore security rules enabled
            if(LOG_ME)ALog.d(TAG, "$methodName(): Inserting user into databases.")
            insertUserIntoDatabases(registeredUser)
            if(LOG_ME)ALog.d(TAG, "$methodName(): Populating databases with test data.")
            this.user = registeredUser
            populateDatabases(entity1Owner)
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }

    suspend fun registerAndLoginUserInFirebase(user: ProjectUser): ProjectUser {
        val methodName = "registerAndLoginUserInFirebase"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        return try {
            // Logging old user in
            ALog.d(TAG, "Logging user in.")
            var loggedInOldUser: FirebaseUser? = null
            try {
                loggedInOldUser = userNetworkDataSource.loginUser(user)
            }catch (e: Exception){}
            if(loggedInOldUser != null) {
                // Deleting old user account in Firebase
                ALog.d(TAG, "Deleting old user account")
                userNetworkDataSource.deleteAccount(user)
            } else {
                if(LOG_ME)ALog.d(TAG, ".registerAndLoginUserInFirebase(): " +
                                        "loggedInOldUser == null")
            }

            // Registering user in Firebase
            ALog.d(TAG, "Registering user in Firestore")
            var firestoreUser: FirebaseUser = userNetworkDataSource.registerNewUser(user)!!
            if(LOG_ME)ALog.d(
                TAG, "$methodName(): " +
                    "before login: firestoreUser.uid == ${firestoreUser.uid}")

            // Logging user in
            ALog.d(TAG, "Logging user in.")
            firestoreUser = userNetworkDataSource.loginUser(user)!!
            if(LOG_ME)ALog.d(
                TAG, "$methodName(): " +
                    "after login: firestoreUser.uid == ${firestoreUser.uid}")
            user.id = UserUniqueID(firestoreUser.uid, firestoreUser.uid)
            if(LOG_ME)ALog.d(
                TAG, "$methodName(): " +
                    "after login: user.id == ${user.id}")

            user
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
            ALog.e(TAG, "$methodName(): user == $user")
            user
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }

    suspend fun logoutUser(user: ProjectUser) {
        val methodName = "signOutUser"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            userNetworkDataSource.logoutUser(user)
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }

    suspend fun clearUsersInDatabases() {
        val methodName: String = "clearUsersInDatabases"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            // Deleting old User entities in databases
            ALog.d(TAG, "Deleting all entities in userDao")
            userDao.deleteAllEntities()
            ALog.d(TAG, "Deleting all entities in userNetworkDataSource")
            userNetworkDataSource.deleteAllEntities()

        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }

    suspend fun insertUserIntoDatabases(user: ProjectUser) {
        val methodName: String = "insertUserIntoDatabases"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            // Adding User entity to databases
            ALog.d(TAG, "Inserting new user entity to userCacheDataSource")
            userCacheDataSource.insertOrUpdateEntity(user)
            ALog.d(TAG, "Inserting new user entity to userNetworkDataSource")
            userNetworkDataSource.insertOrUpdateEntity(user)
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }

    suspend fun clearDatabases(user: ProjectUser) {
        val methodName = "clearDatabases"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            // Entity1
            ALog.d(TAG, "Deleting all entities in entity1Dao")
            entity1Dao.deleteAllEntities()
            ALog.d(TAG, "Deleting all entities in entity1NetworkDataSource")
            entity1NetworkDataSource.deleteAllEntities()

            // Entity3
            ALog.d(TAG, "Deleting all entities in entity3Dao")
            entity3Dao.deleteAllEntities()
            ALog.d(TAG, "Deleting all entities in entity3NetworkDataSource")
            entity3NetworkDataSource.deleteAllEntities()

            // Entity4
            ALog.d(TAG, "Deleting all entities in entity4Dao")
            entity4Dao.deleteAllEntities()
            ALog.d(TAG, "Deleting all entities in entity4NetworkDataSource")
            entity4NetworkDataSource.deleteAllEntities()

            //Entity2
            ALog.d(TAG, "Deleting all entities in entity2Dao")
            entity2Dao.deleteAllEntities()
            ALog.d(TAG, "Deleting all entities in entity2NetworkDataSource")
            entity2NetworkDataSource.deleteAllEntities()
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }

    private fun populateDatabases(entity1Owner: ProjectUser) {
        val methodName = "populateDatabases"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            // Entity1
            ALog.d(TAG, "Inserting entity1 test data")
            insertEntity1TestData(entity1Owner)

            // Entity3
            ALog.d(TAG, "Inserting entity1 details test data")
            insertEntity3TestData()

            // Entity4
            insertEntity4TestData()

            // Entity2
            insertEntity2TestData()
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }

    open fun insertEntity1TestData(entity1Owner: ProjectUser) = runBlocking{
        this.javaClass.classLoader?.let { classLoader ->
            entity1DataFactory = EntityDataFactoryEntity1(application)
        }
        val entity1s = entity1DataFactory.produceListOfEntities()
        if(entity1Owner.id != null) {
            for(entity1 in entity1s) {
                entity1.ownerID = entity1Owner.id
            }
        }

        entity1CacheDataSource.insertEntities(entity1s)
        for(entity1 in entity1s) {
            entity1NetworkDataSource.insertOrUpdateEntity(entity1)
        }
    }

    open fun insertEntity3TestData() = runBlocking{
        val methodName: String = "insertEntity3TestData"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            this.javaClass.classLoader?.let { classLoader ->
                entity3DataFactory = EntityDataFactoryEntity3(application)
            }
            val entity3s = entity3DataFactory.produceListOfEntities()
            entity3CacheDataSource.insertEntities(entity3s)
            entity3NetworkDataSource.insertOrUpdateEntities(entity3s)
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }        
    }

    open fun insertEntity2TestData() = runBlocking{
        val methodName: String = "insertEntity2TestData"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        if (LOG_ME) ALog.d(TAG, "Started insertEntity2TestData")
        try {
            if (LOG_ME) ALog.d(TAG, "Started insertEntity2TestData from BaseInstrumentedTest")
            this.javaClass.classLoader?.let { classLoader ->
                entity2DataFactory = EntityDataFactoryEntity2(application)
            }
            val entity2List = entity2DataFactory.produceListOfEntities()

            entity2CacheDataSource.insertEntities(entity2List)
            entity2NetworkDataSource.insertOrUpdateEntities(entity2List)
        } catch (e: Exception) {
            ALog.e(TAG, "$methodName(): Exception occured while inserting rental offer test data.")
            ALog.e(TAG, methodName, e)
        } finally {
            if (LOG_ME) ALog.d(
                TAG, "Method end: $methodName")
        }
    }

    open fun insertEntity4TestData() = runBlocking{
        this.javaClass.classLoader?.let { classLoader ->
            entity4DataFactory = EntityDataFactoryEntity4(application)
        }
        val entity4s = entity4DataFactory.produceListOfEntities()
        entity4CacheDataSource.insertEntities(entity4s)
    }

    open fun disableInternetConnection() {
        InstrumentationRegistry.getInstrumentation().uiAutomation.executeShellCommand("svc wifi disable")
        InstrumentationRegistry.getInstrumentation().uiAutomation.executeShellCommand("svc data disable")
    }

    open fun enableInternetConnection() {
        InstrumentationRegistry.getInstrumentation().uiAutomation.executeShellCommand("svc wifi enable")
        InstrumentationRegistry.getInstrumentation().uiAutomation.executeShellCommand("svc data enable")
    }

    open fun isConnected(context: Context): Boolean {
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo.isConnected
    }

    fun checkInternetConnection() : Boolean {
        return try {
            val sock = Socket()
            sock.connect(InetSocketAddress("8.8.8.8", 53), 1500)
            sock.close()
            if(LOG_ME)ALog.d(TAG, ".checkInternetConnection(): Internet is available.")
            true
        } catch (e: IOException) {
            if(LOG_ME)ALog.d(TAG, ".checkInternetConnection(): Internet is not available.")
            false
        }
    }

    open fun checkInternetConnection(context:Context) : Boolean {
        var connectivity : ConnectivityManager?
        var info : NetworkInfo?
        connectivity = context.getSystemService(Service.CONNECTIVITY_SERVICE)
                as ConnectivityManager
        if ( connectivity != null) {
            info = connectivity.activeNetworkInfo
            if (info != null) {
                if (info.state == NetworkInfo.State.CONNECTED) {
                    if(LOG_ME)ALog.d(TAG, ".checkInternetConnection(): Device has Internet connection.")
                    return true
                }
            } else {
                if(LOG_ME)ALog.d(TAG, ".checkInternetConnection(): Device doesn't have Internet connection.")
                return false
            }
        }
        if(LOG_ME)ALog.d(TAG, ".checkInternetConnection(): Device doesn't have Internet connection.")
        return false
    }

    open fun grantLocationPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            with(InstrumentationRegistry.getInstrumentation().uiAutomation) {
                executeShellCommand("appops set " + context.packageName + " android:mock_location allow")
                Thread.sleep(1000)
            }
        }
    }

    open fun grantPhonePermission() {
        // In M+, trying to call a number will trigger a runtime dialog. Make sure
        // the permission is granted before running this test.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            InstrumentationRegistry.getInstrumentation().uiAutomation.executeShellCommand(
                "pm grant " + getTargetContext().packageName
                        + " android.permission.CALL_PHONE"
            )
        }
    }

    open fun grantOpenDocumentPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            InstrumentationRegistry.getInstrumentation().uiAutomation.executeShellCommand(
                "pm grant " + getTargetContext().packageName
                        + " android.permission.MANAGE_DOCUMENTS"
            )
            Thread.sleep(1000)
        }
    }

        open fun startTestActivityWithUserWhoHasIncompleteProfile() : ActivityHomeScreen {
//        val scenario = launchActivity<ActivityHomeScreen>()       // Launches activity without extras
        if(user == null)user = userFactory.generateTestUserWithIncompleteProfile()
        val intent = Intent()
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        intent.putExtra(IntentExtras.APP_USER, user)
        return mActivityRule.launchActivity(intent)
    }

    open fun startTestActivityWithUserWhoHasCompleteProfile() : ActivityHomeScreen {
//        val scenario = launchActivity<ActivityHomeScreen>()       // Launches activity without extras
        if(user == null)user = userFactory.generateTestUserWithCompleteProfile()
        val intent = Intent()
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        intent.putExtra(IntentExtras.APP_USER, user)
        return mActivityRule.launchActivity(intent)
    }

    fun <R : TestRule, A : androidx.activity.ComponentActivity> pauseTestHere(
        tag: String,
        methodName: String,
        composeTestRule: AndroidComposeTestRule<R, A>?,
        printComposeTree: Boolean = true,
    ) {
        if(composeTestRule != null) {
            if(LOG_ME) ALog.w(tag, ".$methodName(): " +
                    "GotHere: composable test paused using root.")
//            if(printComposeTree) composeTestRule.onRoot(useUnmergedTree = true).printToLog(tag)
            if(printComposeTree) composeTestRule.onRoot().printToLog(tag)
            for(i in 0..Long.MAX_VALUE) {
                try {
                    composeTestRule.onRoot().assertExists()
                } catch(e: java.lang.AssertionError) {
//                    if(LOG_ME)ALog.e(TAG, ".$methodName(): " +
//                            "AssertionError")
                } catch (e: java.lang.IllegalStateException) {
//                    if(LOG_ME)ALog.e(TAG, ".$methodName(): " +
//                                            "IllegalStateException: ", e)
                } catch (e: Exception) {
//                    if(LOG_ME)ALog.e(TAG, ".$methodName(): " +
//                            "Exception: ", e)
                }
            }
        }
        if(LOG_ME) ALog.w(tag, ".$methodName(): GotHere: " +
                "non composable test paused by sleeping the thread.")
        Thread.sleep(Long.MAX_VALUE)
    }
}