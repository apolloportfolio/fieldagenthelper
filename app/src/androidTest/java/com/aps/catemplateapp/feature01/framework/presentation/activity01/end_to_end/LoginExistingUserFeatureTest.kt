package com.aps.catemplateapp.feature01.framework.presentation.activity01.end_to_end

import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.provider.Settings
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.test.core.app.launchActivity
import androidx.test.espresso.Espresso
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.action.ViewActions.closeSoftKeyboard
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.filters.MediumTest
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.platform.app.InstrumentationRegistry.getInstrumentation
import androidx.test.uiautomator.By
import androidx.test.uiautomator.BySelector
import androidx.test.uiautomator.UiDevice
import androidx.test.uiautomator.Until
import com.aps.catemplateapp.BaseInstrumentedTest
import com.aps.catemplateapp.R
import com.aps.catemplateapp.common.util.ALog
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.core.business.data.cache.abstraction.UserCacheDataSource
import com.aps.catemplateapp.core.business.data.network.abstraction.UserNetworkDataSource
import com.aps.catemplateapp.core.business.domain.model.entities.ProjectUser
import com.aps.catemplateapp.core.business.domain.model.factories.UserFactory
import com.aps.catemplateapp.core.di.NetworkDataSourcesModule
import com.aps.catemplateapp.core.di.ProductionModule
import com.aps.catemplateapp.core.di.RoomModule
import com.aps.catemplateapp.core.framework.datasources.EntityDataFactoryUser
import com.aps.catemplateapp.core.framework.datasources.cachedatasources.room.daos.UserDao
import com.aps.catemplateapp.core.framework.datasources.cachedatasources.room.mappers.UserCacheMapper
import com.aps.catemplateapp.core.framework.datasources.cachedatasources.room.model.UserCacheEntity
import com.aps.catemplateapp.core.util.*
import com.aps.catemplateapp.core.util.extensions.generateTestUserWithCompleteProfile
import com.aps.catemplateapp.feature01.framework.presentation.activity01.RegisterLoginActivity
import com.aps.catemplateapp.feature01.framework.presentation.activity01.fragments.RegisterLoginActivityMainFragment
import com.aps.catemplateapp.launchFragmentInHiltContainer
import com.google.firebase.auth.FirebaseUser
import dagger.hilt.android.testing.HiltAndroidTest
import dagger.hilt.android.testing.UninstallModules
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.runBlocking
import org.hamcrest.core.StringContains.containsString
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.jupiter.api.Assertions
import org.junit.runner.RunWith
import org.mockito.Mockito.mock
import java.lang.Thread.sleep
import java.lang.reflect.Field
import java.lang.reflect.InvocationTargetException
import java.lang.reflect.Method
import javax.inject.Inject


private const val TAG = "LoginExistingUserFeatureTest"

/*
    Test case scenarios:
    1. User has already registered in database and is now logging in on a device that he hasn't logged in before.
    2. User has already registered in database and is now logging on a device that he has already logged in before.
    3. User has already registered in database and is now logging in on a device that he hasn't logged in before. Internet connection is unavailable.
    4. User has already registered in database and is now logging on a device that he has already logged in before. Internet connection is unavailable.
    5. User has already registered in database and is now logging in on a device that he hasn't logged in before. User entered wrong password.
    6. User has already registered in database and is now logging in on a device that he hasn't logged in before. User has entered wrong password and email.
    7. User has already registered in database and is now logging in on a device that he hasn't logged in before. User has entered no email.
    8. User has already registered in database and is now logging in on a device that he hasn't logged in before. User has entered no password.
 */

@MediumTest
@ExperimentalCoroutinesApi
@FlowPreview
@RunWith(AndroidJUnit4ClassRunner::class)
@HiltAndroidTest
@UninstallModules(
    RoomModule::class,
    NetworkDataSourcesModule::class,
    ProductionModule::class,
//    InteractorsBindingModule::class,
)
class LoginExistingUserFeatureTest: BaseInstrumentedTest() {

//    @get:Rule(order = 2)
//    val animationsRule = AnimationsRule()


    @Inject
    lateinit var cacheMapper: UserCacheMapper
    
    @Inject
    lateinit var entityFactory: UserFactory

    @Inject
    lateinit var entityDataFactory: EntityDataFactoryUser

    @Inject
    lateinit var dao: UserDao

    @Inject
    lateinit var userEntityNetworkDataSource: UserNetworkDataSource

    @Inject
    lateinit var entityCacheDataSource: UserCacheDataSource

    private lateinit var testEntityList: List<UserCacheEntity>

    init {
//        hiltRule.inject()
//        injectTest()
//        testEntityList = cacheMapper.mapToEntityList(
//            entityDataFactory.produceListOfEntities()
//        )
//        prepareDataSet(testEntityList)
    }

    @Before
    override fun runBeforeEveryTest() {
//        IdlingPolicies.setMasterPolicyTimeout(1, TimeUnit.MINUTES);
//        IdlingPolicies.setIdlingResourceTimeout(1, TimeUnit.MINUTES);
        hiltRule.inject()
        injectTest()
        context = InstrumentationRegistry.getInstrumentation().targetContext
    }

    @After
    override fun runAfterEveryTest() {
        enableInternetConnection()
    }

    private fun prepareDataSetSimulatingNewDevice() = runBlocking{
        val methodName = "prepareDataSetSimulatingNewDevice()"
        ALog.d(TAG, "Method start: $methodName")
        try {
            registerUserInFirestore(generateTestUser())
            dao.deleteAllEntities()
            userEntityNetworkDataSource.deleteAllEntities()
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
            ALog.d(TAG, "Method end: $methodName")
        }
    }

    private fun prepareDataSetSimulatingUsedDevice(testData: List<UserCacheEntity>?) = runBlocking{
        val methodName = "prepareDataSetSimulatingUsedDevice()"
        ALog.d(TAG, "Method start: $methodName")
        try {
            dao.deleteAllEntities()
            userEntityNetworkDataSource.deleteAllEntities()
//            if(testData != null)dao.insertEntities(testData)  // There should be only one user in local database
            val user : ProjectUser = generateTestUser()
            val firestoreUser = registerUserInFirestore(user)!!
            user.id = UserUniqueID(firestoreUser.uid, firestoreUser.uid)
            addUserToDatabase(user)
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
            ALog.d(TAG, "Method end: $methodName")
        }
    }

    private fun generateTestUser() : ProjectUser {
        val user: ProjectUser = entityFactory.generateRandomEntity()
        user.emailAddress = TEST_USER_REAL_EMAIL_ADDRESS
        user.password = TEST_USER_REAL_PASSWORD
        return user
    }

    private fun generateFullyVerifiedTestUser() : ProjectUser {
        val user: ProjectUser = entityFactory.generateRandomEntity()
        user.emailAddress = TEST_USER_REAL_EMAIL_ADDRESS
        user.password = TEST_USER_REAL_PASSWORD
        user.emailAddressVerified = true
        user.name = TEST_USER_NAME
        user.surname = TEST_USER_SURNAME
        return user
    }

    private suspend fun addUserToDatabase(user : ProjectUser) {
        ALog.d(TAG, ".addUserToDatabase(): Adding user to cache and Firestore: $user")
        entityCacheDataSource.insertOrUpdateEntity(user)
        userEntityNetworkDataSource.insertOrUpdateEntity(user)
    }

    private suspend fun registerUserInFirestore(user : ProjectUser): FirebaseUser? {
        return userEntityNetworkDataSource.registerNewUser(user)
    }

    // https://stackoverflow.com/questions/27620976/android-instrumentation-test-offline-cases
    // Doesn't work.
    private fun setAirplaneMode(enable: Boolean) {
        if ((if (enable) 1 else 0) == Settings.System.getInt(
                getInstrumentation().context.contentResolver,
                Settings.Global.AIRPLANE_MODE_ON, 0
            )
        ) {
            return
        }
        val device: UiDevice = UiDevice.getInstance(getInstrumentation())
        device.openQuickSettings()
        // Find the text of your language
        val description: BySelector = By.desc("Airplane mode")
        // Need to wait for the button, as the opening of quick settings is animated.
        device.wait(Until.hasObject(description), 500)
        device.findObject(description).click()
        getInstrumentation().context.sendBroadcast(Intent(Intent.ACTION_CLOSE_SYSTEM_DIALOGS))
    }

    @Deprecated("Doesn't work")
    // https://stackoverflow.com/questions/52878138/unable-to-toggle-airplane-mode-espresso
    // Doesn't work. Throws exception about no field even though taht field exists.
    @Throws(
        ClassNotFoundException::class,
        NoSuchFieldException::class,
        IllegalAccessException::class,
        NoSuchMethodException::class,
        InvocationTargetException::class
    )
    fun setMobileDataEnabled(context: Context, enabled: Boolean) {
        val conman = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val conmanClass = Class.forName(conman.javaClass.name)
        val connectivityManagerField: Field = conmanClass.getDeclaredField("mService")
        connectivityManagerField.setAccessible(true)
        val connectivityManager: Any = connectivityManagerField.get(conman)
        val connectivityManagerClass = Class.forName(connectivityManager.javaClass.name)
        val setMobileDataEnabledMethod: Method = connectivityManagerClass.getDeclaredMethod(
            "setMobileDataEnabled",
            java.lang.Boolean.TYPE
        )
        setMobileDataEnabledMethod.setAccessible(true)
        setMobileDataEnabledMethod.invoke(connectivityManager, enabled)
    }

    //    1. User has already registered in database and is now logging in on a device that he hasn't logged in before.
    @Test
    fun loginUserOnNewDeviceTest() {
        val userEmailAddress: String = TEST_USER_REAL_EMAIL_ADDRESS
        val userPassword: String = TEST_USER_REAL_PASSWORD
        prepareDataSetSimulatingNewDevice()
        Assertions.assertTrue(loginUserOnNewDevice(userEmailAddress, userPassword, null))
    }

    //    2. User has already registered in database and is now logging on a device that he has already logged in before.
    // Upon opening the app it should take the user directly to the homescreen, skipping login.
    //    Due to new Firestore security rules, it is impossible to unregister and delete user from client.
    //    WARNING! For above reason delete user manually in Firestore
    @Test
    fun loginUserOnUsedDeviceTest() {
        prepareDataSetSimulatingUsedDevice(
            cacheMapper.mapToEntityList(entityDataFactory.produceListOfEntities())
        )
        Assertions.assertTrue(loginUserOnUsedDevice())
    }

    //    3. User has already registered in database and is now logging in on a device that he hasn't logged in before. Internet connection is unavailable.
    @Test
    fun loginUserOnNewDeviceWithoutInternetConnectionTest() {
        val userEmailAddress: String = TEST_USER_REAL_EMAIL_ADDRESS
        val userPassword: String = TEST_USER_REAL_PASSWORD
        val expectedWarningText = context.getString(R.string.NO_INTERNET_CONNECTION)
        prepareDataSetSimulatingNewDevice()
        disableInternetConnection()
        sleep(2000)
        Assertions.assertFalse(isConnected(context))
        Assertions.assertFalse(checkInternetConnection(context))
        Assertions.assertTrue(loginUserOnNewDevice(userEmailAddress, userPassword, expectedWarningText))
    }

    //    4. User has already registered in database and is now logging on a device that
    //    he has already logged in before. Internet connection is unavailable.
    @Test
    fun loginUserOnUsedDeviceWithoutInternetConnectionTest() {
        val expectedWarningText = context.getString(
            R.string.NO_INTERNET_CONNECTION_ENABLE_AND_RESTART
        )
        prepareDataSetSimulatingUsedDevice(
            cacheMapper.mapToEntityList(entityDataFactory.produceListOfEntities())
        )
        disableInternetConnection()
        sleep(2000)
        Assertions.assertFalse(isConnected(context))
        Assertions.assertFalse(checkInternetConnection(context))
        Assertions.assertFalse(loginUserOnUsedDevice())
    }

    //    5. User has already registered in database and is now logging in on a device that
    //    he hasn't logged in before. User entered wrong password.
    @Test
    fun loginExistingUserOnNewDeviceWithWrongPasswordTest() {
        val userEmailAddress: String = TEST_USER_REAL_EMAIL_ADDRESS
        val userPassword: String = TEST_USER_FAKE_PASSWORD
        val expectedWarningText = context.getString(R.string.unsuccessful_login)
        prepareDataSetSimulatingNewDevice()
        Assertions.assertTrue(loginUserOnNewDevice(userEmailAddress, userPassword, expectedWarningText))
    }


    //    6. User has already registered in database and is now logging in on a device that he hasn't logged in before. User has entered wrong password and email.
    @Test
    fun loginFakeUserOnNewDeviceTest() {
        val userEmailAddress: String = TEST_USER_FAKE_EMAIL_ADDRESS
        val userPassword: String = TEST_USER_REAL_PASSWORD
        val expectedWarningText =  context.getString(R.string.unsuccessful_login)
        prepareDataSetSimulatingNewDevice()
        Assertions.assertTrue(loginUserOnNewDevice(userEmailAddress, userPassword, expectedWarningText))
    }


    //    7. User has already registered in database and is now logging in on a device that he hasn't logged in before. User has entered no email.
    @Test
    fun loginUserOnNewDeviceWithoutEmailTest() {
        val userEmailAddress: String? = null
        val userPassword: String? = TEST_USER_REAL_PASSWORD
//        val expectedWarningText = context.getString(R.string.you_must_enter_an_email)
        val expectedWarningText = context.getString(UserCacheEntity.nullEmailError())
        Assertions.assertTrue(loginUserOnNewDevice(userEmailAddress, userPassword, expectedWarningText))
    }

    //    8. User has already registered in database and is now logging in on a device that he hasn't logged in before. User has entered no password.
    @Test
    fun loginUserOnNewDeviceWithoutPasswordTest() {
        val userEmailAddress: String? = TEST_USER_REAL_EMAIL_ADDRESS
        val userPassword: String? = null
        val expectedWarningText = context.getString(R.string.you_must_enter_a_password)
        prepareDataSetSimulatingNewDevice()
        Assertions.assertTrue(loginUserOnNewDevice(userEmailAddress, userPassword, expectedWarningText))
    }

    // https://www.youtube.com/watch?v=uUmFfZDoOTY
    private fun loginUserD(
        userEmailAddress: String?,
        userPassword: String?,
        expectedWarningText: String?): Boolean {
        val methodName: String = "loginUser()"
        ALog.d("LoginUserFeatureTest", "Method start: $methodName")
        val navController = mock(NavController::class.java)
        launchFragmentInHiltContainer<RegisterLoginActivityMainFragment> {
            Navigation.setViewNavController(requireView(), navController)
        }

        // RegisterLoginActivityMainMainFragment
        // Wait for RegisterLoginActivityMainMainFragment to come into view
        // https://www.google.com/search?client=opera&q=eandroid+espresso+IdleResources&sourceid=opera&ie=UTF-8&oe=UTF-8
        // confirm RegisterLoginActivityMainMainFragment is in view
        onView(withId(R.id.fragment_activity_id_main)).check(matches(isDisplayed()))

        // RegisterLoginActivityMainOptionsFragment
        // Wait for RegisterLoginActivityMainOptionsFragment to come into view

        // confirm RegisterLoginActivityMainOptionsFragment is in view
        onView(withId(R.id.fragment_activity_start_options)).check(matches(isDisplayed()))
        // click on login option button
        onView(withId(R.id.button_goto_login))

        // RegisterLoginActivityMainLoginFragment
        // Wait for RegisterLoginActivityMainLoginFragment to come into view

        // confirm RegisterLoginActivityMainLoginFragment is in view
        onView(withId(R.id.fragment_activity_registerlogin_login)).check(matches(isDisplayed()))
        // Type in the email address

        // Type in the password

        // Click the login button

        // Check if login progress bar appeared with appropriate message

        // Check if alert dialog with info about wrong credentials appeared


        return false
    }

    @Test
    fun loginFullyVerifiedUserOnNewDevice(): Unit = runBlocking  {
        val methodName: String = "loginFullyVerifiedUserOnNewDevice()"
        ALog.d("LoginUserFeatureTest", "Method start: $methodName")

        val userEmailAddress: String = TEST_USER_REAL_EMAIL_ADDRESS
        val userPassword: String = TEST_USER_REAL_PASSWORD


        ALog.d(TAG, "Deleting all user entities in local storage.")
        dao.deleteAllEntities()
        ALog.d(TAG, "Creating new user entity")
        user = userFactory.generateTestUserWithCompleteProfile()
        ALog.d(TAG, "Checking Internet connection.")
        checkInternetConnection()
        ALog.d(TAG, "$methodName(): Registering user in Firebase.")
//        val registeredUser = registerAndLoginUserInFirebase(user!!)
        ALog.d(TAG, "$methodName(): Inserting user into databases.")
//        insertUserIntoDatabases(registeredUser)
//        logoutUser(user!!)
//        logoutUser(registeredUser)

//        registerUserInFirestore(generateFullyVerifiedTestUser())

        val scenario = launchActivity<RegisterLoginActivity>()

        sleep(9000000000)

        // RegisterLoginActivityMainMainFragment
        // Wait for RegisterLoginActivityMainMainFragment to come into view
        ALog.d(TAG, "$methodName: Wait for RegisterLoginActivityMainMainFragment to come into view")
        waitViewShown(withId(R.id.logo_slogan))
        val logoSlogan = Espresso.onView(withId(R.id.logo_slogan))
        // confirm RegisterLoginActivityMainMainFragment is in view
        ALog.d("LoginUserFeatureTest", "$methodName: confirm RegisterLoginActivityMainMainFragment is in view")
        logoSlogan.check(ViewAssertions.matches(ViewMatchers.isDisplayed()))


        // RegisterLoginActivityMainOptionsFragment
        // Wait for RegisterLoginActivityMainOptionsFragment to come into view
        ALog.d(TAG, "$methodName: Wait for RegisterLoginActivityMainOptionsFragment to come into view")
        waitViewShown(withId(R.id.button_goto_login))

        // confirm RegisterLoginActivityMainOptionsFragment is in view
        ALog.d(TAG, "$methodName: confirm RegisterLoginActivityMainOptionsFragment is in view")
        val loginOptionButton = Espresso.onView(withId(R.id.button_goto_login))
        loginOptionButton.check(ViewAssertions.matches(ViewMatchers.isDisplayed()))

        // click on login option button
        ALog.d(TAG, "$methodName: click on login button")
        Espresso.onView(withId(R.id.button_goto_login)).perform(ViewActions.click())


        // RegisterLoginActivityMainLoginFragment
        // Wait for RegisterLoginActivityMainLoginFragment to come into view
        ALog.d(TAG, "$methodName: Wait for RegisterLoginActivityMainLoginFragment to come into view")
        waitViewShown(withId(R.id.button_login))

        // Type in the email address
        ALog.d(TAG, "$methodName: Type in the email address")
        val emailEditText = Espresso.onView(withId(R.id.email_edittext))
        emailEditText.perform(ViewActions.click(), closeSoftKeyboard())
        emailEditText.perform(ViewActions.replaceText(userEmailAddress))


        // Type in the password
        ALog.d(TAG, "$methodName: Type in the password")
        val passwordEditText = Espresso.onView(withId(R.id.password_edittext))
        passwordEditText.perform(ViewActions.click(), closeSoftKeyboard())
        passwordEditText.perform(ViewActions.replaceText(userPassword))

        // Click the login button
        ALog.d(TAG, "$methodName: Click the login button")
        val loginButton = Espresso.onView(withId(R.id.button_login))
        sleep(4000)

        loginButton.check(matches(ViewMatchers.withEffectiveVisibility(ViewMatchers.Visibility.VISIBLE)))
        Espresso.onView(withId(R.id.main_progress_bar)).check(matches(ViewMatchers.withEffectiveVisibility(ViewMatchers.Visibility.GONE)))
        loginButton.perform(ViewActions.click())
        // Check if login progress bar appeared
//        Espresso.onView(withId(R.id.main_progress_bar)).check(matches(ViewMatchers.withEffectiveVisibility(ViewMatchers.Visibility.VISIBLE)))   //Even though progress bar is visible on the screen Espresso can't find it after clicking the button

//        sleep(9000000000000)
    }

    private fun loginUserOnNewDevice(
            userEmailAddress: String?,
            userPassword: String?,
            expectedWarningText: String?): Boolean {
        val methodName: String = "loginUserOnNewDevice()"
        ALog.d("LoginUserFeatureTest", "Method start: $methodName")
        val scenario = launchActivity<RegisterLoginActivity>()


        // RegisterLoginActivityMainMainFragment
        // Wait for RegisterLoginActivityMainMainFragment to come into view
        ALog.d("LoginUserFeatureTest", "$methodName: Wait for RegisterLoginActivityMainMainFragment to come into view")
        waitViewShown(withId(R.id.logo_slogan))
        val logoSlogan = Espresso.onView(withId(R.id.logo_slogan))
        // confirm RegisterLoginActivityMainMainFragment is in view
        ALog.d("LoginUserFeatureTest", "$methodName: confirm RegisterLoginActivityMainMainFragment is in view")
        logoSlogan.check(ViewAssertions.matches(ViewMatchers.isDisplayed()))


        // RegisterLoginActivityMainOptionsFragment
        // Wait for RegisterLoginActivityMainOptionsFragment to come into view
        ALog.d("LoginUserFeatureTest", "$methodName: Wait for RegisterLoginActivityMainOptionsFragment to come into view")
        waitViewShown(withId(R.id.button_goto_login))

        // confirm RegisterLoginActivityMainOptionsFragment is in view
        ALog.d("LoginUserFeatureTest", "$methodName: confirm RegisterLoginActivityMainOptionsFragment is in view")
        val loginOptionButton = Espresso.onView(withId(R.id.button_goto_login))
        loginOptionButton.check(ViewAssertions.matches(ViewMatchers.isDisplayed()))

        // click on login option button
        ALog.d("LoginUserFeatureTest", "$methodName: click on login button")
        Espresso.onView(withId(R.id.button_goto_login)).perform(ViewActions.click())


        // RegisterLoginActivityMainLoginFragment
        // Wait for RegisterLoginActivityMainLoginFragment to come into view
        ALog.d("LoginUserFeatureTest", "$methodName: Wait for RegisterLoginActivityMainLoginFragment to come into view")
        waitViewShown(withId(R.id.button_login))

        // Type in the email address
        ALog.d("LoginUserFeatureTest", "$methodName: Type in the email address")
        if(userEmailAddress != null) {
            val emailEditText = Espresso.onView(withId(R.id.email_edittext))
            emailEditText.perform(ViewActions.click(), closeSoftKeyboard())
            emailEditText.perform(ViewActions.replaceText(userEmailAddress));
        } else {
            ALog.d("LoginUserFeatureTest", "$methodName: Provided email is null.")
        }


        // Type in the password
        ALog.d("LoginUserFeatureTest", "$methodName: Type in the password")
        if(userPassword != null) {
            val passwordEditText = Espresso.onView(withId(R.id.password_edittext))
            passwordEditText.perform(ViewActions.click(), closeSoftKeyboard())
            passwordEditText.perform(ViewActions.replaceText(userPassword))
        } else {
            ALog.d("LoginUserFeatureTest", "$methodName: Provided password is null.")
        }

        // Click the login button
        ALog.d("LoginUserFeatureTest", "$methodName: Click the login button")
        val loginButton = Espresso.onView(withId(R.id.button_login))
        sleep(4000)

        loginButton.check(matches(ViewMatchers.withEffectiveVisibility(ViewMatchers.Visibility.VISIBLE)))
        Espresso.onView(withId(R.id.main_progress_bar)).check(matches(ViewMatchers.withEffectiveVisibility(ViewMatchers.Visibility.GONE)))
        loginButton.perform(ViewActions.click())
        // Check if login progress bar appeared
//        Espresso.onView(withId(R.id.main_progress_bar)).check(matches(ViewMatchers.withEffectiveVisibility(ViewMatchers.Visibility.VISIBLE)))   //Even though progress bar is visible on the screen Espresso can't find it after clicking the button

//        sleep(9000000000000)

        // Check if alert dialog with info about wrong credentials or no Internet connection appeared
        return if(expectedWarningText != null) {

            ALog.d("LoginUserFeatureTest", "$methodName: Check if snackbar with expected warning text appeared;    $expectedWarningText")
            var snackBarAppeared = false
            try {
                onView(withText(containsString(expectedWarningText)))
                    .check(matches(withEffectiveVisibility(ViewMatchers.Visibility.VISIBLE)));
                ALog.d("LoginUserFeatureTest", "$methodName: Snackbar with expected warning text did appear;    $expectedWarningText")
                snackBarAppeared = true
            } catch(e:Exception) {
                ALog.d("LoginUserFeatureTest", "$methodName: Snackbar with expected warning text did not appear;    $expectedWarningText")
                ALog.e(TAG, methodName, e)
            }


            // https://stackoverflow.com/questions/21045509/check-if-a-dialog-is-displayed-with-espresso
            ALog.d("LoginUserFeatureTest", "$methodName: Check if alert dialog with expected warning text appeared;    $expectedWarningText")
            var dialogAppeared = false
            try {
//                onView(withText(containsString(expectedWarningText))).check(matches(isDisplayed()))
                onView(withText(containsString(expectedWarningText))).check(matches(withEffectiveVisibility(ViewMatchers.Visibility.VISIBLE)))
                ALog.d("LoginUserFeatureTest", "$methodName: Alert dialog with expected warning text appeared;    $expectedWarningText")
                dialogAppeared = true
            } catch(e:Exception) {
                ALog.d("LoginUserFeatureTest", "$methodName: Alert dialog with expected warning text did not appear;    $expectedWarningText")
                ALog.e(TAG, methodName, e)
            }
            dialogAppeared || snackBarAppeared
        } else {
            // Check if user successfully navigated to home screen or profile screen
            // Could use Espresso Intents from here:
            // https://developer.android.com/training/testing/espresso/intents
            // https://stackoverflow.com/questions/53595837/androidx-no-instrumentation-registered-must-run-under-a-registering-instrumen
            // but a bug with Gradle prevents it.
            ALog.d("LoginUserFeatureTest", "$methodName: Check if user successfully navigated to home screen or profile screen")
            ALog.d(TAG, ".$methodName(): Wait for ActivityHomeScreenFragment1 to appear")
            waitViewShown(withId(R.id.search_query_edittext))
            Espresso.onView(withId(R.id.search_query_edittext)).check(matches(ViewMatchers.withEffectiveVisibility(ViewMatchers.Visibility.VISIBLE)))
            ALog.d(TAG, "Method end: $methodName")
            return true
        }
    }

    private fun loginUserOnUsedDevice() : Boolean {
        val methodName: String = "loginUserOnNewDevice()"
        ALog.d(TAG, "Method start: $methodName")
        val scenario = launchActivity<RegisterLoginActivity>()

        // Check if user successfully navigated to home screen or profile screen
        // Could use Espresso Intents from here:
        // https://developer.android.com/training/testing/espresso/intents
        // https://stackoverflow.com/questions/53595837/androidx-no-instrumentation-registered-must-run-under-a-registering-instrumen
        // but a bug with Gradle prevents it.
        ALog.d("LoginUserFeatureTest", "$methodName: Check if user successfully navigated to home screen or profile screen")
        return try {
            ALog.d(TAG, ".$methodName(): Wait for ActivityHomeScreenFragment1 to appear")
            waitViewShown(withId(R.id.search_query_edittext))
            Espresso.onView(withId(R.id.search_query_edittext))
                .check(
                    matches(
                        ViewMatchers.withEffectiveVisibility(ViewMatchers.Visibility.VISIBLE)
                    )
                )
            ALog.d(TAG, "Method end: $methodName")
            true
        } catch(e:Exception) {
            ALog.d(TAG, "Method end: $methodName")
            false
        }
    }



    private fun loginTestUser() {
        val userEmailAddress = TEST_USER_REAL_EMAIL_ADDRESS
        val userPassword = TEST_USER_REAL_PASSWORD
        val expectedWarningText = null
        val scenario = launchActivity<RegisterLoginActivity>()

        // RegisterLoginActivityMainMainFragment
        // Wait for RegisterLoginActivityMainMainFragment to come into view
        waitViewShown(withId(R.id.logo_slogan))
        val logoSlogan = Espresso.onView(withId(R.id.logo_slogan))
        // confirm UserListFragment is in view
        logoSlogan.check(ViewAssertions.matches(ViewMatchers.isDisplayed()))


        // RegisterLoginActivityMainOptionsFragment
        // Wait for RegisterLoginActivityMainOptionsFragment to come into view
        waitViewShown(withId(R.id.button_goto_login))

        // confirm RegisterLoginActivityMainOptionsFragment is in view
        val loginOptionButton = Espresso.onView(withId(R.id.button_goto_login))
        loginOptionButton.check(ViewAssertions.matches(ViewMatchers.isDisplayed()))

        // click on login button
        Espresso.onView(withId(R.id.button_goto_login)).perform(ViewActions.click())


        // RegisterLoginActivityMainLoginFragment
        // Wait for RegisterLoginActivityMainLoginFragment to come into view
        waitViewShown(withId(R.id.button_goto_login))

        // Type in the email address
        val emailEditText = Espresso.onView(withId(R.id.email_edittext))
        emailEditText.perform(ViewActions.typeText(userEmailAddress))

        // Type in the password
        val passwordEditText = Espresso.onView(withId(R.id.email_edittext))
        passwordEditText.perform(ViewActions.typeText(userPassword))

        // Click the login button
        val loginButton = Espresso.onView(withId(R.id.button_login))
        loginButton.perform(ViewActions.click())

        // Check if login progress bar appeared with appropriate message
        val progressBar = UtilitiesForAndroidTesting.uiObjectWithId(R.id.main_progress_bar)
        if (progressBar != null) {
            Assertions.assertTrue(progressBar.exists())
        } else {
            ALog.e("LoginFeatureTest", "progressBar == null")
            Assertions.assertTrue(false)
        }

        // Check if alert dialog with info about wrong credentials appeared

    }



    override fun injectTest() {
//        (application.appComponent as TestAppComponent).inject(this)
    }


}