package com.aps.catemplateapp.feature01.framework.presentation.activity01.end_to_end

import androidx.test.core.app.launchActivity
import androidx.test.espresso.Espresso
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.action.ViewActions.closeSoftKeyboard
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.filters.MediumTest
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import com.aps.catemplateapp.BaseInstrumentedTest
import com.aps.catemplateapp.R
import com.aps.catemplateapp.common.util.ALog
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.core.business.data.cache.abstraction.UserCacheDataSource
import com.aps.catemplateapp.core.business.data.network.abstraction.UserNetworkDataSource
import com.aps.catemplateapp.core.business.domain.model.entities.ProjectUser
import com.aps.catemplateapp.core.business.domain.model.factories.UserFactory
import com.aps.catemplateapp.core.di.NetworkDataSourcesModule
import com.aps.catemplateapp.core.di.ProductionModule
import com.aps.catemplateapp.core.di.RoomModule
import com.aps.catemplateapp.core.framework.datasources.EntityDataFactoryUser
import com.aps.catemplateapp.core.framework.datasources.cachedatasources.room.daos.UserDao
import com.aps.catemplateapp.core.framework.datasources.cachedatasources.room.mappers.UserCacheMapper
import com.aps.catemplateapp.core.framework.datasources.cachedatasources.room.model.UserCacheEntity
import com.aps.catemplateapp.core.util.*
import com.aps.catemplateapp.feature01.framework.presentation.activity01.RegisterLoginActivity
import com.google.firebase.auth.FirebaseUser
import dagger.hilt.android.testing.HiltAndroidTest
import dagger.hilt.android.testing.UninstallModules
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.runBlocking
import org.hamcrest.core.StringContains.containsString
import org.junit.Test
import org.junit.jupiter.api.Assertions
import org.junit.runner.RunWith
import java.lang.Thread.sleep
import javax.inject.Inject


private const val TAG = "RegisterUserFeatureTest"


//    Test case scenarios:
//    1. User has never registered in database and is now registering in on a device that he hasn't logged in before. Both passwords match.
//    2. User is trying to register using an email that is already taken and different password than the one in database.
//    3. User has never registered in database and is now registering in on a device that he hasn't logged in before. Passwords do not match.
//    4. User has never registered in database and is now registering in on a device that he hasn't logged in before. Both passwords match. Email is empty.
//    5. User has never registered in database and is now registering in on a device that he hasn't logged in before. Both passwords match. Email is invalid.
//    6. User has never registered in database and is now registering in on a device that he hasn't logged in before. Both passwords match. Internet connection is unavailable.
//    7. User has already registered in database and is now registering in on a device that he hasn't logged in before. User made a mistake and chose register instead of login.


@MediumTest
@ExperimentalCoroutinesApi
@FlowPreview
@RunWith(AndroidJUnit4ClassRunner::class)
@HiltAndroidTest
@UninstallModules(
    RoomModule::class,
    NetworkDataSourcesModule::class,
    ProductionModule::class,
)
class RegisterUserFeatureTest: BaseInstrumentedTest() {

    @Inject
    lateinit var cacheMapper: UserCacheMapper

    @Inject
    lateinit var entityFactory: UserFactory

    @Inject
    lateinit var entityDataFactory: EntityDataFactoryUser

    @Inject
    lateinit var dao: UserDao

    @Inject
    lateinit var entityNetworkDataSource: UserNetworkDataSource

    @Inject
    lateinit var entityCacheDataSource: UserCacheDataSource

    private lateinit var testEntityList: List<UserCacheEntity>

    init {

    }

    private fun prepareDataSetSimulatingNewDevice() = runBlocking{
        val methodName = "prepareDataSetSimulatingNewDevice()"
        ALog.d(TAG, "Method start: $methodName")
        try {
            dao.deleteAllEntities()
            entityNetworkDataSource.deleteAllEntities()
            val user : ProjectUser = generateTestUser()
            deleteUserInFirestore(user)
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
            ALog.d(TAG, "Method end: $methodName")
        }
    }

    private suspend fun deleteUserInFirestore(user: ProjectUser) {
        ALog.d(TAG, ".deleteUserInFirestore(): Unregistering and deleting user from Firestore: $user")
        entityNetworkDataSource.deleteAccount(user)
    }

    private fun prepareDataSetSimulatingNewDeviceAndExistingUser() = runBlocking{
        val methodName = "prepareDataSetSimulatingNewDevice()"
        ALog.d(TAG, "Method start: $methodName")
        try {
            dao.deleteAllEntities()
            entityNetworkDataSource.deleteAllEntities()
            val user : ProjectUser = generateTestUser()
            deleteUserInFirestore(user)
            val firebaseUser = registerUserInFirestore(user)
            user.id = UserUniqueID(firebaseUser!!.uid, firebaseUser.uid)
            addUserToDatabase(user)
            dao.deleteAllEntities()
            entityNetworkDataSource.deleteAllEntities()
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
            ALog.d(TAG, "Method end: $methodName")
        }
    }

    private fun generateTestUser() : ProjectUser {
        val user: ProjectUser = entityFactory.generateRandomEntity()
        user.emailAddress = TEST_USER_REAL_EMAIL_ADDRESS
        user.password = TEST_USER_REAL_PASSWORD
        return user
    }

    private suspend fun addUserToDatabase(user : ProjectUser) {
        ALog.d(TAG, ".addUserToDatabase(): Adding user to cache and Firestore: $user")
        entityCacheDataSource.insertOrUpdateEntity(user)
        entityNetworkDataSource.insertOrUpdateEntity(user)
    }

    private suspend fun registerUserInFirestore(user : ProjectUser): FirebaseUser? {
        ALog.d(TAG, ".registerUserInFirestore(): Registering user in Firestore: $user")
        return entityNetworkDataSource.registerNewUser(user)
    }

    //    1. User has never registered in database and is now registering in on a device that he hasn't logged in before.
    @Test
    fun registerUserOnNewDeviceTest() {
        val userEmailAddress: String = TEST_USER_REAL_EMAIL_ADDRESS
        val userPassword: String = TEST_USER_REAL_PASSWORD
        val userPasswordConfirmation: String = TEST_USER_REAL_PASSWORD
        prepareDataSetSimulatingNewDevice()
        Assertions.assertTrue(registerUserOnNewDevice(userEmailAddress, userPassword, userPasswordConfirmation, null))
    }

    //    2. User is trying to register using an email that is already taken and different password than the one in database.
    //    A dialog or snackbar should appear with message stating that this email is taken.
    @Test
    fun registerUserWithExistingEmailAndWrongPasswordTest() {
        val userEmailAddress: String = TEST_USER_REAL_EMAIL_ADDRESS
        val userPassword: String = TEST_USER_FAKE_PASSWORD
        val userPasswordConfirmation: String = TEST_USER_FAKE_PASSWORD
        val expectedWarningText = context.getString(R.string.possible_registration_problems)
        prepareDataSetSimulatingNewDeviceAndExistingUser()
        Assertions.assertTrue(registerUserOnNewDevice(userEmailAddress, userPassword, userPasswordConfirmation, expectedWarningText))
    }

    //    3. User has never registered in database and is now registering in on a device that he hasn't logged in before. Passwords do not match.
    @Test
    fun registerUserOnNewDevicePasswordsDoNotMatchTest() {
        val userEmailAddress: String = TEST_USER_REAL_EMAIL_ADDRESS
        val userPassword: String = TEST_USER_REAL_PASSWORD
        val userPasswordConfirmation: String = TEST_USER_FAKE_PASSWORD
        val expectedWarningText = context.getString(R.string.passwords_do_not_match)
        prepareDataSetSimulatingNewDevice()
        Assertions.assertTrue(registerUserOnNewDevice(userEmailAddress, userPassword, userPasswordConfirmation, expectedWarningText))
    }

    //    4. User has never registered in database and is now registering in on a device that he hasn't logged in before. Both passwords match. Email is empty.
    @Test
    fun registerUserOnNewDevicePasswordsMatchEmailIsEmptyTest() {
        val userEmailAddress: String = ""
        val userPassword: String = TEST_USER_REAL_PASSWORD
        val userPasswordConfirmation: String = TEST_USER_REAL_PASSWORD
        val expectedWarningText = context.getString(R.string.you_must_enter_an_email)
        prepareDataSetSimulatingNewDevice()
        Assertions.assertTrue(registerUserOnNewDevice(userEmailAddress, userPassword, userPasswordConfirmation, expectedWarningText))
    }

    //    5. User has never registered in database and is now registering in on a device that he hasn't logged in before. Both passwords match. Email is invalid.
    //    http://softwaretesterfriend.com/manual-testing/valid-invalid-email-address-format-validation/
    @Test
    fun registerUserOnNewDevicePasswordsMatchEmailIsInvalidTest() {
        val userPassword: String = TEST_USER_REAL_PASSWORD
        val userPasswordConfirmation: String = TEST_USER_REAL_PASSWORD
        val expectedWarningText = context.getString(R.string.you_must_enter_an_email)
        prepareDataSetSimulatingNewDevice()
        ALog.d(TAG, "registerUserOnNewDevicePasswordsMatchEmailIsInvalidTest(): Checking for invalid email: $TEST_USER_INVALID_EMAIL_ADDRESS_1")
        Assertions.assertTrue(registerUserOnNewDevice(TEST_USER_INVALID_EMAIL_ADDRESS_1, userPassword, userPasswordConfirmation, expectedWarningText))

        ALog.d(TAG, "registerUserOnNewDevicePasswordsMatchEmailIsInvalidTest(): Checking for invalid email: $TEST_USER_INVALID_EMAIL_ADDRESS_2")
        Assertions.assertTrue(registerUserOnNewDevice(TEST_USER_INVALID_EMAIL_ADDRESS_2, userPassword, userPasswordConfirmation, expectedWarningText))

        // "a”b(c)d,e:f;gi[j\\k]l@vp.pl"
        ALog.d(TAG, "registerUserOnNewDevicePasswordsMatchEmailIsInvalidTest(): Checking for invalid email: $TEST_USER_INVALID_EMAIL_ADDRESS_3")
        Assertions.assertTrue(registerUserOnNewDevice(TEST_USER_INVALID_EMAIL_ADDRESS_3, userPassword, userPasswordConfirmation, expectedWarningText))

        // "abc”test”email@.pl"
        ALog.d(TAG, "registerUserOnNewDevicePasswordsMatchEmailIsInvalidTest(): Checking for invalid email: $TEST_USER_INVALID_EMAIL_ADDRESS_4")
        Assertions.assertTrue(registerUserOnNewDevice(TEST_USER_INVALID_EMAIL_ADDRESS_4, userPassword, userPasswordConfirmation, expectedWarningText))

        // "abc is”not\\valid@vp.pl"
        ALog.d(TAG, "registerUserOnNewDevicePasswordsMatchEmailIsInvalidTest(): Checking for invalid email: $TEST_USER_INVALID_EMAIL_ADDRESS_5")
        Assertions.assertTrue(registerUserOnNewDevice(TEST_USER_INVALID_EMAIL_ADDRESS_5, userPassword, userPasswordConfirmation, expectedWarningText))

        // ".test@vp.pl"
        ALog.d(TAG, "registerUserOnNewDevicePasswordsMatchEmailIsInvalidTest(): Checking for invalid email: $TEST_USER_INVALID_EMAIL_ADDRESS_6")
        Assertions.assertTrue(registerUserOnNewDevice(TEST_USER_INVALID_EMAIL_ADDRESS_6, userPassword, userPasswordConfirmation, expectedWarningText))

        // ".test@vp.pl"
        ALog.d(TAG, "registerUserOnNewDevicePasswordsMatchEmailIsInvalidTest(): Checking for invalid email: $TEST_USER_INVALID_EMAIL_ADDRESS_7")
        Assertions.assertTrue(registerUserOnNewDevice(TEST_USER_INVALID_EMAIL_ADDRESS_7, userPassword, userPasswordConfirmation, expectedWarningText))

        // "test@vp..pl"
        ALog.d(TAG, "registerUserOnNewDevicePasswordsMatchEmailIsInvalidTest(): Checking for invalid email: $TEST_USER_INVALID_EMAIL_ADDRESS_8")
        Assertions.assertTrue(registerUserOnNewDevice(TEST_USER_INVALID_EMAIL_ADDRESS_8, userPassword, userPasswordConfirmation, expectedWarningText))

        // " noalternative67@vp.pl"
        ALog.d(TAG, "registerUserOnNewDevicePasswordsMatchEmailIsInvalidTest(): Checking for invalid email: $TEST_USER_INVALID_EMAIL_ADDRESS_9")
        Assertions.assertTrue(registerUserOnNewDevice(TEST_USER_INVALID_EMAIL_ADDRESS_9, userPassword, userPasswordConfirmation, expectedWarningText))

        // "noalternative67@vp.pl "
        ALog.d(TAG, "registerUserOnNewDevicePasswordsMatchEmailIsInvalidTest(): Checking for invalid email: $TEST_USER_INVALID_EMAIL_ADDRESS_10")
        Assertions.assertTrue(registerUserOnNewDevice(TEST_USER_INVALID_EMAIL_ADDRESS_10, userPassword, userPasswordConfirmation, expectedWarningText))
    }

    //    6. User has never registered in database and is now registering in on a device that he hasn't logged in before. Both passwords match. Internet connection is unavailable.
    @Test
    fun registerUserOnNewDeviceWithoutInternetConnectionTest() {
        val userEmailAddress: String = TEST_USER_REAL_EMAIL_ADDRESS
        val userPassword: String = TEST_USER_REAL_PASSWORD
        val userPasswordConfirmation: String = TEST_USER_REAL_PASSWORD
        val expectedWarningText = context.getString(R.string.NETWORK_ERROR)
        prepareDataSetSimulatingNewDevice()
        disableInternetConnection()
        sleep(2000)
        Assertions.assertFalse(isConnected(context))
        Assertions.assertFalse(checkInternetConnection(context))
        Assertions.assertTrue(registerUserOnNewDevice(userEmailAddress, userPassword, userPasswordConfirmation, expectedWarningText))
    }

    //    7. User has already registered in database and is now registering in on a device that he hasn't logged in before. User made a mistake and chose register instead of login.
    @Test
    fun registerUserOnNewDevicePasswordsMatchEmailIsAlreadyInDatabasePasswordMatchesEmailTest() {
        val userEmailAddress: String = TEST_USER_REAL_EMAIL_ADDRESS
        val userPassword: String = TEST_USER_REAL_PASSWORD
        val userPasswordConfirmation: String = TEST_USER_REAL_PASSWORD
        Assertions.assertTrue(isConnected(context))
        Assertions.assertTrue(checkInternetConnection(context))
        prepareDataSetSimulatingNewDeviceAndExistingUser()
        Assertions.assertTrue(registerUserOnNewDevice(userEmailAddress, userPassword, userPasswordConfirmation, null))
    }

    private fun registerUserOnNewDevice(
        userEmailAddress: String?,
        userPassword: String?,
        userPasswordConfirmation: String?,
        expectedWarningText: String?): Boolean {
        val methodName: String = "registerUserOnNewDevice()"
        ALog.d("RegisterUserFeatureTest", "Method start: $methodName")
        val scenario = launchActivity<RegisterLoginActivity>()

        // ActivityStartMainFragment
        // Wait for ActivityStartMainFragment to come into view
        ALog.d("RegisterUserFeatureTest", "$methodName: Wait for ActivityStartMainFragment to come into view")
        waitViewShown(withId(R.id.logo_slogan))
        val logoSlogan = Espresso.onView(withId(R.id.logo_slogan))
        // confirm ActivityStartMainFragment is in view
        ALog.d("RegisterUserFeatureTest", "$methodName: confirm ActivityStartMainFragment is in view")
        logoSlogan.check(ViewAssertions.matches(ViewMatchers.isDisplayed()))


        // ActivityStartOptionsFragment
        // Wait for ActivityStartOptionsFragment to come into view
        ALog.d("RegisterUserFeatureTest", "$methodName: Wait for ActivityStartOptionsFragment to come into view")
        waitViewShown(withId(R.id.button_goto_register))

        // confirm ActivityStartOptionsFragment is in view
        ALog.d("RegisterUserFeatureTest", "$methodName: confirm ActivityStartOptionsFragment is in view")
        val registerOptionButton = Espresso.onView(withId(R.id.button_goto_register))
        registerOptionButton.check(ViewAssertions.matches(ViewMatchers.isDisplayed()))

        // click on register option button
        ALog.d("RegisterUserFeatureTest", "$methodName: click on login button")
        Espresso.onView(withId(R.id.button_goto_register)).perform(ViewActions.click())


        // ActivityStartRegisterFragment
        // Wait for ActivityStartRegisterFragment to come into view
        ALog.d("RegisterUserFeatureTest", "$methodName: Wait for ActivityStartRegisterFragment to come into view")
        waitViewShown(withId(R.id.button_register))

        // Type in the email address
        ALog.d("RegisterUserFeatureTest", "$methodName: Type in the email address")
        if(userEmailAddress != null) {
            val emailEditText = Espresso.onView(withId(R.id.email_edittext))
            emailEditText.perform(ViewActions.click(), closeSoftKeyboard())
            emailEditText.perform(ViewActions.replaceText(userEmailAddress));
        } else {
            ALog.d("RegisterUserFeatureTest", "$methodName: Provided email is null.")
        }
        sleep(2000)

        // Type in the password
        ALog.d("RegisterUserFeatureTest", "$methodName: Type in the password")
        if(userPassword != null) {
            val passwordEditText = Espresso.onView(withId(R.id.password_edittext))
            passwordEditText.perform(ViewActions.click(), closeSoftKeyboard())
            passwordEditText.perform(ViewActions.replaceText(userPassword))
        } else {
            ALog.d("RegisterUserFeatureTest", "$methodName: Provided userPassword is null.")
        }
        sleep(4000)

        // Type in the password confirmation
        ALog.d("RegisterUserFeatureTest", "$methodName: Type in the password")
        if(userPasswordConfirmation != null) {
            val passwordConfirmationEditText = Espresso.onView(withId(R.id.password_confirm_edittext))
            passwordConfirmationEditText.perform(ViewActions.click(), closeSoftKeyboard())
            passwordConfirmationEditText.perform(ViewActions.replaceText(userPasswordConfirmation))
        } else {
            ALog.d("RegisterUserFeatureTest", "$methodName: Provided userPasswordConfirmation is null.")
        }

        // Click the register button
        ALog.d("RegisterUserFeatureTest", "$methodName: Click the login button")
        val registerButton = Espresso.onView(withId(R.id.button_register))
        sleep(4000)

        registerButton.check(matches(ViewMatchers.withEffectiveVisibility(ViewMatchers.Visibility.VISIBLE)))
        Espresso.onView(withId(R.id.main_progress_bar)).check(matches(ViewMatchers.withEffectiveVisibility(ViewMatchers.Visibility.GONE)))
        registerButton.perform(ViewActions.click())
        sleep(1000)

        // Check if alert dialog with info about wrong credentials or no Internet connection appeared
        return if(expectedWarningText != null) {
            val checkInterval: Long = 1000
            var i = 0
            val n = 6
            var snackBarAppeared = false
            var dialogAppeared = false

            while((!dialogAppeared || !snackBarAppeared) && i<n) {
                ALog.d("RegisterUserFeatureTest", "$methodName: Check if snackbar with expected warning text appeared;    $expectedWarningText;    $i/$n")
                try {
                    onView(withText(containsString(expectedWarningText)))
                        .check(matches(withEffectiveVisibility(ViewMatchers.Visibility.VISIBLE)));
                    ALog.d("RegisterUserFeatureTest", "$methodName: Snackbar with expected warning text did appear;    $expectedWarningText")
                    snackBarAppeared = true
                } catch(e:Exception) {
                    ALog.d("RegisterUserFeatureTest", "$methodName: Snackbar with expected warning text did not appear;    $expectedWarningText")
                    ALog.e(TAG, methodName, e)
                }


                // https://stackoverflow.com/questions/21045509/check-if-a-dialog-is-displayed-with-espresso
                ALog.d("RegisterUserFeatureTest", "$methodName: Check if alert dialog with expected warning text appeared;    $expectedWarningText")
                try {
//                onView(withText(containsString(expectedWarningText))).check(matches(isDisplayed()))
                    onView(withText(containsString(expectedWarningText))).check(matches(withEffectiveVisibility(ViewMatchers.Visibility.VISIBLE)))
                    ALog.d("RegisterUserFeatureTest", "$methodName: Alert dialog with expected warning text appeared;    $expectedWarningText")
                    dialogAppeared = true
                } catch(e:Exception) {
                    ALog.d("RegisterUserFeatureTest", "$methodName: Alert dialog with expected warning text did not appear;    $expectedWarningText")
                    ALog.e(TAG, methodName, e)
                }
                sleep(checkInterval)
                i++
            }

            dialogAppeared || snackBarAppeared
        } else {
            // Check if user successfully navigated to home screen or profile screen
            // Could use Espresso Intents from here:
            // https://developer.android.com/training/testing/espresso/intents
            // https://stackoverflow.com/questions/53595837/androidx-no-instrumentation-registered-must-run-under-a-registering-instrumen
            // but a bug with Gradle prevents it.
            ALog.d("RegisterUserFeatureTest", "$methodName: Check if user successfully navigated to home screen or profile screen")
            waitViewShown(withId(R.id.search_query_edittext))
            Espresso.onView(withId(R.id.search_query_edittext)).check(matches(ViewMatchers.withEffectiveVisibility(ViewMatchers.Visibility.VISIBLE)))
            ALog.d(TAG, "Method end: $methodName")
            return true
        }
    }

    override fun injectTest() {
    }
}