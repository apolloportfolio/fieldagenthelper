package com.aps.catemplateapp.feature03.framework.presentation.activity01.fragments

import android.os.Bundle
import android.os.Parcel
import android.os.Parcelable
import android.view.View
import androidx.activity.OnBackPressedCallback
import androidx.lifecycle.ViewModelProvider
import com.aps.catemplateapp.R
import com.aps.catemplateapp.common.business.domain.state.StateEvent
import com.aps.catemplateapp.common.framework.presentation.BaseMVIFragment
import com.aps.catemplateapp.common.framework.presentation.BaseViewModel
import com.aps.catemplateapp.common.util.ALog
import com.aps.catemplateapp.common.util.DateUtil
//import com.aps.catemplateapp.common.util.GlideApp
import com.aps.catemplateapp.common.util.extensions.hideKeyboard
import com.aps.catemplateapp.core.util.ProjectConstants.FIRESTORE_ENTITY_1_IMAGES_SUB_COLLECTION
import com.aps.catemplateapp.core.util.ProjectConstants.FIRESTORE_IMAGES_COLLECTION
import com.aps.catemplateapp.databinding.FragmentActivityEntity1DetailsFragment1Binding
import com.aps.catemplateapp.feature03.framework.presentation.activity01.ActivityEntity1Details
import com.aps.catemplateapp.feature03.framework.presentation.activity01.ActivityEntity1DetailsNavigation
import com.aps.catemplateapp.feature03.framework.presentation.activity01.ActivityEntity1DetailsViewModel
import com.aps.catemplateapp.feature03.framework.presentation.activity01.state.ActivityEntity1DetailsStateEvent
import com.aps.catemplateapp.feature03.framework.presentation.activity01.state.ActivityEntity1DetailsViewState
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.ktx.storage
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.InternalCoroutinesApi
import com.aps.catemplateapp.core.util.Entity1


const val activityEntity1Details_Fragment1_STATE_BUNDLE_KEY = "com.aps.cleanarchitecturetemplateapplication.feature03.framework.presentation.activity01.fragments.fragment1"


private const val TAG = "ActivityEntity1DetailsFragment1"
private const val LOG_ME = true

@FlowPreview
@ExperimentalCoroutinesApi
@AndroidEntryPoint
class ActivityEntity1DetailsFragment1
constructor(
        private val viewModelFactory: ViewModelProvider.Factory,
        private val dateUtil: DateUtil
): BaseMVIFragment<Entity1, ActivityEntity1DetailsViewState<Entity1>>(layoutRes = R.layout.fragment_activity_entity1_details_fragment1),
    ActivityEntity1DetailsNavigation {

    private val viewModel : ActivityEntity1DetailsViewModel
        get() = (activity as ActivityEntity1Details).viewModel
//    private val viewModel: ViewModelActivityEntity1Details by viewModels {
//        viewModelFactory
//    }

    private lateinit var binding: FragmentActivityEntity1DetailsFragment1Binding

    constructor(parcel: Parcel) : this(
            TODO("viewModelFactory"),
            TODO("dateUtil")) {
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.setupChannel()
    }

    @InternalCoroutinesApi
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        restoreInstanceState(savedInstanceState)
        setupOnBackPressDispatcher()
    }

    override fun onResume() {
        super.onResume()
    }

    override fun onPause() {
        super.onPause()
    }

    override fun restoreInstanceState(savedInstanceState: Bundle?){
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
    }

    private fun setupOnBackPressDispatcher() {
        val callback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                onBackPressed()
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner, callback)
    }

    override fun onBackPressed() {
        val methodName: String = "onBackPressed"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName()")
        try {
            super.onBackPressed()
            navigateBack(this)
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName()")
        }
    }
    override fun setupUI(){
        view?.hideKeyboard()
    }

    override fun onDestroyView() {
        super.onDestroyView()
    }

    override fun setupBinding(view: View) {
        binding = FragmentActivityEntity1DetailsFragment1Binding.bind(view)
    }

    companion object CREATOR : Parcelable.Creator<ActivityEntity1DetailsFragment1> {
        override fun createFromParcel(parcel: Parcel): ActivityEntity1DetailsFragment1 {
            return ActivityEntity1DetailsFragment1(parcel)
        }

        override fun newArray(size: Int): Array<ActivityEntity1DetailsFragment1?> {
            return arrayOfNulls(size)
        }
    }

    override fun subscribeCustomObservers() {
        // Nothing to set as there are no input fields
    }

    override fun getViewModel(): BaseViewModel<Entity1, ActivityEntity1DetailsViewState<Entity1>> {
        return viewModel
    }

    override fun getInitStateEvent(): StateEvent {
        return ActivityEntity1DetailsStateEvent.None
    }

    override fun changeUiOrNavigateDependingOnViewState(viewState: ActivityEntity1DetailsViewState<Entity1>) {
        val methodName: String = "changeUiOrNavigateDependingOnViewState"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            if (LOG_ME) ALog.d(TAG, "$methodName(): viewState.entity1 == ${viewState.entity1}")
            if (LOG_ME) ALog.d(TAG,
                "$methodName(): viewState == ${viewState.javaClass.simpleName} $viewState    " +
                        "viewModel == ${viewModel.javaClass.simpleName} ${viewModel.hashCode()}")
            viewState.entity1?.let {
                populateUiWithEntity1Data(it)
            }

            viewState.userWantsToActOnEntity1?.let {
                if(it){
                    actOnEntity1()
                    viewModel.setUserWantsToActOnEntity1(false)
                }
            }
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }

    override fun onEntityUpdateSuccess() {

    }

    override fun onEntityDeleteSuccess() {
        
    }

    override fun getStateBundleKey(): String? {
        return activityEntity1Details_Fragment1_STATE_BUNDLE_KEY
    }

    override fun setAllOnClickAndOnTouchListeners() {
        binding.actionButton.setOnClickListener(this)
        binding.entity1Picture1.setOnClickListener(this)
        binding.entity1Picture2.setOnClickListener(this)
        binding.entity1Picture3.setOnClickListener(this)
        binding.entity1Picture4.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        if (LOG_ME) ALog.d(TAG, "onClick(): Method start")
        super.onClick(v)
        when(v) {
            binding.actionButton -> {
                if (LOG_ME) ALog.d(TAG, "onClick(): binding.actionButton")
                viewModel.setStateEvent(ActivityEntity1DetailsStateEvent.Entity1ActionEvent)
            }

            binding.entity1Picture1 -> {
                if (LOG_ME) ALog.d(TAG, "onClick(): binding.entity1Picture1")
                showPicture1()
            }

//            binding.entity1Picture2 -> {
//                if (LOG_ME) ALog.d(TAG, "onClick(): binding.entity1Picture2")
//                showPicture2()
//            }
//
//            binding.entity1Picture3 -> {
//                if (LOG_ME) ALog.d(TAG, "onClick(): binding.entity1Picture3")
//                showPicture3()
//            }
//
//            binding.entity1Picture4 -> {
//                if (LOG_ME) ALog.d(TAG, "onClick(): binding.entity1Picture4")
//                showPicture4()
//            }
        }
    }

    override fun updateUIInViewModel() {
        if(viewModel.checkEditState()){
            view?.hideKeyboard()
            viewModel.exitEditState()
        }
    }

    private fun updateMainEntityInViewModel() {
        if(viewModel.getIsUpdatePending()){
            viewModel.getCurrentViewStateOrNew().mainEntity?.let {
                ActivityEntity1DetailsStateEvent.UpdateMainEntityEvent(
                    it
                )
            }?.let {
                if (LOG_ME) ALog.d(TAG, "updateMainEntityInViewModel(): Sending UpdateMainEntityEvent")
                viewModel.setStateEvent(
                    it
                )
            }
        }
    }

    // Method takes data from ViewModel about a entity1 and populates all text views and check boxes
    // with data, setting visibility to gone for all check boxes that would be unchecked.
    private fun populateUiWithEntity1Data(entity1 : Entity1) {
        val methodName: String = "populateUiWithEntity1Data"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            val glideOptions: RequestOptions = RequestOptions()
                .placeholder(R.drawable.ic_launcher)
                .error(R.drawable.ic_launcher)
                .diskCacheStrategy(DiskCacheStrategy.ALL)

            if(entity1.picture1URI != null && entity1.picture1URI != "") {
                entity1.picture1URI?.let { pictureURI ->
                    val storageReference = Firebase.storage
                    val imageRef = storageReference.reference
                        .child(FIRESTORE_IMAGES_COLLECTION)
                        .child(FIRESTORE_ENTITY_1_IMAGES_SUB_COLLECTION)
                        .child(entity1.id!!.firestoreDocumentID)
//                        .child(pictureURI)
                        .child("1")

                    // Legacy code that
//                    GlideApp.with(binding.entity1Picture1)
//                        .load(imageRef)
//                        .apply(glideOptions)
//                        .into(binding.entity1Picture1)
                }
            } else {
                if (LOG_ME) ALog.d(TAG, "$methodName(): entity1.picture1URI == null")
                binding.entity1Picture1.visibility = View.GONE
                binding.entity1Picture1Container.visibility = View.GONE
            }

            if(entity1.name != null) {
                if (LOG_ME) ALog.d(TAG, "$methodName(): entity1.name == ${entity1.name}")
                entity1.name?.let {
                    binding.titleLbl.text = it
                }
            } else {
                if (LOG_ME) ALog.d(TAG, "$methodName(): entity1.modelName == null")
                binding.titleLbl.visibility = View.INVISIBLE
            }

            if(entity1.description != null) {
                if (LOG_ME) ALog.d(TAG, "$methodName(): " +
                        "entity1.description == ${entity1.description}")
                entity1.description?.let {
                    binding.entity1Description.text = it
                }
            } else {
                if (LOG_ME) ALog.d(TAG, "$methodName(): entity1.description == null")
                binding.entity1Description.visibility = View.GONE
            }

//            if(entity1.switch1 == true) {
//                if (LOG_ME) ALog.d(TAG, "$methodName(): entity1.switch1 == ${entity1.switch1}")
//                entity1.switch1?.let {
//                    binding.switch1.isChecked = it
//                }
//            } else {
//                if (LOG_ME) ALog.d(TAG, "$methodName(): entity1.switch1 == null")
//                binding.switch1.visibility = View.GONE
//            }
//
//            if(entity1.switch2 == true) {
//                if (LOG_ME) ALog.d(TAG, "$methodName(): entity1.switch2 == ${entity1.switch2}")
//                entity1.switch2?.let {
//                    binding.switch2.isChecked = it
//                }
//            } else {
//                if (LOG_ME) ALog.d(TAG, "$methodName(): entity1.switch2 == null")
//                binding.switch2.visibility = View.GONE
//            }
//
//            if(entity1.switch3 == true) {
//                if (LOG_ME) ALog.d(TAG, "$methodName(): entity1.switch3 == ${entity1.switch3}")
//                entity1.switch3?.let {
//                    binding.switch3.isChecked = it
//                }
//            } else {
//                if (LOG_ME) ALog.d(TAG, "$methodName(): entity1.switch3 == null")
//                binding.switch3.visibility = View.GONE
//            }
//
//            if(entity1.switch4 == true) {
//                if (LOG_ME) ALog.d(TAG, "$methodName(): entity1.switch4 == ${entity1.switch4}")
//                entity1.switch4?.let {
//                    if(it)binding.switch4.isChecked = it
//                }
//            } else {
//                if (LOG_ME) ALog.d(TAG, "$methodName(): entity1.switch4 == ${entity1.switch4}")
//                binding.switch4.visibility = View.GONE
//            }
//
//            if(entity1.switch5 == true) {
//                if (LOG_ME) ALog.d(TAG, "$methodName(): entity1.switch5 == ${entity1.switch5}")
//                entity1.switch5?.let {
//                    if(it)binding.switch5.isChecked = it
//                }
//            } else {
//                if (LOG_ME) ALog.d(TAG, "$methodName(): entity1.switch5 == ${entity1.switch5}")
//                binding.switch5.visibility = View.GONE
//            }
//
//            if(entity1.switch6 == true) {
//                if (LOG_ME) ALog.d(TAG, "$methodName(): entity1.switch6 == ${entity1.switch6}")
//                entity1.switch6?.let {
//                    if(it)binding.switch6.isChecked = it
//                }
//            } else {
//                if (LOG_ME) ALog.d(TAG, "$methodName(): entity1.switch6 == ${entity1.switch6}")
//                binding.switch6.visibility = View.GONE
//            }
//
//            if (LOG_ME) ALog.d(TAG, "$methodName(): entity1.switch7 == ${entity1.switch7}")
//            if(entity1.switch7 == true) {
//                entity1.switch7?.let {
//                    if(it)binding.switch7.isChecked = it
//                }
//            } else {
//                binding.switch7.visibility = View.GONE
//            }

//            if (LOG_ME) ALog.d(TAG, "$methodName(): entity1.country == ${entity1.country}")
//            if(entity1.country != null) {
//                entity1.country?.let {
//                    binding.countryLbl.text = it
//                }
//            } else {
//                binding.countryLbl.visibility = View.GONE
//                binding.countryTitle.visibility = View.GONE
//            }

            if (LOG_ME) ALog.d(TAG, "$methodName(): entity1.city == ${entity1.city}")
            if(entity1.city != null) {
                entity1.city?.let {
                    binding.cityLbl.text = it.toString()
                }
            } else {
                binding.cityLbl.visibility = View.GONE
                binding.cityTitle.visibility = View.GONE
            }

//            if (LOG_ME) ALog.d(TAG, "$methodName(): entity1.streetName == ${entity1.streetName}")
//            if(entity1.streetName != null && entity1.streetName != "") {
//                entity1.streetName?.let {
//                    var address = it
//                    if (LOG_ME) ALog.d(TAG, "$methodName(): address == ${address}")
//                    if(entity1.houseNumber != null) {
//                        address +=  " " + entity1.houseNumber
//                        if (LOG_ME) ALog.d(TAG, "$methodName(): address == ${address}")
//                        if(entity1.apartmentNumber != null) {
//                            address += "/" + entity1.apartmentNumber
//                            if (LOG_ME) ALog.d(TAG, "$methodName(): address == ${address}")
//                        }
//                    }
//                    binding.addressLbl.text = address
//                }
//            } else {
//                binding.addressLbl.visibility = View.GONE
//                binding.addressTitle.visibility = View.GONE
//            }

        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }

    private fun actOnEntity1() {
        val methodName: String = "actOnEntity1()"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            val currentViewState = viewModel.getCurrentViewStateOrNew()
            if(currentViewState.user != null) {
                if(currentViewState.entity1 != null) {
                    navigateToEntity1ActionActivity(
                        this,
                        currentViewState.user!!,
                        currentViewState.entity1!!)
                } else {
                    ALog.e(TAG, ".$methodName(): currentViewState.entity1 != null")
                }
            } else {
                ALog.e(TAG, ".$methodName(): currentViewState.user != null")
            }
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }

    // Method opens a picture in a large window
    // https://github.com/chathuralakmal/AndroidImagePopup
    private fun showPicture1() {
        val methodName: String = "showPicture1"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            val entity1 = viewModel.getCurrentViewStateOrNew().entity1
            if(entity1 != null) {
                if(entity1.picture1URI != null) {
                    showEntity1Image(entity1.picture1URI!!)
                } else {
                    ALog.e(TAG, ".$methodName(): entity1.picture1URI != null")
                }
            } else {
                ALog.e(TAG, ".$methodName(): entity1 != null")
            }
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }

    // Method opens a picture in a large window
    // https://github.com/chathuralakmal/AndroidImagePopup
//    private fun showPicture2() {
//        val methodName: String = "showPicture2"
//        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
//        try {
//            val entity1 = viewModel.getCurrentViewStateOrNew().entity1
//            if(entity1 != null) {
//                if(entity1.picture2URI != null) {
//                    showEntity1Image(entity1.picture2URI!!)
//                } else {
//                    ALog.e(TAG, ".$methodName(): entity1.picture2URI != null")
//                }
//            } else {
//                ALog.e(TAG, ".$methodName(): entity1 != null")
//            }
//        } catch (e: Exception) {
//            ALog.e(TAG, methodName, e)
//        } finally {
//            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
//        }
//    }

    // Method opens a picture in a large window
    // https://github.com/chathuralakmal/AndroidImagePopup
//    private fun showPicture3() {
//        val methodName: String = "showPicture3"
//        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
//        try {
//            val entity1 = viewModel.getCurrentViewStateOrNew().entity1
//            if(entity1 != null) {
//                if(entity1.picture3URI != null) {
//                    showEntity1Image(entity1.picture3URI!!)
//                } else {
//                    ALog.e(TAG, ".$methodName(): entity1.picture3URI != null")
//                }
//            } else {
//                ALog.e(TAG, ".$methodName(): entity1 != null")
//            }
//        } catch (e: Exception) {
//            ALog.e(TAG, methodName, e)
//        } finally {
//            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
//        }
//    }

    // Method opens a picture in a large window
    // https://github.com/chathuralakmal/AndroidImagePopup
//    private fun showPicture4() {
//        val methodName: String = "showPicture4"
//        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
//        try {
//            val entity1 = viewModel.getCurrentViewStateOrNew().entity1
//            if(entity1 != null) {
//                if(entity1.picture4URI != null) {
//                    showEntity1Image(entity1.picture4URI!!)
//                } else {
//                    ALog.e(TAG, ".$methodName(): entity1.picture4URI != null")
//                }
//            } else {
//                ALog.e(TAG, ".$methodName(): entity1 != null")
//            }
//        } catch (e: Exception) {
//            ALog.e(TAG, methodName, e)
//        } finally {
//            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
//        }
//    }

    private fun showEntity1Image(entity1Image : String) {
        val methodName: String = "showEntity1Image"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            val entity1 = viewModel.getCurrentViewStateOrNew().entity1
            entity1.let {
                val storageReference = Firebase.storage
                val imageRef = storageReference.reference
                    .child(FIRESTORE_IMAGES_COLLECTION)
                    .child(FIRESTORE_ENTITY_1_IMAGES_SUB_COLLECTION)
                    .child(entity1!!.id!!.firestoreDocumentID)
                    .child(entity1Image)
                showPictureFromFirestore(imageRef)
            }
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }
}