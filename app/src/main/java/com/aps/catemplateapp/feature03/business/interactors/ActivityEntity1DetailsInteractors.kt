package com.aps.catemplateapp.feature03.business.interactors

import com.aps.catemplateapp.common.business.data.cache.StandardCacheDataSource
import com.aps.catemplateapp.common.business.data.network.StandardNetworkDataSource
import com.aps.catemplateapp.common.business.interactors.abstraction.DoNothingAtAll
import javax.inject.Inject

// Use cases
class ActivityEntity1DetailsInteractors<
        Entity,
        CacheDataSource: StandardCacheDataSource<Entity>,
        NetworkDataSource: StandardNetworkDataSource<Entity>,
        ViewState: com.aps.catemplateapp.common.business.domain.state.ViewState<Entity>>
@Inject
constructor(

    val doNothingAtAll: DoNothingAtAll<
            Entity,
            CacheDataSource,
            NetworkDataSource,
            ViewState
            >,
)