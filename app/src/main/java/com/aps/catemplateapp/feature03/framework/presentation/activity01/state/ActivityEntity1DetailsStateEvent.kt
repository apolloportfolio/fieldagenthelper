package com.aps.catemplateapp.feature03.framework.presentation.activity01.state

import android.content.Context
import androidx.test.core.app.ApplicationProvider
import com.aps.catemplateapp.R
import com.aps.catemplateapp.common.business.domain.state.StateEvent
import com.aps.catemplateapp.common.business.domain.state.StateMessage
import com.aps.catemplateapp.core.util.Entity1

sealed class ActivityEntity1DetailsStateEvent: StateEvent {

	class UpdateMainEntityEvent(
        val mainEntity: Entity1
    ): ActivityEntity1DetailsStateEvent(){

        override fun errorInfo(): String {
            return (ApplicationProvider.getApplicationContext() as Context).getString(R.string.activity_entity1_details_state_event_update_main_entity_error)
        }

        override fun eventName(): String {
            return "UpdateMainEntityEvent"
        }

        override fun shouldDisplayProgressBar() = true
    }

    object GetAllCacheEntities: ActivityEntity1DetailsStateEvent(){

        override fun errorInfo(): String {
            return (ApplicationProvider.getApplicationContext() as Context).getString(R.string.activity_entity1_details_state_event_get_all_cache_entities_error)
        }

        override fun eventName(): String {
            return "GetAllCacheEntities"
        }

        override fun shouldDisplayProgressBar() = true
    }

    object Entity1ActionEvent: ActivityEntity1DetailsStateEvent(){

        override fun errorInfo(): String {
            return (ApplicationProvider.getApplicationContext() as Context).getString(R.string.activity_entity1_details_state_event_act_on_entity1_error)
        }

        override fun eventName(): String {
            return "Entity1ActionEvent"
        }

        override fun shouldDisplayProgressBar() = true
    }

    class CreateStateMessageEvent(
        val stateMessage: StateMessage
    ): ActivityEntity1DetailsStateEvent(){

        override fun errorInfo(): String {
            return (ApplicationProvider.getApplicationContext() as Context).getString(R.string.activity_entity1_details_state_event_create_state_message_error)
        }

        override fun eventName(): String {
            return "CreateStateMessageEvent"
        }

        override fun shouldDisplayProgressBar() = false
    }

    object None: ActivityEntity1DetailsStateEvent(){

        override fun errorInfo(): String {
            return (ApplicationProvider.getApplicationContext() as Context).getString(R.string.activity_entity1_details_state_event_none_error)
        }

        override fun eventName(): String {
            return "None"
        }

        override fun shouldDisplayProgressBar() = false
    }
}
