package com.aps.catemplateapp.feature03.framework.presentation.activity01.state

import android.os.Parcelable
import com.aps.catemplateapp.common.business.domain.state.ViewState
import com.aps.catemplateapp.core.business.domain.model.entities.ProjectUser
import kotlinx.android.parcel.Parcelize
import com.aps.catemplateapp.core.util.Entity1

@Deprecated("Legacy class for views")
@Parcelize
data class ActivityEntity1DetailsViewState<T>(
    var user: ProjectUser? = null,
    var entity1: Entity1? = null,
    var userWantsToActOnEntity1: Boolean? = null,
) : Parcelable, ViewState<T>() {
    override fun copy(): ActivityEntity1DetailsViewState<T> {
        return ActivityEntity1DetailsViewState<T>().apply {
            // Copy properties from the ViewState superclass
            mainEntity = this@ActivityEntity1DetailsViewState.mainEntity
            listOfEntities =
                ArrayList(this@ActivityEntity1DetailsViewState.listOfEntities.orEmpty())
            isUpdatePending = this@ActivityEntity1DetailsViewState.isUpdatePending
            isInternetAvailable = this@ActivityEntity1DetailsViewState.isInternetAvailable
            isNetworkRepositoryAvailable =
                this@ActivityEntity1DetailsViewState.isNetworkRepositoryAvailable
            noCachedEntity = this@ActivityEntity1DetailsViewState.noCachedEntity
            snackBarState = this@ActivityEntity1DetailsViewState.snackBarState?.copy()
            toastState = this@ActivityEntity1DetailsViewState.toastState?.shallowCopy()
            dialogState = this@ActivityEntity1DetailsViewState.dialogState?.copy()

            user = this@ActivityEntity1DetailsViewState.user
            entity1 = this@ActivityEntity1DetailsViewState.entity1
            userWantsToActOnEntity1 = this@ActivityEntity1DetailsViewState.userWantsToActOnEntity1
        }
    }
}