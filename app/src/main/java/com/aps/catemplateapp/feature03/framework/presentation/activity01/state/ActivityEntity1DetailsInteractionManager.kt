package com.aps.catemplateapp.feature03.framework.presentation.activity01.state

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData

class ActivityEntity1DetailsInteractionManager {

    private val _collapsingToolbarState: MutableLiveData<CollapsingToolbarState>
            = MutableLiveData(CollapsingToolbarState.Expanded())

    val collapsingToolbarState: LiveData<CollapsingToolbarState>
        get() = _collapsingToolbarState


    fun setCollapsingToolbarState(state: CollapsingToolbarState){
        if(!state.toString().equals(_collapsingToolbarState.value.toString())){
            _collapsingToolbarState.value = state
        }
    }

    fun exitEditState(){
    }

    // return true if either of input fields are in EditState
    fun checkEditState() = false

}