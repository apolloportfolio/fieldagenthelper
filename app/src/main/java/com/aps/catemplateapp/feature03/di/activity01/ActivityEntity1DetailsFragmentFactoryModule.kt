package com.aps.catemplateapp.feature03.di.activity01

import androidx.lifecycle.ViewModelProvider
import com.aps.catemplateapp.common.util.DateUtil
import com.aps.catemplateapp.feature03.framework.presentation.activity01.ActivityEntity1DetailsFragmentFactory
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import javax.inject.Singleton

@FlowPreview
@ExperimentalCoroutinesApi
@Module
@InstallIn(SingletonComponent::class)
object ActivityEntity1DetailsFragmentFactoryModule {
    @JvmStatic
    @Singleton
    @Provides
    fun provideActivityEntity1DetailsFragmentFactory(
        viewModelFactory: ViewModelProvider.Factory,
        dateUtil: DateUtil
    ): ActivityEntity1DetailsFragmentFactory {
        return ActivityEntity1DetailsFragmentFactory(
            viewModelFactory,
            dateUtil
        )
    }
}