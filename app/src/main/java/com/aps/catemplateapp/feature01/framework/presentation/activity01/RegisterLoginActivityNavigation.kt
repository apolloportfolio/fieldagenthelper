package com.aps.catemplateapp.feature01.framework.presentation.activity01

import android.content.Intent
import android.os.Parcelable
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import com.aps.catemplateapp.common.framework.presentation.IntentExtras
import com.aps.catemplateapp.core.business.domain.model.entities.ProjectUser
import com.aps.catemplateapp.fah.framework.presentation.activity01.ActivityHomeScreen
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview

interface RegisterLoginActivityNavigation {

    @OptIn(FlowPreview::class)
    @ExperimentalCoroutinesApi
    fun navigateToProfileScreen(fragment: Fragment, projectUser: ProjectUser) {
        val intent = Intent(fragment.activity, ActivityHomeScreen::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        intent.putExtra(IntentExtras.APP_USER, projectUser as Parcelable)
        fragment.activity?.startActivity(intent)
        fragment.activity?.finish()
    }

    @FlowPreview
    @ExperimentalCoroutinesApi
    fun navigateToHomeScreen(fragment: Fragment, projectUser: ProjectUser) {
        val intent = Intent(fragment.activity, ActivityHomeScreen::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        intent.putExtra(IntentExtras.APP_USER, projectUser as Parcelable)
        fragment.activity?.startActivity(intent)
        fragment.activity?.finish()
    }

    @FlowPreview
    @ExperimentalCoroutinesApi
    fun closeApplication(fragment: Fragment) {
        fragment.activity?.finish()
    }

    @FlowPreview
    @ExperimentalCoroutinesApi
    fun closeApplicationAndOpenInternetSettings(fragment: Fragment) {
        fragment.requireContext().startActivity(
            Intent(android.provider.Settings.ACTION_WIRELESS_SETTINGS)
        )
        ActivityCompat.finishAffinity(fragment.requireActivity())
    }
}