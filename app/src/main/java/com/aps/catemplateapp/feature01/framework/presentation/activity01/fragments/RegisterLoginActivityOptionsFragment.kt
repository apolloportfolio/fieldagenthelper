package com.aps.catemplateapp.feature01.framework.presentation.activity01.fragments

import android.os.Bundle
import android.os.Parcel
import android.os.Parcelable
import android.view.View
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.aps.catemplateapp.R
import com.aps.catemplateapp.common.business.domain.state.StateEvent
import com.aps.catemplateapp.common.framework.presentation.BaseMVIFragment
import com.aps.catemplateapp.common.framework.presentation.BaseViewModel
import com.aps.catemplateapp.common.util.ALog
import com.aps.catemplateapp.common.util.DateUtil
import com.aps.catemplateapp.common.util.extensions.hideKeyboard
import com.aps.catemplateapp.core.business.domain.model.entities.ProjectUser
import com.aps.catemplateapp.databinding.FragmentActivityRegisterloginOptionsBinding
import com.aps.catemplateapp.feature01.framework.presentation.activity01.RegisterLoginActivityNavigation
import com.aps.catemplateapp.feature01.framework.presentation.activity01.RegisterLoginActivityViewModel
import com.aps.catemplateapp.feature01.framework.presentation.activity01.state.RegisterLoginActivityStateEvent
import com.aps.catemplateapp.feature01.framework.presentation.activity01.state.RegisterLoginActivityViewState
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.InternalCoroutinesApi

const val REGISTERLOGIN_OPTIONS_STATE_BUNDLE_KEY = "com.aps.cleanarchitecturetemplateapplication.feature01.framework.presentation.activity01.fragments.options"

private const val TAG = "RegisterLoginActivityOptionsFragment"
private const val LOG_ME = true

@FlowPreview
@ExperimentalCoroutinesApi
@AndroidEntryPoint
class RegisterLoginActivityOptionsFragment
constructor(
        private val viewModelFactory: ViewModelProvider.Factory,
        private val dateUtil: DateUtil
): BaseMVIFragment<ProjectUser, RegisterLoginActivityViewState<ProjectUser>>(R.layout.fragment_activity_registerlogin_options),
    RegisterLoginActivityNavigation, View.OnClickListener {

    private val viewModel: RegisterLoginActivityViewModel by viewModels {
        viewModelFactory
    }

    private lateinit var binding: FragmentActivityRegisterloginOptionsBinding

    constructor(parcel: Parcel) : this(
            TODO("viewModelFactory"),
            TODO("dateUtil")) {
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getViewModel().setupChannel()
    }

    @InternalCoroutinesApi
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        restoreInstanceState(savedInstanceState)
    }

    override fun onResume() {
        super.onResume()
    }

    override fun onPause() {
        super.onPause()
        //Save data from ui in ViewModel if necessary
    }

    override fun restoreInstanceState(savedInstanceState: Bundle?){
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
    }

    private fun setupOnBackPressDispatcher() {
        val callback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                onBackPressed()
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner, callback)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        view?.hideKeyboard()
        findNavController().popBackStack()
    }
    override fun setupUI(){
        view?.hideKeyboard()
    }

    override fun onDestroyView() {
        super.onDestroyView()
    }

    companion object CREATOR : Parcelable.Creator<RegisterLoginActivityOptionsFragment> {
        override fun createFromParcel(parcel: Parcel): RegisterLoginActivityOptionsFragment {
            return RegisterLoginActivityOptionsFragment(parcel)
        }

        override fun newArray(size: Int): Array<RegisterLoginActivityOptionsFragment?> {
            return arrayOfNulls(size)
        }
    }

    override fun setupBinding(view: View) {
        binding = FragmentActivityRegisterloginOptionsBinding.bind(view)
    }

    override fun subscribeCustomObservers() {
        //Unnecessary
    }

    override fun getViewModel(): BaseViewModel<ProjectUser, RegisterLoginActivityViewState<ProjectUser>> {
        return viewModel
    }

    override fun getInitStateEvent(): StateEvent {
        return RegisterLoginActivityStateEvent.None
    }

    override fun changeUiOrNavigateDependingOnViewState(viewState: RegisterLoginActivityViewState<ProjectUser>) {
        if(viewState.appProjectUser != null) {
            if(viewState.appProjectUser!!.fullyVerified) {
                this.navigateToHomeScreen(this, viewState.appProjectUser!!)
            } else {
                this.navigateToProfileScreen(this, viewState.appProjectUser!!)
            }
        }

        viewState.isInternetAvailable?.let {
            if(!it) {
                hideUI()
            } else {
                showUI()
            }
            viewModel.setIsInternetAvailable(null)
        }

        viewState.killAppAndOpenNetworkSettings?.let{
            if(it) {
                viewModel.setKillAppAndOpenNetworkSettings(null)
                closeApplicationAndOpenInternetSettings(this)
            }
        }
    }

    private fun hideUI() {
        binding.buttonGotoLogin.visibility = View.INVISIBLE
        binding.buttonGotoRegister.visibility = View.INVISIBLE
    }

    private fun showUI() {
        binding.buttonGotoLogin.visibility = View.VISIBLE
        binding.buttonGotoRegister.visibility = View.VISIBLE
    }

    private fun navigateToLoginFragment() {
        findNavController().navigate(
            R.id.action_RegisterLoginActivityOptionsFragment_to_RegisterLoginActivityLoginFragment
        )
    }

    private fun navigateToRegistrationFragment() {
        findNavController().navigate(
            R.id.action_RegisterLoginActivityOptionsFragment_to_RegisterLoginActivityRegistrationFragment
        )
    }

    override fun onEntityUpdateSuccess() {

    }

    override fun onEntityDeleteSuccess() {

    }

    override fun getStateBundleKey(): String? {
        return REGISTERLOGIN_OPTIONS_STATE_BUNDLE_KEY
    }

    override fun setAllOnClickAndOnTouchListeners() {
        binding.buttonGotoLogin.setOnClickListener(this)
        binding.buttonGotoRegister.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        super.onClick(v)
        val methodName: String = "onClick()"
        when(v) {
            binding.buttonGotoLogin -> {
                if (LOG_ME) ALog.d(TAG, "$methodName Navigating to login fragment")
                navigateToLoginFragment()
            }
            binding.buttonGotoRegister -> {
                if (LOG_ME) ALog.d(TAG, "$methodName Navigating to register fragment")
                navigateToRegistrationFragment()
            }
        }
    }

    override fun updateUIInViewModel() {
        //Nothing to set
    }

}