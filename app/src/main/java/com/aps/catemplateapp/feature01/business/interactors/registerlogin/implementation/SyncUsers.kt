package com.aps.catemplateapp.feature01.business.interactors.registerlogin.implementation

import com.aps.catemplateapp.common.business.data.cache.CacheResponseHandler
import com.aps.catemplateapp.common.business.data.network.ApiResponseHandler
import com.aps.catemplateapp.common.business.data.util.safeApiCall
import com.aps.catemplateapp.common.business.data.util.safeCacheCall
import com.aps.catemplateapp.common.business.domain.state.DataState
import com.aps.catemplateapp.common.util.ALog
import com.aps.catemplateapp.common.util.DateUtil
import com.aps.catemplateapp.core.business.data.cache.abstraction.UserCacheDataSource
import com.aps.catemplateapp.core.business.data.network.abstraction.UserNetworkDataSource
import com.aps.catemplateapp.core.business.domain.model.entities.ProjectUser
import com.aps.catemplateapp.feature01.business.interactors.registerlogin.abstraction.SyncUsers
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

private const val TAG = "SyncUsers"
private const val LOG_ME = true

class SyncUsersImpl @Inject constructor(
    private val cacheDataSource: UserCacheDataSource,
    private val networkDataSource: UserNetworkDataSource,
    private val dateUtil: DateUtil
): SyncUsers {

    override suspend fun syncEntities() {
        val cachedEntitiesList = getCachedEntities()
        val networkEntitiesList = getNetworkEntities()

        syncNetworkEntitiesWithCachedEntities(
            ArrayList(cachedEntitiesList),
            networkEntitiesList
        )
    }

    private suspend fun getCachedEntities(): List<ProjectUser> {
        val cacheResult = safeCacheCall(Dispatchers.IO){
            cacheDataSource.getAllEntities()
        }

        val response = object: CacheResponseHandler<List<ProjectUser>, List<ProjectUser>>(
            response = cacheResult,
            stateEvent = null
        ){
            override suspend fun handleSuccess(resultObj: List<ProjectUser>): DataState<List<ProjectUser>>? {
                return DataState.data(
                    response = null,
                    data = resultObj,
                    stateEvent = null
                )
            }

        }.getResult()

        return response?.data ?: ArrayList()
    }

    private suspend fun getNetworkEntities(): List<ProjectUser>{
        val networkResult = safeApiCall(Dispatchers.IO){
            networkDataSource.getAllEntities()
        }

        val response = object: ApiResponseHandler<List<ProjectUser>, List<ProjectUser>>(
            response = networkResult,
            stateEvent = null
        ){
            override suspend fun handleSuccess(resultObj: List<ProjectUser>): DataState<List<ProjectUser>>? {
                return DataState.data(
                    response = null,
                    data = resultObj,
                    stateEvent = null
                )
            }
        }.getResult()

        return response?.data ?: ArrayList()
    }

    // get all entities from network
    // if they do not exist in cache, insert them
    // if they do exist in cache, make sure they are up to date
    // while looping, remove entities from the cachedEntities list. If any remain, it means they
    // should be in the network but aren't. So insert them.
    private suspend fun syncNetworkEntitiesWithCachedEntities(
        cachedEntities: ArrayList<ProjectUser>,
        networkEntities: List<ProjectUser>
    ) = withContext(Dispatchers.IO){

        for(entity in networkEntities){
            cacheDataSource.getEntityById(entity.id)?.let { cachedEntity ->
                cachedEntities.remove(cachedEntity)
                checkIfCachedEntityRequiresUpdate(cachedEntity, entity)
            }?: cacheDataSource.insertEntity(entity)
        }
        // insert remaining into network
        for(cachedEntity in cachedEntities){
            networkDataSource.insertOrUpdateEntity(cachedEntity)
        }
    }

    private suspend fun checkIfCachedEntityRequiresUpdate(
        cachedEntity: ProjectUser,
        networkEntity: ProjectUser
    ){
        val cacheUpdatedAt = cachedEntity.updated_at
        val networkUpdatedAt = networkEntity.updated_at

        // update cache (network has newest data)
        if(dateUtil.convertStringDateToFirebaseTimestamp(networkUpdatedAt!!) > dateUtil.convertStringDateToFirebaseTimestamp(cacheUpdatedAt!!)){
            if(LOG_ME) ALog.d(TAG, ".checkIfCachedEntityRequiresUpdate(): " +
                    "cacheUpdatedAt: ${cacheUpdatedAt}, " +
                    "networkUpdatedAt: ${networkUpdatedAt}, " +
                    "id: ${cachedEntity.id}")
            safeCacheCall(Dispatchers.IO){
                cacheDataSource.updateEntity(
                    networkEntity.id,
                    networkEntity.updated_at,
                    networkEntity.created_at,
                    networkEntity.emailAddress,
                    networkEntity.password,
                    networkEntity.profilePhotoImageURI,
                    networkEntity.name,
                    networkEntity.surname,
                    networkEntity.description,
                    networkEntity.city,
                    networkEntity.emailAddressVerified,
                    networkEntity.phoneNumberVerified,
                )
            }
        }
        // update network (cache has newest data)
        else if(networkUpdatedAt < cacheUpdatedAt){
            safeApiCall(Dispatchers.IO){
                networkDataSource.insertOrUpdateEntity(cachedEntity)
            }
        }
    }
}
