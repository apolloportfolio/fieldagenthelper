package com.aps.catemplateapp.feature01.framework.presentation.activity01.state

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.aps.catemplateapp.common.util.ALog

private const val TAG = "RegisterLoginActivityInteractionManager"
private const val LOG_ME = true

class RegisterLoginActivityInteractionManager {
    private val _emailState: MutableLiveData<RegisterLoginActivityInteractionState>
            = MutableLiveData(RegisterLoginActivityInteractionState.DefaultState())

    private val _passwordState: MutableLiveData<RegisterLoginActivityInteractionState>
            = MutableLiveData(RegisterLoginActivityInteractionState.DefaultState())

    private val _passwordConfirmationState: MutableLiveData<RegisterLoginActivityInteractionState>
            = MutableLiveData(RegisterLoginActivityInteractionState.DefaultState())

    private val _collapsingToolbarState: MutableLiveData<CollapsingToolbarState>
            = MutableLiveData(CollapsingToolbarState.Expanded())


    val emailState: LiveData<RegisterLoginActivityInteractionState>
        get() = _emailState

    val passwordState: LiveData<RegisterLoginActivityInteractionState>
        get() = _passwordState

    val passwordConfirmationState: LiveData<RegisterLoginActivityInteractionState>
        get() = _passwordConfirmationState

    val collapsingToolbarState: LiveData<CollapsingToolbarState>
        get() = _collapsingToolbarState


    fun setCollapsingToolbarState(state: CollapsingToolbarState){
        if(!state.toString().equals(_collapsingToolbarState.value.toString())){
            _collapsingToolbarState.value = state
        }
    }

    fun setNewEmailState(state: RegisterLoginActivityInteractionState){
        if(LOG_ME) ALog.d(TAG, ".setNewEmailState(): $state")
        if(!emailState.toString().equals(state.toString())){
            _emailState.value = state
            when(state){
                is RegisterLoginActivityInteractionState.EditState -> {
                    _passwordState.value = RegisterLoginActivityInteractionState.DefaultState()
                    _passwordConfirmationState.value = RegisterLoginActivityInteractionState.DefaultState()
                } else -> {}
            }
        }
    }

    fun setNewPasswordState(state: RegisterLoginActivityInteractionState){
        if(LOG_ME) ALog.d(TAG, ".setNewPasswordState(): $state")
        if(!passwordState.toString().equals(state.toString())){
            _passwordState.value = state
            when(state){
                is RegisterLoginActivityInteractionState.EditState -> {
                    _emailState.value = RegisterLoginActivityInteractionState.DefaultState()
                    _passwordConfirmationState.value = RegisterLoginActivityInteractionState.DefaultState()
                } else -> {}
            }
        }
    }

    fun setNewPasswordConfirmationState(state: RegisterLoginActivityInteractionState){
        if(LOG_ME) ALog.d(TAG, ".setNewPasswordConfirmationState(): $state")
        if(!passwordConfirmationState.toString().equals(state.toString())){
            _passwordConfirmationState.value = state
            when(state){
                is RegisterLoginActivityInteractionState.EditState -> {
                    _emailState.value = RegisterLoginActivityInteractionState.DefaultState()
                    _passwordState.value = RegisterLoginActivityInteractionState.DefaultState()
                } else -> {}
            }
        }
    }

    fun isEditingEmail() = emailState.value.toString().equals(
        RegisterLoginActivityInteractionState.EditState().toString())

    fun isEditingPassword() = passwordState.value.toString().equals(
        RegisterLoginActivityInteractionState.EditState().toString())

    fun isEditingPasswordConfirmation() = passwordConfirmationState.value.toString().equals(
        RegisterLoginActivityInteractionState.EditState().toString())

    fun exitEditState(){
        _emailState.value = RegisterLoginActivityInteractionState.DefaultState()
        _passwordState.value = RegisterLoginActivityInteractionState.DefaultState()
        _passwordConfirmationState.value = RegisterLoginActivityInteractionState.DefaultState()
    }

    // return true if either email or password or passworConfirmation are in EditState
    fun checkEditState() =
                emailState.value.toString().equals(RegisterLoginActivityInteractionState.EditState().toString())
                || passwordState.value.toString().equals(RegisterLoginActivityInteractionState.EditState().toString())
                || passwordConfirmationState.value.toString().equals(RegisterLoginActivityInteractionState.EditState().toString())
}