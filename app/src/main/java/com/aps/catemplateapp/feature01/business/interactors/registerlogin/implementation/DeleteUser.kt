package com.aps.catemplateapp.feature01.business.interactors.registerlogin.implementation

import com.aps.catemplateapp.R
import com.aps.catemplateapp.common.business.data.cache.CacheResponseHandler
import com.aps.catemplateapp.common.business.data.util.safeApiCall
import com.aps.catemplateapp.common.business.data.util.safeCacheCall
import com.aps.catemplateapp.common.business.domain.state.*
import com.aps.catemplateapp.core.business.data.cache.abstraction.UserCacheDataSource
import com.aps.catemplateapp.core.business.data.network.abstraction.UserNetworkDataSource
import com.aps.catemplateapp.core.business.domain.model.entities.ProjectUser
import com.aps.catemplateapp.feature01.business.interactors.registerlogin.abstraction.DeleteUser
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class DeleteUserImpl<ViewState>
@Inject constructor(
    private val cacheDataSource: UserCacheDataSource,
    private val networkDataSource: UserNetworkDataSource
): DeleteUser<ViewState> {
    override fun deleteEntity(
        entity: ProjectUser,
        stateEvent: StateEvent
    ): Flow<DataState<ViewState>?> = flow {

        val cacheResult = safeCacheCall(Dispatchers.IO){
            cacheDataSource.deleteEntity(entity.id)
        }

        val response = object: CacheResponseHandler<ViewState, Int>(
            response = cacheResult,
            stateEvent = stateEvent
        ){
            override suspend fun handleSuccess(resultObj: Int): DataState<ViewState>? {
                return if(resultObj > 0){
                    DataState.data(
                        response = Response(
                            messageId = R.string.error,
                            message = DELETE_ENTITY_SUCCESS,
                            uiComponentType = UIComponentType.None(),
                            messageType = MessageType.Success()
                        ),
                        data = null,
                        stateEvent = stateEvent
                    )
                }
                else{
                    DataState.data(
                        response = Response(
                            messageId = R.string.error,
                            message = DELETE_ENTITY_FAILED,
                            uiComponentType = UIComponentType.Toast(),
                            messageType = MessageType.Error()
                        ),
                        data = null,
                        stateEvent = stateEvent
                    )
                }
            }
        }.getResult()

        emit(response)

        // update network
        if(response?.stateMessage?.response?.message.equals(DELETE_ENTITY_SUCCESS)){

            // delete from 'entities' node
            safeApiCall(Dispatchers.IO){
                networkDataSource.deleteEntity(entity.id)
            }

            // insert into 'deletes' node
            safeApiCall(Dispatchers.IO){
                networkDataSource.insertDeletedEntity(entity)
            }

        }
    }

    companion object{
        val DELETE_ENTITY_SUCCESS = "Successfully deleted entity."
        val DELETE_ENTITY_PENDING = "Delete pending..."
        val DELETE_ENTITY_FAILED = "Failed to delete entity."
        val DELETE_ARE_YOU_SURE = "Are you sure you want to delete this?"
    }
}