package com.aps.catemplateapp.feature01.business.interactors.registerlogin.implementation

import com.aps.catemplateapp.R
import com.aps.catemplateapp.common.business.data.cache.CacheResponseHandler
import com.aps.catemplateapp.common.business.data.util.safeCacheCall
import com.aps.catemplateapp.common.business.domain.state.*
import com.aps.catemplateapp.core.business.data.cache.abstraction.UserCacheDataSource
import com.aps.catemplateapp.core.business.domain.model.entities.ProjectUser
import com.aps.catemplateapp.feature01.business.interactors.registerlogin.abstraction.SearchCachedUsers
import com.aps.catemplateapp.feature01.framework.presentation.activity01.state.RegisterLoginActivityViewState
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject


class SearchCachedUsersImpl @Inject constructor(
    private val cacheDataSource: UserCacheDataSource
): SearchCachedUsers {

    override fun searchEntities(
        query: String,
        filterAndOrder: String,
        page: Int,
        stateEvent: StateEvent
    ): Flow<DataState<RegisterLoginActivityViewState<ProjectUser>>?> = flow {
        var updatedPage = page
        if(page <= 0){
            updatedPage = 1
        }
        val cacheResult = safeCacheCall(Dispatchers.IO){
            cacheDataSource.searchEntities(
                query = query,
                filterAndOrder = filterAndOrder,
                page = updatedPage
            )
        }

        val response = object: CacheResponseHandler<RegisterLoginActivityViewState<ProjectUser>, List<ProjectUser>>(
            response = cacheResult,
            stateEvent = stateEvent
        ){
            override suspend fun handleSuccess(resultObj: List<ProjectUser>): DataState<RegisterLoginActivityViewState<ProjectUser>>? {
                var message: String? =
                    SEARCH_ENTITIES_SUCCESS
                var uiComponentType: UIComponentType? = UIComponentType.None()
                if(resultObj.isEmpty()){
                    message =
                        SEARCH_ENTITIES_NO_MATCHING_RESULTS
                    uiComponentType = UIComponentType.Toast()
                }

                var data = RegisterLoginActivityViewState<ProjectUser>()
                data.listOfEntities = ArrayList(resultObj)

                return DataState.data(
                    response = Response(
                        messageId = R.string.error,
                        message = message,
                        uiComponentType = uiComponentType as UIComponentType,
                        messageType = MessageType.Success()
                    ),
                    data = data,
                    stateEvent = stateEvent
                )
            }
        }.getResult()

        emit(response)
    }

    companion object{
        val SEARCH_ENTITIES_SUCCESS = "Successfully retrieved list of entities."
        val SEARCH_ENTITIES_NO_MATCHING_RESULTS = "There are no entities that match that query."
        val SEARCH_ENTITIES_FAILED = "Failed to retrieve the list of entities."

    }
}