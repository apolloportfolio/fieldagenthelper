package com.aps.catemplateapp.feature01.di.activity01

import com.aps.catemplateapp.feature01.framework.presentation.activity01.RegisterLoginActivityFragmentFactory
import dagger.hilt.EntryPoint
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent

@EntryPoint
@InstallIn(ActivityComponent::class)
interface RegisterLoginActivityFragmentFactoryEntryPoint {
    fun getFragmentFactory(): RegisterLoginActivityFragmentFactory
}