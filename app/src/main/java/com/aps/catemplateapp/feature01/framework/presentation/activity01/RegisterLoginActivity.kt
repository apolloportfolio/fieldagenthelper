package com.aps.catemplateapp.feature01.framework.presentation.activity01

import android.os.Bundle
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import com.afollestad.materialdialogs.MaterialDialog
import com.aps.catemplateapp.common.business.domain.state.DialogInputCaptureCallback
import com.afollestad.materialdialogs.callbacks.onDismiss
import com.aps.catemplateapp.core.framework.presentation.ProjectActivity
import com.aps.catemplateapp.feature01.di.activity01.RegisterLoginActivityFragmentFactoryEntryPoint
import com.aps.catemplateapp.R
import com.aps.catemplateapp.databinding.ActivityRegisterloginBinding
import dagger.hilt.android.AndroidEntryPoint
import dagger.hilt.android.EntryPointAccessors
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import javax.inject.Inject

private const val TAG = "RegisterLoginActivity"
private const val LOG_ME = true

@ExperimentalCoroutinesApi
@AndroidEntryPoint
@FlowPreview
class RegisterLoginActivity : ProjectActivity() {
    @Inject lateinit var fragmentFactory: RegisterLoginActivityFragmentFactory
    override val getContentView: Int
        get() = R.layout.activity_registerlogin
    override val mainFragmentId: Int
        get() = R.id.nav_host_fragment

    private lateinit var binding: ActivityRegisterloginBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        setFragmentFactory()
        super.onCreate(savedInstanceState)
        setContentView(getContentView)
    }

    private fun setFragmentFactory(){
        val entryPoint = EntryPointAccessors.fromActivity(
            this,
            RegisterLoginActivityFragmentFactoryEntryPoint::class.java
        )

        fragmentFactory = entryPoint.getFragmentFactory()
        supportFragmentManager.fragmentFactory = fragmentFactory
    }

    override fun onSupportNavigateUp(): Boolean {
        return findNavController(R.id.nav_host_fragment)
                .navigateUp(appBarConfiguration as AppBarConfiguration)
                || super.onSupportNavigateUp()
    }

    override fun displayInputCaptureDialog(
            title: String,
            callback: DialogInputCaptureCallback
    ) {
        dialogInView = MaterialDialog(this).show {
            title(text = title)
            positiveButton(R.string.text_ok)
            onDismiss {
                dialogInView = null
            }
            cancelable(true)
        }
    }
}