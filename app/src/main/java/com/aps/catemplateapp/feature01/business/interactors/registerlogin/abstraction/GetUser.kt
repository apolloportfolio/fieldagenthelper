package com.aps.catemplateapp.feature01.business.interactors.registerlogin.abstraction

import com.aps.catemplateapp.common.business.domain.state.DataState
import com.aps.catemplateapp.common.business.domain.state.StateEvent
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.core.business.domain.model.entities.ProjectUser
import com.aps.catemplateapp.feature01.framework.presentation.activity01.state.RegisterLoginActivityViewState
import kotlinx.coroutines.flow.Flow

interface GetUser {
    fun getEntity(id: UniqueID, stateEvent: StateEvent): Flow<DataState<RegisterLoginActivityViewState<ProjectUser>>?>
}