package com.aps.catemplateapp.feature01.framework.presentation.activity01.fragments

import android.os.Bundle
import android.os.Parcel
import android.os.Parcelable
import android.view.View
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.aps.catemplateapp.R
import com.aps.catemplateapp.common.business.domain.state.StateEvent
import com.aps.catemplateapp.common.framework.presentation.BaseMVIFragment
import com.aps.catemplateapp.common.framework.presentation.BaseViewModel
import com.aps.catemplateapp.common.util.ALog
import com.aps.catemplateapp.common.util.DateUtil
import com.aps.catemplateapp.common.util.extensions.hideKeyboard
import com.aps.catemplateapp.core.business.domain.model.entities.ProjectUser
import com.aps.catemplateapp.databinding.FragmentActivityRegisterloginMainBinding
import com.aps.catemplateapp.feature01.framework.presentation.activity01.RegisterLoginActivityNavigation
import com.aps.catemplateapp.feature01.framework.presentation.activity01.RegisterLoginActivityViewModel
import com.aps.catemplateapp.feature01.framework.presentation.activity01.state.RegisterLoginActivityStateEvent
import com.aps.catemplateapp.feature01.framework.presentation.activity01.state.RegisterLoginActivityViewState
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.InternalCoroutinesApi

const val REGISTERLOGIN_MAIN_STATE_BUNDLE_KEY = "com.aps.cleanarchitecturetemplateapplication.feature01.framework.presentation.activity01.fragments.main"

private const val TAG = "RegisterLoginActivityMainFragment"
private const val LOG_ME = true
@FlowPreview
@ExperimentalCoroutinesApi
@AndroidEntryPoint
class RegisterLoginActivityMainFragment
constructor(
        private val viewModelFactory: ViewModelProvider.Factory,
        private val dateUtil: DateUtil
): BaseMVIFragment<ProjectUser, RegisterLoginActivityViewState<ProjectUser>>(layoutRes = R.layout.fragment_activity_registerlogin_main),
    RegisterLoginActivityNavigation {

    private val viewModel: RegisterLoginActivityViewModel by viewModels {
        viewModelFactory
    }
    private lateinit var binding: FragmentActivityRegisterloginMainBinding

    constructor(parcel: Parcel) : this(
            TODO("viewModelFactory"),
            TODO("dateUtil")) {
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    @InternalCoroutinesApi
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        if(LOG_ME) ALog.d(TAG, "onViewCreated(): Method start")
        super.onViewCreated(view, savedInstanceState)
        setupOnBackPressDispatcher()
        if(LOG_ME)ALog.d(TAG, "onViewCreated(): Method end")
    }

    override fun onResume() {
        super.onResume()
        if(LOG_ME)ALog.d(TAG, ".onResume(): " +
                                "Fragment is in view so we are resetting navigastedAway flag to false.")
        viewModel.setNavigatedAway(false)
    }

    override fun onPause() {
        super.onPause()
    }

    override fun restoreInstanceState(savedInstanceState: Bundle?){
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
    }

    private fun setupOnBackPressDispatcher() {
        val callback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                onBackPressed()
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner, callback)
    }

    override fun onBackPressed() {
        view?.hideKeyboard()
        closeApplication(this)
    }
    override fun setupUI(){
        view?.hideKeyboard()
    }

    override fun onDestroyView() {
        super.onDestroyView()
    }

    companion object CREATOR : Parcelable.Creator<RegisterLoginActivityMainFragment> {
        override fun createFromParcel(parcel: Parcel): RegisterLoginActivityMainFragment {
            return RegisterLoginActivityMainFragment(parcel)
        }

        override fun newArray(size: Int): Array<RegisterLoginActivityMainFragment?> {
            return arrayOfNulls(size)
        }
    }

    override fun setupBinding(view: View) {
        binding = FragmentActivityRegisterloginMainBinding.bind(view)
    }

    override fun subscribeCustomObservers() {

    }

    override fun getViewModel(): BaseViewModel<ProjectUser, RegisterLoginActivityViewState<ProjectUser>> {
        return viewModel
    }

    override fun getInitStateEvent(): StateEvent {
        return RegisterLoginActivityStateEvent.CheckDeviceCompatibility
    }

    override fun changeUiOrNavigateDependingOnViewState(viewState: RegisterLoginActivityViewState<ProjectUser>) {
        val methodName: String = "changeUiOrNavigateDependingOnViewState()"
        if(LOG_ME) ALog.d(TAG, ".$methodName: Method start")

        viewState.deviceIsCompatible?.let {
            if(!it) {
                ALog.w(TAG, ".$methodName(): Device is incompatible.")
                viewModel.setDeviceIsCompatible(null)
                requireActivity().finish()
            }
        }

        if(viewState.firebaseUserIsLoggedIn != null) {
            if(LOG_ME) ALog.d(TAG, ".$methodName: viewState.firebaseUserIsLoggedIn != null")
            if(viewState.firebaseUserIsLoggedIn!!) {
                if(LOG_ME) ALog.d(TAG, ".$methodName: " +
                        "viewState.firebaseUserIsLoggedIn == true")
                if(viewState.appProjectUser!!.fullyVerified) {
                    if(LOG_ME) ALog.d(TAG, ".$methodName: " +
                            "Navigating to home screen because firebaseUserIsLoggedIn and " +
                            "appUser is fully verified")
                    this.navigateToHomeScreen(this, viewState.appProjectUser!!)
                } else {
                    if(LOG_ME) ALog.d(TAG, ".$methodName: " +
                            "Navigating to profile screen because firebaseUserIsLoggedIn and " +
                            "appUser is not fully verified")
                    this.navigateToProfileScreen(this, viewState.appProjectUser!!)
                }
            } else {
                if(LOG_ME) ALog.d(TAG, ".$methodName: " +
                        "Navigating to options fragment because " +
                        "firebaseUserIsLoggedIn == false or null")
                this.navigateToOptionsFragment()
            }
        } else if(viewState.noCachedEntity == true){
            if(LOG_ME) ALog.d(TAG, ".$methodName: " +
                    "Navigating to options fragment because firebaseUserIsLoggedIn == null and " +
                    "noCachedEntity == true")
            this.navigateToOptionsFragment()
        }

        viewState.isInternetAvailable?.let {
            if(!it) {
                hideUI()
            } else {
                showUI()
            }
            viewModel.setIsInternetAvailable(null)
        }

        viewState.killAppAndOpenNetworkSettings?.let{
            if(it) {
                viewModel.setKillAppAndOpenNetworkSettings(null)
                closeApplicationAndOpenInternetSettings(this)
            }
        }

        if(LOG_ME) ALog.d(TAG, "$methodName(): Method end")
    }

    private fun hideUI() {
        // Nothing to hide
    }

    private fun showUI() {
        // Nothing to show
    }

    private fun navigateToOptionsFragment() {
        if(!viewModel.getCurrentViewStateOrNew().navigatedAway) {
            findNavController().navigate(
                R.id.action_RegisterLoginActivityMainFragment_to_RegisterLoginActivityOptionsFragment
            )
            viewModel.setNavigatedAway(true)
        } else {
            if(LOG_ME)ALog.d(TAG, ".navigateToOptionsFragment(): " +
                                    "Already navigated away from this fragment.")
        }
    }

    override fun onEntityUpdateSuccess() {

    }

    override fun onEntityDeleteSuccess() {
        this.navigateToOptionsFragment()
    }

    override fun getStateBundleKey(): String? {
        return REGISTERLOGIN_MAIN_STATE_BUNDLE_KEY
    }

    override fun setAllOnClickAndOnTouchListeners() {
        //Nothing to set
    }

    override fun updateUIInViewModel() {
        //Nothing to set
    }

}