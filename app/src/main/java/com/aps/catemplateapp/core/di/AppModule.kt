package com.aps.catemplateapp.core.di

import android.content.SharedPreferences
import com.aps.catemplateapp.common.util.DateUtil
import com.aps.catemplateapp.core.util.SecureKeyStorage
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Qualifier
import javax.inject.Singleton

// https://stackoverflow.com/questions/68587320/how-to-generate-objects-with-the-same-type-in-hilt
@Qualifier
@Retention(AnnotationRetention.BINARY)
annotation class SimpleDateFormatForFirestore

@Qualifier
@Retention(AnnotationRetention.BINARY)
annotation class SimpleDateFormatForUser

@Qualifier
@Retention(AnnotationRetention.BINARY)
annotation class SimpleDateFormatJustYear


@ExperimentalCoroutinesApi
@FlowPreview
@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Singleton
    @Provides
    fun provideSecureKeyStorage() : SecureKeyStorage {
        return SecureKeyStorage()
    }

    @Singleton
    @Provides
    fun provideGsonBuilder() : Gson {
        return GsonBuilder()
            .excludeFieldsWithoutExposeAnnotation()
            .create()
    }

    // https://developer.android.com/reference/java/text/SimpleDateFormat.html?hl=pt-br
    @JvmStatic
    @Singleton
    @Provides
    @SimpleDateFormatForFirestore
    fun provideDateFormatForFirestore(): SimpleDateFormat {
        val sdf = SimpleDateFormat("yyyy-MM-dd hh:mm:ss a", Locale.ENGLISH)
        sdf.timeZone = TimeZone.getTimeZone("UTC-7") // match firestore
        return sdf
    }

    @JvmStatic
    @Singleton
    @Provides
    @SimpleDateFormatForUser
    fun provideDateFormatForUser(): SimpleDateFormat {
        val sdf = SimpleDateFormat("dd.MM.yyyy", Locale.ENGLISH)
        sdf.timeZone = TimeZone.getTimeZone("UTC-7") // match firestore
        return sdf
    }

    @JvmStatic
    @Singleton
    @Provides
    @SimpleDateFormatJustYear
    fun provideDateFormatJustYear(): SimpleDateFormat {
        val sdf = SimpleDateFormat("yyyy", Locale.ENGLISH)
        sdf.timeZone = TimeZone.getTimeZone("UTC-7") // match firestore
        return sdf
    }

    @JvmStatic
    @Singleton
    @Provides
    fun provideDateUtil(
        @SimpleDateFormatForFirestore dateFormatForFirestore: SimpleDateFormat,
        @SimpleDateFormatForUser dateFormatForUser: SimpleDateFormat,
        @SimpleDateFormatJustYear dateFormatJustYear: SimpleDateFormat,
    ): DateUtil {
        return DateUtil(
            dateFormatForFirestore,
            dateFormatForUser,
            dateFormatJustYear,
        )
    }

    @JvmStatic
    @Singleton
    @Provides
    fun provideSharedPrefsEditor(
        sharedPreferences: SharedPreferences
    ): SharedPreferences.Editor {
        return sharedPreferences.edit()
    }

}