package com.aps.catemplateapp.core.business.data.cache.abstraction

import com.aps.catemplateapp.common.business.data.cache.StandardCacheDataSource
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.core.business.domain.model.entities.ProjectUser

interface UserCacheDataSource: StandardCacheDataSource<ProjectUser> {
    override suspend fun insertOrUpdateEntity(entity: ProjectUser): ProjectUser?

    override suspend fun insertEntity(entity: ProjectUser): Long

    override suspend fun deleteEntity(primaryKey: UniqueID?): Int

    suspend fun deleteAllEntities()

    override suspend fun deleteEntities(entities: List<ProjectUser>): Int

    suspend fun updateEntity(
        id: UserUniqueID?,
        updated_at: String?,
        created_at: String?,
        emailAddress: String,
        password: String,
        profilePhotoImageURI: String?,
        name: String?,
        surname: String?,
        description: String?,
        city: String?,
        emailAddressVerified: Boolean = false,
        phoneNumberVerified: Boolean = false,
    ): Int
    
    suspend fun updateUser(projectUser: ProjectUser) {
        updateEntity(
            projectUser.id,
            projectUser.updated_at,
            projectUser.created_at,
            projectUser.emailAddress,
            projectUser.password,
            projectUser.profilePhotoImageURI,
            projectUser.name,
            projectUser.surname,
            projectUser.description,
            projectUser.city,
            projectUser.emailAddressVerified,
            projectUser.phoneNumberVerified,
        )
    }

    suspend fun updateUsersEmail(
        id: UniqueID?,
        updated_at: String?,
        emailAddress: String?,
    ): Int

    override suspend fun searchEntities(
        query: String,
        filterAndOrder: String,
        page: Int
    ): List<ProjectUser>

    override suspend fun getAllEntities(): List<ProjectUser>

    override suspend fun getEntityById(id: UniqueID?): ProjectUser?

    override suspend fun getNumEntities(): Int

    override suspend fun insertEntities(entities: List<ProjectUser>): LongArray
}