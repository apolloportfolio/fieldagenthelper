package com.aps.catemplateapp.core

import android.content.Context
import androidx.test.core.app.ApplicationProvider
import androidx.test.platform.app.InstrumentationRegistry
import com.aps.catemplateapp.common.BaseApplication
import dagger.hilt.android.HiltAndroidApp
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import java.lang.IllegalStateException

private const val TAG = "ProjectApplication"
private const val LOG_ME = true

@HiltAndroidApp
@FlowPreview
@ExperimentalCoroutinesApi
//@InstallIn(SingletonComponent::class)
//@EntryPoint
open class ProjectApplication : BaseApplication() {
    init {
        instance = this
    }

    companion object {
        private var instance: ProjectApplication? = null

        fun applicationContext(tag: String?): Context {
            return try {
                (ApplicationProvider.getApplicationContext() as Context)
            } catch (e : IllegalStateException) {
                InstrumentationRegistry.getInstrumentation().targetContext
            }
        }
    }

    override fun onCreate() {
        super.onCreate()
        val context: Context = applicationContext(TAG)
    }

}