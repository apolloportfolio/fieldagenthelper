package com.aps.catemplateapp.core

import android.content.SharedPreferences
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.test.core.app.ApplicationProvider.getApplicationContext
import com.aps.catemplateapp.common.framework.presentation.ApplicationViewModel
import com.aps.catemplateapp.common.util.DateUtil
import com.aps.catemplateapp.core.business.data.cache.implementation.UserCacheDataSourceImpl
import com.aps.catemplateapp.core.business.data.network.abstraction.UserNetworkDataSource
import com.aps.catemplateapp.core.business.domain.model.entities.ProjectUser
import com.aps.catemplateapp.core.business.domain.model.factories.UserFactory
import com.aps.catemplateapp.feature01.business.interactors.registerlogin.RegisterLoginInteractors
import com.aps.catemplateapp.feature01.framework.presentation.activity01.RegisterLoginActivityViewModel
import com.aps.catemplateapp.feature01.framework.presentation.activity01.state.RegisterLoginActivityViewState
import com.aps.catemplateapp.feature03.business.interactors.ActivityEntity1DetailsInteractors
import com.aps.catemplateapp.feature03.framework.presentation.activity01.ActivityEntity1DetailsViewModel
import com.aps.catemplateapp.feature03.framework.presentation.activity01.state.ActivityEntity1DetailsViewState
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import com.aps.catemplateapp.core.util.Entity1
import com.aps.catemplateapp.core.util.Entity1CacheDataSourceImpl
import com.aps.catemplateapp.core.util.Entity1Factory
import com.aps.catemplateapp.core.util.Entity1NetworkDataSource


@FlowPreview
@ExperimentalCoroutinesApi
class ProjectViewModelFactory
constructor(
    private val registerLoginInteractors: RegisterLoginInteractors<
            ProjectUser,
            UserCacheDataSourceImpl,
            UserNetworkDataSource,
            RegisterLoginActivityViewState<ProjectUser>
            >,
    private val activityEntityDetailsInteractors: ActivityEntity1DetailsInteractors<
            Entity1,
            Entity1CacheDataSourceImpl,
            Entity1NetworkDataSource,
            ActivityEntity1DetailsViewState<Entity1>
            >,
    private val dateUtil: DateUtil,
    private val userFactory: UserFactory,
    private val entity1Factory: Entity1Factory,
    private val editor: SharedPreferences.Editor,
    //private val sharedPreferences: SharedPreferences
) : ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return when(modelClass){

            RegisterLoginActivityViewModel::class.java -> {
                RegisterLoginActivityViewModel(
                    interactors = registerLoginInteractors,
                    dateUtil = dateUtil,
                    entityFactory = userFactory
                ) as T
            }

            ActivityEntity1DetailsViewModel::class.java -> {
                ActivityEntity1DetailsViewModel(
                    interactors = activityEntityDetailsInteractors,
                    dateUtil = dateUtil,
                    entityFactory = entity1Factory
                ) as T
            }




            ApplicationViewModel::class.java -> {
                ApplicationViewModel(
                    application = getApplicationContext()
                ) as T
            }

            else -> {
                throw IllegalArgumentException("unknown model class $modelClass")
            }
        }
    }
}