package com.aps.catemplateapp.core.framework.datasources.cachedatasources.room

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.aps.catemplateapp.core.framework.datasources.cachedatasources.room.daos.*
import com.aps.catemplateapp.core.framework.datasources.cachedatasources.room.model.*
import com.aps.catemplateapp.fah.framework.datasources.cachedatasources.room.daos.ClientContactDao
import com.aps.catemplateapp.fah.framework.datasources.cachedatasources.room.daos.FieldSurveyDao
import com.aps.catemplateapp.fah.framework.datasources.cachedatasources.room.daos.SurveyQuestionDao
import com.aps.catemplateapp.fah.framework.datasources.cachedatasources.room.daos.UsersPresentationDao
import com.aps.catemplateapp.fah.framework.datasources.cachedatasources.room.model.ClientContactCacheEntity
import com.aps.catemplateapp.fah.framework.datasources.cachedatasources.room.model.FieldSurveyCacheEntity
import com.aps.catemplateapp.fah.framework.datasources.cachedatasources.room.model.SurveyQuestionCacheEntity
import com.aps.catemplateapp.fah.framework.datasources.cachedatasources.room.model.UsersPresentationCacheEntity

@Database(entities = [
    UserCacheEntity::class,

    FieldSurveyCacheEntity::class,
    UsersPresentationCacheEntity::class,
    SurveyQuestionCacheEntity::class,
    ClientContactCacheEntity::class,
], version = 1, exportSchema = false)
@TypeConverters(RoomTypeConverters::class)
abstract class ProjectRoomDatabase : RoomDatabase() {
    abstract fun userDao() : UserDao

    // Field Agent Helper
    abstract fun fieldSurveyDao() : FieldSurveyDao

    abstract fun usersPresentationDao() : UsersPresentationDao

    abstract fun surveyQuestionDao() : SurveyQuestionDao

    abstract fun clientContactDao() : ClientContactDao


    companion object {
        const val DATABASE_NAME: String = "APSDB"
    }
}