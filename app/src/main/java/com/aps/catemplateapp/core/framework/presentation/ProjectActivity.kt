package com.aps.catemplateapp.core.framework.presentation

import android.os.Bundle
import com.aps.catemplateapp.common.framework.presentation.*

private const val TAG = "ProjectActivity"
private const val LOG_ME = true

abstract class ProjectActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onStart() {
        super.onStart()

    }

    override fun onResume() {
        super.onResume()

    }

    override fun onPause() {
        super.onPause()

    }

    override fun onStop() {
        super.onStop()

    }

    override fun onRestart() {
        super.onRestart()

    }

    override fun onDestroy() {
        super.onDestroy()

    }
}