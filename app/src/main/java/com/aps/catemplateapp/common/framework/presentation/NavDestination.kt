package com.aps.catemplateapp.common.framework.presentation

interface NavDestination {
    val route: String
}