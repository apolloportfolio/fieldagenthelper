package com.aps.catemplateapp.common.business.interactors.abstraction

import com.aps.catemplateapp.common.business.data.cache.StandardCacheDataSource
import com.aps.catemplateapp.common.business.data.network.StandardNetworkDataSource
import com.aps.catemplateapp.common.business.domain.state.DataState
import com.aps.catemplateapp.common.business.domain.state.StateEvent
import kotlinx.coroutines.flow.Flow

interface UpdateEntity<
        Entity,
        CacheDataSource: StandardCacheDataSource<Entity>,
        NetworkDataSource: StandardNetworkDataSource<Entity>,
        ViewState: com.aps.catemplateapp.common.business.domain.state.ViewState<Entity>> {
    fun updateEntity(
        entity: Entity,
        stateEvent: StateEvent,
        returnViewState: ViewState,
        updateReturnViewState: (ViewState, Entity?)->ViewState,
    ): Flow<DataState<ViewState>?>
}