package com.aps.catemplateapp.common.framework.presentation

import android.app.Activity
import android.app.ActivityManager
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.res.Resources
import android.graphics.BitmapFactory
import android.net.*
import android.os.Bundle
import android.provider.Settings
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.ImageView
import androidx.activity.OnBackPressedCallback
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.aps.catemplateapp.R
import com.aps.catemplateapp.common.business.domain.state.StateEvent
import com.aps.catemplateapp.common.business.domain.state.StateMessageCallback
import com.aps.catemplateapp.common.business.domain.state.ViewState
import com.aps.catemplateapp.common.util.ALog
import com.aps.catemplateapp.common.util.GetFilePathFromDevice.getPath
import com.aps.catemplateapp.common.util.ObjectSizeCalculator
import com.aps.catemplateapp.common.util.TodoCallback
import com.aps.catemplateapp.common.util.extensions.showToast
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.InternalCoroutinesApi
import java.util.*

private const val TAG = "BaseMVIFragment"
private const val LOG_ME = true

abstract class BaseMVIFragment<EntityType, T : ViewState<EntityType>>
constructor(
    @LayoutRes private val layoutRes: Int
): BaseFragment(layoutRes), View.OnClickListener {

    var progressBarBlocksClicking = false
    var clicksAreDisabledByProgressBar = false

    abstract fun setupUI()
    abstract fun subscribeCustomObservers()
    abstract fun restoreInstanceState(savedInstanceState: Bundle?)
    abstract fun getViewModel() : BaseViewModel<EntityType, T>
    abstract fun getInitStateEvent() : StateEvent
    abstract fun changeUiOrNavigateDependingOnViewState(viewState: T)
    abstract fun onEntityUpdateSuccess()
    abstract fun onEntityDeleteSuccess()
    abstract fun getStateBundleKey(): String?
    abstract fun setAllOnClickAndOnTouchListeners()
    abstract fun setupBinding(view: View)
    abstract fun updateUIInViewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getViewModel().setupChannel()
    }

    @InternalCoroutinesApi
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val methodName: String = "onViewCreated"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            super.onViewCreated(view, savedInstanceState)

            setupBinding(view)
            setupUI()
            setupOnBackPressDispatcher()
            subscribeObservers()
            setAllOnClickAndOnTouchListeners()

            restoreInstanceState(savedInstanceState)

        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }

    private fun registerNetworkCallback() {
        val methodName = "registerNetworkCallback"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            val connectivityManager: ConnectivityManager = requireContext().getSystemService(
                Context.CONNECTIVITY_SERVICE
            ) as ConnectivityManager
            val builder: NetworkRequest.Builder = NetworkRequest.Builder()

            connectivityManager.registerNetworkCallback(
                builder.build(),
                object : ConnectivityManager.NetworkCallback() {

                    override fun onAvailable(network: Network) {
                        val networkCapabilities = connectivityManager.getNetworkCapabilities(network)
                        var cellularAvailable = false
                        var wiFiAvailable = false
                        var ethernetAvailable = false
                        if(networkCapabilities != null) {
                            if(networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)) {
                                if(LOG_ME)ALog.d(TAG, ".registerNetworkCallback().onAvailable(): TRANSPORT_CELLULAR is available")
                                cellularAvailable = true
                            }
                            if(networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) {
                                if(LOG_ME)ALog.d(TAG, ".registerNetworkCallback().onAvailable(): TRANSPORT_WIFI is available")
                                wiFiAvailable = true
                            }
                            if(networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET)) {
                                if(LOG_ME)ALog.d(TAG, ".registerNetworkCallback().onAvailable(): TRANSPORT_ETHERNET is available")
                                ethernetAvailable = true
                            }
                        }

                        if(cellularAvailable || wiFiAvailable || ethernetAvailable) {
                            getViewModel().postIsInternetAvailable(true)
                        } else {
                            if(LOG_ME)ALog.d(
                                TAG, ".registerNetworkCallback().onAvailable(): " +
                                    "No internet connection is actually available."
                            )
                            getViewModel().postIsInternetAvailable(false)
                        }
                    }

                    override fun onLost(network: Network) {
                        if(LOG_ME)ALog.d(TAG, ".registerNetworkCallback().onLost(): Network lost.")
                        getViewModel().postIsInternetAvailable(false)
                    }

                    override fun onUnavailable() {
                        super.onUnavailable()
                        if(LOG_ME)ALog.d(TAG, ".registerNetworkCallback().onUnavailable(): Network is unavailable.")
                        getViewModel().postIsInternetAvailable(false)
                    }

                    override fun onLosing(network: Network, maxMsToLive: Int) {
                        super.onLosing(network, maxMsToLive)
                        if(LOG_ME)ALog.d(TAG, ".registerNetworkCallback().onLosing(): Loosing network connection.")
                    }
                }
            )
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }

    override fun onResume() {
        super.onResume()
        if(LOG_ME)logBackStack()
        //Set state to ViewModel to get data from repository
        getViewModel().setStateEvent(getInitStateEvent())
        updateUIInViewModel()

        registerNetworkCallback()

        getViewModel().setIsInternetAvailable(this.isNetworkAvailable())
    }

    override fun onPause() {
        super.onPause()
        updateUIInViewModel()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        updateUIInViewModel()
    }

    open fun onBackPressed() {
        updateUIInViewModel()
    }

    fun blockClicksIfProgressBarCommandsIt() : Boolean? {
        if(clicksAreDisabledByProgressBar){
            if(LOG_ME)ALog.d(TAG, ".onClick(): clicksAreDisabledByProgressBar == true")
            (requireActivity() as BaseActivity).showToast(R.string.please_wait)
            return true
        }
        return null
    }

    override fun onClick(v: View?) {
    }

    override fun onSaveInstanceState(outState: Bundle) {
        if (LOG_ME) ALog.d(TAG, "Method start: onSaveInstanceState()")
        try {
            val viewState = getViewModel().getCurrentViewStateOrNew()

//            if(LOG_ME)logStateSize(viewState, outState)

            outState.putParcelable(getStateBundleKey(), viewState)
        } catch (e: Exception) {
            ALog.e(TAG, "onSaveInstanceState()", e)
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: onSaveInstanceState()")
        }

        super.onSaveInstanceState(outState)
    }

    fun logStateSize(viewState: T, outState: Bundle) {
        try {
            val outStateSize = ObjectSizeCalculator.getBundleSizeInBytes(outState)
            ALog.d(
                TAG, ".logStateSize(): " +
                    "outStateSize == $outStateSize Bytes")
        } catch (e: Exception) {
            ALog.e(
                TAG, "logStateSize(): " +
                    "Exception occurred while calculating outState's size.", e)
        }
        try {
            val viewStateSize = ObjectSizeCalculator.getBundleSizeInBytes(viewState)
            ALog.d(
                TAG, ".logStateSize(): " +
                    "viewStateSize == $viewStateSize Bytes\n" +
                    "viewState == $viewState")
        } catch (e: Exception) {
            ALog.e(
                TAG, "logStateSize(): " +
                    "Exception occurred while calculating viewState's size.", e)
        }
    }

    private fun setupOnBackPressDispatcher() {
        val callback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                onBackPressed()
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner, callback)
    }

    private fun observeViewState(baseMVIFragmentViewState: T) {
        val methodName: String = "observeViewState()"
        if(LOG_ME) ALog.d(TAG, "$methodName: Observing ViewModelStart.viewState")
        if (baseMVIFragmentViewState != null) {
            if(LOG_ME) ALog.d(
                TAG, "$methodName: " +
                    "baseMVIFragmentViewState.isInternetAvailable == ${baseMVIFragmentViewState.isInternetAvailable}")
            baseMVIFragmentViewState.isInternetAvailable?.let {
                if (!it) {
                    displaySnackBarNoInternetConnection()
                } else {
                    if (!baseMVIFragmentViewState.isNetworkRepositoryAvailable)
                        displaySnackBarNoServerConnection()
                }
            }

            changeUiOrNavigateDependingOnViewState(baseMVIFragmentViewState)
        }
    }

    fun subscribeStateMessageObserver() {
        getViewModel().stateMessage.observe(this.viewLifecycleOwner, Observer { stateMessage ->
            stateMessage?.response?.let { response ->
                when (response.message) {

                    UPDATE_ENTITY_SUCCESS -> {
                        getViewModel().setIsUpdatePending(false)
                        getViewModel().clearStateMessage()
                        onEntityUpdateSuccess()
                    }

                    DELETE_ENTITY_SUCCESS -> {
                        getViewModel().clearStateMessage()
                        onEntityDeleteSuccess()
                    }

                    else -> {
                        uiController.onResponseReceived(
                            response = stateMessage.response,
                            stateMessageCallback = object : StateMessageCallback {
                                override fun removeMessageFromStack() {
                                    getViewModel().clearStateMessage()
                                }
                            }
                        )
                        when (response.message) {
                            UPDATE_ENTITY_FAILED_DUE_TO_INVALID_PK -> {
                                findNavController().popBackStack()
                            }
                            ERROR_RETRIEVING_SELECTED_ENTITY -> {
                                findNavController().popBackStack()
                            }

                            else -> {
                                // do nothing
                            }
                        }
                    }
                }
            }
        })
    }

    @FlowPreview
    open fun subscribeObservers() {
        val methodName: String = "subscribeObservers"
        if(LOG_ME) ALog.d(TAG, "$methodName: Method start")
        getViewModel().viewState.observe(
            this.viewLifecycleOwner,
            androidx.lifecycle.Observer { baseMVIFragmentViewState ->
                observeViewState(baseMVIFragmentViewState)
            })

        getViewModel().shouldDisplayProgressBar.observe(viewLifecycleOwner, Observer {
            if (LOG_ME) ALog.d(TAG, "$methodName(): Observing shouldDisplayProgressBar == $it")
            try {
                uiController.displayProgressBar(it)
                if(progressBarBlocksClicking) {
                    clicksAreDisabledByProgressBar = it
                }
            } catch (e: Exception) {
                ALog.e(TAG, methodName, e)
            } finally {
                if (LOG_ME) ALog.d(TAG, "Method end: $methodName(): shouldDisplayProgressBar observed successfully")
            }
        })

        getViewModel().stateMessage.observe(this.viewLifecycleOwner, Observer { stateMessage ->
            stateMessage?.response?.let { response ->
                when (response.message) {

                    UPDATE_ENTITY_SUCCESS -> {
                        getViewModel().setIsUpdatePending(false)
                        getViewModel().clearStateMessage()
                        onEntityUpdateSuccess()
                    }

                    DELETE_ENTITY_SUCCESS -> {
                        getViewModel().clearStateMessage()
                        onEntityDeleteSuccess()
                    }

                    else -> {
                        uiController.onResponseReceived(
                            response = stateMessage.response,
                            stateMessageCallback = object : StateMessageCallback {
                                override fun removeMessageFromStack() {
                                    getViewModel().clearStateMessage()
                                }
                            }
                        )
                        when (response.message) {
                            UPDATE_ENTITY_FAILED_DUE_TO_INVALID_PK -> {
                                findNavController().popBackStack()
                            }
                            ERROR_RETRIEVING_SELECTED_ENTITY -> {
                                findNavController().popBackStack()
                            }

                            else -> {
                                // do nothing
                            }
                        }
                    }
                }
            }
        })

        subscribeCustomObservers()
        if(LOG_ME) ALog.d(TAG, "$methodName(): Method end")
    }

    private fun displaySnackBarNoInternetConnection() {
        val messageId: Int = R.string.NO_INTERNET_CONNECTION
        val onDismissCallback: TodoCallback = object : TodoCallback {
            override fun execute() {
                if(LOG_ME)ALog.d(TAG, ".displaySnackBarNoInternetConnection(): snackbar dismissed")
            }
        }
        val actionTextId: Int = R.string.connect
        val actionTextString: String? = null
        val actionOnClickListener = View.OnClickListener {
                if(LOG_ME)ALog.d(TAG, ".displaySnackBarNoInternetConnection(): snackbar button pressed")
                openInternetSettings()
            }
        val stateMessageCallback = object : StateMessageCallback {
            override fun removeMessageFromStack() {
                getViewModel().clearStateMessage()
            }
        }
        uiController.displaySnackBar(
            isDisplayed = true,
            messageId = messageId,
            messageText = null,
            actionTextId = actionTextId,
            actionTextString = actionTextString,
            actionOnClickListener = actionOnClickListener,
            onDismissCallback = onDismissCallback,
            stateMessageCallback = stateMessageCallback,
            dismissSnackbarOnClick = true
        )
    }

    private fun displaySnackBarNoServerConnection() {
        val messageId: Int = R.string.NO_CONNECTION_TO_INTERNET_REPOSITORY
        val onDismissCallback: TodoCallback = object : TodoCallback {
            override fun execute() {
                if(LOG_ME)ALog.d(TAG, ".displaySnackBarNoServerConnection(): snackbar dismissed")
            }
        }
        val actionTextId: Int = R.string.connect
        val actionTextString: String? = null
        val actionOnClickListener = View.OnClickListener {
            if(LOG_ME)ALog.d(TAG, ".displaySnackBarNoServerConnection(): snackbar button pressed")
            openInternetSettings()
            ActivityCompat.finishAffinity(requireActivity())
        }
        val stateMessageCallback = object : StateMessageCallback {
            override fun removeMessageFromStack() {
                getViewModel().clearStateMessage()
            }
        }
        uiController.displaySnackBar(
            isDisplayed = true,
            messageId = messageId,
            messageText = null,
            actionTextId = actionTextId,
            actionTextString = actionTextString,
            actionOnClickListener = actionOnClickListener,
            onDismissCallback = onDismissCallback,
            stateMessageCallback = stateMessageCallback,
            dismissSnackbarOnClick = true
        )
    }

    fun showDialogToConnectToInternet() {
        val methodName = "showDialogToConnectToInternet"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            if(context != null) {
                val builder: AlertDialog.Builder = AlertDialog.Builder(requireContext())
                builder.setMessage(requireContext().getString(R.string.you_need_internet_connection))
                    .setTitle(requireContext().getString(R.string.unable_to_connect))
                    .setCancelable(false)
                    .setPositiveButton(
                        Resources.getSystem().getString(R.string.settings),
                        DialogInterface.OnClickListener { dialog, id ->
                            openInternetSettings()
                        }
                    )
                    .setNegativeButton(
                        Resources.getSystem().getString(R.string.text_cancel),
                        DialogInterface.OnClickListener { dialog, id -> dialog.dismiss() }
                    )
                val alert: AlertDialog = builder.create()
                alert.show()
            }
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }

    private fun logBackStack() {
        val methodName = "logBackStack"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName()")
        try {
            activity?.let { activity ->
                val mActivityManager = activity.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
                val mRecentTasks: List<ActivityManager.RunningTaskInfo> = Objects.requireNonNull(
                    mActivityManager
                ).getRunningTasks(Int.MAX_VALUE)
                val recentTasksIterator: Iterator<ActivityManager.RunningTaskInfo> = mRecentTasks.iterator()

                if(LOG_ME)ALog.d(TAG, ".$methodName(): mRecentTasks.size == ${mRecentTasks.size}")
                while (recentTasksIterator.hasNext()) {
                    val runningTaskInfo = recentTasksIterator.next()
                    val id = runningTaskInfo.id
                    val desc = runningTaskInfo.description
                    val numOfActivities = runningTaskInfo.numActivities
                    val topActivity = runningTaskInfo.topActivity!!.shortClassName
                    runningTaskInfo.baseActivity
                    if(LOG_ME)ALog.d(TAG, ".$methodName(): $id    $desc    $numOfActivities    $topActivity")
                }
            } ?: if(LOG_ME)ALog.d(TAG, ".$methodName(): activity == null")else{}
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName()")
        }
    }

    fun showSoftKeyboard() {
        activity?.let { activity ->
            val imm = activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager?
            imm!!.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, 0)
        } ?: if(LOG_ME)ALog.d(TAG, ".showSoftKeyboard(): activity == null")else{}
    }

    @Deprecated(
        message = "Method throws exception.",
        replaceWith = ReplaceWith("loadPhotoIntoUI(pictureUri : Uri, imageView : ImageView)"),
    )
    fun loadPhotoIntoUI(pictureString : String, imageView : ImageView) {
        val methodName: String = "loadPhotoIntoUI"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            pictureString.let {
                if(LOG_ME)ALog.d(TAG, ".$methodName(): pictureString != null")
            }
            if(pictureString.isNotBlank() && pictureString != "null") {
                var pictureUri = Uri.parse(pictureString)
                if(LOG_ME)ALog.d(
                    TAG, ".$methodName(): \n" +
                        "pictureString == $pictureString\n" +
                        "pictureUri == $pictureUri"
                )
                val filePath = getPath(requireContext(), pictureUri)
                val bm = BitmapFactory.decodeFile(filePath)
                imageView.setImageBitmap(bm)
            } else {
                ALog.d(TAG, ".$methodName(): No picture to set.")
            }
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }

    fun loadPhotoIntoUI(pictureUri : Uri, imageView : ImageView) {
        val methodName: String = "loadPhotoIntoUI"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            imageView.setImageURI(pictureUri)
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }

    fun isNetworkAvailable(): Boolean {
        val methodName = "isNetworkAvailable"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            val connectivityManager: ConnectivityManager = requireContext().getSystemService (
                Context.CONNECTIVITY_SERVICE
            ) as ConnectivityManager
            val networkCapabilities: NetworkCapabilities? =
                connectivityManager.getNetworkCapabilities (
                    connectivityManager.activeNetwork
                )

            if(networkCapabilities != null) {
                if(networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)) {
                    if(LOG_ME)ALog.v(TAG, ".$methodName(): TRANSPORT_CELLULAR is available")
                    return true
                }
                if(networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) {
                    if(LOG_ME)ALog.v(TAG, ".$methodName(): TRANSPORT_WIFI is available")
                    return true
                }
                if(networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET)) {
                    if(LOG_ME)ALog.v(TAG, ".$methodName(): TRANSPORT_ETHERNET is available")
                    return true
                }
            }
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
        if(LOG_ME)ALog.v(TAG, ".$methodName(): no Internet connection is available")
        return false
    }

    fun openInternetSettings() {
        val methodName: String = "openInternetSettings"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            val i = Intent(Settings.ACTION_WIRELESS_SETTINGS)
            startActivity(i)
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }

    companion object{
        val UPDATE_ENTITY_SUCCESS = "Successfully updated entity in $TAG"
        val DELETE_ENTITY_SUCCESS = "Failed to update entity in $TAG"
        val UPDATE_ENTITY_FAILED_DUE_TO_INVALID_PK = "Update failed. Entity is missing primary key in $TAG"
        val ERROR_RETRIEVING_SELECTED_ENTITY = "Error retrieving selected entity from bundle in $TAG"
    }
}