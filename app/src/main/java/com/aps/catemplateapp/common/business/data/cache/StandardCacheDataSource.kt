package com.aps.catemplateapp.common.business.data.cache

import com.aps.catemplateapp.common.util.UniqueID


interface StandardCacheDataSource<Entity> {
    suspend fun insertOrUpdateEntity(entity: Entity): Entity?

    suspend fun insertEntity(entity: Entity): Long

    suspend fun deleteEntity(primaryKey: UniqueID?): Int

    suspend fun deleteEntities(entities: List<Entity>): Int

    suspend fun searchEntities(
        query: String,
        filterAndOrder: String,
        page: Int
    ): List<Entity>

    suspend fun getAllEntities(): List<Entity>

    suspend fun getEntityById(id: UniqueID?): Entity?

    suspend fun getNumEntities(): Int

    suspend fun insertEntities(entities: List<Entity>): LongArray
}