package com.aps.catemplateapp.common.business.data.network

import com.aps.catemplateapp.common.util.UniqueID

interface StandardNetworkDataSource<Entity> {
    suspend fun insertOrUpdateEntity(entity: Entity): Entity?

    suspend fun deleteEntity(primaryKey: UniqueID?)

    suspend fun insertDeletedEntity(entity: Entity)

    suspend fun insertDeletedEntities(Entities: List<Entity>)

    suspend fun deleteDeletedEntity(entity: Entity)

    suspend fun getDeletedEntities(): List<Entity>

    suspend fun deleteAllEntities()

    suspend fun searchEntity(entity: Entity): Entity?

    suspend fun getAllEntities(): List<Entity>

    suspend fun insertOrUpdateEntities(Entities: List<Entity>): List<Entity>?

    suspend fun getEntityById(id: UniqueID): Entity?
}