package com.aps.catemplateapp.common.framework.presentation

import android.content.Context
import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.test.core.app.ApplicationProvider
import com.aps.catemplateapp.R
import com.aps.catemplateapp.common.business.data.util.GenericErrors
import com.aps.catemplateapp.common.business.domain.state.*
import com.aps.catemplateapp.common.util.ALog
import com.aps.catemplateapp.common.util.TodoCallback
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

private const val TAG = "BaseViewModel"
private const val LOG_ME = true

@FlowPreview
@ExperimentalCoroutinesApi
abstract class BaseViewModel<EntityType, EntityViewState : ViewState<EntityType>> : ViewModel()
{
    private val _viewState: MutableLiveData<EntityViewState> = MutableLiveData()

    val dataChannelManager: DataChannelManager<EntityViewState>
            = object: DataChannelManager<EntityViewState>(){

        override fun handleNewData(data: EntityViewState) {
            if(LOG_ME) ALog.d(TAG, "DataChannelManager.handleNewData(): Method start")
            this@BaseViewModel.handleNewData(data)
            if(LOG_ME) ALog.d(TAG, "DataChannelManager.handleNewData(): Method end")
        }
    }

    val viewState: LiveData<EntityViewState>
        get() = _viewState

    val shouldDisplayProgressBar: LiveData<Boolean>
            = dataChannelManager.shouldDisplayProgressBar

    val stateMessage: LiveData<StateMessage?>
        get() = dataChannelManager.messageStack.stateMessage

    // FOR DEBUGGING
    fun getMessageStackSize(): Int{
        return dataChannelManager.messageStack.size
    }

    fun setupChannel() = dataChannelManager.setupChannel()

    abstract fun handleNewData(data: EntityViewState)

    abstract fun setStateEvent(stateEvent: StateEvent)

    fun emitStateMessageEvent(
        stateMessage: StateMessage,
        stateEvent: StateEvent
    ) = flow {
        emit(
            DataState.error<EntityViewState>(
                response = stateMessage.response,
                stateEvent = stateEvent
            )
        )
    }

    fun emitInvalidStateEvent(stateEvent: StateEvent) = flow {
        emit(
            DataState.error<EntityViewState>(
                response = Response(
                    messageId = R.string.error,
                    message = GenericErrors.INVALID_STATE_EVENT,
                    uiComponentType = UIComponentType.None(),
                    messageType = MessageType.Error()
                ),
                stateEvent = stateEvent
            )
        )
    }

    fun launchJob(
        stateEvent: StateEvent,
        jobFunction: Flow<DataState<EntityViewState>?>,
        launchJob: Boolean = true
    ) {
        if(LOG_ME)ALog.d(TAG, "launchJob(): Trying to launch job in dataChannelManager    launchJob == $launchJob")
        if(launchJob)dataChannelManager.launchJob(stateEvent, jobFunction)
    }

    fun getCurrentViewStateOrNew(): EntityViewState{
        return viewState.value ?: initNewViewState()
    }

    fun setViewState(viewState: EntityViewState){
        _viewState.value = viewState
    }

    fun postViewState(viewState: EntityViewState){
        _viewState.postValue(viewState)
    }

    fun clearStateMessage(index: Int = 0){
        if(LOG_ME)ALog.d(TAG, ".clearStateMessage(): " +
                                "clearStateMessage")
        dataChannelManager.clearStateMessage(index)
    }

    fun clearActiveStateEvents() = dataChannelManager.clearActiveStateEventCounter()

    fun clearAllStateMessages() = dataChannelManager.clearAllStateMessages()

    fun printStateMessages() = dataChannelManager.printStateMessages()

    fun cancelActiveJobs() = dataChannelManager.cancelJobs()

    abstract fun initNewViewState(): EntityViewState

    fun getIsUpdatePending(): Boolean{
        return getCurrentViewStateOrNew().isUpdatePending?: false
    }

    fun setIsUpdatePending(isPending: Boolean) {
        val update = getCurrentViewStateOrNew()
        update.isUpdatePending = isPending
        setViewState(update)
    }

    open fun setMainEntity(mainEntity: EntityType?){
        if (LOG_ME) ALog.d(
            TAG,
            "setMainEntity(): viewState == ${viewState.javaClass.simpleName} $viewState    " +
                    "this == ${this.javaClass.simpleName} ${this.hashCode()}")
        val update = getCurrentViewStateOrNew()
        update.mainEntity = mainEntity
        setViewState(update)
    }

    open fun setIsInternetAvailable(isInternetAvailable: Boolean?){
        if (LOG_ME) ALog.d(
            TAG,
            "setIsInternetAvailable(): viewState == ${viewState.javaClass.simpleName} $viewState    " +
                    "this == ${this.javaClass.simpleName} ${this.hashCode()}")
        val update = getCurrentViewStateOrNew()
        update.isInternetAvailable = isInternetAvailable
        setViewState(update)
    }

    open fun postIsInternetAvailable(isInternetAvailable: Boolean){
        if (LOG_ME) ALog.d(
            TAG,
            "postIsInternetAvailable(): viewState == ${viewState.javaClass.simpleName} $viewState    " +
                    "this == ${this.javaClass.simpleName} ${this.hashCode()}")
        val update = getCurrentViewStateOrNew()
        update.isInternetAvailable = isInternetAvailable
        postViewState(update)
    }

    open fun setListOfEntities(listOfEntities: List<EntityType>?){
        val update = getCurrentViewStateOrNew()
        update.listOfEntities = listOfEntities
        setViewState(update)
    }

    protected fun emitDoNothingStateMessageEvent(stateEvent: StateEvent): Flow<DataState<EntityViewState>> {
        return emitStateMessageEvent(
            stateMessage = StateMessage(
                response = Response(
                    messageId = R.string.empty_string,
                    message = (ApplicationProvider.getApplicationContext() as Context).getString(R.string.empty_string),
                    uiComponentType = UIComponentType.None(),
                    messageType = MessageType.Info()
                )
            ),
            stateEvent = stateEvent
        )
    }

    protected fun emitToastStateMessageEvent(
        stateEvent: StateEvent,
        messageID : Int,
    ): Flow<DataState<EntityViewState>> {
        return emitStateMessageEvent(
            stateMessage = StateMessage(
                response = Response(
                    messageId = messageID,
                    message = (ApplicationProvider.getApplicationContext() as Context).getString(messageID),
                    uiComponentType = UIComponentType.Toast(),
                    messageType = MessageType.Info()
                )
            ),
            stateEvent = stateEvent
        )
    }

    protected fun emitQuestionDialogStateMessageEvent(
        stateEvent: StateEvent,
        titleID: Int,
        messageID : Int,
        yesCallback: AreYouSureCallback,
    ): Flow<DataState<EntityViewState>> {
        return emitStateMessageEvent(
            stateMessage = StateMessage(
                response = Response(
                    messageId = messageID,
                    message = (ApplicationProvider.getApplicationContext() as Context).getString(messageID),
                    uiComponentType = UIComponentType.AreYouSureDialog(titleID, yesCallback),
                    messageType = MessageType.Info()
                )
            ),
            stateEvent = stateEvent
        )
    }

    protected fun emitOkDialogStateMessageEvent(
        stateEvent: StateEvent,
        titleID: Int,
        messageID : Int,
        okCallback: OkButtonCallback,
    ): Flow<DataState<EntityViewState>> {
        return emitStateMessageEvent(
            stateMessage = StateMessage(
                response = Response(
                    messageId = messageID,
                    message = (ApplicationProvider.getApplicationContext() as Context).getString(messageID),
                    uiComponentType = UIComponentType.Dialog(okCallback, titleID),
                    messageType = MessageType.Info()
                )
            ),
            stateEvent = stateEvent
        )
    }

    protected fun emitSnackbarStateMessageEvent(
        stateEvent: StateEvent,
        onDismissCallback: TodoCallback?,
        actionTextId: Int,
        actionTextString: String? = null,
        actionOnClickListener: View.OnClickListener = View.OnClickListener {
            if(LOG_ME)ALog.d(TAG, ".emitSnackbarStateMessageEvent(): Snackbar clicked.")
        }
    ): Flow<DataState<EntityViewState>> {
        val message = (ApplicationProvider.getApplicationContext() as Context).getString(actionTextId)
        return emitStateMessageEvent(
            stateMessage = StateMessage(
                response = Response(
                    messageId = actionTextId,
                    message = message,
                    uiComponentType = UIComponentType.SnackBar(
                        onDismissCallback,
                        actionTextId,
                        actionTextString,
                        actionOnClickListener,
                    ),
                    messageType = MessageType.Info()
                )
            ),
            stateEvent = stateEvent
        )
    }

}