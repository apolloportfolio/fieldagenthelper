package com.aps.catemplateapp.common.util.extensions

import androidx.lifecycle.MutableLiveData
import com.aps.catemplateapp.common.util.ALog

private const val TAG = "MutableLiveDataExtensions"
private const val LOG_ME = false

fun <T : Any?> MutableLiveData<T>.updateValue(newValue : T?) {
    val methodName: String = "updateValue()"
//    if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
    try {
        if(value == null) {
            if(LOG_ME) ALog.d(TAG, ".$methodName(): Updating value from $value to $newValue")
            value = newValue
        }
        else {
            if(!this.value!!.equals(newValue)){
                if(LOG_ME) ALog.d(TAG, ".$methodName(): Updating value from $value to $newValue")
                value = newValue
            }
        }
    } catch (e: Exception) {
        ALog.e(TAG, methodName, e)
    } finally {
//        if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
    }
}