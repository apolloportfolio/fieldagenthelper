package com.aps.catemplateapp.common.framework.presentation.views.appbar

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Row
import androidx.compose.material.LocalContentAlpha
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.MoreVert
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.res.stringResource

import androidx.compose.material.*
import androidx.compose.material.icons.filled.Settings
import androidx.compose.material.icons.filled.Share
import androidx.compose.runtime.*
import androidx.compose.ui.tooling.preview.Preview
import com.aps.catemplateapp.R
import com.aps.catemplateapp.common.framework.presentation.values.CommonTheme

// Reference: https://gist.github.com/MachFour/369ebb56a66e2f583ebfb988dda2decf
// Note: should be used in a RowScope
@Composable
fun TraditionalActionMenu(
    items: List<AppBarItem>,
    numIcons: Int = 3, // includes overflow menu icon; may be overridden by NEVER_OVERFLOW
    menuVisible: MutableState<Boolean> = remember { mutableStateOf(false) }
) {
    if (items.isEmpty()) {
        return
    }
    // decide how many action items to show as icons
    val (appbarActions, overflowActions) = remember(items, numIcons) {
        separateIntoIconAndOverflow(items, numIcons)
    }

    for (item in appbarActions) {
        key(item.hashCode()) {
            val name = stringResource(item.nameRes)
            if (item.icon != null) {
                IconButton(onClick = item.doAction) {
                    Icon(
                        imageVector = item.icon,
                        contentDescription = name,
                        tint = MaterialTheme.colors.primary,
                    )
                }
            } else {
                TextButton(onClick = item.doAction) {
                    Text(
                        text = name,
                        color = MaterialTheme.colors.onPrimary.copy(alpha = LocalContentAlpha.current),
                    )
                }
            }
        }
    }

    if (overflowActions.isNotEmpty()) {
        IconButton(onClick = { menuVisible.value = true }) {
            Icon(
                imageVector = Icons.Default.MoreVert,
                contentDescription = "More actions",
                tint = MaterialTheme.colors.primary,
            )
        }
        DropdownMenu(
            expanded = menuVisible.value,
            onDismissRequest = { menuVisible.value = false },
        ) {
            for (item in overflowActions) {
                key(item.hashCode()) {
                    DropdownMenuItem(onClick = {
                        menuVisible.value = false
                        item.doAction() }) {
                        Text(stringResource(item.nameRes))
                    }
                }
            }
        }
    }
}

private fun separateIntoIconAndOverflow(
    items: List<AppBarItem>,
    numIcons: Int
): Pair<List<AppBarItem>, List<AppBarItem>> {
    var (iconCount, overflowCount, preferIconCount) = Triple(0, 0, 0)
    for (item in items) {
        when (item.showAppBarItemAsAction) {
            ShowAppBarItemAsAction.NEVER_OVERFLOW -> iconCount++
            ShowAppBarItemAsAction.IF_NECESSARY -> preferIconCount++
            ShowAppBarItemAsAction.ALWAYS_OVERFLOW -> overflowCount++
            ShowAppBarItemAsAction.NOT_SHOWN -> {}
        }
    }

    val needsOverflow = iconCount + preferIconCount > numIcons || overflowCount > 0
    val actionIconSpace = numIcons - (if (needsOverflow) 1 else 0)

    val iconActions = ArrayList<AppBarItem>()
    val overflowActions = ArrayList<AppBarItem>()

    var iconsAvailableBeforeOverflow = actionIconSpace - iconCount
    for (item in items) {
        when (item.showAppBarItemAsAction) {
            ShowAppBarItemAsAction.NEVER_OVERFLOW -> {
                iconActions.add(item)
            }
            ShowAppBarItemAsAction.ALWAYS_OVERFLOW -> {
                overflowActions.add(item)
            }
            ShowAppBarItemAsAction.IF_NECESSARY -> {
                if (iconsAvailableBeforeOverflow > 0) {
                    iconActions.add(item)
                    iconsAvailableBeforeOverflow--
                } else {
                    overflowActions.add(item)
                }
            }
            ShowAppBarItemAsAction.NOT_SHOWN -> {
                // skip
            }
        }
    }
    return Pair(iconActions, overflowActions)
}

// Preview =========================================================================================
@Preview
@Composable
fun TraditionalActionMenuPreview() {
    CommonTheme {
        val appBarItems: MutableList<AppBarItem> = mutableListOf()

        appBarItems.add(
            AppBarItem(
                nameRes = R.string.share_app,
                icon = Icons.Filled.Share,
                showAppBarItemAsAction = ShowAppBarItemAsAction.NEVER_OVERFLOW,
                doAction = {},
            )
        )

        appBarItems.add(
            AppBarItem(
                nameRes = R.string.settings,
                icon = Icons.Filled.Settings,
                showAppBarItemAsAction = ShowAppBarItemAsAction.IF_NECESSARY,
                doAction = {},
            )
        )

        Row {
            TraditionalActionMenu(items = appBarItems)
        }
    }
}