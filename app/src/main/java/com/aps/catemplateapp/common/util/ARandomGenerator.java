package com.aps.catemplateapp.common.util;

import java.security.SecureRandom;
import java.util.List;
import java.util.Random;

public class ARandomGenerator {
	
	public static int getRandomInteger(int aStart, int aEnd){
	    if (aStart > aEnd) {
	      throw new IllegalArgumentException("Start cannot exceed End.");
	    }
	    Random random = new Random();
	    //getEntityBySQLite the range, casting to long to avoid overflow problems
	    long range = (long)aEnd - (long)aStart + 1;
	    // compute a fraction of the range, 0 <= frac < range
	    long fraction = (long)(range * random.nextDouble());
	    int randomNumber =  (int)(fraction + aStart);    
	    return randomNumber;
	}
	
	public static int getRandomInteger(int end){
		Random random = new Random();
		return random.nextInt(end);
	}

	public static int getRandomInteger(Integer[] possibleResults) {
		boolean logme = false;
		if(logme) ALog.d("", "ARandomGenerator.getRandomInteger(): Method start getRandomInteger()");
		int result = 0;
		try {
			result = possibleResults[getRandomInteger(0, possibleResults.length-1)];
		} catch(Exception e) {
			ALog.e("", "ARandomGenerator.getRandomInteger(): Exception: " + e.toString());
			e.printStackTrace();
		}
		if(logme)ALog.d("", "ARandomGenerator.getRandomInteger(): Method end getRandomInteger()");
		return result;
	}

	public static int getRandomInteger(List<Integer> possibleResults) {
		boolean logme = false;
		if(logme)ALog.d("", "ARandomGenerator.getRandomInteger(): Method start getRandomInteger()");
		int result = 0;
		try {
			result = possibleResults.get(getRandomInteger(0, possibleResults.size()-1));
		} catch(Exception e) {
			ALog.e("", "ARandomGenerator.getRandomInteger(): Exception: " + e.toString());
			e.printStackTrace();
		}
		if(logme)ALog.d("", "ARandomGenerator.getRandomInteger(): Method end getRandomInteger()");
		return result;
	}

    private static final String ALLOWED_CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    public static String generateRandomString(int length) {
        Random random = new SecureRandom();
        StringBuilder sb = new StringBuilder(length);
        for (int i = 0; i < length; i++) {
            int randomIndex = random.nextInt(ALLOWED_CHARS.length());
            sb.append(ALLOWED_CHARS.charAt(randomIndex));
        }
        return sb.toString();
    }
}
