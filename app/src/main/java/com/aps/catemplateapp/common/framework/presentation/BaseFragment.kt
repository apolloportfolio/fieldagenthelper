package com.aps.catemplateapp.common.framework.presentation

import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.annotation.LayoutRes
import com.aps.catemplateapp.common.util.ALog
import com.aps.catemplateapp.common.util.TodoCallback
import com.aps.catemplateapp.common.util.extensions.initiatePopupWithGlideAndFirestore
import com.ceylonlabs.imageviewpopup.ImagePopup
import com.google.firebase.storage.StorageReference

private const val TAG = "BaseFragment"
private const val LOG_ME = true

abstract class BaseFragment
constructor(
        @LayoutRes private val layoutRes: Int
): androidx.fragment.app.Fragment() {
    lateinit var uiController: UIController

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(layoutRes, container, false)
    }

    fun displayToolbarTitle(textView: TextView, title: String?, useAnimation: Boolean) {
        if(title != null){
            showToolbarTitle(textView, title, useAnimation)
        }
        else{
            hideToolbarTitle(textView, useAnimation)
        }
    }

    private fun hideToolbarTitle(textView: TextView, animation: Boolean){
        if(animation){
            textView.fadeOut(
                    object: TodoCallback {
                        override fun execute() {
                            textView.text = ""
                        }
                    }
            )
        }
        else{
            textView.text = ""
            textView.gone()
        }
    }

    private fun showToolbarTitle(
        textView: TextView,
        title: String,
        animation: Boolean
    ){
        textView.text = title
        if(animation){
            textView.fadeIn()
        }
        else{
            textView.visible()
        }
    }


    override fun onAttach(context: Context) {
        Log.d(TAG, "onAttach: Method called")
        super.onAttach(context)
        setUIController(null) // null in production
    }

    fun setUIController(mockController: UIController?){
        // TEST: Set interface from mock
        if(mockController != null){
            this.uiController = mockController
        }
        else{
            activity?.let {
                if(it is BaseActivity){
                    try{
//                        uiController = context as UIController
                        uiController = it as UIController
                    }catch (e: ClassCastException){
                        e.printStackTrace()
                    }
                }
            }
        }
    }

    // Method shows a picture floating above activity
    // https://github.com/chathuralakmal/AndroidImagePopup
    fun showPictureFromFirestore(imageRef : StorageReference) {
        val methodName: String = "showPictureFromFirestore"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            val imagePopup = ImagePopup(activity)
            imagePopup.apply {
                windowHeight = 800
                windowWidth = 800
                backgroundColor = Color.TRANSPARENT
                isFullScreen = false
                isHideCloseIcon = false
                isImageOnClickClose = true
            }.initiatePopupWithGlideAndFirestore(imageRef)
            imagePopup.viewPopup()
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }
}