package com.aps.catemplateapp.common.framework.presentation.views

import androidx.compose.foundation.layout.Box
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.zIndex
import com.aps.catemplateapp.R
import com.aps.catemplateapp.common.framework.presentation.PermissionMediaProvider
import com.aps.catemplateapp.common.framework.presentation.TAG_PERMISSION_ASKING
import com.aps.catemplateapp.common.framework.presentation.values.CommonTheme
import com.aps.catemplateapp.common.util.ALog

private const val TAG = "PermissionDialog"
private const val LOG_ME = true

@Composable
fun PermissionDialog(
    permissionMediaProvider: PermissionMediaProvider,
    isPermanentlyDeclined: Boolean,
    onDismiss: () -> Unit,
    onOkClick: () -> Unit,
    onGoToAppSettingsClick: () -> Unit,
    modifier: Modifier = Modifier.zIndex(1f),
) {
    val dialogState = DialogState(
        show = true,
        onDismissRequest = onDismiss,
        title = stringResource(id = permissionMediaProvider.getDialogTitleID(isPermanentlyDeclined)),
        text = stringResource(id = permissionMediaProvider.getDescriptionID(isPermanentlyDeclined)),
        leftButtonText = stringResource(id = R.string.dismiss),
        leftButtonOnClick = onDismiss,
        rightButtonText = stringResource(id = R.string.grant),
        rightButtonOnClick = {
            if(isPermanentlyDeclined) {
                onGoToAppSettingsClick()
            } else {
                onOkClick()
            }
        },
        updateDialogInViewModel = {},
    )
    if(LOG_ME)ALog.d(
        TAG_PERMISSION_ASKING+TAG, "(): " +
                            "Showing CustomDialog for permission: ${permissionMediaProvider.getPermissionName()}")

    Box(modifier = modifier) {
        CustomDialog(
            dialogState = dialogState,
            onDialogDismiss = onDismiss,
            topBannerBackgroundImageID = permissionMediaProvider.getImageID(isPermanentlyDeclined),
            leftButtonTextID = R.string.dismiss,
            rightButtonTextID = R.string.grant,
        )
    }
}


// Preview ========================================================================================
@Preview
@Composable
internal fun PermissionDialogPreview() {
    CommonTheme {
        PermissionDialog(
            permissionMediaProvider = object : PermissionMediaProvider {
                override fun getDescriptionID(isPermanentlyDeclined: Boolean): Int {
                    return if(isPermanentlyDeclined) {
                        R.string.example_permission_description_perm_declined
                    } else {
                        R.string.example_permission_description
                    }
                }

                override fun getDialogTitleID(isPermanentlyDeclined: Boolean): Int {
                    return if(isPermanentlyDeclined) {
                        R.string.example_permission_dialog_title_perm_declined
                    } else {
                        R.string.example_permission_dialog_title
                    }
                }

                override fun getImageID(isPermanentlyDeclined: Boolean): Int {
                    return if(isPermanentlyDeclined) {
                        R.drawable.ic_launcher
                    } else {
                        R.drawable.ic_launcher
                    }
                }

                override fun getPermissionName(): String {
                    return "PreviewPermission"
                }
            },
            isPermanentlyDeclined = false,
            onDismiss = {
                if(LOG_ME)ALog.d("PermissionDialogPreview", "onDismiss() ")
            },
            onOkClick = {
                if(LOG_ME)ALog.d("PermissionDialogPreview", "onOkClick() ")
            },
            onGoToAppSettingsClick = {
                if(LOG_ME)ALog.d("PermissionDialogPreview", "onGoToAppSettingsClick() ")
            })        
    }
}