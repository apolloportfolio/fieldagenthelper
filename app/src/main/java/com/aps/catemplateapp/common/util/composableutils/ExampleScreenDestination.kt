package com.aps.catemplateapp.common.util.composableutils

import com.aps.catemplateapp.common.framework.presentation.NavDestination

enum class ExampleScreenDestination(val type: String): NavDestination {
    Card1("Card1") {
        override val route: String
            get() = name
    },
    Card2("Card2") {
        override val route: String
            get() = name
    },
    Card3("Card3") {
        override val route: String
            get() = name
    },
    Card4("Card4") {
        override val route: String
            get() = name
    },
    Card5("Card5") {
        override val route: String
            get() = name
    },
    DetailsScreen1("detailsscreen1") {
        override val route: String
            get() = name
    },
    DetailsScreen2("detailsscreen2") {
        override val route: String
            get() = name
    },
    DetailsScreen3("detailsscreen3") {
        override val route: String
            get() = name
    },
    DetailsScreen4("detailsscreen4") {
        override val route: String
            get() = name
    },
    DetailsScreen5("detailsscreen5") {
        override val route: String
            get() = name
    },
    DetailsPlaceholderScreen("detailsplaceholderscreen") {
        override val route: String
            get() = name
    };

    fun toNumber(): Int {
        return when (this) {
            Card1 -> 1
            Card2 -> 2
            Card3 -> 3
            Card4 -> 4
            Card5 -> 5
            DetailsScreen1 -> 10
            DetailsScreen2 -> 20
            DetailsScreen3 -> 30
            DetailsScreen4 -> 40
            DetailsScreen5 -> 50
            DetailsPlaceholderScreen -> 0
        }
    }

    companion object {
        fun fromString(name: String?): ExampleScreenDestination {
            return values().find { it.name == name }
                ?: throw IllegalArgumentException("Invalid name: $name")
        }
        fun fromNumber(number: Int): ExampleScreenDestination {
            return when (number) {
                1 -> Card1
                2 -> Card2
                3 -> Card3
                4 -> Card4
                5 -> Card5
                10 -> DetailsScreen1
                20 -> DetailsScreen2
                30 -> DetailsScreen3
                40 -> DetailsScreen4
                50 -> DetailsScreen5
                0 -> DetailsPlaceholderScreen
                else -> throw IllegalArgumentException("Invalid number: $number")
            }
        }
    }
}