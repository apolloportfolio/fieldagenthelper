package com.aps.catemplateapp.common.framework.presentation

import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.snapshots.SnapshotStateList
import androidx.core.content.ContextCompat
import com.aps.catemplateapp.common.util.ALog

private const val TAG = "PermissionHandlingData"
private const val LOG_ME = true

const val TAG_PERMISSION_ASKING = "PERMISSION_ASKING: "

// References:
// https://www.youtube.com/watch?v=D3JCtaK8LSU
// https://github.com/philipplackner/PermissionsGuideCompose
// https://betterprogramming.pub/jetpack-compose-request-permissions-in-two-ways-fd81c4a702c
data class PermissionHandlingData(
    var permissionsToRequestWithDialogs: SnapshotStateList<String> = mutableStateListOf(),
    var requiredPermissions: MutableSet<String> = mutableSetOf(),
    var grantedPermissions: MutableSet<String> = mutableSetOf(),
    var addRequiredPermissionsInViewModel: (
        activity: Activity?,
        permissionsToAdd: MutableSet<String>
    ) -> Unit = { _, _ -> },
    var addGrantedAndDeniedPermissionsInViewModel: (
        grantedPermissions: MutableSet<String>,
        deniedPermissions: MutableSet<String>,
    ) -> Unit = { _, _ -> },
    var dismissPermissionDialog: (permission: String) -> Unit = {},
) {
    fun copy(): PermissionHandlingData {
        return PermissionHandlingData().apply {
            permissionsToRequestWithDialogs = this@PermissionHandlingData.permissionsToRequestWithDialogs
            requiredPermissions = this@PermissionHandlingData.requiredPermissions
            grantedPermissions = this@PermissionHandlingData.grantedPermissions
            addRequiredPermissionsInViewModel = this@PermissionHandlingData.addRequiredPermissionsInViewModel
            addGrantedAndDeniedPermissionsInViewModel = this@PermissionHandlingData.addGrantedAndDeniedPermissionsInViewModel
            dismissPermissionDialog = this@PermissionHandlingData.dismissPermissionDialog
        }
    }

    fun allNecessaryPermissionsAreGranted(
        context: Context,
    ): Boolean {
        for(permission in requiredPermissions) {
            val permissionCheckResult = ContextCompat.checkSelfPermission(context, permission)
            if (permissionCheckResult == PackageManager.PERMISSION_GRANTED) {
                if(LOG_ME) ALog.d(
                    TAG_PERMISSION_ASKING+TAG,
                    "allNecessaryPermissionsAreGranted(): Permission $permission was granted."
                )
            } else {
                if(LOG_ME) ALog.d(
                    TAG_PERMISSION_ASKING+TAG,
                    "allNecessaryPermissionsAreGranted(): Permission $permission was not granted."
                )
                return false
            }
        }
        return true
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as PermissionHandlingData

        if (permissionsToRequestWithDialogs != other.permissionsToRequestWithDialogs) return false
        if (requiredPermissions != other.requiredPermissions) return false
        if (grantedPermissions != other.grantedPermissions) return false
        if (addRequiredPermissionsInViewModel != other.addRequiredPermissionsInViewModel) return false
        if (addGrantedAndDeniedPermissionsInViewModel != other.addGrantedAndDeniedPermissionsInViewModel) return false
        if (dismissPermissionDialog != other.dismissPermissionDialog) return false

        return true
    }

    override fun hashCode(): Int {
        var result = permissionsToRequestWithDialogs.hashCode()
        result = 31 * result + requiredPermissions.hashCode()
        result = 31 * result + grantedPermissions.hashCode()
        result = 31 * result + addRequiredPermissionsInViewModel.hashCode()
        result = 31 * result + addGrantedAndDeniedPermissionsInViewModel.hashCode()
        result = 31 * result + dismissPermissionDialog.hashCode()
        return result
    }
}