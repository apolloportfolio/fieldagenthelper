package com.aps.catemplateapp.common.business.domain.state


import com.aps.catemplateapp.common.util.ALog

private const val TAG = "StateEventTracker"
private const val LOG_ME = true

class StateEventTracker(
    var isRefreshing: Boolean = false,
    private var currentlyRunningStateEvents: MutableSet<StateEvent> = mutableSetOf(),
) {
    fun notifyStateEventStarting(stateEvent: StateEvent) {
        if (LOG_ME) ALog.d(TAG,
            "notifyStateEventStarting(): Adding stateEvent: ${stateEvent.eventName()}")
        try {
            this.currentlyRunningStateEvents.add(stateEvent)
            if(LOG_ME)logCurrentlyRunningStateEvents("notifyStateEventStarting")
        } catch (e: Exception) {
            ALog.e(TAG, "notifyStateEventStarting()", e)
        } finally {
            isRefreshing = currentlyRunningStateEvents.isNotEmpty()
        }
    }

    private fun logCurrentlyRunningStateEvents(methodName: String) {
        val stringBuilder = StringBuilder()

        currentlyRunningStateEvents.forEach { stateEvent ->
            stringBuilder.appendLine("${stateEvent.eventName()} ${stateEvent.shouldDisplayProgressBar()}")
        }
        if(LOG_ME)ALog.d(TAG, ".$methodName(): currentlyRunningStateEvents:\n$stringBuilder")
    }

    fun notifyStateEventEnded(stateEvent: StateEvent) {
        if (LOG_ME) ALog.d(TAG,
            "notifyStateEventEnded(): Removing stateEvent: ${stateEvent.eventName()}")
        try {
            this.currentlyRunningStateEvents.remove(stateEvent)
            if(LOG_ME)logCurrentlyRunningStateEvents("notifyStateEventStarting")
        } catch (e: Exception) {
            ALog.e(TAG, "notifyStateEventEnded()", e)
        } finally {
            isRefreshing = currentlyRunningStateEvents.isNotEmpty()
        }
    }

    companion object {
        val PreviewStateEventTracker = StateEventTracker()
    }
}