package com.aps.catemplateapp.common.framework.presentation

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import javax.inject.Inject

private const val TAG = "ApplicationViewModel"
private const val LOG_ME = true

@ExperimentalCoroutinesApi
@FlowPreview
@HiltViewModel
class ApplicationViewModel
@Inject
constructor(application: Application) : AndroidViewModel(application) {
    private val locationLiveData = LocationLiveData(application)
    internal fun getLocationLiveData() = locationLiveData

    init {

    }
}