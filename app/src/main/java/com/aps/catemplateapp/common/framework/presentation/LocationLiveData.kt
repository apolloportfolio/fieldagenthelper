package com.aps.catemplateapp.common.framework.presentation


import android.annotation.SuppressLint
import android.content.Context
import android.location.Location
import android.os.Looper
import androidx.lifecycle.LiveData
import com.aps.catemplateapp.common.util.ALog
import com.aps.catemplateapp.common.util.DeviceLocation
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationResult
import com.google.android.gms.location.LocationServices

private const val TAG = "LocationLiveData"
private const val LOG_ME = true

class LocationLiveData(var context: Context) : LiveData<DeviceLocation>() {
    private var fusedLocationClient = LocationServices.getFusedLocationProviderClient(context)


    override fun onInactive() {
        super.onInactive()
        // turn off location updates
        fusedLocationClient.removeLocationUpdates(locationCallback)
    }

    @SuppressLint("MissingPermission")
    override fun onActive() {
        super.onActive()
        val methodName = "onActive"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            val lastLocationTask = fusedLocationClient.lastLocation
            if(fusedLocationClient == null)ALog.e(
                TAG, "$methodName(): " +
                    "fusedLocationClient == null")
            if(lastLocationTask == null)ALog.e(
                TAG, "$methodName(): " +
                    "lastLocationTask == null")
            val onSuccessListener = {
                    location: Location -> location.also {
                    val methodName: String = "OnSuccessListener.onActive()"
                    if(LOG_ME)ALog.d(TAG, "$methodName(): $it")
                    try {
                        setLocationData(it)
                    } catch (e: Exception) {
                        ALog.e(TAG, methodName, e)
                    }
                }
            }
            lastLocationTask.addOnSuccessListener {
                onSuccessListener
            }
            startLocationUpdates()
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
            return
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }

    @SuppressLint("MissingPermission")
    private fun startLocationUpdates() {
        val methodName = "startLocationUpdates"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            val mainLooper = Looper.getMainLooper()
            if(fusedLocationClient == null)ALog.e(
                TAG, "$methodName(): " +
                    "fusedLocationClient == null")
            if(locationRequest == null)ALog.e(
                TAG, "$methodName(): " +
                    "locationRequest == null")
            if(locationCallback == null)ALog.e(
                TAG, "$methodName(): " +
                    "locationCallback == null")
            if(mainLooper == null)ALog.e(
                TAG, "$methodName(): " +
                    "mainLooper == null")
            fusedLocationClient.requestLocationUpdates(
                locationRequest,
                locationCallback,
                mainLooper,
            )
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
            return
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }

    private val locationCallback = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult) {
            val methodName: String = "onLocationResult"
            if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
            try {
                super.onLocationResult(locationResult)
                locationResult ?: return

                for (location in locationResult.locations) {
                    setLocationData(location)
                }
            } catch (e: Exception) {
                ALog.e(TAG, methodName, e)
            } finally {
                if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
            }
        }
    }

    /**
     * If we've received a location update, this function will be called.
     */
    private fun setLocationData(location: Location?) {
        location?.let {
            value = DeviceLocation(true, location)
        }
    }

    companion object {
        const val ONE_MINUTE : Long = 60000
        val locationRequest : LocationRequest = LocationRequest.create().apply {
            interval = ONE_MINUTE
            fastestInterval = ONE_MINUTE /4
            priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        }

    }
}