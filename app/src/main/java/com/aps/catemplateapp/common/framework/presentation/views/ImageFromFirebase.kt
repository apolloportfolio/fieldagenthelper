package com.aps.catemplateapp.common.framework.presentation.views

import android.net.Uri
import androidx.compose.foundation.Image
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import coil.annotation.ExperimentalCoilApi
import coil.compose.rememberImagePainter
import coil.request.ImageRequest
import com.aps.catemplateapp.R
import com.aps.catemplateapp.common.util.ALog
import com.aps.catemplateapp.common.util.composableutils.fallbackPainterResource
import com.google.firebase.storage.StorageReference

private const val TAG = "ImageFromFirebase"
private const val LOG_ME = false

// References:
// https://proandroiddev.com/loading-images-for-jetpack-compose-using-glide-coil-and-fresco-1211261a296e
// https://www.youtube.com/watch?v=jhUiTT2mjco
// https://stackoverflow.com/questions/69152726/how-can-i-load-images-from-firebase-storage-in-a-lazycolumn-in-jetpack-compose-i
// https://stackoverflow.com/questions/73390032/how-to-retrieve-images-from-firestore-to-jetpack-compose
// https://stackoverflow.com/questions/69689168/how-can-i-display-a-bitmap-in-compose-image
// https://stackoverflow.com/questions/73400464/coil-loading-image-from-firebase-not-working-unable-to-fetch-data-no-fetcher
@OptIn(ExperimentalCoilApi::class)
@Composable
internal fun ImageFromFirebase(
    firebaseImageRef: StorageReference?,
    modifier: Modifier,
    contentDescription: String?,
    contentScale: ContentScale,
    placeholderID: Int = R.drawable.ic_launcher,
) {
    val methodName = "ImageFromFirebase"
    if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
    if(firebaseImageRef == null) {
        if(LOG_ME) ALog.w(TAG, ".$methodName(): " +
                "Can't set image from Firebase because firebaseImageRef == null")
        Image(
            modifier = modifier,
            painter = fallbackPainterResource(placeholderID),
            contentDescription = contentDescription,
            contentScale = ContentScale.Fit
        )
        return
    }

    var downloadUrl by remember { mutableStateOf<Uri?>(null) }
    LaunchedEffect(key1 = firebaseImageRef){
        firebaseImageRef.downloadUrl.addOnSuccessListener {
            // Got the download URL for imageRef
            downloadUrl = it
            if(LOG_ME) ALog.d(TAG, ".$methodName(): " +
                    "Successfully got downloadUrl for firebaseImageRef: $downloadUrl")
        }.addOnFailureListener { e ->
            if(LOG_ME) ALog.e(TAG, ".$methodName(): " +
                    "Failed to get downloadUrl for firebaseImageRef: $firebaseImageRef", e)
        }
    }


    if(LOG_ME) ALog.d(TAG, "$methodName(): " +
            "firebaseImageRef == $firebaseImageRef:\n" +
            "downloadUrl == $downloadUrl")

    downloadUrl?.let {
        Image(
            modifier = modifier,
            painter = rememberImagePainter(
                ImageRequest.Builder(LocalContext.current)
                    .data(downloadUrl)
                    .crossfade(false)
                    .listener(object : ImageRequest.Listener {
                        override fun onError(request: ImageRequest, throwable: Throwable) {
                            super.onError(request, throwable)
                            if(LOG_ME) ALog.e(TAG, ".$methodName(): " +
                                    "Error when downloading image for\n" +
                                    "imageRef == $firebaseImageRef:\n" +
                                    "downloadUrl == $downloadUrl\n" +
                                    "${throwable.message}")
                        }
                    })
                    .placeholder(placeholderID)
                    .build()
            ),
            contentDescription = contentDescription,
            contentScale = ContentScale.Fit
        )
    } ?: run {
        Image(
            modifier = modifier,
            painter = painterResource(placeholderID),
            contentDescription = contentDescription,
            contentScale = ContentScale.Fit
        )
    }
    if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
}
