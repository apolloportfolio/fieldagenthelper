package com.aps.catemplateapp.common.framework.presentation.views

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.material.MaterialTheme.colors
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.aps.catemplateapp.R
import com.aps.catemplateapp.common.framework.presentation.values.CommonTheme
import com.aps.catemplateapp.common.util.composableutils.fallbackPainterResource

@Composable
fun BoxWithBackground(
    modifier: Modifier = Modifier
        .fillMaxWidth()
        .wrapContentHeight(),
    backgroundDrawableId: Int? = null,
    composableBackground: DrawnBackground? = null,
    contentAlignment: Alignment = Alignment.Center,
    content: @Composable () -> Unit,
) {
    if(composableBackground != null && backgroundDrawableId != null) {
        Box(
            modifier = modifier,
            contentAlignment = contentAlignment,
        ) {

            Image(
                fallbackPainterResource(id = backgroundDrawableId),
                contentDescription = "",
                contentScale = ContentScale.FillBounds,
                modifier = Modifier
                    .matchParentSize()
            )
            composableBackground(modifier, contentAlignment) {
                content()
            }
        }
    } else if(composableBackground != null) {
        composableBackground(modifier, contentAlignment) {
            content()
        }
    } else {
        Box(
            modifier = modifier,
            contentAlignment = contentAlignment,
        ) {
            if(backgroundDrawableId != null) {
                Image(
                    fallbackPainterResource(id = backgroundDrawableId),
                    contentDescription = "",
                    contentScale = ContentScale.FillBounds,
                    modifier = Modifier
                        .matchParentSize()
                )
            }

            content()
        }
    }
}

//@Composable
//fun BoxWithBackground(
//    modifier: Modifier = Modifier
//        .fillMaxWidth()
//        .wrapContentHeight(),
//    backgroundDrawableId: Int? = null,
//    composableBackground: DrawnBackground? = null,
//    contentAlignment: Alignment = Alignment.Center,
//    content: @Composable () -> Unit,
//) {
//    if(composableBackground != null && backgroundDrawableId != null) {
//        Box(
//            modifier = modifier,
//            contentAlignment = contentAlignment,
//        ) {
//
//            Image(
//                fallbackPainterResource(id = backgroundDrawableId),
//                contentDescription = "",
//                contentScale = ContentScale.FillBounds,
//                modifier = Modifier
//                    .matchParentSize()
//            )
//            composableBackground(modifier, contentAlignment) {
//                content()
//            }
//        }
//    } else if(composableBackground != null) {
//        composableBackground(modifier, contentAlignment) {
//            content()
//        }
//    } else {
//        Box(
//            modifier = modifier,
//            contentAlignment = contentAlignment,
//        ) {
//            if(backgroundDrawableId != null) {
//                Image(
//                    fallbackPainterResource(id = backgroundDrawableId),
//                    contentDescription = "",
//                    contentScale = ContentScale.FillBounds,
//                    modifier = Modifier
//                        .matchParentSize()
//                )
//            }
//
//            content()
//        }
//    }
//}

// Preview =========================================================================================
@Preview
@Composable
internal fun BoxWithBackgroundPreview1() {
    CommonTheme {
        BoxWithBackground(
            backgroundDrawableId = R.drawable.example_background_1
        ) {
            Text(
               text = "BoxWithBackground"
            )
        }
    }
}
@Preview
@Composable
internal fun BoxWithBackgroundPreview2() {
    CommonTheme {
        BoxWithBackground(
            composableBackground = BackgroundsOfLayoutsImpl.backgroundScreen01(
                colors = null,
                brush = null,
                shape = null,
                alpha = null,
            )
        ) {
            Text(
                text = "BoxWithBackground"
            )
        }
    }
}
@Preview
@Composable
internal fun BoxWithBackgroundPreview3() {
    CommonTheme {
        BoxWithBackground(
            backgroundDrawableId = R.drawable.example_background_1,
            composableBackground = BackgroundsOfLayoutsImpl.backgroundScreen01(
                colors = null,
                brush = null,
                shape = null,
                alpha = null,
            )
        ) {
            Text(
                text = "BoxWithBackground"
            )
        }
    }
}
@Preview
@Composable
internal fun BoxWithBackgroundPreview4() {
    CommonTheme {
        BoxWithBackground(
            modifier = Modifier
                .height(200.dp),
            backgroundDrawableId = R.drawable.example_background_1,
            composableBackground = BackgroundsOfLayoutsImpl.backgroundScreen01(
                colors = null,
                brush = null,
                shape = null,
                alpha = null,
            )
        ) {
            Text(
                text = "BoxWithBackground",
            )
        }
    }
}