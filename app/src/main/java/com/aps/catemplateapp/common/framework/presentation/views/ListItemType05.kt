package com.aps.catemplateapp.common.framework.presentation.views

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.Card
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.graphics.DefaultAlpha
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.layout.onGloballyPositioned
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.*
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.tooling.preview.PreviewParameter
import androidx.compose.ui.tooling.preview.PreviewParameterProvider
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.toSize
import androidx.constraintlayout.compose.ConstrainScope
import androidx.constraintlayout.compose.ConstraintLayout
import androidx.constraintlayout.compose.Dimension
import com.aps.catemplateapp.R
import com.aps.catemplateapp.common.framework.presentation.values.CommonTheme
import com.aps.catemplateapp.common.util.ALog
import com.aps.catemplateapp.common.util.composableutils.fallbackPainterResource
import com.google.firebase.storage.StorageReference
import com.gowtham.ratingbar.RatingBar
import com.gowtham.ratingbar.RatingBarStyle
import kotlin.math.roundToInt

private const val TAG = "ListItemType05"
private const val LOG_ME = true

@Composable
fun ListItemType05(
    index: Int,
    titleString: String?,
    descriptionString: String?,
    onListItemClick: () -> Unit,
    getItemsRating: () -> Float?,
    itemRef: StorageReference?,
    itemDrawableId: Int? = null,
    imageContentDescription: String = "",
    imageTint: Color? = MaterialTheme.colors.primary,
    composableBackground: DrawnBackground? = null,
    backgroundDrawableId: Int = R.drawable.list_item_example_background_1,
) {
    if(LOG_ME)ALog.d("test2", "ListItemType02(): " +
            "index == $index, itemTitleString == $titleString\n")
    val transparentColor = remember {
        Color(0x00000000)
    }
    val startMargin = remember { 4 }.dp
    val endMargin = remember { 4 }.dp
    val topMargin = dimensionResource(id = R.dimen.list_item_vertical_margin)
    val bottomMargin = dimensionResource(id = R.dimen.list_item_vertical_margin)
    Card(
        modifier = Modifier
            .testTag(index.toString())
            .fillMaxWidth()
            .padding(
                start = dimensionResource(id = R.dimen.list_tile_horizontal_margin),
                end = dimensionResource(id = R.dimen.list_tile_horizontal_margin),
                top = dimensionResource(id = R.dimen.list_tile_vertical_margin),
                bottom = dimensionResource(id = R.dimen.list_tile_vertical_margin)
            )
            .background(transparentColor)
            .clickable { onListItemClick() },
    ) {
        BoxWithBackground(
            backgroundDrawableId = backgroundDrawableId,
            composableBackground = composableBackground,
        ) {

            ConstraintLayout(
                modifier = Modifier
                    .fillMaxWidth()
                    .wrapContentHeight()
                    .padding(
                        start = startMargin,
                        end = endMargin,
                        top = topMargin,
                        bottom = bottomMargin,
                    )
            ) {
                val itemsRating = remember { getItemsRating() }

                val (itemThumbnail, itemTitle, itemDescription, userRatingBar,
                ) = createRefs()
                val pictureIsPresent = itemRef != null || itemDrawableId != null

                val itemThumbnailConstrain: ConstrainScope.() -> Unit = {
                    top.linkTo(parent.top)
                    bottom.linkTo(parent.bottom)
                    start.linkTo(parent.start)
//                    end.linkTo(itemTitle.start)
                }

                val userRatingBarConstrain: ConstrainScope.() -> Unit = {
                    top.linkTo(itemTitle.bottom)
                    bottom.linkTo(parent.bottom)
                    start.linkTo(
                        if(pictureIsPresent) {
                            itemThumbnail.end
                        } else {
                            parent.start
                        }
                    )
                }

                val itemTitleConstrain: ConstrainScope.() -> Unit = {
                    top.linkTo(parent.top)
                    bottom.linkTo(itemDescription.top)
                    start.linkTo(
                        if(pictureIsPresent) {
                            itemThumbnail.end
                        } else {
                            parent.start
                        }
                    )
                    end.linkTo(parent.end)
                    width = Dimension.fillToConstraints
                }

                val itemDescriptionConstrain: ConstrainScope.() -> Unit = {
                    top.linkTo(itemTitle.bottom)
                    bottom.linkTo(parent.bottom)
                    start.linkTo(
                        if(itemsRating != null) {
                            userRatingBar.end
                        } else {
                            parent.start
                        }
                    )
                    end.linkTo(parent.end)
                    if(itemsRating != null)centerVerticallyTo(userRatingBar)
                    width = Dimension.fillToConstraints
                }

                // Image to the start.
                if(itemRef != null) {
                    ImageFromFirebase(
                        itemRef,
                        modifier = Modifier
                            .constrainAs(itemThumbnail, itemThumbnailConstrain)
                            .height(56.dp)
                            .width(56.dp)
                            .background(transparentColor)
                            .padding(
                                start = startMargin,
                                end = endMargin,
                                top = topMargin,
                                bottom = bottomMargin
                            ),
                        contentDescription = null,
                        contentScale = ContentScale.Fit,
                    )
                } else if(itemDrawableId != null) {
                    Image(
                        painter = fallbackPainterResource(id = itemDrawableId),
                        contentDescription = imageContentDescription,
                        modifier = Modifier
                            .constrainAs(itemThumbnail) {
                                top.linkTo(parent.top)
                                bottom.linkTo(parent.bottom)
                                start.linkTo(parent.start)
                                end.linkTo(itemTitle.start)
                            }
                            .height(56.dp)
                            .width(56.dp)
                            .background(transparentColor)
                            .padding(
                                start = startMargin,
                                end = endMargin,
                                top = topMargin,
                                bottom = bottomMargin,
                            ),
                        alignment = Alignment.Center,
                        contentScale = ContentScale.Fit,
                        alpha = DefaultAlpha,
                        colorFilter = imageTint?.let { ColorFilter.tint(imageTint) },
                    )
                }

                // Item's title
                Text(
                    modifier = Modifier
                        .constrainAs(itemTitle, itemTitleConstrain)
                        .padding(
                            start = startMargin,
                            end = dimensionResource(id = R.dimen.margin_between_text_views),
                            top = topMargin,
                            bottom = bottomMargin,
                        ),
                    text = titleString ?: "",
                    maxLines = 1,
                    textAlign = TextAlign.Start,
                    style = TextStyle(
                        color = MaterialTheme.colors.onSurface,
                        fontWeight = FontWeight.ExtraBold,
                    ),
                    overflow = TextOverflow.Ellipsis,
                )

                // Item's description
                Text(
                    modifier = Modifier
                        .fillMaxWidth(0.2f)
                        .constrainAs(itemDescription, itemDescriptionConstrain)
                        .padding(
                            start = startMargin,
                            end = dimensionResource(id = R.dimen.margin_between_text_views),
                            top = topMargin,
                            bottom = bottomMargin
                        )
                        .background(transparentColor),
                    text = descriptionString ?: "",
                    textAlign = TextAlign.Start,
                    style = TextStyle(
                        color = MaterialTheme.colors.onSurface,
                        fontWeight = FontWeight.Normal,
                    ),
                    maxLines = 3,
                )

                // Item's rating
                if(itemsRating != null) {
                    val maximumNumberOfIndicators = remember { 5 }
                    val fractionOfParentsWidth = remember { 0.25f }
                    val spaceBetweenIndicators = remember { 1.dp }
                    var indicatorSize = remember { 14 }
                    RatingBar(
                        size = indicatorSize.dp,
                        spaceBetween = spaceBetweenIndicators,
                        numOfStars = maximumNumberOfIndicators,
                        value = itemsRating,
                        style = RatingBarStyle.Fill(),
                        onValueChange = {},
                        onRatingChanged = {
                            ALog.d("TAG", "onRatingChanged: $it")
                        },
                        isIndicator = true,
                        modifier = Modifier
                            .padding(
                                top = topMargin,
                                bottom = bottomMargin,
                            )
                            .constrainAs(userRatingBar, userRatingBarConstrain)
                            .fillMaxWidth(fractionOfParentsWidth)
                            .wrapContentWidth()
                            .onGloballyPositioned {
                                //here u can access the parent layout coordinate size
                                val parentSize =
                                    it.parentLayoutCoordinates?.size?.toSize() ?: Size.Zero
                                val spaceLeftForIndicators =
                                    parentSize.width - maximumNumberOfIndicators * spaceBetweenIndicators.value
                                indicatorSize =
                                    (spaceLeftForIndicators * fractionOfParentsWidth / maximumNumberOfIndicators).roundToInt()
                                ALog.d(
                                    "ListItemType02", ": " +
                                            "indicatorSize == $indicatorSize"
                                )
                            },
                    )
                }
            }
        }
    }
}


// Preview =========================================================================================
data class ListItemType05Params(
    val itemTitleString: String?,
    val descriptionString: String?,
    val onListItemClick: () -> Unit,
    val getItemsRating: () -> Float?,
    val ratingBarColor: Color,
)

class ListItemType05ParamsProvider : PreviewParameterProvider<ListItemType05Params> {
    override val values: Sequence<ListItemType05Params> = sequenceOf(
        ListItemType05Params(
            itemTitleString = "Lorem ipsum dolor sit amet",
            descriptionString = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.",
            onListItemClick = {},
            getItemsRating = { 5f },
            ratingBarColor = Color(0xFFFFCC01)
        ),
    )
}

@Preview
@Composable
fun ListItemType05Preview(
    @PreviewParameter(
        ListItemType05ParamsProvider::class
    ) params: ListItemType05Params
) {
    CommonTheme {
        ListItemType05(
            index = 0,
            titleString = params.itemTitleString,
            descriptionString = params.descriptionString,
            onListItemClick = params.onListItemClick,
            getItemsRating = params.getItemsRating,
            itemRef = null,
            itemDrawableId = R.drawable.ic_launcher,
            imageTint = MaterialTheme.colors.primary,
        )
    }
}