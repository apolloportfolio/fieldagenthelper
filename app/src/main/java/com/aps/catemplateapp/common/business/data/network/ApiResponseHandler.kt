package com.aps.catemplateapp.common.business.data.network

import com.aps.catemplateapp.R
import com.aps.catemplateapp.common.business.data.network.NetworkErrors.NETWORK_DATA_NULL
import com.aps.catemplateapp.common.business.data.network.NetworkErrors.NETWORK_ERROR
import com.aps.catemplateapp.common.business.domain.state.*
import com.aps.catemplateapp.common.util.ALog
import com.aps.catemplateapp.common.util.extensions.internetConnectionIsAvailable

private const val TAG = "ApiResponseHandler"
private const val LOG_ME = true

abstract class ApiResponseHandler <ViewState, Data>(
    private val response: ApiResult<Data?>,
    private val stateEvent: StateEvent?,
    private val networkErrorMessage: String = NETWORK_ERROR,
    private val networkDataNullMessage: String = NETWORK_DATA_NULL,
    private val okButtonCallbackWhenNoInternetConnection: OkButtonCallback? = null,
    )
{
    suspend fun getResult(): DataState<ViewState>? {
        return when(response){

            is ApiResult.GenericError -> {
                handleGenericError()
                DataState.error(
                    response = Response(
                        messageId = R.string.error,
                        message = "${stateEvent?.errorInfo()}\n\nReason: ${response.errorMessage.toString()}",
                        uiComponentType = UIComponentType.Dialog(),
                        messageType = MessageType.Error()
                    ),
                    stateEvent = stateEvent
                )
            }

            is ApiResult.NetworkError -> {
                if(LOG_ME) ALog.d(TAG, ".getResult(): NetworkError")
                handleNetworkError()
                DataState.error(
                    response = Response(
                        messageId = R.string.error,
                        message = "${stateEvent?.errorInfo()}\n\nReason: ${networkErrorMessage}",
                        uiComponentType = UIComponentType.Dialog(),
                        messageType = MessageType.Error()
                    ),
                    stateEvent = stateEvent
                )
            }

            is ApiResult.Success -> {
                val internetConnectionIsAvailable : Boolean = internetConnectionIsAvailable()
                if(response.value == null && internetConnectionIsAvailable){
                    handleNetworkDataNullError()
                    DataState.error(
                        response = Response(
                            messageId = R.string.error,
                            message = "${stateEvent?.errorInfo()}\n\nReason: ${networkDataNullMessage}.",
                            uiComponentType = UIComponentType.Dialog(),
                            messageType = MessageType.Error()
                        ),
                        stateEvent = stateEvent
                    )
                } else if(response.value == null && !internetConnectionIsAvailable) {
                    if(LOG_ME) ALog.d(TAG, ".getResult(): response.value == null && !internetConnectionIsAvailable")
                    handleNetworkError()
                    DataState.error(
                        response = Response(
                            messageId = R.string.error,
                            message = "${stateEvent?.errorInfo()}\n\nReason: ${networkErrorMessage}.",
                            uiComponentType = UIComponentType.Dialog(
                                okButtonCallbackWhenNoInternetConnection
                            ),
                            messageType = MessageType.Error()
                        ),
                        stateEvent = stateEvent
                    )
                } else {
                    handleSuccess(resultObj = response.value!!)
                }
            }
        }
    }

    abstract suspend fun handleSuccess(resultObj: Data): DataState<ViewState>?
    open suspend fun handleGenericError(): DataState<ViewState>?{return null}
    open suspend fun handleNetworkDataNullError(){}
    open suspend fun handleNetworkError(){}

}