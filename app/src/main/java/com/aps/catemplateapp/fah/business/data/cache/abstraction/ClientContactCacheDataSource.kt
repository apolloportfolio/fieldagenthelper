package com.aps.catemplateapp.fah.business.data.cache.abstraction


import com.aps.catemplateapp.common.business.data.cache.StandardCacheDataSource
import com.aps.catemplateapp.common.util.ParcelableGeoPoint
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.fah.business.domain.model.entities.ClientContact

interface ClientContactCacheDataSource: StandardCacheDataSource<ClientContact> {
    override suspend fun insertEntity(entity: ClientContact): Long

    override suspend fun deleteEntity(primaryKey: UniqueID?): Int

    override suspend fun deleteEntities(entities: List<ClientContact>): Int

    suspend fun updateEntity(
        id: UniqueID?,
        created_at: String?,
        updated_at: String?,

        surname: String?,
        title: String?,
        phoneNumber: String?,
        emailAddress: String?,

        lattitude : Double?,
        longitude: Double?,
        geoLocation: ParcelableGeoPoint?,
        firestoreGeoLocation: Double?,

        picture1URI: String?,
        name: String?,
        description: String?,
    ): Int

    override suspend fun searchEntities(
        query: String,
        filterAndOrder: String,
        page: Int
    ): List<ClientContact>

    override suspend fun getAllEntities(): List<ClientContact>

    override suspend fun getEntityById(id: UniqueID?): ClientContact?

    override suspend fun getNumEntities(): Int

    override suspend fun insertEntities(entities: List<ClientContact>): LongArray
}