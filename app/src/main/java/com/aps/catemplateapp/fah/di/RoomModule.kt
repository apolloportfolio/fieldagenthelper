package com.aps.catemplateapp.fah.di

import com.aps.catemplateapp.core.framework.datasources.cachedatasources.room.ProjectRoomDatabase
import com.aps.catemplateapp.core.framework.datasources.cachedatasources.room.daos.*
import com.aps.catemplateapp.fah.framework.datasources.cachedatasources.room.daos.FieldSurveyDao
import com.aps.catemplateapp.fah.framework.datasources.cachedatasources.room.daos.UsersPresentationDao
import com.aps.catemplateapp.fah.framework.datasources.cachedatasources.room.daos.SurveyQuestionDao
import com.aps.catemplateapp.fah.framework.datasources.cachedatasources.room.daos.ClientContactDao
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
class RoomModule {

    @Singleton
    @Provides
    fun provideEntity1Dao(database : ProjectRoomDatabase) : FieldSurveyDao {
        return database.fieldSurveyDao()
    }

    @Singleton
    @Provides
    fun provideEntity2Dao(database : ProjectRoomDatabase) : UsersPresentationDao {
        return database.usersPresentationDao()
    }

    @Singleton
    @Provides
    fun provideEntity3Dao(database : ProjectRoomDatabase) : SurveyQuestionDao {
        return database.surveyQuestionDao()
    }

    @Singleton
    @Provides
    fun provideEntity4Dao(database : ProjectRoomDatabase) : ClientContactDao {
        return database.clientContactDao()
    }
}