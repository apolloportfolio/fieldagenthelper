package com.aps.catemplateapp.fah.framework.datasources.cachedatasources.room.daos

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.fah.framework.datasources.cachedatasources.room.model.UsersPresentationCacheEntity
import java.util.*


@Dao
interface UsersPresentationDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(entity : UsersPresentationCacheEntity) : Long

    @Query("SELECT * FROM userspresentation")
    suspend fun get() : List<UsersPresentationCacheEntity>
    
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertEntities(entities: List<UsersPresentationCacheEntity>): LongArray

    @Query("SELECT * FROM userspresentation WHERE id = :id")
    suspend fun getEntityById(id: UniqueID?): UsersPresentationCacheEntity?

    @Query("DELETE FROM userspresentation WHERE id IN (:ids)")
    suspend fun deleteEntities(ids: List<UniqueID?>): Int

    @Query("DELETE FROM userspresentation")
    suspend fun deleteAllEntities()

    @Query("SELECT * FROM userspresentation")
    suspend fun getAllEntities(): List<UsersPresentationCacheEntity>

    @Query("""
        UPDATE userspresentation 
        SET 
        created_at = :created_at,
        updated_at = :updated_at,
        presentationFileURI = :presentationFileURI,
        picture1URI = :picture1URI,
        name = :name,
        description = :description,
        ownerId = :ownerID
        WHERE id = :id
        """)
    suspend fun updateEntity(
        id: UniqueID?,
        created_at: String?,
        updated_at: String?,

        presentationFileURI: String?,

        picture1URI: String?,

        name: String?,
        description: String?,

        ownerID: UniqueID?,
    ): Int

    @Query("DELETE FROM userspresentation WHERE id = :primaryKey")
    suspend fun deleteEntity(primaryKey: UniqueID?): Int

    @Query("SELECT * FROM userspresentation")
    suspend fun searchEntities(): List<UsersPresentationCacheEntity>
    
    @Query("SELECT COUNT(*) FROM userspresentation")
    suspend fun getNumEntities(): Int
}