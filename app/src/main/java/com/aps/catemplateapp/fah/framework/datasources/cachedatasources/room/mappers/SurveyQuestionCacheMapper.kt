package com.aps.catemplateapp.fah.framework.datasources.cachedatasources.room.mappers

import com.aps.catemplateapp.common.business.domain.model.EntityMapper
import com.aps.catemplateapp.fah.business.domain.model.entities.SurveyQuestion
import com.aps.catemplateapp.fah.framework.datasources.cachedatasources.room.model.SurveyQuestionCacheEntity
import javax.inject.Inject

class SurveyQuestionCacheMapper
@Inject
constructor() : EntityMapper<SurveyQuestionCacheEntity, SurveyQuestion> {
    override fun mapFromEntity(entity: SurveyQuestionCacheEntity): SurveyQuestion {
        return SurveyQuestion(
            entity.id,
            entity.created_at,
            entity.updated_at,

            entity.picture1URI,

            entity.question,
            entity.answer,

            entity.fieldSurveyID,
        )
    }

    override fun mapToEntity(domainModel: SurveyQuestion): SurveyQuestionCacheEntity {
        return SurveyQuestionCacheEntity(
            domainModel.id!!,
            domainModel.created_at,
            domainModel.updated_at,

            domainModel.picture1URI,

            domainModel.question,
            domainModel.answer,

            domainModel.fieldSurveyID,
        )
    }

    override fun mapFromEntityList(entities : List<SurveyQuestionCacheEntity>) : List<SurveyQuestion> {
        return entities.map{mapFromEntity(it)}
    }

    override fun mapToEntityList(entities: List<SurveyQuestion>): List<SurveyQuestionCacheEntity> {
        return entities.map{mapToEntity(it)}
    }
}