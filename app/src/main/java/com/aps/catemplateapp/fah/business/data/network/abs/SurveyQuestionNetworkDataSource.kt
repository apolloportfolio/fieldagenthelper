package com.aps.catemplateapp.fah.business.data.network.abs

import com.aps.catemplateapp.common.business.data.network.StandardNetworkDataSource
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.fah.business.domain.model.entities.SurveyQuestion

interface SurveyQuestionNetworkDataSource: StandardNetworkDataSource<SurveyQuestion> {
    override suspend fun insertOrUpdateEntity(entity: SurveyQuestion): SurveyQuestion?

    override suspend fun deleteEntity(primaryKey: UniqueID?)

    override suspend fun insertDeletedEntity(entity: SurveyQuestion)

    override suspend fun insertDeletedEntities(Entities: List<SurveyQuestion>)

    override suspend fun deleteDeletedEntity(entity: SurveyQuestion)

    override suspend fun getDeletedEntities(): List<SurveyQuestion>

    override suspend fun deleteAllEntities()

    override suspend fun searchEntity(entity: SurveyQuestion): SurveyQuestion?

    override suspend fun getAllEntities(): List<SurveyQuestion>

    override suspend fun insertOrUpdateEntities(Entities: List<SurveyQuestion>): List<SurveyQuestion>?

    override suspend fun getEntityById(id: UniqueID): SurveyQuestion?

    suspend fun getUsersEntities3(userId: UserUniqueID): List<SurveyQuestion>?
}