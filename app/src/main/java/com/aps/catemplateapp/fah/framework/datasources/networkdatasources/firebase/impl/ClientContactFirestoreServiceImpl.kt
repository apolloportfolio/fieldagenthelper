package com.aps.catemplateapp.fah.framework.datasources.networkdatasources.firebase.impl

import com.aps.catemplateapp.common.util.DateUtil
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.core.business.data.network.abstraction.UserNetworkDataSource
import com.aps.catemplateapp.core.framework.datasources.networkdatasources.firebase.implementation.BasicFirestoreServiceImpl
import com.aps.catemplateapp.fah.business.domain.model.entities.ClientContact
import com.aps.catemplateapp.fah.framework.datasources.networkdatasources.firebase.abs.ClientContactFirestoreService
import com.aps.catemplateapp.fah.framework.datasources.networkdatasources.firebase.mappers.ClientContactFirestoreMapper
import com.aps.catemplateapp.fah.framework.datasources.networkdatasources.firebase.model.ClientContactFirestoreEntity
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class ClientContactFirestoreServiceImpl
@Inject
constructor(
//    private val firebaseAuth: FirebaseAuth,
//    private val firestore: FirebaseFirestore,
    private val networkMapper: ClientContactFirestoreMapper,
    private val dateUtil: DateUtil,
    private val userNetworkDataSource: UserNetworkDataSource,
): ClientContactFirestoreService,
    BasicFirestoreServiceImpl<ClientContact, ClientContactFirestoreEntity, ClientContactFirestoreMapper>(
//        firebaseAuth,
//        firestore,
        networkMapper,
        dateUtil
    ) {

    override fun getCollectionName(): String {
        return COLLECTION_NAME
    }

    override fun getDeletesCollectionName(): String {
        return DELETES_COLLECTION_NAME
    }

    override fun setEntityID(entity: ClientContact, id: UniqueID) {
        entity.id = id
    }

    override fun getEntityID(entity: ClientContact): UniqueID? {
        return entity.id
    }

    override fun setFirestoreEntityID(entity: ClientContactFirestoreEntity, id: UniqueID) {
        entity.id = id
    }

    override fun updateEntityTimestamp(entity: ClientContact, timestamp: String){
        entity.updated_at = timestamp
    }

    override fun updateFirestoreEntityTimestamp(entity: ClientContactFirestoreEntity, timestamp: String){
        entity.updated_at = timestamp
    }

    override fun getFirestoreEntityType(): Class<ClientContactFirestoreEntity> {
        return ClientContactFirestoreEntity::class.java
    }

    override fun getFirestoreEntityID(entity: ClientContactFirestoreEntity): UniqueID? {
        return entity.id
    }

    companion object {
        const val TAG = "Entity4FirestoreServiceImpl"
        const val LOG_ME = true
        const val COLLECTION_NAME = "entity4"
        const val DELETES_COLLECTION_NAME = "entity4_d"
    }
}