package com.aps.catemplateapp.fah.business.domain.model.entities

import android.os.Parcelable
import com.aps.catemplateapp.common.util.ALog
import com.aps.catemplateapp.common.util.ParcelableGeoPoint
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.core.util.ProjectConstants
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.ktx.storage
import kotlinx.parcelize.IgnoredOnParcel
import kotlinx.parcelize.Parcelize
import java.io.Serializable

private const val TAG = "FieldSurvey"
private const val LOG_ME = true

// Entity1
@Parcelize
data class FieldSurvey(
    var id: UniqueID?,
    var created_at: String?,
    var updated_at: String?,

    var creatorId: UserUniqueID?,
    var surveyedClientContactID: UniqueID?,
    var fillingTime: String?,
    var isTemplate: Boolean = false,

    var latitude: Double?,
    var longitude: Double?,
    var geoLocation: ParcelableGeoPoint?,
    var firestoreGeoLocation: Double?,

    var picture1URI: String?,
    var description: String?,

    var city: String?,

    var ownerID: UserUniqueID?,

    var name: String?,

    var switch1: Boolean? = null,
    var switch2: Boolean? = null,
    var switch3: Boolean? = null,
    var switch4: Boolean? = null,
    var switch5: Boolean? = null,
    var switch6: Boolean? = null,
    var switch7: Boolean? = null,
): Parcelable, Serializable {

    var hasPictures:Boolean =
        picture1URI != null

    @IgnoredOnParcel
    val picture1FirebaseImageRef: StorageReference?
        get() {
            return if(id != null && picture1URI != null) {
                if(LOG_ME) ALog.d(
                    TAG, ".picture1ImageRef(): " +
                            "this.id!!.firestoreDocumentID == ${this.id!!.firestoreDocumentID}"
                )
                Firebase.storage.reference
                    .child(ProjectConstants.FIRESTORE_IMAGES_COLLECTION)
                    .child(ProjectConstants.FIRESTORE_ENTITY_1_IMAGES_SUB_COLLECTION)
                    .child(this.id!!.firestoreDocumentID)
                    .child(this.picture1URI!!)
            } else {
                null
            }
        }


    object Entity1Constants {
        const val LOCATION_FIELD_NAME = "firestoreGeoLocation"
    }

    override fun toString(): String {
        return "${id.toString()}: geoLocation == $geoLocation \n"
    }

    fun toIdString() : String {
        return "${this.javaClass.simpleName}($id)"
    }

    companion object {
        const val ownerIDFieldName = "ownerID"
    }
}