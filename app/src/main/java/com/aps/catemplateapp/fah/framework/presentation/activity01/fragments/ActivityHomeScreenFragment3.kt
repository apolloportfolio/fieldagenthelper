package com.aps.catemplateapp.fah.framework.presentation.activity01.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.os.Parcel
import android.os.Parcelable
import android.view.*
import androidx.activity.OnBackPressedCallback
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aps.catemplateapp.R
import com.aps.catemplateapp.common.business.domain.state.StateEvent
import com.aps.catemplateapp.common.framework.presentation.BaseMVIFragment
import com.aps.catemplateapp.common.framework.presentation.BaseViewModel
import com.aps.catemplateapp.common.util.ALog
import com.aps.catemplateapp.common.util.DateUtil
import com.aps.catemplateapp.common.util.extensions.hideKeyboard
import com.aps.catemplateapp.core.business.domain.model.entities.ProjectUser
import com.aps.catemplateapp.databinding.FragmentHomeScreenFragment3Binding
import com.aps.catemplateapp.fah.framework.presentation.activity01.ActivityHomeScreen
import com.aps.catemplateapp.fah.framework.presentation.activity01.ActivityHomeScreenNavigation
import com.aps.catemplateapp.fah.framework.presentation.activity01.HomeScreenViewModel
import com.aps.catemplateapp.fah.framework.presentation.activity01.adapters.AdapterFieldSurveyToListItemType3
import com.aps.catemplateapp.fah.framework.presentation.activity01.adapters.AdapterSurveyQuestionToListItemType5
import com.aps.catemplateapp.fah.framework.presentation.activity01.state.HomeScreenStateEvent
import com.aps.catemplateapp.fah.framework.presentation.activity01.state.HomeScreenViewState
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.*

const val HomeScreen_Fragment3_STATE_BUNDLE_KEY = "com.aps.catemplateapp.fah.framework.presentation.activity01.fragments.fragment3"

private const val TAG = "ActivityHomeScreenFragment3"
private const val LOG_ME = true

@FlowPreview
@ExperimentalCoroutinesApi
@AndroidEntryPoint
class ActivityHomeScreenFragment3
constructor(
        private val viewModelFactory: ViewModelProvider.Factory,
        private val dateUtil: DateUtil
): BaseMVIFragment<ProjectUser, HomeScreenViewState<ProjectUser>>(
    layoutRes = R.layout.fragment_home_screen_fragment3
), ActivityHomeScreenNavigation {

    private val viewModel : HomeScreenViewModel
        get() = (activity as ActivityHomeScreen).viewModel

    private lateinit var binding: FragmentHomeScreenFragment3Binding

    private lateinit var recyclerView : RecyclerView

    private val fragment = this
    private val onItemClickListener : AdapterFieldSurveyToListItemType3.OnItemClickListener =
        object : AdapterFieldSurveyToListItemType3.OnItemClickListener {
            override fun onItemClick(position: Int) {
                val methodName: String = "onItemClick"
                if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
                try {
                    val clickedEntity3 =
                        getViewModel().getCurrentViewStateOrNew().usersSurveyTemplates?.get(position)
                    if(clickedEntity3 != null) {
                        if (LOG_ME) ALog.d(TAG, "$methodName(): Navigating to entity3 $position")
                        val user = getViewModel().getCurrentViewStateOrNew().mainEntity
                        if(user != null) {
                            navigateToEntity3DetailsScreen(
                                fragment,
                                clickedEntity3,
                                user,
                            )
                        } else {
                            ALog.e(TAG, ".$methodName(): user == null")
                        }
                    } else {
                        if(LOG_ME)ALog.e(TAG, ".onItemClick(): clickedItem == null")
                    }
                } catch (e: Exception) {
                    ALog.e(TAG, methodName, e)
                    return
                } finally {
                    if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
                }
            }
        }

    constructor(parcel: Parcel) : this(
            TODO("viewModelFactory"),
            TODO("dateUtil")) {
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.setupChannel()
    }

    @InternalCoroutinesApi
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
		recyclerView = binding.recyclerView

        recyclerView = binding.recyclerView
        recyclerView.adapter = AdapterFieldSurveyToListItemType3(
            getViewModel().getCurrentViewStateOrNew().usersSurveyTemplates,
            onItemClickListener,
            onItemClickListener,
        )
        recyclerView.layoutManager = LinearLayoutManager(activity)
        recyclerView.setHasFixedSize(false)

        restoreInstanceState(savedInstanceState)
        setupOnBackPressDispatcher()
    }

    override fun onResume() {
        super.onResume()
        activity?.apply {
            setTitle(R.string.app_bar_title_home_screen_fragment_3)
        }
    }

    override fun restoreInstanceState(savedInstanceState: Bundle?){
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
    }

    private fun setupOnBackPressDispatcher() {
        val callback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                onBackPressed()
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner, callback)
    }

    override fun onBackPressed() {
        viewModel.exitEditState()
        super.onBackPressed()
        view?.hideKeyboard()
        findNavController().popBackStack()
    }
    override fun setupUI(){
        view?.hideKeyboard()
    }

    override fun onDestroyView() {
        super.onDestroyView()
    }

    override fun setupBinding(view: View) {
        binding = FragmentHomeScreenFragment3Binding.bind(view)
    }

    companion object CREATOR : Parcelable.Creator<ActivityHomeScreenFragment3> {
        override fun createFromParcel(parcel: Parcel): ActivityHomeScreenFragment3 {
            return ActivityHomeScreenFragment3(parcel)
        }

        override fun newArray(size: Int): Array<ActivityHomeScreenFragment3?> {
            return arrayOfNulls(size)
        }
    }

    override fun subscribeCustomObservers() {
    }

    override fun getViewModel(): BaseViewModel<ProjectUser, HomeScreenViewState<ProjectUser>> {
        return viewModel
    }

    override fun getInitStateEvent(): StateEvent {
        if(LOG_ME)ALog.d(TAG, ".getInitStateEvent(): Getting entities3.")
        return HomeScreenStateEvent.GetEntities3
    }

    @SuppressLint("NotifyDataSetChanged")
    override fun changeUiOrNavigateDependingOnViewState(viewState: HomeScreenViewState<ProjectUser>) {
        val methodName: String = "changeUiOrNavigateDependingOnViewState"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {

            viewState.mainEntity?.let {
                if(viewState.mainEntity!!.fullyVerified) {
                    if(LOG_ME)ALog.d(TAG, ".$methodName(): User is fully verified.")
                    binding.recyclerView.visibility = View.VISIBLE
                    binding.profileCompletionStateLbl.visibility = View.GONE
                    binding.disabledFunctionalityExplanationLbl.visibility = View.GONE
                } else {
                    if(LOG_ME)ALog.d(TAG, ".$methodName(): User is not fully verified.")
                    binding.recyclerView.visibility = View.GONE
                    binding.profileCompletionStateLbl.visibility = View.VISIBLE
                    binding.disabledFunctionalityExplanationLbl.visibility = View.VISIBLE
                }
            }

            if(viewState.refreshListOfEntities3) {
                viewState.usersSurveyTemplates?.let { list ->
                    if(LOG_ME){
                        ALog.d(TAG, ".$methodName(): Got entities3:")
                        for((index, entity3) in list.withIndex()) {
                            ALog.d(TAG, ".$methodName(): Entity3 #$index == $entity3")
                        }
                    }
                    if(list.isNotEmpty()){
                        binding.recyclerView.visibility = View.VISIBLE
                        binding.tipWhenListIsEmpty.visibility = View.GONE
                    } else {
                        binding.tipWhenListIsEmpty.visibility = View.VISIBLE
                    }
                    val adapter = (recyclerView.adapter as AdapterFieldSurveyToListItemType3)
                    adapter.setData(list)

                    recyclerView.adapter?.let {
                        it.notifyDataSetChanged()
                        if(LOG_ME)ALog.d(TAG, ".$methodName(): notifyDataSetChanged")
                    }
                }
                viewModel.setRefreshListOfEntites3(false)
                return
            }

            if(viewState.navigateToStartScreen == true) {
                navigateToStartScreen(this)
                viewModel.setNavigateToStartScreen(null)
                return
            }

        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }

    override fun onEntityUpdateSuccess() {

    }

    override fun onEntityDeleteSuccess() {
        
    }

    override fun getStateBundleKey(): String? {
        return HomeScreen_Fragment3_STATE_BUNDLE_KEY
    }

    override fun setAllOnClickAndOnTouchListeners() {

    }

    override fun onClick(v: View?) {
        if (LOG_ME) ALog.d(TAG, "onClick(): Method start")
        super.onClick(v)
        when(v) {

        }
    }

    override fun updateUIInViewModel() {
        if(viewModel.checkEditState()){
            if (LOG_ME)ALog.d(TAG, "updateUIInViewModel():  Users interface is in EditState")
            view?.hideKeyboard()
            viewModel.exitEditState()
        }
    }

    private fun updateMainEntityInViewModel() {
        if(viewModel.getIsUpdatePending()){
            viewModel.getCurrentViewStateOrNew().mainEntity?.let {
                HomeScreenStateEvent.UpdateMainEntityEvent(
                    it
                )
            }?.let {
                if (LOG_ME) ALog.d(TAG, "updateMainEntityInViewModel(): Sending UpdateMainEntityEvent")
                viewModel.setStateEvent(
                    it
                )
            }
        }
    }
}