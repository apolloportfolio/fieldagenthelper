package com.aps.catemplateapp.fah.business.domain.model.factories

import com.aps.catemplateapp.common.util.DateUtil
import com.aps.catemplateapp.common.util.ParcelableGeoPoint
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.fah.business.domain.model.entities.FieldSurvey
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class FieldSurveyFactory
@Inject
constructor(
    private val dateUtil: DateUtil
) {
    fun createSingleEntity(
        id: UniqueID?,

        creatorId: UserUniqueID?,
        surveyedClientContactID: UniqueID?,
        fillingTime: String?,
        isTemplate: Boolean = false,

        latitude : Double?,
        longitude: Double?,
        geoLocation: ParcelableGeoPoint?,
        firestoreGeoLocation: Double?,

        picture1URI: String?,
        description : String?,

        city: String?,

        ownerID: UserUniqueID?,

        name: String?,
    ): FieldSurvey {
        return FieldSurvey(
            id,
            dateUtil.getCurrentTimestamp(),
            dateUtil.getCurrentTimestamp(),

            creatorId,
            surveyedClientContactID,
            fillingTime,
            isTemplate,

            latitude,
            longitude,
            geoLocation,
            firestoreGeoLocation,

            picture1URI,
            description,

            city,

            ownerID,

            name,
        )
    }

    fun createEntitiesList(numEntities: Int): List<FieldSurvey> {
        val list: ArrayList<FieldSurvey> = ArrayList()
        for(i in 0 until numEntities){ // exclusive on upper bound
            list.add(
                createSingleEntity(
                    UniqueID.generateFakeRandomID(),
                    null,
                    null,
                    null,
                    false,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                )
            )
        }
        return list
    }


    fun generateEmpty(): FieldSurvey {
        return createSingleEntity(
            UniqueID.generateFakeRandomID(),
            null,
            null,
            null,
            false,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
        )
    }
    companion object {
        fun createPreviewEntitiesList(): List<FieldSurvey> {
            return arrayListOf(
                FieldSurvey(
                    id = UniqueID("survey01"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    creatorId = UserUniqueID("666", "666"),
                    surveyedClientContactID = UniqueID.generateFakeRandomID(),
                    fillingTime = "2019-04-14 10:41:22 AM",
                    isTemplate = false,
                    latitude = 54.23154,
                    longitude = 21.614117,
                    geoLocation = null,
                    firestoreGeoLocation = null,
                    picture1URI = "1",
                    description = "This field survey focuses on assessing the condition of public parks in the city of Warsaw. It aims to gather data on park amenities, greenery, cleanliness, and visitor satisfaction. The findings from this survey will be used to improve park management and enhance the overall experience for park visitors.",
                    city = "Warsaw",
                    ownerID = UserUniqueID("666", "666"),
                    name = "Park Assessment Survey"
                ),
                FieldSurvey(
                    id = UniqueID("survey02"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    creatorId = UserUniqueID("666", "666"),
                    surveyedClientContactID = UniqueID.generateFakeRandomID(),
                    fillingTime = "2019-04-14 10:41:22 AM",
                    isTemplate = false,
                    latitude = 54.23154,
                    longitude = 21.614117,
                    geoLocation = null,
                    firestoreGeoLocation = null,
                    picture1URI = "1",
                    description = "This field survey aims to evaluate the quality of road infrastructure in urban areas of Warsaw. It includes assessments of road conditions, signage, and traffic flow. Additionally, the survey will gather feedback from commuters and residents to identify areas for improvement in the city's transportation network.",
                    city = "Warsaw",
                    ownerID = UserUniqueID("666", "666"),
                    name = "Urban Road Assessment Survey"
                ),
                FieldSurvey(
                    id = UniqueID("survey03"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    creatorId = UserUniqueID("666", "666"),
                    surveyedClientContactID = UniqueID.generateFakeRandomID(),
                    fillingTime = "2019-04-14 10:41:22 AM",
                    isTemplate = false,
                    latitude = 54.23154,
                    longitude = 21.614117,
                    geoLocation = null,
                    firestoreGeoLocation = null,
                    picture1URI = "1",
                    description = "This field survey is designed to assess the accessibility of public buildings and facilities for people with disabilities in Warsaw. It will examine factors such as ramp availability, elevator functionality, and signage clarity. By identifying accessibility barriers, the survey aims to promote inclusivity and improve accessibility standards in the city.",
                    city = "Warsaw",
                    ownerID = UserUniqueID("666", "666"),
                    name = "Accessibility Evaluation Survey"
                ),
                FieldSurvey(
                    id = UniqueID("survey04"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    creatorId = UserUniqueID("666", "666"),
                    surveyedClientContactID = UniqueID.generateFakeRandomID(),
                    fillingTime = "2019-04-14 10:41:22 AM",
                    isTemplate = false,
                    latitude = 54.23154,
                    longitude = 21.614117,
                    geoLocation = null,
                    firestoreGeoLocation = null,
                    picture1URI = "1",
                    description = "This field survey aims to assess air quality in various districts of Warsaw. It will measure pollutants such as particulate matter, nitrogen dioxide, and sulfur dioxide. The data collected will help local authorities to develop strategies for improving air quality and protecting public health.",
                    city = "Warsaw",
                    ownerID = UserUniqueID("666", "666"),
                    name = "Air Quality Assessment Survey"
                ),
                FieldSurvey(
                    id = UniqueID("survey05"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    creatorId = UserUniqueID("666", "666"),
                    surveyedClientContactID = UniqueID.generateFakeRandomID(),
                    fillingTime = "2019-04-14 10:41:22 AM",
                    isTemplate = false,
                    latitude = 54.23154,
                    longitude = 21.614117,
                    geoLocation = null,
                    firestoreGeoLocation = null,
                    picture1URI = "1",
                    description = "This field survey aims to assess the effectiveness of waste management practices in Warsaw. It will evaluate recycling rates, waste disposal methods, and public awareness of waste reduction. By identifying areas for improvement, the survey aims to promote sustainable waste management in the city.",
                    city = "Warsaw",
                    ownerID = UserUniqueID("666", "666"),
                    name = "Waste Management Survey"
                ),
                FieldSurvey(
                    id = UniqueID("survey06"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    creatorId = UserUniqueID("666", "666"),
                    surveyedClientContactID = UniqueID.generateFakeRandomID(),
                    fillingTime = "2019-04-14 10:41:22 AM",
                    isTemplate = false,
                    latitude = 54.23154,
                    longitude = 21.614117,
                    geoLocation = null,
                    firestoreGeoLocation = null,
                    picture1URI = "1",
                    description = "This field survey aims to evaluate the condition of public transportation infrastructure in Warsaw. It will assess factors such as the reliability of buses and trains, accessibility for people with disabilities, and overall passenger satisfaction. The findings will be used to enhance public transportation services and infrastructure.",
                    city = "Warsaw",
                    ownerID = UserUniqueID("666", "666"),
                    name = "Public Transportation Survey"
                ),
                FieldSurvey(
                    id = UniqueID("survey07"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    creatorId = UserUniqueID("666", "666"),
                    surveyedClientContactID = UniqueID.generateFakeRandomID(),
                    fillingTime = "2019-04-14 10:41:22 AM",
                    isTemplate = false,
                    latitude = 54.23154,
                    longitude = 21.614117,
                    geoLocation = null,
                    firestoreGeoLocation = null,
                    picture1URI = "1",
                    description = "This field survey aims to assess the condition of historical landmarks and cultural heritage sites in Warsaw. It will evaluate factors such as preservation efforts, visitor engagement, and the impact of tourism on cultural sites. The findings will guide conservation efforts and promote cultural heritage tourism.",
                    city = "Warsaw",
                    ownerID = UserUniqueID("666", "666"),
                    name = "Cultural Heritage Survey"
                ),
                FieldSurvey(
                    id = UniqueID("survey08"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    creatorId = UserUniqueID("666", "666"),
                    surveyedClientContactID = UniqueID.generateFakeRandomID(),
                    fillingTime = "2019-04-14 10:41:22 AM",
                    isTemplate = false,
                    latitude = 54.23154,
                    longitude = 21.614117,
                    geoLocation = null,
                    firestoreGeoLocation = null,
                    picture1URI = "1",
                    description = "This field survey aims to study the impact of urbanization on wildlife habitats in Warsaw. It will assess biodiversity, habitat fragmentation, and human-wildlife interactions in urban areas. The findings will inform urban planning decisions to promote coexistence between humans and wildlife.",
                    city = "Warsaw",
                    ownerID = UserUniqueID("666", "666"),
                    name = "Urban Wildlife Survey"
                ),
                FieldSurvey(
                    id = UniqueID("survey09"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    creatorId = UserUniqueID("666", "666"),
                    surveyedClientContactID = UniqueID.generateFakeRandomID(),
                    fillingTime = "2019-04-14 10:41:22 AM",
                    isTemplate = false,
                    latitude = 54.23154,
                    longitude = 21.614117,
                    geoLocation = null,
                    firestoreGeoLocation = null,
                    picture1URI = "1",
                    description = "This field survey aims to assess the state of public health facilities and services in Warsaw. It will evaluate factors such as healthcare access, service quality, and patient satisfaction. The findings will be used to improve healthcare delivery and address the healthcare needs of the population.",
                    city = "Warsaw",
                    ownerID = UserUniqueID("666", "666"),
                    name = "Public Health Facilities Survey"
                ),
                FieldSurvey(
                    id = UniqueID("survey10"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    creatorId = UserUniqueID("666", "666"),
                    surveyedClientContactID = UniqueID.generateFakeRandomID(),
                    fillingTime = "2019-04-14 10:41:22 AM",
                    isTemplate = false,
                    latitude = 54.23154,
                    longitude = 21.614117,
                    geoLocation = null,
                    firestoreGeoLocation = null,
                    picture1URI = "1",
                    description = "This field survey aims to assess the availability and quality of public recreational facilities in Warsaw. It will evaluate parks, playgrounds, sports fields, and other recreational spaces. The findings will guide investment decisions to enhance public recreation opportunities in the city.",
                    city = "Warsaw",
                    ownerID = UserUniqueID("666", "666"),
                    name = "Public Recreation Facilities Survey"
                )
            )
        }
    }
}