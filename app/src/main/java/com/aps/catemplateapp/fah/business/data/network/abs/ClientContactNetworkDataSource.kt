package com.aps.catemplateapp.fah.business.data.network.abs

import com.aps.catemplateapp.common.business.data.network.StandardNetworkDataSource
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.fah.business.domain.model.entities.ClientContact

interface ClientContactNetworkDataSource: StandardNetworkDataSource<ClientContact> {
    override suspend fun insertOrUpdateEntity(entity: ClientContact): ClientContact?

    override suspend fun deleteEntity(primaryKey: UniqueID?)

    override suspend fun insertDeletedEntity(entity: ClientContact)

    override suspend fun insertDeletedEntities(Entities: List<ClientContact>)

    override suspend fun deleteDeletedEntity(entity: ClientContact)

    override suspend fun getDeletedEntities(): List<ClientContact>

    override suspend fun deleteAllEntities()

    override suspend fun searchEntity(entity: ClientContact): ClientContact?

    override suspend fun getAllEntities(): List<ClientContact>

    override suspend fun insertOrUpdateEntities(Entities: List<ClientContact>): List<ClientContact>?

    override suspend fun getEntityById(id: UniqueID): ClientContact?
}