package com.aps.catemplateapp.fah.framework.datasources.cachedatasources.room.abs


import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.fah.business.domain.model.entities.UsersPresentation

interface UsersPresentationDaoService {
    suspend fun insertOrUpdateEntity(entity: UsersPresentation): UsersPresentation

    suspend fun insertEntity(entity: UsersPresentation): Long

    suspend fun insertEntities(Entities: List<UsersPresentation>): LongArray

    suspend fun getEntityById(id: UniqueID?): UsersPresentation?

    suspend fun updateEntity(
        id: UniqueID?,
        updated_at: String?,
        created_at: String?,

        presentationFileURI: String?,

        picture1URI: String?,

        name: String?,
        description: String?,

        ownerID: UniqueID?,
    ): Int

    suspend fun deleteEntity(primaryKey: UniqueID?): Int

    suspend fun deleteEntities(Entities: List<UsersPresentation>): Int

    suspend fun searchEntities(): List<UsersPresentation>

    suspend fun getAllEntities(): List<UsersPresentation>

    suspend fun getNumEntities(): Int
}