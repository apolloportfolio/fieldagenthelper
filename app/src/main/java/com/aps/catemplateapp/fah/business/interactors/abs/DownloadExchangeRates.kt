package com.aps.catemplateapp.fah.business.interactors.abs

import com.aps.catemplateapp.common.business.domain.state.DataState
import com.aps.catemplateapp.common.business.domain.state.StateEvent
import com.aps.catemplateapp.core.business.domain.model.currencies.Currency
import com.aps.catemplateapp.core.business.domain.model.currencies.ExchangeRates
import com.aps.catemplateapp.core.business.domain.model.entities.ProjectUser
import com.aps.catemplateapp.fah.framework.presentation.activity01.state.HomeScreenViewState
import kotlinx.coroutines.flow.Flow

interface DownloadExchangeRates {
    fun downloadExchangeRates(
        baseCurrency: Currency,
        stateEvent : StateEvent,
        onErrorAction: () -> Unit = {},
        returnViewState : HomeScreenViewState<ProjectUser>,
        updateReturnViewState : (
            HomeScreenViewState<ProjectUser>,
            ExchangeRates?,
        ) -> (HomeScreenViewState<ProjectUser>),
    ): Flow<DataState<HomeScreenViewState<ProjectUser>>?>
}