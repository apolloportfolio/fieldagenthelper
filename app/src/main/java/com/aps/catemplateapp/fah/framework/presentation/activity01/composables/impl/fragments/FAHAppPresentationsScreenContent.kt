package com.aps.catemplateapp.fah.framework.presentation.activity01.composables.impl.fragments

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import com.aps.catemplateapp.R
import com.aps.catemplateapp.common.business.domain.state.StateEventTracker
import com.aps.catemplateapp.common.framework.presentation.layouts.LazyColumnWithPullToRefresh
import com.aps.catemplateapp.common.framework.presentation.views.BoxWithBackground
import com.aps.catemplateapp.common.framework.presentation.views.ListItemType03
import com.aps.catemplateapp.common.framework.presentation.views.ProfileStatusBar
import com.aps.catemplateapp.common.framework.presentation.views.SearchBarWithClearButton
import com.aps.catemplateapp.common.framework.presentation.views.TipContentIsUnavailable
import com.aps.catemplateapp.common.framework.presentation.views.TitleRowType01
import com.aps.catemplateapp.common.util.ALog
import com.aps.catemplateapp.common.util.DeviceLocation
import com.aps.catemplateapp.common.util.composableutils.getDrawableIdInPreview
import com.aps.catemplateapp.fah.business.domain.model.entities.UsersPresentation
import com.aps.catemplateapp.fah.business.domain.model.factories.UsersPresentationFactory
import com.aps.catemplateapp.fah.framework.presentation.activity01.composables.impl.values.BackgroundsOfLayoutsFAHApp
import com.aps.catemplateapp.fah.framework.presentation.activity01.composables.impl.values.Dimens
import com.aps.catemplateapp.fah.framework.presentation.activity01.composables.impl.values.HomeScreenTheme

private const val TAG = "FAHAppPresentationsScreenContent"
private const val LOG_ME = true

// HomeScreenCompFragment2
@Composable
fun FAHAppPresentationsScreenContent(
    stateEventTracker: StateEventTracker,
    deviceLocation: DeviceLocation?,
    showProfileStatusBar: Boolean,
    finishVerification: () -> Unit,
    initialUsersPresentationsSearchQuery: String = "",
    currentlyShownUsersPresentations: List<UsersPresentation>?,
    onUsersPresentationsSearchQueryUpdate: (String) -> Unit,
    onUsersPresentationItemClick: (UsersPresentation) -> Unit,
    launchInitStateEvent: () -> Unit,
    showTitleBar: Boolean = true,

    isPreview: Boolean = false,
) {
    if(LOG_ME) ALog.d(TAG, "(): Recomposition start.")
    BoxWithBackground(
        backgroundDrawableId = R.drawable.example_background_1,
        composableBackground = BackgroundsOfLayoutsFAHApp.backgroundScreen01(
            colors = null,
            brush = null,
            shape = null,
            alpha = 0.6f,
        ),
        modifier = Modifier
            .wrapContentHeight(),
    ) {
        Column(
            modifier = Modifier
                .fillMaxSize()
                .padding(Dimens.columnPadding)
        ) {
            var searchQuery by remember { mutableStateOf(initialUsersPresentationsSearchQuery) }
            var searchQueryUpdated by remember { mutableStateOf(false) }

            val titleBackgroundColors = listOf(
                MaterialTheme.colors.surface,
                MaterialTheme.colors.primaryVariant,
                MaterialTheme.colors.primary,
            )
            if(showTitleBar) {
                TitleRowType01(
                    titleString = stringResource(id = R.string.your_presentations),
                    textAlign = TextAlign.Center,
                    composableBackground = BackgroundsOfLayoutsFAHApp.backgroundScreen01(
                        colors = titleBackgroundColors,
                        brush = null,
                        shape = null,
                        alpha = 0.7f,
                    ),
                    leftPictureDrawableId = null,
                    titleFontSize = Dimens.titleFontSize,
                    leftImageTint = MaterialTheme.colors.secondary,
                    titleColor = MaterialTheme.colors.secondary,
                    modifier = Modifier
                        .fillMaxWidth()
                )
            }

            val searchBarBackgroundColors = listOf(
                MaterialTheme.colors.surface,
                MaterialTheme.colors.primaryVariant,
                MaterialTheme.colors.primary,
            )

            SearchBarWithClearButton(
                searchQuery = searchQuery,
                onSearchQueryChange = { query -> searchQuery = query },
                onSearchBackClick = {
                    searchQuery = ""
                    searchQueryUpdated = true
                },
                onSearchClick = {
                    searchQueryUpdated = true
                },
                composableBackground = BackgroundsOfLayoutsFAHApp.backgroundScreen01(
                    colors = searchBarBackgroundColors,
                    brush = null,
                    shape = null,
                    alpha = 0.6f,
                ),
            )

            if(LOG_ME) ALog.d(TAG, "(): " +
                    "showProfileStatusBar = $showProfileStatusBar")
            if(showProfileStatusBar) {
                if(LOG_ME) ALog.d(TAG, "(): " +
                        "Showing profile completion bar")

                val profileStatusBarBackgroundColors = listOf(
                    MaterialTheme.colors.surface,
                    MaterialTheme.colors.primaryVariant,
                    MaterialTheme.colors.primary,
                )
                ProfileStatusBar(
                    finishVerification,
                    composableBackground = BackgroundsOfLayoutsFAHApp.backgroundScreen01(
                        colors = profileStatusBarBackgroundColors,
                        brush = null,
                        shape = null,
                        alpha = 0.6f,
                    ),
                    titleTint = MaterialTheme.colors.secondary,
                    subtitleTint = MaterialTheme.colors.error,
                )
            }

            if(currentlyShownUsersPresentations?.isNotEmpty() == true) {
                val userPresentationsBackgroundColors = listOf(
                    MaterialTheme.colors.surface,
                    MaterialTheme.colors.surface,
                    MaterialTheme.colors.surface,
                    MaterialTheme.colors.surface,
                    MaterialTheme.colors.surface,
                    MaterialTheme.colors.surface,
                    MaterialTheme.colors.primary,
                )
                LazyColumnWithPullToRefresh(
                    onRefresh = launchInitStateEvent,
                    isRefreshing = stateEventTracker.isRefreshing,
                ) {
                    itemsIndexed(currentlyShownUsersPresentations) { index, item ->
                        ListItemType03(
                            index = index,
                            itemTitleString = item.name,
                            itemPriceString = "",
                            itemLikesString = "",
                            itemDistanceString = "",
                            lblSmallShortAttribute3String = "",
                            itemDescriptionString = item.description,
                            onListItemClick = { onUsersPresentationItemClick(item) },
                            getItemsRating = { null },
                            itemRef = if(isPreview) {
                                null
                            } else {
                                item.picture1FirebaseImageRef
                            },
                            itemDrawableId = if(isPreview) {
                                getDrawableIdInPreview("fah_app_example_field_survey_", index)
                            } else { null },
                            imageTint = null,
                            backgroundDrawableId = null,
                            composableBackground = BackgroundsOfLayoutsFAHApp.backgroundScreen01(
                                colors = userPresentationsBackgroundColors,
                                brush = null,
                                shape = null,
                                alpha = 0.6f,
                            ),
                        )
                    }
                }
            } else {
                TipContentIsUnavailable(stringResource(id = R.string.home_screen_fragment1_list_is_empty_tip))
            }

            var firstComposition by rememberSaveable { mutableStateOf(true) }
            LaunchedEffect(searchQueryUpdated) {
                if(firstComposition) {
                    if(LOG_ME) ALog.d(TAG, "LaunchedEffect(searchFiltersUpdated): " +
                            "This is the first composition.")
                    firstComposition = false
                } else {
                    if(searchQueryUpdated) {
                        if(LOG_ME) ALog.d(TAG, "(): LaunchedEffect: searchQueryUpdated")
                        onUsersPresentationsSearchQueryUpdate(searchQuery)
                    }
                }
                searchQueryUpdated = false
            }

            if(LOG_ME) ALog.d(TAG, "(): Recomposition start.")
        }
    }

}



@Preview
@Composable
private fun FAHAppPresentationsScreenContentPreview1() {
    HomeScreenTheme {
        FAHAppPresentationsScreenContent(
            stateEventTracker = StateEventTracker.PreviewStateEventTracker,
            deviceLocation = DeviceLocation.PreviewDeviceLocation,
            showProfileStatusBar = true,
            finishVerification = {},
            initialUsersPresentationsSearchQuery = "",
            currentlyShownUsersPresentations = UsersPresentationFactory.createPreviewEntitiesList(),
            onUsersPresentationsSearchQueryUpdate = {},
            onUsersPresentationItemClick = {},
            launchInitStateEvent = {},

            isPreview = true,
        )
    }
}

@Preview
@Composable
private fun FAHAppPresentationsScreenContentPreview2() {
    HomeScreenTheme {
        FAHAppPresentationsScreenContent(
            stateEventTracker = StateEventTracker.PreviewStateEventTracker,
            deviceLocation = DeviceLocation.PreviewDeviceLocation,
            showProfileStatusBar = false,
            finishVerification = {},
            initialUsersPresentationsSearchQuery = "",
            currentlyShownUsersPresentations = UsersPresentationFactory.createPreviewEntitiesList(),
            onUsersPresentationsSearchQueryUpdate = {},
            onUsersPresentationItemClick = {},
            launchInitStateEvent = {},

            isPreview = true,
        )
    }
}