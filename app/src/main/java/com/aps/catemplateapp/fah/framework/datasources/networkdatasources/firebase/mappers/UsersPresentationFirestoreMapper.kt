package com.aps.catemplateapp.fah.framework.datasources.networkdatasources.firebase.mappers

import com.aps.catemplateapp.common.business.domain.model.EntityMapper
import com.aps.catemplateapp.fah.business.domain.model.entities.UsersPresentation
import com.aps.catemplateapp.fah.framework.datasources.networkdatasources.firebase.model.UsersPresentationFirestoreEntity
import javax.inject.Inject

class UsersPresentationFirestoreMapper
@Inject
constructor() : EntityMapper<UsersPresentationFirestoreEntity, UsersPresentation> {
    override fun mapFromEntity(entity: UsersPresentationFirestoreEntity): UsersPresentation {
        return UsersPresentation(
            entity.id,
            entity.created_at,
            entity.updated_at,

            entity.presentationFileURI,

            entity.picture1URI,

            entity.name,
            entity.description,

            entity.ownerID,
        )
    }

    override fun mapToEntity(domainModel: UsersPresentation): UsersPresentationFirestoreEntity {
        return UsersPresentationFirestoreEntity(
            domainModel.id,
            domainModel.created_at,
            domainModel.updated_at,
            domainModel.presentationFileURI,
            domainModel.picture1URI,
            domainModel.name,
            domainModel.description,
            domainModel.ownerID,
        )
    }

    override fun mapFromEntityList(entities : List<UsersPresentationFirestoreEntity>) : List<UsersPresentation> {
        return entities.map{mapFromEntity(it)}
    }

    override fun mapToEntityList(entities: List<UsersPresentation>): List<UsersPresentationFirestoreEntity> {
        return entities.map{mapToEntity(it)}
    }
}