package com.aps.catemplateapp.fah.framework.datasources.cachedatasources.room.model

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.aps.catemplateapp.common.util.ParcelableGeoPoint
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID

@Entity(tableName = "fieldsurvey")
data class FieldSurveyCacheEntity(
    @PrimaryKey(autoGenerate = false)
    @ColumnInfo(name = "id")
    @NonNull
    var id : UniqueID,

    @ColumnInfo(name = "created_at")
    val created_at: String?,

    @ColumnInfo(name = "updated_at")
    val updated_at: String?,


    @ColumnInfo(name = "creatorId")
    var creatorId: UserUniqueID?,

    @ColumnInfo(name = "surveyedClientContactID")
    var surveyedClientContactID: UniqueID?,

    @ColumnInfo(name = "fillingTime")
    var fillingTime: String?,

    @ColumnInfo(name = "isTemplate")
    var isTemplate: Boolean?,

    @ColumnInfo(name = "latitude")
    var latitude : Double?,

    @ColumnInfo(name = "longitude")
    var longitude: Double?,

    @ColumnInfo(name = "geoLocation")
    var geoLocation: ParcelableGeoPoint?,

    @ColumnInfo(name = "firestoreGeoLocation")
    var firestoreGeoLocation: Double?,

    @ColumnInfo(name = "picture1URI")
    var picture1URI: String?,

    @ColumnInfo(name = "description")
    var description : String?,

    @ColumnInfo(name = "city")
    var city : String?,

    @ColumnInfo(name = "ownerID")
    var ownerID: UserUniqueID?,

    @ColumnInfo(name = "name")
    var name : String?,

    @ColumnInfo(name = "switch1")
    var switch1 : Boolean?,

    @ColumnInfo(name = "switch2")
    var switch2 : Boolean?,

    @ColumnInfo(name = "switch3")
    var switch3 : Boolean?,

    @ColumnInfo(name = "switch4")
    var switch4 : Boolean?,

    @ColumnInfo(name = "switch5")
    var switch5 : Boolean?,

    @ColumnInfo(name = "switch6")
    var switch6 : Boolean?,

    @ColumnInfo(name = "switch7")
    var switch7 : Boolean?,

    ) {
}