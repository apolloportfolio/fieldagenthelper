package com.aps.catemplateapp.fah.business.data.network.abs

import android.net.Uri
import com.aps.catemplateapp.common.business.data.network.StandardNetworkDataSource
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.fah.business.domain.model.entities.FieldSurvey
import com.aps.catemplateapp.fah.business.interactors.impl.FirestoreEntity1SearchParameters

interface FieldSurveyNetworkDataSource: StandardNetworkDataSource<FieldSurvey> {
    override suspend fun insertOrUpdateEntity(entity: FieldSurvey): FieldSurvey?

    override suspend fun deleteEntity(primaryKey: UniqueID?)

    override suspend fun insertDeletedEntity(entity: FieldSurvey)

    override suspend fun insertDeletedEntities(Entities: List<FieldSurvey>)

    override suspend fun deleteDeletedEntity(entity: FieldSurvey)

    override suspend fun getDeletedEntities(): List<FieldSurvey>

    override suspend fun deleteAllEntities()

    override suspend fun searchEntity(entity: FieldSurvey): FieldSurvey?

    override suspend fun getAllEntities(): List<FieldSurvey>

    override suspend fun insertOrUpdateEntities(Entities: List<FieldSurvey>): List<FieldSurvey>?

    override suspend fun getEntityById(id: UniqueID): FieldSurvey?

    suspend fun searchEntities(
        searchParameters : FirestoreEntity1SearchParameters
    ) : List<FieldSurvey>?

    suspend fun getUsersFieldSurveyTemplates(userID: UserUniqueID) : List<FieldSurvey>?

    suspend fun uploadEntity1PhotoToFirestore(
        entity : FieldSurvey,
        entitysPhotoUri : Uri,
        entitysPhotoNumber: Int,
    ) : String?
}