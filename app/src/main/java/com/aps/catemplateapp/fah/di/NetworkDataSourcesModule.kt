package com.aps.catemplateapp.fah.di

import com.aps.catemplateapp.fah.business.data.network.abs.FieldSurveyNetworkDataSource
import com.aps.catemplateapp.fah.business.data.network.abs.UsersPresentationNetworkDataSource
import com.aps.catemplateapp.fah.business.data.network.abs.SurveyQuestionNetworkDataSource
import com.aps.catemplateapp.fah.business.data.network.abs.ClientContactNetworkDataSource
import com.aps.catemplateapp.fah.business.data.network.impl.FieldSurveyNetworkDataSourceImpl
import com.aps.catemplateapp.fah.business.data.network.impl.UsersPresentationNetworkDataSourceImpl
import com.aps.catemplateapp.fah.business.data.network.impl.SurveyQuestionNetworkDataSourceImpl
import com.aps.catemplateapp.fah.business.data.network.impl.ClientContactNetworkDataSourceImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
abstract class NetworkDataSourcesModule {

    @Binds
    abstract fun bindFieldSurveyNetworkDataSource(implementation: FieldSurveyNetworkDataSourceImpl): FieldSurveyNetworkDataSource

    @Binds
    abstract fun bindUsersPresentationNetworkDataSource(implementation: UsersPresentationNetworkDataSourceImpl): UsersPresentationNetworkDataSource

    @Binds
    abstract fun bindSurveyQuestionNetworkDataSource(implementation: SurveyQuestionNetworkDataSourceImpl): SurveyQuestionNetworkDataSource

    @Binds
    abstract fun bindClientContactNetworkDataSource(implementation: ClientContactNetworkDataSourceImpl): ClientContactNetworkDataSource

}