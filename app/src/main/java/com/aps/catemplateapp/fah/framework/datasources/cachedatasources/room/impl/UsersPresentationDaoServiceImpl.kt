package com.aps.catemplateapp.fah.framework.datasources.cachedatasources.room.impl

import com.aps.catemplateapp.common.util.DateUtil
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.fah.business.domain.model.entities.UsersPresentation
import com.aps.catemplateapp.fah.framework.datasources.cachedatasources.room.abs.UsersPresentationDaoService
import com.aps.catemplateapp.fah.framework.datasources.cachedatasources.room.daos.UsersPresentationDao
import com.aps.catemplateapp.fah.framework.datasources.cachedatasources.room.mappers.UsersPresentationCacheMapper
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class UsersPresentationDaoServiceImpl
@Inject
constructor(
    private val dao: UsersPresentationDao,
    private val mapper: UsersPresentationCacheMapper,
    private val dateUtil: DateUtil
): UsersPresentationDaoService {

    override suspend fun insertOrUpdateEntity(entity: UsersPresentation): UsersPresentation {
        val databaseEntity = dao.getEntityById(entity.id)
        if(databaseEntity != null) {
            dao.insert(mapper.mapToEntity(entity))
            return mapper.mapFromEntity(databaseEntity)
        } else {
            this.updateEntity(
                entity.id,
                entity.updated_at,
                entity.created_at,

                entity.presentationFileURI,

                entity.picture1URI,

                entity.name,
                entity.description,

                entity.ownerID,
            )
            return mapper.mapFromEntity(dao.getEntityById(entity.id)!!)
        }
    }


    override suspend fun insertEntity(entity: UsersPresentation): Long {
        return dao.insert(mapper.mapToEntity(entity))
    }

    override suspend fun insertEntities(entities: List<UsersPresentation>): LongArray {
        return dao.insertEntities(
            mapper.mapToEntityList(entities)
        )
    }

    override suspend fun getEntityById(id: UniqueID?): UsersPresentation? {
        return dao.getEntityById(id)?.let { entity ->
            mapper.mapFromEntity(entity)
        }
    }

    override suspend fun updateEntity(
        id: UniqueID?,
        updated_at: String?,
        created_at: String?,

        presentationFileURI: String?,

        picture1URI: String?,

        name: String?,
        description: String?,

        ownerID: UniqueID?,
    ): Int {
        return if(updated_at != null){
            dao.updateEntity(
                id,
                updated_at,
                created_at,

                presentationFileURI,

                picture1URI,

                name,
                description,

                ownerID,
            )
        } else {
            dao.updateEntity(
                id,
                dateUtil.getCurrentTimestamp(),
                created_at,

                presentationFileURI,

                picture1URI,

                name,
                description,

                ownerID,
            )
        }

    }

    override suspend fun deleteEntity(primaryKey: UniqueID?): Int {
        return dao.deleteEntity(primaryKey)
    }

    override suspend fun deleteEntities(entities: List<UsersPresentation>): Int {
        val ids = entities.mapIndexed {index, value -> value.id}
        return dao.deleteEntities(ids)
    }

    override suspend fun searchEntities(): List<UsersPresentation> {
        return mapper.mapFromEntityList(
            dao.searchEntities()
        )
    }

    override suspend fun getAllEntities(): List<UsersPresentation> {
        return mapper.mapFromEntityList(dao.getAllEntities())
    }

    override suspend fun getNumEntities(): Int {
        return dao.getNumEntities()
    }
}