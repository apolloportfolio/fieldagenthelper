package com.aps.catemplateapp.fah.business.data.cache.implementation

import com.aps.catemplateapp.common.util.ParcelableGeoPoint
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.fah.business.data.cache.abstraction.FieldSurveyCacheDataSource
import com.aps.catemplateapp.fah.business.domain.model.entities.FieldSurvey
import com.aps.catemplateapp.fah.framework.datasources.cachedatasources.room.abs.FieldSurveyDaoService
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class FieldSurveyCacheDataSourceImpl
@Inject
constructor(
    private val entityDaoService: FieldSurveyDaoService
): FieldSurveyCacheDataSource {

    override suspend fun insertOrUpdateEntity(entity: FieldSurvey): FieldSurvey? {
        return entityDaoService.insertOrUpdateEntity(entity)
    }

    override suspend fun insertEntity(entity: FieldSurvey): Long {
        return entityDaoService.insertEntity(entity)
    }

    override suspend fun deleteEntity(primaryKey: UniqueID?): Int {
        return entityDaoService.deleteEntity(primaryKey)
    }

    override suspend fun deleteEntities(entities: List<FieldSurvey>): Int {
        return entityDaoService.deleteEntities(entities)
    }

    override suspend fun updateEntity(
        id: UniqueID?,
        created_at: String?,
        updated_at: String?,

        creatorId: UserUniqueID?,
        surveyedClientContactID: UniqueID?,
        fillingTime: String?,
        isTemplate: Boolean,

        lattitude : Double?,
        longitude: Double?,
        geoLocation: ParcelableGeoPoint?,
        firestoreGeoLocation: Double?,

        picture1URI: String?,
        description: String?,

        city: String?,

        ownerID: UserUniqueID?,

        name: String?,

        switch1: Boolean?,
        switch2: Boolean?,
        switch3: Boolean?,
        switch4: Boolean?,
        switch5: Boolean?,
        switch6: Boolean?,
        switch7: Boolean?,
    ): Int {
        return entityDaoService.updateEntity(
            id,
            created_at,
            updated_at,

            creatorId,
            surveyedClientContactID,
            fillingTime,
            isTemplate,

            lattitude,
            longitude,
            geoLocation,
            firestoreGeoLocation,

            picture1URI,
            description,
            city,
            ownerID,
            name,
            
            switch1,
            switch2,
            switch3,
            switch4,
            switch5,
            switch6,
            switch7,
        )
    }

    override suspend fun searchEntities(
        query: String,
        filterAndOrder: String,
        page: Int
    ): List<FieldSurvey> {
        return entityDaoService.searchEntities()
    }

    override suspend fun getAllEntities(): List<FieldSurvey> {
        return entityDaoService.getAllEntities()
    }

    override suspend fun getEntityById(id: UniqueID?): FieldSurvey? {
        return entityDaoService.getEntityById(id)
    }

    override suspend fun getNumEntities(): Int {
        return entityDaoService.getNumEntities()
    }

    override suspend fun insertEntities(entities: List<FieldSurvey>): LongArray{
        return entityDaoService.insertEntities(entities)
    }
}