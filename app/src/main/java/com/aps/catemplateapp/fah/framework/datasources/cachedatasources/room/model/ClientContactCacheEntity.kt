package com.aps.catemplateapp.fah.framework.datasources.cachedatasources.room.model

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.aps.catemplateapp.common.util.ParcelableGeoPoint
import com.aps.catemplateapp.common.util.UniqueID

@Entity(tableName = "clientcontact")
data class ClientContactCacheEntity(
    @PrimaryKey(autoGenerate = false)
    @ColumnInfo(name = "id")
    @NonNull
    var id : UniqueID,

    @ColumnInfo(name = "created_at")
    val created_at: String?,

    @ColumnInfo(name = "updated_at")
    val updated_at: String?,

    @ColumnInfo(name = "surname")
    val surname: String?,

    @ColumnInfo(name = "title")
    val title: String?,

    @ColumnInfo(name = "phoneNumber")
    val phoneNumber: String?,

    @ColumnInfo(name = "emailAddress")
    val emailAddress: String?,

    @ColumnInfo(name = "latitude")
    var latitude : Double?,

    @ColumnInfo(name = "longitude")
    var longitude: Double?,

    @ColumnInfo(name = "geoLocation")
    var geoLocation: ParcelableGeoPoint?,

    @ColumnInfo(name = "firestoreGeoLocation")
    var firestoreGeoLocation: Double?,

    @ColumnInfo(name = "picture1URI")
    var picture1URI: String?,

    @ColumnInfo(name = "name")
    var name : String?,

    @ColumnInfo(name = "description")
    var description : String?,

    ) {
}