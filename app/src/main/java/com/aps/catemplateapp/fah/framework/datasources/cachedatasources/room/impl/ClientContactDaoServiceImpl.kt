package com.aps.catemplateapp.fah.framework.datasources.cachedatasources.room.impl

import com.aps.catemplateapp.common.util.ALog
import com.aps.catemplateapp.common.util.DateUtil
import com.aps.catemplateapp.common.util.ParcelableGeoPoint
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.fah.business.domain.model.entities.ClientContact
import com.aps.catemplateapp.fah.framework.datasources.cachedatasources.room.abs.ClientContactDaoService
import com.aps.catemplateapp.fah.framework.datasources.cachedatasources.room.daos.ClientContactDao
import com.aps.catemplateapp.fah.framework.datasources.cachedatasources.room.mappers.ClientContactCacheMapper
import javax.inject.Inject
import javax.inject.Singleton

private const val TAG = "ClientContactDaoServiceImpl"
private const val LOG_ME = true

@Singleton
class ClientContactDaoServiceImpl
@Inject
constructor(
    private val dao: ClientContactDao,
    private val mapper: ClientContactCacheMapper,
    private val dateUtil: DateUtil
): ClientContactDaoService {

    override suspend fun insertOrUpdateEntity(entity: ClientContact): ClientContact {
        val databaseEntity = dao.getEntityById(entity.id)
        if(LOG_ME) ALog.d(TAG, ".insertOrUpdateEntity(): databaseEntity == $databaseEntity")
        if(databaseEntity == null) {
            if(LOG_ME)ALog.d(TAG, ".insertOrUpdateEntity(): entity before inserting:\n$entity")
            dao.insert(mapper.mapToEntity(entity))
            val entityFromDao = dao.getEntityById(entity.id)
            if(LOG_ME) ALog.d(TAG, ".insertOrUpdateEntity(): entity after inserting == ${entityFromDao}\n")
            return mapper.mapFromEntity(entityFromDao!!)
        } else {
            this.updateEntity(
                entity.id,
                entity.created_at,
                entity.updated_at,

                entity.surname,
                entity.title,
                entity.phoneNumber,
                entity.emailAddress,

                entity.latitude ,
                entity.longitude,
                entity.geoLocation,
                entity.firestoreGeoLocation,

                entity.picture1URI,
                entity.name,
                entity.description,
            )
                val entityFromDao = dao.getEntityById(entity.id)
            if(LOG_ME) ALog.d(TAG, ".insertOrUpdateEntity(): entityFromDao == $entityFromDao")
            return mapper.mapFromEntity(entityFromDao!!)
        }
    }

    override suspend fun insertEntity(entity: ClientContact): Long {
        return dao.insert(mapper.mapToEntity(entity))
    }

    override suspend fun insertEntities(entities: List<ClientContact>): LongArray {
        return dao.insertEntities(
            mapper.mapToEntityList(entities)
        )
    }

    override suspend fun getEntityById(id: UniqueID?): ClientContact? {
        return dao.getEntityById(id)?.let { entity ->
            mapper.mapFromEntity(entity)
        }
    }

    override suspend fun updateEntity(
        id: UniqueID?,
        created_at: String?,
        updated_at: String?,

        surname: String?,
        title: String?,
        phoneNumber: String?,
        emailAddress: String?,

        latitude : Double?,
        longitude: Double?,
        geoLocation: ParcelableGeoPoint?,
        firestoreGeoLocation: Double?,

        picture1URI: String?,
        name: String?,
        description : String?,
    ): Int {
        return if(updated_at != null){
            dao.updateEntity(
                id,
                created_at,
                updated_at,

                surname,
                title,
                phoneNumber,
                emailAddress,

                latitude,
                longitude,
                geoLocation,
                firestoreGeoLocation,

                picture1URI,
                name,
                description,
            )
        }else{
            dao.updateEntity(
                id,
                created_at,
                dateUtil.getCurrentTimestamp(),

                surname,
                title,
                phoneNumber,
                emailAddress,

                latitude,
                longitude,
                geoLocation,
                firestoreGeoLocation,

                picture1URI,
                name,
                description,
            )
        }

    }

    override suspend fun deleteEntity(primaryKey: UniqueID?): Int {
        return dao.deleteEntity(primaryKey)
    }

    override suspend fun deleteEntities(entities: List<ClientContact>): Int {
        val ids = entities.mapIndexed {index, value -> value.id}
        return dao.deleteEntities(ids)
    }

    override suspend fun searchEntities(): List<ClientContact> {
        return mapper.mapFromEntityList(
            dao.searchEntities()
        )
    }

    override suspend fun getAllEntities(): List<ClientContact> {
        return mapper.mapFromEntityList(dao.getAllEntities())
    }

    override suspend fun getNumEntities(): Int {
        return dao.getNumEntities()
    }
}