package com.aps.catemplateapp.fah.framework.presentation.activity01.state

//import kotlinx.android.parcel.Parcelize
import android.os.Parcelable
import com.aps.catemplateapp.common.business.domain.state.ViewState
import com.aps.catemplateapp.common.util.ALog
import com.aps.catemplateapp.common.util.DeviceLocation
import com.aps.catemplateapp.common.util.ObjectSizeCalculator
import com.aps.catemplateapp.core.business.domain.model.currencies.ExchangeRates
import com.aps.catemplateapp.fah.business.domain.model.entities.FieldSurvey
import com.aps.catemplateapp.fah.business.domain.model.entities.UsersPresentation
import com.aps.catemplateapp.fah.business.domain.model.entities.ClientContact
import com.aps.catemplateapp.core.business.domain.model.entities.ProjectUsersRating
import com.aps.catemplateapp.fah.framework.presentation.activity01.composables.HomeScreenDestination
import kotlinx.parcelize.Parcelize
import java.util.Date

private const val TAG = "HomeScreenViewState"
private const val LOG_ME = true

@Parcelize
data class HomeScreenViewState<T>(
    private var instanceNumber: Int? = null,                // For some reason Jetpack Compose didn't consider a copy of this class's object to be different than the original, which prevented recomposition. This property fixed that.
    // Card 1
    var card1CurrentlyShownEntity: FieldSurvey? = null,
    var location: DeviceLocation? = null,
    var searchedEntities1List: ArrayList<FieldSurvey>? = null,
    var refreshListOfSearchedEntities1: Boolean = false,
    var entity1SearchQuery: String? = null,
    var searchedEntities1ListRefreshDate: Date? = null,
    // Card 1 Entity1 search filters
    var range1min: Double? = null,
    var range1max: Double? = null,
    var range2min: Double? = null,
    var range2max: Double? = null,
    var switch1: Boolean? = null,
    var switch2: Boolean? = null,
    var switch3: Boolean? = null,
    var switch4: Boolean? = null,
    var switch5: Boolean? = null,
    var switch6: Boolean? = null,
    var switch7: Boolean? = null,

    // Card 2
    var card2CurrentlyShownEntity: UsersPresentation? = null,
    var entities2List: ArrayList<UsersPresentation>? = null,
    var refreshListOfEntities2: Boolean = false,
    var usersPresentationsSearchQuery: String? = null,

    // Card 3
    var card3CurrentlyShownEntity: FieldSurvey? = null,
    var usersSurveyTemplates: ArrayList<FieldSurvey>? = null,
    var refreshListOfEntities3: Boolean = false,
    var fieldSurveysTemplatesSearchQuery: String? = null,

    // Card 4
    var card4CurrentlyShownEntity: ClientContact? = null,
    var entities4List: ArrayList<ClientContact>? = null,
    var refreshListOfEntities4: Boolean = false,
    var clientContactsSearchQuery: String? = null,

    // Card 5
    var usersName: String? = null,
    var usersSurname: String? = null,
    var usersCity: String? = null,
    var usersDescription: String? = null,
    var usersProfilePhotoImageURI: String? = null,
    var usersRating: ProjectUsersRating? = null,

    // Navigation flags
    var navigateToStartScreen: Boolean? = null,
    var previousComposableDestination: HomeScreenDestination? = null,
    var currentComposableDestination: HomeScreenDestination = HomeScreenDestination.Card1,
    var wasDualPane: Boolean = false,
    var isDualPane: Boolean = false,

    // Google pay
    var googlePayIsAvailable: Boolean? = null,
    var merchantName: String? = null,
    var gatewayName: String? = null,
    var gatewayMerchantID: String? = null,
    var exchangeRates: ExchangeRates? = null,

    ) : Parcelable, ViewState<T>(), java.io.Serializable {
    override fun copy(): HomeScreenViewState<T> {
        return HomeScreenViewState<T>().apply {
            // Copy properties from the ViewState superclass
            mainEntity = this@HomeScreenViewState.mainEntity
            listOfEntities = ArrayList(this@HomeScreenViewState.listOfEntities.orEmpty())
            isUpdatePending = this@HomeScreenViewState.isUpdatePending
            isInternetAvailable = this@HomeScreenViewState.isInternetAvailable
            isNetworkRepositoryAvailable = this@HomeScreenViewState.isNetworkRepositoryAvailable
            noCachedEntity = this@HomeScreenViewState.noCachedEntity
            snackBarState = this@HomeScreenViewState.snackBarState?.copy()
            toastState = this@HomeScreenViewState.toastState?.shallowCopy()
            dialogState = this@HomeScreenViewState.dialogState?.copy()
            progressIndicatorState = this@HomeScreenViewState.progressIndicatorState?.copy()
            permissionHandlingData = this@HomeScreenViewState.permissionHandlingData.copy()


            // Copy properties specific to HomeScreenViewState
            usersPresentationsSearchQuery = this@HomeScreenViewState.usersPresentationsSearchQuery
            fieldSurveysTemplatesSearchQuery = this@HomeScreenViewState.fieldSurveysTemplatesSearchQuery
            clientContactsSearchQuery = this@HomeScreenViewState.clientContactsSearchQuery
            card1CurrentlyShownEntity = this@HomeScreenViewState.card1CurrentlyShownEntity
            location = this@HomeScreenViewState.location?.copy()
            searchedEntities1List = ArrayList(this@HomeScreenViewState.searchedEntities1List.orEmpty())
            refreshListOfSearchedEntities1 = this@HomeScreenViewState.refreshListOfSearchedEntities1
            entity1SearchQuery = this@HomeScreenViewState.entity1SearchQuery
            searchedEntities1ListRefreshDate = this@HomeScreenViewState.searchedEntities1ListRefreshDate
            range1min = this@HomeScreenViewState.range1min
            range1max = this@HomeScreenViewState.range1max
            range2min = this@HomeScreenViewState.range2min
            range2max = this@HomeScreenViewState.range2max
            switch1 = this@HomeScreenViewState.switch1
            switch2 = this@HomeScreenViewState.switch2
            switch3 = this@HomeScreenViewState.switch3
            switch4 = this@HomeScreenViewState.switch4
            switch5 = this@HomeScreenViewState.switch5
            switch6 = this@HomeScreenViewState.switch6
            switch7 = this@HomeScreenViewState.switch7
            card2CurrentlyShownEntity = this@HomeScreenViewState.card2CurrentlyShownEntity
            entities2List = ArrayList(this@HomeScreenViewState.entities2List.orEmpty())
            refreshListOfEntities2 = this@HomeScreenViewState.refreshListOfEntities2
            card3CurrentlyShownEntity = this@HomeScreenViewState.card3CurrentlyShownEntity
            usersSurveyTemplates = ArrayList(this@HomeScreenViewState.usersSurveyTemplates.orEmpty())
            refreshListOfEntities3 = this@HomeScreenViewState.refreshListOfEntities3
            card4CurrentlyShownEntity = this@HomeScreenViewState.card4CurrentlyShownEntity
            entities4List = ArrayList(this@HomeScreenViewState.entities4List.orEmpty())
            refreshListOfEntities4 = this@HomeScreenViewState.refreshListOfEntities4
            usersName = this@HomeScreenViewState.usersName
            usersSurname = this@HomeScreenViewState.usersSurname
            usersCity = this@HomeScreenViewState.usersCity
            usersDescription = this@HomeScreenViewState.usersDescription
            usersProfilePhotoImageURI = this@HomeScreenViewState.usersProfilePhotoImageURI
            usersRating = this@HomeScreenViewState.usersRating
            navigateToStartScreen = this@HomeScreenViewState.navigateToStartScreen
            previousComposableDestination = this@HomeScreenViewState.previousComposableDestination
            currentComposableDestination = this@HomeScreenViewState.currentComposableDestination
            wasDualPane = this@HomeScreenViewState.wasDualPane
            isDualPane = this@HomeScreenViewState.isDualPane
            googlePayIsAvailable = this@HomeScreenViewState.googlePayIsAvailable
            merchantName = this@HomeScreenViewState.merchantName
            gatewayName = this@HomeScreenViewState.gatewayName
            gatewayMerchantID = this@HomeScreenViewState.gatewayMerchantID
            exchangeRates = this@HomeScreenViewState.exchangeRates
        }
    }
    override fun describeContents(): Int {
        return 0
    }

    fun clearEntity1SearchFilters() {
        range1min = null
        range1max = null
        range2min = null
        range2max = null
        switch1 = null
        switch2 = null
        switch3 = null
        switch4 = null
        switch5 = null
        switch6 = null
        switch7 = null
    }
    
    fun getHomeScreenCard1SearchFilters(): HomeScreenCard1SearchFilters {
        return HomeScreenCard1SearchFilters(
            range1min = this.range1min,
            range1max = this.range1max,
            range2min = this.range2min,
            range2max = this.range2max,
            switch1 = this.switch1,
            switch2 = this.switch2,
            switch3 = this.switch3,
            switch4 = this.switch4,
            switch5 = this.switch5,
            switch6 = this.switch6,
            switch7 = this.switch7,
        )
    }

    fun setEntity1SearchFilters(filters: HomeScreenCard1SearchFilters) {
        range1min = filters.range1min
        range1max = filters.range1max
        range2min = filters.range2min
        range2max = filters.range2max
        switch1 = filters.switch1
        switch2 = filters.switch2
        switch3 = filters.switch3
        switch4 = filters.switch4
        switch5 = filters.switch5
        switch6 = filters.switch6
        switch7 = filters.switch7
    }

    fun printProgressIndicatorState(): String {
        val instance = this
        return buildString {
            appendLine("HomeScreenViewState (${System.identityHashCode(instance)})(${instance.hashCode()}):")
            appendLine("progressIndicatorState: (${System.identityHashCode(progressIndicatorState)})(${progressIndicatorState.hashCode()})")
//            appendLine(progressIndicatorState)
            appendLine(entity1SearchQuery)
        }
    }
    fun printEntity1SearchQuery(): String {
        val instance = this
        return buildString {
            appendLine("HomeScreenViewState (${System.identityHashCode(instance)})(${instance.hashCode()}):")
            appendLine(entity1SearchQuery)
        }
    }

    fun logHashCode() {
        val thisHashCode = hashCode()
        val superHashCode = super.hashCode()
        if(LOG_ME)ALog.d(TAG, "logHashCode(): " +
                                "thisHashCode == $thisHashCode " +
                "superHashCode == $superHashCode")
    }
    companion object {
        private var instanceCount = 0

        fun getInstanceCount(): Int {
            return instanceCount
        }
    }

    init {
        instanceCount++
        if(this.instanceNumber == null)instanceNumber = instanceCount
    }

//    override fun hashCode(): Int {
//        return instanceNumber ?: super.hashCode()
//    }

    override fun toString(): String {
        val instance = this
        return buildString {
            appendLine(super.toString())
            appendLine("HomeScreenViewState (${System.identityHashCode(instance)})(${instance.hashCode()}):")
            appendLine("Card 1:")
            appendLine("card1CurrentlyShownEntity: $card1CurrentlyShownEntity")
            appendLine("location: $location")
            appendLine("searchedEntities1List: $searchedEntities1List")
            appendLine("refreshListOfSearchedEntities1: $refreshListOfSearchedEntities1")
            appendLine("entity1SearchQuery: $entity1SearchQuery")
            appendLine("searchedEntities1ListRefreshDate: $searchedEntities1ListRefreshDate")
            appendLine("range1min: $range1min")
            appendLine("range1max: $range1max")
            appendLine("range2min: $range2min")
            appendLine("range2max: $range2max")
            appendLine("switch1: $switch1")
            appendLine("switch2: $switch2")
            appendLine("switch3: $switch3")
            appendLine("switch4: $switch4")
            appendLine("switch5: $switch5")
            appendLine("switch6: $switch6")
            appendLine("switch7: $switch7")
            appendLine("Card 2:")
            appendLine("card2CurrentlyShownEntity: $card2CurrentlyShownEntity")
            appendLine("entities2List: $entities2List")
            appendLine("refreshListOfEntities2: $refreshListOfEntities2")
            appendLine("Card 3:")
            appendLine("card3CurrentlyShownEntity: $card3CurrentlyShownEntity")
            appendLine("entities3List: $usersSurveyTemplates")
            appendLine("refreshListOfEntities3: $refreshListOfEntities3")
            appendLine("Card 4:")
            appendLine("card4CurrentlyShownEntity: $card4CurrentlyShownEntity")
            appendLine("entities4List: $entities4List")
            appendLine("refreshListOfEntities4: $refreshListOfEntities4")
            appendLine("Card 5:")
            appendLine("usersName: $usersName")
            appendLine("usersSurname: $usersSurname")
            appendLine("usersCity: $usersCity")
            appendLine("usersDescription: $usersDescription")
            appendLine("usersProfilePhotoImageURI: $usersProfilePhotoImageURI")
            appendLine("usersRating: $usersRating")
            appendLine("Navigation Flags:")
            appendLine("navigateToStartScreen: $navigateToStartScreen")
            appendLine("previousComposableDestination: $previousComposableDestination")
            appendLine("currentComposableDestination: $currentComposableDestination")
            appendLine("wasDualPane: $wasDualPane")
            appendLine("isDualPane: $isDualPane")
            appendLine("Google Pay:")
            appendLine("googlePayIsAvailable: $googlePayIsAvailable")
            appendLine("merchantName: $merchantName")
            appendLine("gatewayName: $gatewayName")
            appendLine("gatewayMerchantID: $gatewayMerchantID")
            appendLine("exchangeRates: $exchangeRates")
        }
    }

    private fun oSize(o: Any?): Int {
        return ObjectSizeCalculator.getBundleSizeInBytes(o)
    }
}
data class HomeScreenCard1SearchFilters(
    var range1min: Double? = null,
    var range1max: Double? = null,
    var range2min: Double? = null,
    var range2max: Double? = null,
    var switch1: Boolean? = null,
    var switch2: Boolean? = null,
    var switch3: Boolean? = null,
    var switch4: Boolean? = null,
    var switch5: Boolean? = null,
    var switch6: Boolean? = null,
    var switch7: Boolean? = null,
) {
    fun removeFilters() {
        range1min = null
        range1max = null
        range2min = null
        range2max = null
        switch1 = null
        switch2 = null
        switch3 = null
        switch4 = null
        switch5 = null
        switch6 = null
        switch7 = null
    }

    override fun toString(): String {
        return "HomeScreenCard1SearchFilters(\n" +
                "range1min: $range1min,\n" +
                "range1max: $range1max,\n" +
                "range2min: $range2min,\n" +
                "range2max: $range2max,\n" +
                "switch1: $switch1,\n" +
                "switch2: $switch2,\n" +
                "switch3: $switch3,\n" +
                "switch4: $switch4,\n" +
                "switch5: $switch5,\n" +
                "switch6: $switch6,\n" +
                "switch7: $switch7\n" +
                ")"
    }
}