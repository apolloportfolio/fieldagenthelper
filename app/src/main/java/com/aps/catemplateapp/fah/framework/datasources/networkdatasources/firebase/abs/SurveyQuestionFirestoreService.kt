package com.aps.catemplateapp.fah.framework.datasources.networkdatasources.firebase.abs

import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.core.framework.datasources.networkdatasources.firebase.abstraction.BasicFirestoreService
import com.aps.catemplateapp.fah.business.domain.model.entities.SurveyQuestion
import com.aps.catemplateapp.fah.framework.datasources.networkdatasources.firebase.mappers.SurveyQuestionFirestoreMapper
import com.aps.catemplateapp.fah.framework.datasources.networkdatasources.firebase.model.SurveyQuestionFirestoreEntity

interface SurveyQuestionFirestoreService: BasicFirestoreService<
        SurveyQuestion,
        SurveyQuestionFirestoreEntity,
        SurveyQuestionFirestoreMapper
        > {
    suspend fun getUsersEntities3(userId: UserUniqueID): List<SurveyQuestion>?

}