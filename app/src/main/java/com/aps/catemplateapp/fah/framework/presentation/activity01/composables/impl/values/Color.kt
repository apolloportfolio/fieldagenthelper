package com.aps.catemplateapp.fah.framework.presentation.activity01.composables.impl.values

import androidx.compose.ui.graphics.Color


val Skyblue_200 = Color(0x77AAE5FF)
val Skyblue_500 = Color(0xB7AAE5FF)
val Skyblue_700 = Color(0xAAE5FF)
val Purple_200 = Color(0xFFBB86FC)
val Purple_500 = Color(0xFF6200EE)
val Purple_700 = Color(0xFF3700B3)
val Teal_200 = Color(0xFF03DAC5)
val Teal_700 = Color(0xFF018786)
val Black = Color(0xFF000000)
val White = Color(0xFFFFFFFF)
val Yellow = Color(0xFFFFEB3B)
val Orange = Color(0xFFFFC107)
val Dark_orange = Color(0xFFFF9800)
val Light_yellow = Color(0xBCFFEB3B)
val Orange_red = Color(0xFFFF5722)
val Red = Color(0xFFFF2222)
val Green = Color(0xFF4CAF50)


val ColorPrimary = Color(0xFF9C27B0)
val ColorPrimaryVariant = Skyblue_500
val ColorPrimaryInverse = Color(0xFFFFCC01)
val ColorPrimaryDark = Color(0xFF3700B3)
val ColorAccent = Color(0xFF03DAC5)
val ColorTransparent = Color(0x00000000)
val ColorGoldStar = Color(0xFFFFCC01)
val ColorSecondary = Color(0xFFFFCC01)
val ColorTextPrimary = Color(0xFFFFCC01)
val ColorTextSecondary = Color(0xFFFFCC01)
val ColorTextTertiary = Color(0xFFFF9800)
val ColorTextPrimaryInverse = Color(0xFF9C27B0)
val ColorCalendarViewHighlight = Color(0xFF9C27B0)

