package com.aps.catemplateapp.fah.framework.datasources.cachedatasources.room.daos

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.fah.framework.datasources.cachedatasources.room.model.ClientContactCacheEntity
import com.google.firebase.firestore.GeoPoint
import java.util.*


@Dao
interface ClientContactDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(entity : ClientContactCacheEntity) : Long

    @Query("SELECT * FROM clientcontact")
    suspend fun get() : List<ClientContactCacheEntity>
    
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertEntities(entities: List<ClientContactCacheEntity>): LongArray

    @Query("SELECT * FROM clientcontact WHERE id = :id")
    suspend fun getEntityById(id: UniqueID?): ClientContactCacheEntity?

    @Query("DELETE FROM clientcontact WHERE id IN (:ids)")
    suspend fun deleteEntities(ids: List<UniqueID?>): Int

    @Query("DELETE FROM clientcontact")
    suspend fun deleteAllEntities()

    @Query("SELECT * FROM clientcontact")
    suspend fun getAllEntities(): List<ClientContactCacheEntity>

    @Query(
        """
        UPDATE clientcontact 
        SET 
        created_at = :created_at,
        updated_at = :updated_at,
        surname = :surname,
        title = :title,
        phoneNumber = :phoneNumber,
        emailAddress = :emailAddress,
        latitude = :latitude,
        longitude = :longitude,
        geoLocation = :geoLocation,
        firestoreGeoLocation = :firestoreGeoLocation,
        picture1URI = :picture1URI,
        name = :name,
        description = :description
        WHERE id = :id
        """
    )
    suspend fun updateEntity(
        id: UniqueID?,
        created_at: String?,
        updated_at: String?,
        
        surname: String?,
        title: String?,
        phoneNumber: String?,
        emailAddress: String?,

        latitude : Double?,
        longitude: Double?,
        geoLocation: GeoPoint?,
        firestoreGeoLocation: Double?,

        picture1URI: String?,
        name: String?,
        description : String?,
    ): Int

    @Query("DELETE FROM clientcontact WHERE id = :primaryKey")
    suspend fun deleteEntity(primaryKey: UniqueID?): Int

    @Query("SELECT * FROM clientcontact")
    suspend fun searchEntities(): List<ClientContactCacheEntity>
    
    @Query("SELECT COUNT(*) FROM clientcontact")
    suspend fun getNumEntities(): Int
}