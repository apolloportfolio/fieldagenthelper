package com.aps.catemplateapp.fah.business.domain.model.factories

import com.aps.catemplateapp.common.util.DateUtil
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.fah.business.domain.model.entities.SurveyQuestion
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SurveyQuestionFactory
@Inject
constructor(
    private val dateUtil: DateUtil
) {
    fun createSingleEntity(
        id: UniqueID?,

        picture1URI: String?,
        name: String?,
        description : String?,

        ownerID: UserUniqueID?,
    ): SurveyQuestion {
        return SurveyQuestion(
            id,
            dateUtil.getCurrentTimestamp(),
            dateUtil.getCurrentTimestamp(),

            picture1URI,
            name,
            description,

            ownerID,
        )
    }

    fun createEntitiesList(numEntities: Int): List<SurveyQuestion> {
        val list: ArrayList<SurveyQuestion> = ArrayList()
        for(i in 0 until numEntities){ // exclusive on upper bound
            list.add(
                createSingleEntity(
                    UniqueID.generateFakeRandomID(),
                    null,
                    null,
                    null,
                    null,
                )
            )
        }
        return list
    }


    fun generateEmpty(): SurveyQuestion {
        return createSingleEntity(
            UniqueID.generateFakeRandomID(),
            null,
            null,
            null,
            null,
        )
    }

    companion object {
        fun createPreviewEntitiesList(): List<SurveyQuestion> {
            return arrayListOf(
                SurveyQuestion(
                    id = UniqueID("test01"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    picture1URI = "1",
                    question = "test01",
                    answer = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas sapien arcu, pellentesque id faucibus a, gravida sit amet leo. Quisque aliquam mattis pretium.",
                    fieldSurveyID = UserUniqueID("666", "666")
                ),
                SurveyQuestion(
                    id = UniqueID("test02"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    picture1URI = "1",
                    question = "test02",
                    answer = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas sapien arcu, pellentesque id faucibus a, gravida sit amet leo. Quisque aliquam mattis pretium.",
                    fieldSurveyID = UserUniqueID("666", "666")
                ),
                SurveyQuestion(
                    id = UniqueID("test03"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    picture1URI = "1",
                    question = "test03",
                    answer = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas sapien arcu, pellentesque id faucibus a, gravida sit amet leo. Quisque aliquam mattis pretium.",
                    fieldSurveyID = UserUniqueID("666", "666")
                ),
                SurveyQuestion(
                    id = UniqueID("test04"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    picture1URI = "1",
                    question = "test04",
                    answer = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas sapien arcu, pellentesque id faucibus a, gravida sit amet leo. Quisque aliquam mattis pretium.",
                    fieldSurveyID = UserUniqueID("666", "666")
                ),
                SurveyQuestion(
                    id = UniqueID("test05"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    picture1URI = "1",
                    question = "test05",
                    answer = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas sapien arcu, pellentesque id faucibus a, gravida sit amet leo. Quisque aliquam mattis pretium.",
                    fieldSurveyID = UserUniqueID("666", "666")
                ),
                SurveyQuestion(
                    id = UniqueID("test06"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    picture1URI = "1",
                    question = "test06",
                    answer = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas sapien arcu, pellentesque id faucibus a, gravida sit amet leo. Quisque aliquam mattis pretium.",
                    fieldSurveyID = UserUniqueID("666", "666"),
                ),
                SurveyQuestion(
                    id = UniqueID("test07"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    picture1URI = "1",
                    question = "test07",
                    answer = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas sapien arcu, pellentesque id faucibus a, gravida sit amet leo. Quisque aliquam mattis pretium.",
                    fieldSurveyID = UserUniqueID("666", "666")
                ),
                SurveyQuestion(
                    id = UniqueID("test08"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    picture1URI = "1",
                    question = "test08",
                    answer = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas sapien arcu, pellentesque id faucibus a, gravida sit amet leo. Quisque aliquam mattis pretium.",
                    fieldSurveyID = UserUniqueID("666", "666")
                ),
                SurveyQuestion(
                    id = UniqueID("test09"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    picture1URI = "1",
                    question = "test09",
                    answer = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas sapien arcu, pellentesque id faucibus a, gravida sit amet leo. Quisque aliquam mattis pretium.",
                    fieldSurveyID = UserUniqueID("666", "666")
                ),
                SurveyQuestion(
                    id = UniqueID("test10"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    picture1URI = "1",
                    question = "test10",
                    answer = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas sapien arcu, pellentesque id faucibus a, gravida sit amet leo. Quisque aliquam mattis pretium.",
                    fieldSurveyID = UserUniqueID("666", "666")
                )
            )
        }
    }
}