package com.aps.catemplateapp.fah.business.interactors

import com.aps.catemplateapp.common.business.data.cache.StandardCacheDataSource
import com.aps.catemplateapp.common.business.data.network.StandardNetworkDataSource
import com.aps.catemplateapp.common.business.interactors.abstraction.DoNothingAtAll
import com.aps.catemplateapp.fah.business.interactors.abs.*
import javax.inject.Inject

// Use cases
class HomeScreenInteractors<
        Entity,
        CacheDataSource: StandardCacheDataSource<Entity>,
        NetworkDataSource: StandardNetworkDataSource<Entity>,
        ViewState: com.aps.catemplateapp.common.business.domain.state.ViewState<Entity>>
@Inject
constructor(
    val getFieldSurveysAroundUser: GetFieldSurveysAroundUser,
    val searchFieldSurveys: SearchFieldSurveys,
    val getUsersPresentations: GetUsersPresentations,
    val getFieldSurveyTemplates: GetFieldSurveyTemplates,
    val getUsersRating: GetUsersRating,
    val logout: LogoutUser,
    val checkGooglePayAvailability: CheckGooglePayAvailability,
    val downloadExchangeRates: DownloadExchangeRates,
    val getMerchantName: GetMerchantName,
    val getGatewayNameAndMerchantID: GetGatewayNameAndMerchantID,

    val doNothingAtAll: DoNothingAtAll<
            Entity,
            CacheDataSource,
            NetworkDataSource,
            ViewState
            >,
)