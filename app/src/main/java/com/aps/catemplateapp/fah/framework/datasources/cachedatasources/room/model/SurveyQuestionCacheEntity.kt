package com.aps.catemplateapp.fah.framework.datasources.cachedatasources.room.model

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.aps.catemplateapp.common.util.UniqueID

@Entity(tableName = "surveyquestion")
data class SurveyQuestionCacheEntity(
    @PrimaryKey(autoGenerate = false)
    @ColumnInfo(name = "id")
    @NonNull
    var id: UniqueID,

    @ColumnInfo(name = "updated_at")
    val updated_at: String?,

    @ColumnInfo(name = "created_at")
    val created_at: String?,

    @ColumnInfo(name = "picture1URI")
    var picture1URI: String?,

    @ColumnInfo(name = "question")
    var question: String?,

    @ColumnInfo(name = "answer")
    var answer: String?,

    @ColumnInfo(name = "fieldSurveyID")
    var fieldSurveyID: UniqueID?,

    ) {
}