package com.aps.catemplateapp.fah.framework.datasources.networkdatasources.firebase.mappers

import com.aps.catemplateapp.common.business.domain.model.EntityMapper
import com.aps.catemplateapp.common.util.ParcelableGeoPoint
import com.aps.catemplateapp.fah.business.domain.model.entities.ClientContact
import com.aps.catemplateapp.fah.framework.datasources.networkdatasources.firebase.model.ClientContactFirestoreEntity
import javax.inject.Inject

class ClientContactFirestoreMapper
@Inject
constructor() : EntityMapper<ClientContactFirestoreEntity, ClientContact> {
    override fun mapFromEntity(entity: ClientContactFirestoreEntity): ClientContact {
        val clientContact = ClientContact(
            entity.id,
            entity.created_at,
            entity.updated_at,

            entity.surname,
            entity.title,
            entity.phoneNumber,
            entity.emailAddress,

            entity.latitude ,
            entity.longitude,
            null,
            entity.firestoreGeoLocation,

            entity.picture1URI,
            entity.name,
            entity.description,
        )
        if(entity.geoLocation != null){
            clientContact.geoLocation = ParcelableGeoPoint(
                entity.geoLocation!!.latitude,
                entity.geoLocation!!.longitude
            )
        }
        return clientContact
    }

    override fun mapToEntity(domainModel: ClientContact): ClientContactFirestoreEntity {
        return ClientContactFirestoreEntity(
            domainModel.id!!,
            domainModel.created_at,
            domainModel.updated_at,

            domainModel.surname,
            domainModel.title,
            domainModel.phoneNumber,
            domainModel.emailAddress,

            domainModel.latitude ,
            domainModel.longitude,
            domainModel.geoLocation,
            domainModel.firestoreGeoLocation,

            domainModel.picture1URI,
            domainModel.name,
            domainModel.description,
        )
    }

    override fun mapFromEntityList(entities : List<ClientContactFirestoreEntity>) : List<ClientContact> {
        return entities.map{mapFromEntity(it)}
    }

    override fun mapToEntityList(entities: List<ClientContact>): List<ClientContactFirestoreEntity> {
        return entities.map{mapToEntity(it)}
    }
}