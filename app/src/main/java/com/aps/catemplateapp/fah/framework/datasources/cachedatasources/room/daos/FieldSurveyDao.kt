package com.aps.catemplateapp.fah.framework.datasources.cachedatasources.room.daos

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.fah.framework.datasources.cachedatasources.room.model.FieldSurveyCacheEntity
import com.google.firebase.firestore.GeoPoint
import java.util.*


@Dao
interface FieldSurveyDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(entity : FieldSurveyCacheEntity) : Long

    @Query("SELECT * FROM fieldsurvey")
    suspend fun get() : List<FieldSurveyCacheEntity>
    
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertEntities(entities: List<FieldSurveyCacheEntity>): LongArray

    @Query("SELECT * FROM fieldsurvey WHERE id = :id")
    suspend fun getEntityById(id: UniqueID?): FieldSurveyCacheEntity?

    @Query("DELETE FROM fieldsurvey WHERE id IN (:ids)")
    suspend fun deleteEntities(ids: List<UniqueID?>): Int

    @Query("DELETE FROM fieldsurvey")
    suspend fun deleteAllEntities()

    @Query("SELECT * FROM fieldsurvey")
    suspend fun getAllEntities(): List<FieldSurveyCacheEntity>

    @Query(
        """
        UPDATE fieldsurvey 
        SET 
        created_at = :created_at,
        updated_at = :updated_at,

        creatorId = :creatorId,
        surveyedClientContactID = :surveyedClientContactID,
        fillingTime = :fillingTime,
        isTemplate = :isTemplate,
                
        latitude = :latitude,
        longitude = :longitude,
        geoLocation = :geoLocation,
        firestoreGeoLocation = :firestoreGeoLocation,
        picture1URI = :picture1URI,
        description = :description,
        city = :city,
        ownerID = :ownerID,
        name = :name,
        switch1 = :switch1,
        switch2 = :switch2,
        switch3 = :switch3,
        switch4 = :switch4,
        switch5 = :switch5,
        switch6 = :switch6,
        switch7 = :switch7
        WHERE id = :id
        """
    )
    suspend fun updateEntity(
        id: UniqueID?,
        created_at: String?,
        updated_at: String?,
        
        creatorId: UserUniqueID?,
        surveyedClientContactID: UniqueID?,
        fillingTime: String?,
        isTemplate: Boolean = false,

        latitude : Double?,
        longitude: Double?,
        geoLocation: GeoPoint?,
        firestoreGeoLocation: Double?,

        picture1URI: String?,

        description : String?,

        city : String?,

        ownerID: UserUniqueID?,

        name: String?,

        switch1: Boolean? = null,
        switch2: Boolean? = null,
        switch3: Boolean? = null,
        switch4: Boolean? = null,
        switch5: Boolean? = null,
        switch6: Boolean? = null,
        switch7: Boolean? = null,
    ): Int

    @Query("DELETE FROM fieldsurvey WHERE id = :primaryKey")
    suspend fun deleteEntity(primaryKey: UniqueID?): Int

    @Query("SELECT * FROM fieldsurvey")
    suspend fun searchEntities(): List<FieldSurveyCacheEntity>
    
    @Query("SELECT COUNT(*) FROM fieldsurvey")
    suspend fun getNumEntities(): Int
}