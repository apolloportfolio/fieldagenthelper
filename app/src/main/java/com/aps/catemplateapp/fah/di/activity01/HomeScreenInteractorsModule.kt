package com.aps.catemplateapp.fah.di.activity01

import com.aps.catemplateapp.common.business.interactors.abstraction.DoNothingAtAll
import com.aps.catemplateapp.common.business.interactors.implementation.DoNothingAtAllImpl
import com.aps.catemplateapp.common.util.DateUtil
import com.aps.catemplateapp.core.business.data.cache.abstraction.UserCacheDataSource
import com.aps.catemplateapp.core.business.data.cache.implementation.UserCacheDataSourceImpl
import com.aps.catemplateapp.core.business.data.network.abstraction.UserNetworkDataSource
import com.aps.catemplateapp.core.business.domain.model.entities.ProjectUser
import com.aps.catemplateapp.core.util.SecureKeyStorage
import com.aps.catemplateapp.fah.business.data.cache.abstraction.FieldSurveyCacheDataSource
import com.aps.catemplateapp.fah.business.data.cache.abstraction.UsersPresentationCacheDataSource
import com.aps.catemplateapp.fah.business.data.network.abs.FieldSurveyNetworkDataSource
import com.aps.catemplateapp.fah.business.data.network.abs.SurveyQuestionNetworkDataSource
import com.aps.catemplateapp.fah.business.data.network.abs.UsersPresentationNetworkDataSource
import com.aps.catemplateapp.fah.business.domain.model.factories.FieldSurveyFactory
import com.aps.catemplateapp.fah.business.domain.model.factories.SurveyQuestionFactory
import com.aps.catemplateapp.fah.business.domain.model.factories.UsersPresentationFactory
import com.aps.catemplateapp.fah.business.interactors.abs.*
import com.aps.catemplateapp.fah.business.interactors.impl.*
import com.aps.catemplateapp.fah.framework.presentation.activity01.state.HomeScreenViewState
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview

@ExperimentalCoroutinesApi
@FlowPreview
@Module
@InstallIn(SingletonComponent::class)
object HomeScreenInteractorsModule {

    @Provides
    fun provideGetUsersRating(
        cacheDataSource: UserCacheDataSource,
        networkDataSource: UserNetworkDataSource,
    ): GetUsersRating {
        return GetUsersRatingImpl(cacheDataSource, networkDataSource)
    }

    @Provides
    fun InitiateGooglePayPaymentProcess(
        secureKeyStorage: SecureKeyStorage,
        dateUtil: DateUtil,
    ): InitiateGooglePayPaymentProcess {
        return InitiateGooglePayPaymentProcessImpl(secureKeyStorage, dateUtil)
    }

    @Provides
    fun provideGetMerchantName(
        secureKeyStorage: SecureKeyStorage,
        dateUtil: DateUtil,
    ): GetMerchantName {
        return GetMerchantNameImpl(secureKeyStorage, dateUtil)
    }

    @Provides
    fun provideGetGatewayNameAndMerchantID(
        secureKeyStorage: SecureKeyStorage,
        dateUtil: DateUtil,
    ): GetGatewayNameAndMerchantID {
        return GetGatewayNameAndMerchantIDImpl(secureKeyStorage, dateUtil)
    }

    @Provides
    fun provideDownloadExchangeRates(): DownloadExchangeRates {
        return DownloadExchangeRatesImpl()
    }

    @Provides
    fun provideCheckGooglePayAvailability(): CheckGooglePayAvailability {
        return CheckGooglePayAvailabilityImpl()
    }

    @Provides
    fun provideLogoutUser(
        userCacheDataSource: UserCacheDataSource,
        userNetworkDataSource: UserNetworkDataSource,
    ): LogoutUser {
        return LogoutUserImpl(
            userCacheDataSource,
            userNetworkDataSource,
        )
    }

    @Provides
    fun provideGetEntities1AroundUser(
        cacheDataSource: FieldSurveyCacheDataSource,
        networkDataSource: FieldSurveyNetworkDataSource,
        entityFactory: FieldSurveyFactory
    ): GetFieldSurveysAroundUser {
        return GetFieldSurveysAroundUserImpl(
            cacheDataSource,
            networkDataSource,
            entityFactory,
        )
    }


    @Provides
    fun provideGetEntities2(
        cacheDataSource: UsersPresentationCacheDataSource,
        networkDataSource: UsersPresentationNetworkDataSource,
        entityFactory: UsersPresentationFactory
    ): GetUsersPresentations {
        return GetUsersPresentationsImpl(
            cacheDataSource,
            networkDataSource,
            entityFactory,
        )
    }

    @Provides
    fun provideGetEntities3(
        cacheDataSource: FieldSurveyCacheDataSource,
        networkDataSource: FieldSurveyNetworkDataSource,
        entityFactory: FieldSurveyFactory
    ): GetFieldSurveyTemplates {
        return GetFieldFieldSurveyTemplatesImpl(
            cacheDataSource,
            networkDataSource,
            entityFactory,
        )
    }

    @Provides
    fun provideSearchEntities1(
        cacheDataSource: FieldSurveyCacheDataSource,
        networkDataSource: FieldSurveyNetworkDataSource,
        entityFactory: FieldSurveyFactory
    ): SearchFieldSurveys {
        return SearchFieldSurveysImpl(
            cacheDataSource,
            networkDataSource,
        )
    }

    @Provides
    fun provideDoNothingAtAll(
        cacheDataSource: UserCacheDataSourceImpl,
        networkDataSource: UserNetworkDataSource
    ): DoNothingAtAll<
            ProjectUser,
            UserCacheDataSourceImpl,
            UserNetworkDataSource,
            HomeScreenViewState<ProjectUser>> {
        return DoNothingAtAllImpl<
                ProjectUser,
                UserCacheDataSourceImpl,
                UserNetworkDataSource,
                HomeScreenViewState<ProjectUser>>()
    }
}