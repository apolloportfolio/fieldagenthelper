package com.aps.catemplateapp.fah.business.data.network.abs

import com.aps.catemplateapp.common.business.data.network.StandardNetworkDataSource
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.fah.business.domain.model.entities.UsersPresentation

interface UsersPresentationNetworkDataSource: StandardNetworkDataSource<UsersPresentation> {
    override suspend fun insertOrUpdateEntity(entity: UsersPresentation): UsersPresentation?

    override suspend fun deleteEntity(primaryKey: UniqueID?)

    override suspend fun insertDeletedEntity(entity: UsersPresentation)

    override suspend fun insertDeletedEntities(Entities: List<UsersPresentation>)

    override suspend fun deleteDeletedEntity(entity: UsersPresentation)

    override suspend fun getDeletedEntities(): List<UsersPresentation>

    override suspend fun deleteAllEntities()

    override suspend fun searchEntity(entity: UsersPresentation): UsersPresentation?

    override suspend fun getAllEntities(): List<UsersPresentation>

    override suspend fun insertOrUpdateEntities(Entities: List<UsersPresentation>): List<UsersPresentation>?

    override suspend fun getEntityById(id: UniqueID): UsersPresentation?

    suspend fun getUsersEntities2(userId : UserUniqueID) : List<UsersPresentation>?
}