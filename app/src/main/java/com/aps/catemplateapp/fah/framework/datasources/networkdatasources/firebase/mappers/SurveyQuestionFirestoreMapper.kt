package com.aps.catemplateapp.fah.framework.datasources.networkdatasources.firebase.mappers

import com.aps.catemplateapp.common.business.domain.model.EntityMapper
import com.aps.catemplateapp.fah.business.domain.model.entities.SurveyQuestion
import com.aps.catemplateapp.fah.framework.datasources.networkdatasources.firebase.model.SurveyQuestionFirestoreEntity
import javax.inject.Inject

class SurveyQuestionFirestoreMapper
@Inject
constructor() : EntityMapper<SurveyQuestionFirestoreEntity, SurveyQuestion> {
    override fun mapFromEntity(entity: SurveyQuestionFirestoreEntity): SurveyQuestion {
        return SurveyQuestion(
            entity.id,
            entity.created_at,
            entity.updated_at,

            entity.picture1URI,

            entity.question,
            entity.answer,

            entity.fieldSurveyID,
        )
    }

    override fun mapToEntity(domainModel: SurveyQuestion): SurveyQuestionFirestoreEntity {
        return SurveyQuestionFirestoreEntity(
            domainModel.id,
            domainModel.created_at,
            domainModel.updated_at,
            domainModel.picture1URI,
            domainModel.question,
            domainModel.answer,
            domainModel.fieldSurveyID,
        )
    }

    override fun mapFromEntityList(entities : List<SurveyQuestionFirestoreEntity>) : List<SurveyQuestion> {
        return entities.map{mapFromEntity(it)}
    }

    override fun mapToEntityList(entities: List<SurveyQuestion>): List<SurveyQuestionFirestoreEntity> {
        return entities.map{mapToEntity(it)}
    }
}