package com.aps.catemplateapp.fah.framework.datasources.networkdatasources.firebase.mappers

import com.aps.catemplateapp.common.business.domain.model.EntityMapper
import com.aps.catemplateapp.common.util.ParcelableGeoPoint
import com.aps.catemplateapp.fah.business.domain.model.entities.FieldSurvey
import com.aps.catemplateapp.fah.framework.datasources.networkdatasources.firebase.model.FieldSurveyFirestoreEntity
import javax.inject.Inject

class FieldSurveyFirestoreMapper
@Inject
constructor() : EntityMapper<FieldSurveyFirestoreEntity, FieldSurvey> {
    override fun mapFromEntity(entity: FieldSurveyFirestoreEntity): FieldSurvey {
        val fieldSurvey = FieldSurvey(
            entity.id,
            entity.created_at,
            entity.updated_at,

            entity.creatorId,
            entity.surveyedClientContactID,
            entity.fillingTime,
            entity.isTemplate ?: false,

            entity.lattitude,
            entity.longitude,
            null,
            entity.firestoreGeoLocation,

            entity.picture1URI,
            entity.description,

            entity.city,

            entity.ownerID,

            entity.name,

            entity.switch1,
            entity.switch2,
            entity.switch3,
            entity.switch4,
            entity.switch5,
            entity.switch6,
            entity.switch7,
        )
        if(entity.geoLocation != null){
            fieldSurvey.geoLocation = ParcelableGeoPoint(
                entity.geoLocation!!.latitude,
                entity.geoLocation!!.longitude
            )
        }
        return fieldSurvey
    }

    override fun mapToEntity(domainModel: FieldSurvey): FieldSurveyFirestoreEntity {
        val entity1NetworkEntity =
        FieldSurveyFirestoreEntity(
            domainModel.id,
            domainModel.created_at,
            domainModel.updated_at,

            domainModel.creatorId,
            domainModel.surveyedClientContactID,
            domainModel.fillingTime,
            domainModel.isTemplate ?: false,

            domainModel.latitude,
            domainModel.longitude,
            domainModel.geoLocation,
            domainModel.firestoreGeoLocation,

            domainModel.picture1URI,
            domainModel.description,
            
            domainModel.city,

            null,           //keywords
            
            domainModel.ownerID,

            domainModel.name,
            
            domainModel.switch1,
            domainModel.switch2,
            domainModel.switch3,
            domainModel.switch4,
            domainModel.switch5,
            domainModel.switch6,
            domainModel.switch7,
        )
        return entity1NetworkEntity.generateKeywords()
    }

    override fun mapFromEntityList(entities : List<FieldSurveyFirestoreEntity>) : List<FieldSurvey> {
        return entities.map{mapFromEntity(it)}
    }

    override fun mapToEntityList(entities: List<FieldSurvey>): List<FieldSurveyFirestoreEntity> {
        return entities.map{mapToEntity(it)}
    }
}