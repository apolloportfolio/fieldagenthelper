package com.aps.catemplateapp.fah.framework.datasources.cachedatasources.room.abs

import com.aps.catemplateapp.common.util.ParcelableGeoPoint
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.fah.business.domain.model.entities.ClientContact

interface ClientContactDaoService {
    suspend fun insertOrUpdateEntity(entity: ClientContact): ClientContact

    suspend fun insertEntity(entity: ClientContact): Long

    suspend fun insertEntities(Entities: List<ClientContact>): LongArray

    suspend fun getEntityById(id: UniqueID?): ClientContact?

    suspend fun updateEntity(
        id: UniqueID?,
        created_at: String?,
        updated_at: String?,

        surname: String?,
        title: String?,
        phoneNumber: String?,
        emailAddress: String?,

        latitude : Double?,
        longitude: Double?,
        geoLocation: ParcelableGeoPoint?,
        firestoreGeoLocation: Double?,

        picture1URI: String?,
        name: String?,
        description : String?,
    ): Int

    suspend fun deleteEntity(primaryKey: UniqueID?): Int

    suspend fun deleteEntities(Entities: List<ClientContact>): Int

    suspend fun searchEntities(): List<ClientContact>

    suspend fun getAllEntities(): List<ClientContact>

    suspend fun getNumEntities(): Int
}