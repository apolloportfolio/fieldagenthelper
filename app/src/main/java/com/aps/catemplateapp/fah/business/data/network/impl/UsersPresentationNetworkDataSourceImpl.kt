package com.aps.catemplateapp.fah.business.data.network.impl

import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.fah.business.data.network.abs.UsersPresentationNetworkDataSource
import com.aps.catemplateapp.fah.business.domain.model.entities.UsersPresentation
import com.aps.catemplateapp.fah.framework.datasources.networkdatasources.firebase.abs.UsersPresentationFirestoreService
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class UsersPresentationNetworkDataSourceImpl
@Inject
constructor(
    private val firestoreService: UsersPresentationFirestoreService
): UsersPresentationNetworkDataSource {
    override suspend fun insertOrUpdateEntity(entity: UsersPresentation): UsersPresentation? {
        return firestoreService.insertOrUpdateEntity(entity)
    }

    override suspend fun deleteEntity(primaryKey: UniqueID?) {
        return firestoreService.deleteEntity(primaryKey)
    }

    override suspend fun insertDeletedEntity(entity: UsersPresentation) {
        return firestoreService.insertDeletedEntity(entity)
    }

    override suspend fun insertDeletedEntities(entities: List<UsersPresentation>) {
        return firestoreService.insertDeletedEntities(entities)
    }

    override suspend fun deleteDeletedEntity(entity: UsersPresentation) {
        return firestoreService.deleteDeletedEntity(entity)
    }

    override suspend fun getDeletedEntities(): List<UsersPresentation> {
        return firestoreService.getAllDeletedEntities()
    }

    override suspend fun deleteAllEntities() {
        firestoreService.deleteAllEntities()
    }

    override suspend fun searchEntity(entity: UsersPresentation): UsersPresentation? {
        return firestoreService.searchEntity(entity)
    }

    override suspend fun getAllEntities(): List<UsersPresentation> {
        return firestoreService.getAllEntities()
    }

    override suspend fun insertOrUpdateEntities(entities: List<UsersPresentation>): List<UsersPresentation>? {
        return firestoreService.insertOrUpdateEntities(entities)
    }

    override suspend fun getEntityById(id: UniqueID): UsersPresentation? {
        return firestoreService.getEntityById(id)
    }

    override suspend fun getUsersEntities2(userId : UserUniqueID) : List<UsersPresentation>? {
        return firestoreService.getUsersEntities2(userId)
    }
}