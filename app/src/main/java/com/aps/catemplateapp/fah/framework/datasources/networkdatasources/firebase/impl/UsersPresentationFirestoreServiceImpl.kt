package com.aps.catemplateapp.fah.framework.datasources.networkdatasources.firebase.impl

import com.aps.catemplateapp.common.business.data.util.cLog
import com.aps.catemplateapp.common.util.ALog
import com.aps.catemplateapp.common.util.DateUtil
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.common.util.extensions.getSizeString
import com.aps.catemplateapp.core.business.data.network.abstraction.UserNetworkDataSource
import com.aps.catemplateapp.core.framework.datasources.networkdatasources.firebase.implementation.BasicFirestoreServiceImpl
import com.aps.catemplateapp.fah.business.domain.model.entities.UsersPresentation
import com.aps.catemplateapp.fah.framework.datasources.networkdatasources.firebase.abs.UsersPresentationFirestoreService
import com.aps.catemplateapp.fah.framework.datasources.networkdatasources.firebase.mappers.UsersPresentationFirestoreMapper
import com.aps.catemplateapp.fah.framework.datasources.networkdatasources.firebase.model.UsersPresentationFirestoreEntity
import kotlinx.coroutines.tasks.await
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class UsersPresentationFirestoreServiceImpl
@Inject
constructor(
//    private val firebaseAuth: FirebaseAuth,
//    private val firestore: FirebaseFirestore,
    private val networkMapper: UsersPresentationFirestoreMapper,
    private val dateUtil: DateUtil,
    private val userNetworkDataSource: UserNetworkDataSource,
): UsersPresentationFirestoreService,
    BasicFirestoreServiceImpl<UsersPresentation, UsersPresentationFirestoreEntity, UsersPresentationFirestoreMapper>(
//        firebaseAuth,
//        firestore,
        networkMapper,
        dateUtil
    ) {

    override fun getCollectionName(): String {
        return COLLECTION_NAME
    }

    override fun getDeletesCollectionName(): String {
        return DELETES_COLLECTION_NAME
    }

    override fun setEntityID(entity: UsersPresentation, id: UniqueID) {
        entity.id = id
    }

    override fun getEntityID(entity: UsersPresentation): UniqueID? {
        return entity.id
    }

    override fun setFirestoreEntityID(entity: UsersPresentationFirestoreEntity, id: UniqueID) {
        entity.id = id
    }

    override fun updateEntityTimestamp(entity: UsersPresentation, timestamp: String){
        entity.updated_at = timestamp
    }

    override fun updateFirestoreEntityTimestamp(entity: UsersPresentationFirestoreEntity, timestamp: String){
        entity.updated_at = timestamp
    }

    override fun getFirestoreEntityType(): Class<UsersPresentationFirestoreEntity> {
        return UsersPresentationFirestoreEntity::class.java
    }

    override fun getFirestoreEntityID(entity: UsersPresentationFirestoreEntity): UniqueID? {
        return entity.id
    }

    override suspend fun getUsersEntities2(userId: UserUniqueID): List<UsersPresentation>? {
        val methodName: String = "getUsersEntities2"
        var result: List<UsersPresentation>
        ALog.d(TAG, "Method start: $methodName")
        try {
            val collectionName = getCollectionName()
            result = networkMapper.mapFromEntityList(
                firestore
                    .collection(collectionName)
                    .whereEqualTo(UsersPresentation.ownerIDFieldName, userId)
                    .get()
                    .addOnFailureListener {
                        // send error reports to Firebase Crashlytics
                        cLog(it.message)
                        ALog.w(TAG, "$methodName(): Failed to get user's entities2.")
                    }
                    .addOnSuccessListener {
                        if(LOG_ME) ALog.d(
                            TAG, "$methodName(): " +
                                "Successfully got all user's entities2.")
                    }
                    .await()
                    .toObjects(getFirestoreEntityType())
            )
            if(LOG_ME)ALog.d(
                TAG, ".$methodName(): " +
                    "Got ${result.getSizeString()} entities2 " +
                    "belonging to user: $userId stored in $collectionName")
            return result
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
            return emptyList()
        } finally {
            ALog.d(TAG, "Method end: $methodName")
        }
    }

    companion object {
        const val TAG = "Entity2FirestoreServiceImpl"
        const val LOG_ME = true
        const val COLLECTION_NAME = "entity2"
        const val DELETES_COLLECTION_NAME = "entity2_d"
    }
}