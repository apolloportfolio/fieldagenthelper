package com.aps.catemplateapp.fah.business.interactors.impl

import com.aps.catemplateapp.R
import com.aps.catemplateapp.common.business.data.network.ApiResponseHandler
import com.aps.catemplateapp.common.business.data.util.safeApiCall
import com.aps.catemplateapp.common.business.domain.state.*
import com.aps.catemplateapp.common.util.ALog
import com.aps.catemplateapp.core.business.domain.model.entities.ProjectUser
import com.aps.catemplateapp.fah.business.data.cache.abstraction.FieldSurveyCacheDataSource
import com.aps.catemplateapp.fah.business.data.network.abs.FieldSurveyNetworkDataSource
import com.aps.catemplateapp.fah.business.domain.model.entities.FieldSurvey
import com.aps.catemplateapp.fah.business.domain.model.factories.FieldSurveyFactory
import com.aps.catemplateapp.fah.business.interactors.abs.GetFieldSurveyTemplates
import com.aps.catemplateapp.fah.framework.presentation.activity01.state.HomeScreenViewState
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

private const val TAG = "GetFieldSurveyTemplatesImpl"
private const val LOG_ME = true

class GetFieldFieldSurveyTemplatesImpl
@Inject
constructor(
    private val cacheDataSource: FieldSurveyCacheDataSource,
    private val networkDataSource: FieldSurveyNetworkDataSource,
    private val entityFactory: FieldSurveyFactory
): GetFieldSurveyTemplates {
    override fun getFieldSurveyTemplates(
        stateEvent: StateEvent,
        returnViewState : HomeScreenViewState<ProjectUser>,
        updateReturnViewState : (HomeScreenViewState<ProjectUser>, List<FieldSurvey>?) -> HomeScreenViewState<ProjectUser>,
    ): Flow<DataState<HomeScreenViewState<ProjectUser>>?> = flow {
        val fieldSurveyTemplates = safeApiCall(
            dispatcher = Dispatchers.IO,
            apiCall = {
                returnViewState.mainEntity?.id?.let { networkDataSource.getUsersFieldSurveyTemplates(it) }
            }
        )

        val response = object : ApiResponseHandler<HomeScreenViewState<ProjectUser>, List<FieldSurvey>>(
            response = fieldSurveyTemplates,
            stateEvent = stateEvent,
        ) {
            override suspend fun handleSuccess(resultObj: List<FieldSurvey>): DataState<HomeScreenViewState<ProjectUser>>? {
                var message: String? = GetEntities3Constants.GET_ENTITY_SUCCESS
                var uiComponentType: UIComponentType? = UIComponentType.None()
                ALog.d(TAG, "getEntities3().handleSuccess(): ")


                if (resultObj == null) {
                    ALog.d(TAG, "getEntities3(): resultObj == null")
                    message = GetEntities3Constants.GET_ENTITY_NO_MATCHING_RESULTS
                    uiComponentType = UIComponentType.Toast()
                } else {
                    ALog.d(TAG, "getEntities3(): resultObj != null")
                    returnViewState.usersSurveyTemplates = ArrayList(resultObj)
                    updateReturnViewState(returnViewState, resultObj)
                }
                return DataState.data(
                    response = Response(
                        messageId = R.string.error,
                        message = message,
                        uiComponentType = uiComponentType as UIComponentType,
                        messageType = MessageType.Success()
                    ),
                    data = returnViewState,
                    stateEvent = stateEvent
                )
            }
        }.getResult()

        emit(response)
    }

    object GetEntities3Constants{
        const val GET_ENTITY_SUCCESS = "Successfully got entities 3."
        const val GET_ENTITY_NO_MATCHING_RESULTS = "There are no entities 3 in database."
        const val SEARCH_ENTITIES_FAILED = "Failed to retrieve the list of entities 3."
    }
}