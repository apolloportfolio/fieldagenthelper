package com.aps.catemplateapp.fah.framework.datasources.cachedatasources.room.daos

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.fah.framework.datasources.cachedatasources.room.model.SurveyQuestionCacheEntity
import java.util.*


@Dao
interface SurveyQuestionDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(entity : SurveyQuestionCacheEntity) : Long

    @Query("SELECT * FROM surveyquestion")
    suspend fun get() : List<SurveyQuestionCacheEntity>
    
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertEntities(entities: List<SurveyQuestionCacheEntity>): LongArray

    @Query("SELECT * FROM surveyquestion WHERE id = :id")
    suspend fun getEntityById(id: UniqueID?): SurveyQuestionCacheEntity?

    @Query("DELETE FROM surveyquestion WHERE id IN (:ids)")
    suspend fun deleteEntities(ids: List<UniqueID?>): Int

    @Query("DELETE FROM surveyquestion")
    suspend fun deleteAllEntities()

    @Query("SELECT * FROM surveyquestion")
    suspend fun getAllEntities(): List<SurveyQuestionCacheEntity>

    @Query(
        """
        UPDATE surveyquestion 
        SET 
        created_at = :created_at,
        updated_at = :updated_at,
        picture1URI = :picture1URI,
        question = :question,
        answer = :answer,
        fieldSurveyID = :fieldSurveyID
        WHERE id = :id
        """
    )
    suspend fun updateEntity(
        id : UniqueID?,
        created_at: String?,
        updated_at: String?,

        picture1URI: String?,

        question: String?,
        answer: String?,

        fieldSurveyID: UniqueID?,
    ): Int

    @Query("DELETE FROM surveyquestion WHERE id = :primaryKey")
    suspend fun deleteEntity(primaryKey: UniqueID?): Int

    @Query("SELECT * FROM surveyquestion")
    suspend fun searchEntities(): List<SurveyQuestionCacheEntity>
    
    @Query("SELECT COUNT(*) FROM surveyquestion")
    suspend fun getNumEntities(): Int
}