package com.aps.catemplateapp.fah.framework.datasources.cachedatasources.room.mappers

import com.aps.catemplateapp.common.business.domain.model.EntityMapper
import com.aps.catemplateapp.fah.business.domain.model.entities.UsersPresentation
import com.aps.catemplateapp.fah.framework.datasources.cachedatasources.room.model.UsersPresentationCacheEntity
import javax.inject.Inject

class UsersPresentationCacheMapper
@Inject
constructor() : EntityMapper<UsersPresentationCacheEntity, UsersPresentation> {
    override fun mapFromEntity(entity: UsersPresentationCacheEntity): UsersPresentation {
        return UsersPresentation(
            entity.id,
            entity.created_at,
            entity.updated_at,

            entity.presentationFileURI,

            entity.picture1URI,

            entity.name,
            entity.description,

            entity.ownerID,
        )
    }

    override fun mapToEntity(domainModel: UsersPresentation): UsersPresentationCacheEntity {
        return UsersPresentationCacheEntity(
            domainModel.id!!,
            domainModel.created_at,
            domainModel.updated_at,

            domainModel.presentationFileURI,

            domainModel.picture1URI,

            domainModel.name,
            domainModel.description,

            domainModel.ownerID,
        )
    }

    override fun mapFromEntityList(entities : List<UsersPresentationCacheEntity>) : List<UsersPresentation> {
        return entities.map{mapFromEntity(it)}
    }

    override fun mapToEntityList(entities: List<UsersPresentation>): List<UsersPresentationCacheEntity> {
        return entities.map{mapToEntity(it)}
    }
}