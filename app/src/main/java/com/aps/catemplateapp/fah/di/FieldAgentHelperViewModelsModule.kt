package com.aps.catemplateapp.fah.di

import android.content.SharedPreferences
import androidx.lifecycle.ViewModelProvider
import com.aps.catemplateapp.common.util.DateUtil
import com.aps.catemplateapp.fah.FieldAgentHelperViewModelFactory
import com.aps.catemplateapp.core.business.data.cache.implementation.UserCacheDataSourceImpl
import com.aps.catemplateapp.core.business.data.network.abstraction.UserNetworkDataSource
import com.aps.catemplateapp.core.business.domain.model.entities.ProjectUser
import com.aps.catemplateapp.fah.business.domain.model.factories.FieldSurveyFactory
import com.aps.catemplateapp.core.business.domain.model.factories.UserFactory
import com.aps.catemplateapp.fah.business.interactors.HomeScreenInteractors
import com.aps.catemplateapp.fah.framework.presentation.activity01.state.HomeScreenViewState
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import javax.inject.Named
import javax.inject.Singleton

@ExperimentalCoroutinesApi
@FlowPreview
@Module
@InstallIn(SingletonComponent::class)
object FieldAgentHelperViewModelsModule {
    @Singleton
    @JvmStatic
    @Provides
    @Named("FieldAgentHelperViewModelFactory")
    fun provideFieldAgentHelperViewModelFactory(
        homeScreenInteractors: HomeScreenInteractors<
                ProjectUser,
                UserCacheDataSourceImpl,
                UserNetworkDataSource,
                HomeScreenViewState<ProjectUser>
                >,
        dateUtil: DateUtil,
        entityFactory: UserFactory,
        fieldSurveyFactory: FieldSurveyFactory,
        editor: SharedPreferences.Editor,
        //sharedPreferences: SharedPreferences
    ): ViewModelProvider.Factory {
        return FieldAgentHelperViewModelFactory(
            homeScreenInteractors = homeScreenInteractors,
            dateUtil = dateUtil,
            userFactory = entityFactory,
            fieldSurveyFactory = fieldSurveyFactory,
            editor = editor,
        )
    }
}