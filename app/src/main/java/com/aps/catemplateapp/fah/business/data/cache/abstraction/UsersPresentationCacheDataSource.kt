package com.aps.catemplateapp.fah.business.data.cache.abstraction

import com.aps.catemplateapp.common.business.data.cache.StandardCacheDataSource
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.fah.business.domain.model.entities.UsersPresentation

interface UsersPresentationCacheDataSource: StandardCacheDataSource<UsersPresentation> {
    override suspend fun insertOrUpdateEntity(entity: UsersPresentation): UsersPresentation?

    override suspend fun insertEntity(entity: UsersPresentation): Long

    override suspend fun deleteEntity(primaryKey: UniqueID?): Int

    override suspend fun deleteEntities(entities: List<UsersPresentation>): Int

    suspend fun updateEntity(
        id: UniqueID?,
        updated_at: String?,
        created_at: String?,

        presentationFileURI: String?,

        picture1URI: String?,

        name: String?,
        description: String?,

        ownerID: UserUniqueID?,
    ): Int

    override suspend fun searchEntities(
        query: String,
        filterAndOrder: String,
        page: Int
    ): List<UsersPresentation>

    override suspend fun getAllEntities(): List<UsersPresentation>

    override suspend fun getEntityById(id: UniqueID?): UsersPresentation?

    override suspend fun getNumEntities(): Int

    override suspend fun insertEntities(entities: List<UsersPresentation>): LongArray
}