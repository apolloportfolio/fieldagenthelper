package com.aps.catemplateapp.fah.framework.datasources.networkdatasources.firebase.abs

import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.core.framework.datasources.networkdatasources.firebase.abstraction.BasicFirestoreService
import com.aps.catemplateapp.fah.business.domain.model.entities.UsersPresentation
import com.aps.catemplateapp.fah.framework.datasources.networkdatasources.firebase.mappers.UsersPresentationFirestoreMapper
import com.aps.catemplateapp.fah.framework.datasources.networkdatasources.firebase.model.UsersPresentationFirestoreEntity

interface UsersPresentationFirestoreService: BasicFirestoreService<
        UsersPresentation,
        UsersPresentationFirestoreEntity,
        UsersPresentationFirestoreMapper
        > {
    suspend fun getUsersEntities2(userId: UserUniqueID): List<UsersPresentation>?

}