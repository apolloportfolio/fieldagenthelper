package com.aps.catemplateapp.fah.di

import com.aps.catemplateapp.core.business.data.cache.abstraction.*
import com.aps.catemplateapp.core.business.data.cache.implementation.*
import com.aps.catemplateapp.fah.business.data.cache.abstraction.FieldSurveyCacheDataSource
import com.aps.catemplateapp.fah.business.data.cache.abstraction.UsersPresentationCacheDataSource
import com.aps.catemplateapp.fah.business.data.cache.abstraction.SurveyQuestionCacheDataSource
import com.aps.catemplateapp.fah.business.data.cache.abstraction.ClientContactCacheDataSource
import com.aps.catemplateapp.fah.business.data.cache.implementation.FieldSurveyCacheDataSourceImpl
import com.aps.catemplateapp.fah.business.data.cache.implementation.UsersPresentationCacheDataSourceImpl
import com.aps.catemplateapp.fah.business.data.cache.implementation.SurveyQuestionCacheDataSourceImpl
import com.aps.catemplateapp.fah.business.data.cache.implementation.ClientContactCacheDataSourceImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
abstract class CacheDataSourcesModule {

    @Binds
    abstract fun bindFieldSurveyCacheDataSource(implementation: FieldSurveyCacheDataSourceImpl): FieldSurveyCacheDataSource

    @Binds
    abstract fun bindUsersPresentationCacheDataSource(implementation: UsersPresentationCacheDataSourceImpl): UsersPresentationCacheDataSource

    @Binds
    abstract fun bindSurveyQuestionCacheDataSource(implementation: SurveyQuestionCacheDataSourceImpl): SurveyQuestionCacheDataSource

    @Binds
    abstract fun bindClientContactCacheDataSource(implementation: ClientContactCacheDataSourceImpl): ClientContactCacheDataSource
}