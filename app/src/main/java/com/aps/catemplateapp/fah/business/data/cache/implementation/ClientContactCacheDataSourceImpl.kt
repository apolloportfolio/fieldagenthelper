package com.aps.catemplateapp.fah.business.data.cache.implementation

import com.aps.catemplateapp.common.util.ParcelableGeoPoint
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.fah.business.data.cache.abstraction.ClientContactCacheDataSource
import com.aps.catemplateapp.fah.business.domain.model.entities.ClientContact
import com.aps.catemplateapp.fah.framework.datasources.cachedatasources.room.abs.ClientContactDaoService
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ClientContactCacheDataSourceImpl
@Inject
constructor(
    private val entityDaoService: ClientContactDaoService
): ClientContactCacheDataSource {

    override suspend fun insertOrUpdateEntity(entity: ClientContact): ClientContact? {
        return entityDaoService.insertOrUpdateEntity(entity)
    }

    override suspend fun insertEntity(entity: ClientContact): Long {
        return entityDaoService.insertEntity(entity)
    }

    override suspend fun deleteEntity(primaryKey: UniqueID?): Int {
        return entityDaoService.deleteEntity(primaryKey)
    }

    override suspend fun deleteEntities(entities: List<ClientContact>): Int {
        return entityDaoService.deleteEntities(entities)
    }

    override suspend fun updateEntity(
        id: UniqueID?,
        created_at: String?,
        updated_at: String?,

        surname: String?, 
        title: String?, 
        phoneNumber: String?, 
        emailAddress: String?, 

        lattitude : Double?,
        longitude: Double?,
        geoLocation: ParcelableGeoPoint?,
        firestoreGeoLocation: Double?,

        picture1URI: String?,
        name: String?,
        description: String?,
    ): Int {
        return entityDaoService.updateEntity(
            id,
            created_at,
            updated_at,
            
            surname,
            title,
            phoneNumber,
            emailAddress,

            lattitude,
            longitude,
            geoLocation,
            firestoreGeoLocation,

            picture1URI,
            name,
            description,
        )
    }

    override suspend fun searchEntities(
        query: String,
        filterAndOrder: String,
        page: Int
    ): List<ClientContact> {
        return entityDaoService.searchEntities()
    }

    override suspend fun getAllEntities(): List<ClientContact> {
        return entityDaoService.getAllEntities()
    }

    override suspend fun getEntityById(id: UniqueID?): ClientContact? {
        return entityDaoService.getEntityById(id)
    }

    override suspend fun getNumEntities(): Int {
        return entityDaoService.getNumEntities()
    }

    override suspend fun insertEntities(entities: List<ClientContact>): LongArray{
        return entityDaoService.insertEntities(entities)
    }
}