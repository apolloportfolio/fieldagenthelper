package com.aps.catemplateapp.fah.framework.datasources.cachedatasources.room.impl

import com.aps.catemplateapp.common.util.*
import com.aps.catemplateapp.fah.business.domain.model.entities.FieldSurvey
import com.aps.catemplateapp.fah.framework.datasources.cachedatasources.room.abs.FieldSurveyDaoService
import com.aps.catemplateapp.fah.framework.datasources.cachedatasources.room.daos.FieldSurveyDao
import com.aps.catemplateapp.fah.framework.datasources.cachedatasources.room.mappers.FieldSurveyCacheMapper
import javax.inject.Inject
import javax.inject.Singleton

private const val TAG = "Entity1DaoServiceImpl"
private const val LOG_ME = true

@Singleton
class FieldSurveyDaoServiceImpl
@Inject
constructor(
    private val dao: FieldSurveyDao,
    private val mapper: FieldSurveyCacheMapper,
    private val dateUtil: DateUtil
): FieldSurveyDaoService {

    override suspend fun insertOrUpdateEntity(entity: FieldSurvey): FieldSurvey {
        val databaseEntity = dao.getEntityById(entity.id)
        if(LOG_ME) ALog.d(TAG, ".insertOrUpdateEntity(): databaseEntity == $databaseEntity")
        if(databaseEntity == null) {
            if(LOG_ME)ALog.d(TAG, ".insertOrUpdateEntity(): entity before inserting:\n$entity")
            dao.insert(mapper.mapToEntity(entity))
            val entityFromDao = dao.getEntityById(entity.id)
            if(LOG_ME) ALog.d(TAG, ".insertOrUpdateEntity(): entity after inserting == ${entityFromDao}\n")
            return mapper.mapFromEntity(entityFromDao!!)
        } else {
            this.updateEntity(
                entity.id,
                entity.created_at,
                entity.updated_at,

                entity.creatorId,
                entity.surveyedClientContactID,
                entity.fillingTime,
                entity.isTemplate,

                entity.latitude ,
                entity.longitude,
                entity.geoLocation,
                entity.firestoreGeoLocation,
                entity.picture1URI,
                entity.description,
                entity.city,
                entity.ownerID,
                entity.name,

                entity.switch1,
                entity.switch2,
                entity.switch3,
                entity.switch4,
                entity.switch5,
                entity.switch6,
                entity.switch7,
            )
            val entityFromDao = dao.getEntityById(entity.id)
            if(LOG_ME) ALog.d(TAG, ".insertOrUpdateEntity(): entityFromDao == $entityFromDao")
            return mapper.mapFromEntity(entityFromDao!!)
        }
    }

    override suspend fun insertEntity(entity: FieldSurvey): Long {
        return dao.insert(mapper.mapToEntity(entity))
    }

    override suspend fun insertEntities(entities: List<FieldSurvey>): LongArray {
        return dao.insertEntities(
            mapper.mapToEntityList(entities)
        )
    }

    override suspend fun getEntityById(id: UniqueID?): FieldSurvey? {
        return dao.getEntityById(id)?.let { entity ->
            mapper.mapFromEntity(entity)
        }
    }

    override suspend fun updateEntity(
        id: UniqueID?,
        created_at: String?,
        updated_at: String?,
        
        creatorId: UserUniqueID?,
        surveyedClientContactID: UniqueID?,
        fillingTime: String?,
        isTemplate: Boolean,

        latitude : Double?,
        longitude: Double?,
        geoLocation: ParcelableGeoPoint?,
        firestoreGeoLocation: Double?,

        picture1URI: String?,
        description : String?,

        city : String?,

        ownerID: UserUniqueID?,

        name: String?,

        switch1: Boolean?,
        switch2: Boolean?,
        switch3: Boolean?,
        switch4: Boolean?,
        switch5: Boolean?,
        switch6: Boolean?,
        switch7: Boolean?,
    ): Int {
        return if(updated_at != null){
            dao.updateEntity(
                id,
                created_at,
                updated_at,
                
                creatorId,
                surveyedClientContactID,
                fillingTime,
                isTemplate,

                latitude,
                longitude,
                geoLocation,
                firestoreGeoLocation,

                picture1URI,
                description,

                city,

                ownerID,

                name,

                switch1,
                switch2,
                switch3,
                switch4,
                switch5,
                switch6,
                switch7,
            )
        }else{
            dao.updateEntity(
                id,
                created_at,
                dateUtil.getCurrentTimestamp(),

                creatorId,
                surveyedClientContactID,
                fillingTime,
                isTemplate,

                latitude,
                longitude,
                geoLocation,
                firestoreGeoLocation,

                picture1URI,

                description,

                city,

                ownerID,

                name,

                switch1,
                switch2,
                switch3,
                switch4,
                switch5,
                switch6,
                switch7,
            )
        }

    }

    override suspend fun deleteEntity(primaryKey: UniqueID?): Int {
        return dao.deleteEntity(primaryKey)
    }

    override suspend fun deleteEntities(entities: List<FieldSurvey>): Int {
        val ids = entities.mapIndexed {index, value -> value.id}
        return dao.deleteEntities(ids)
    }

    override suspend fun searchEntities(): List<FieldSurvey> {
        return mapper.mapFromEntityList(
            dao.searchEntities()
        )
    }

    override suspend fun getAllEntities(): List<FieldSurvey> {
        return mapper.mapFromEntityList(dao.getAllEntities())
    }

    override suspend fun getNumEntities(): Int {
        return dao.getNumEntities()
    }
}