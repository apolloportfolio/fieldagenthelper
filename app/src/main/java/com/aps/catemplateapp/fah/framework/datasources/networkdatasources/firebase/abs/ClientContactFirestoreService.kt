package com.aps.catemplateapp.fah.framework.datasources.networkdatasources.firebase.abs

import com.aps.catemplateapp.core.framework.datasources.networkdatasources.firebase.abstraction.BasicFirestoreService
import com.aps.catemplateapp.fah.business.domain.model.entities.ClientContact
import com.aps.catemplateapp.fah.framework.datasources.networkdatasources.firebase.mappers.ClientContactFirestoreMapper
import com.aps.catemplateapp.fah.framework.datasources.networkdatasources.firebase.model.ClientContactFirestoreEntity

interface ClientContactFirestoreService: BasicFirestoreService<
        ClientContact,
        ClientContactFirestoreEntity,
        ClientContactFirestoreMapper
        > {

}