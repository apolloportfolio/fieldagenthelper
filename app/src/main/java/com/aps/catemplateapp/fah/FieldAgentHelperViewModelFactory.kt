package com.aps.catemplateapp.fah

import android.content.SharedPreferences
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.test.core.app.ApplicationProvider.getApplicationContext
import com.aps.catemplateapp.common.framework.presentation.ApplicationViewModel
import com.aps.catemplateapp.common.util.DateUtil
import com.aps.catemplateapp.core.business.data.cache.implementation.UserCacheDataSourceImpl
import com.aps.catemplateapp.core.business.data.network.abstraction.UserNetworkDataSource
import com.aps.catemplateapp.core.business.domain.model.entities.ProjectUser
import com.aps.catemplateapp.fah.business.domain.model.factories.FieldSurveyFactory
import com.aps.catemplateapp.core.business.domain.model.factories.UserFactory
import com.aps.catemplateapp.fah.business.interactors.HomeScreenInteractors
import com.aps.catemplateapp.fah.framework.presentation.activity01.HomeScreenViewModel
import com.aps.catemplateapp.fah.framework.presentation.activity01.state.HomeScreenViewState
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview


@FlowPreview
@ExperimentalCoroutinesApi
class FieldAgentHelperViewModelFactory
constructor(
    private val homeScreenInteractors: HomeScreenInteractors<
            ProjectUser,
            UserCacheDataSourceImpl,
            UserNetworkDataSource,
            HomeScreenViewState<ProjectUser>
            >,
    private val dateUtil: DateUtil,
    private val userFactory: UserFactory,
    private val fieldSurveyFactory: FieldSurveyFactory,
    private val editor: SharedPreferences.Editor,
    //private val sharedPreferences: SharedPreferences
) : ViewModelProvider.Factory
{

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return when(modelClass){

            HomeScreenViewModel::class.java -> {
                HomeScreenViewModel(
                    interactors = homeScreenInteractors,
                    dateUtil = dateUtil,
                    entityFactory = userFactory
                ) as T
            }




            ApplicationViewModel::class.java -> {
                ApplicationViewModel(
                application = getApplicationContext()
                ) as T
            }

            else -> {
                throw IllegalArgumentException("unknown model class $modelClass")
            }
        }
    }
}