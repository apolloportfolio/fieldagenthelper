package com.aps.catemplateapp.fah.business.interactors.impl

import android.location.Location
import com.aps.catemplateapp.R
import com.aps.catemplateapp.common.business.data.network.ApiResponseHandler
import com.aps.catemplateapp.common.business.data.network.NetworkConstants
import com.aps.catemplateapp.common.business.data.util.safeApiCall
import com.aps.catemplateapp.common.business.domain.state.*
import com.aps.catemplateapp.common.util.ALog
import com.aps.catemplateapp.fah.business.data.cache.abstraction.FieldSurveyCacheDataSource
import com.aps.catemplateapp.fah.business.data.network.abs.FieldSurveyNetworkDataSource
import com.aps.catemplateapp.fah.business.domain.model.entities.FieldSurvey
import com.aps.catemplateapp.core.business.domain.model.entities.ProjectUser
import com.aps.catemplateapp.fah.business.domain.model.factories.FieldSurveyFactory
import com.aps.catemplateapp.fah.business.interactors.abs.GetFieldSurveysAroundUser
import com.aps.catemplateapp.fah.framework.presentation.activity01.state.HomeScreenViewState
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

private const val TAG = "GetEntities1AroundUserImpl"
private const val LOG_ME = true

class GetFieldSurveysAroundUserImpl
@Inject
constructor(
    private val fieldSurveyCacheDataSource: FieldSurveyCacheDataSource,
    private val fieldSurveyNetworkDataSource: FieldSurveyNetworkDataSource,
    private val entityFactory: FieldSurveyFactory
): GetFieldSurveysAroundUser {

    override fun getFieldSurveysAroundUser(
        location : Location?,
        stateEvent: StateEvent,
        returnViewState: HomeScreenViewState<ProjectUser>,
        updateReturnViewState: (HomeScreenViewState<ProjectUser>, List<FieldSurvey>?)-> HomeScreenViewState<ProjectUser>,
        onErrorAction: () -> Unit,
    ): Flow<DataState<HomeScreenViewState<ProjectUser>>?> = flow {
        val syncedEntities = safeApiCall(
            Dispatchers.IO,
            networkTimeout = NetworkConstants.NETWORK_TIMEOUT,
            onErrorAction = onErrorAction,
        ){
            syncEntities(location)
        }

        val response = object: ApiResponseHandler<HomeScreenViewState<ProjectUser>, List<FieldSurvey>>(
            response = syncedEntities,
            stateEvent = stateEvent
        ){
            override suspend fun handleSuccess(resultObj: List<FieldSurvey>): DataState<HomeScreenViewState<ProjectUser>>? {
                var message: String? = GET_ENTITIES_SUCCESS
                var uiComponentType: UIComponentType? = UIComponentType.None()
                if(LOG_ME) ALog.d(TAG, "getEntities1AroundUser().handleSuccess(): ")


                if(resultObj == null){
                    if(LOG_ME) ALog.d(TAG, "getEntities1AroundUser(): resultObj == null")
                    message = GET_ENTITY_NO_MATCHING_RESULTS
                    uiComponentType = UIComponentType.Toast()
                } else {
                    returnViewState.searchedEntities1List = ArrayList(resultObj)
                    updateReturnViewState(returnViewState, resultObj)
                }
                return DataState.data(
                    response = Response(
                        messageId = R.string.error,
                        message = message,
                        uiComponentType = uiComponentType as UIComponentType,
                        messageType = MessageType.Success()
                    ),
                    data = returnViewState,
                    stateEvent = stateEvent
                )
            }
        }.getResult()

        emit(response)
    }

    // Method downloads entities from network and saves them in cache
    private suspend fun syncEntities(
        location : Location?,
    ) : List<FieldSurvey> {
        val firestoreSearchParameters =
            FirestoreEntity1SearchParameters(null, location)
        val networkEntitiesList = getNetworkEntities(firestoreSearchParameters)
        for((index, fieldSurvey: FieldSurvey) in networkEntitiesList.withIndex()) {
            if(LOG_ME)ALog.d(TAG, ".syncEntities(): $index. $fieldSurvey")
            try {
                fieldSurveyCacheDataSource.insertOrUpdateEntity(fieldSurvey)
            } catch (e: Exception) {
                ALog.e(TAG, "syncEntities", e)
            }
        }
        return networkEntitiesList
    }

    private suspend fun getNetworkEntities(searchParameters : FirestoreEntity1SearchParameters): List<FieldSurvey>{
        val networkResult = safeApiCall(Dispatchers.IO){
            fieldSurveyNetworkDataSource.searchEntities(searchParameters)
        }

        val response = object: ApiResponseHandler<List<FieldSurvey>, List<FieldSurvey>>(
            response = networkResult,
            stateEvent = null
        ){
            override suspend fun handleSuccess(resultObj: List<FieldSurvey>): DataState<List<FieldSurvey>>? {
                return DataState.data(
                    response = null,
                    data = resultObj,
                    stateEvent = null
                )
            }
        }.getResult()

        return response?.data ?: ArrayList()
    }

    companion object {
        const val GET_ENTITIES_SUCCESS = "Successfully got entities1 around the user."
        const val GET_ENTITY_NO_MATCHING_RESULTS = "There are no entities that match that query."
        const val SEARCH_ENTITIES_FAILED = "Failed to retrieve the list of entities."
    }
}
