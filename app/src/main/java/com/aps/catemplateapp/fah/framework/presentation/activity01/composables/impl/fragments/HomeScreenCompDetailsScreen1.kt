package com.aps.catemplateapp.fah.framework.presentation.activity01.composables.impl.fragments

import android.app.Activity
import android.widget.Toast
import androidx.compose.animation.core.Spring
import androidx.compose.animation.core.spring
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.BottomSheetValue
import androidx.compose.material.Button
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.MaterialTheme
import androidx.compose.material.SnackbarDuration
import androidx.compose.material.Text
import androidx.compose.material.rememberBottomSheetState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.semantics.contentDescription
import androidx.compose.ui.semantics.semantics
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.sp
import com.aps.catemplateapp.R
import com.aps.catemplateapp.common.business.domain.state.StateEventTracker
import com.aps.catemplateapp.common.framework.presentation.PermissionHandlingData
import com.aps.catemplateapp.common.framework.presentation.views.BoxWithBackground
import com.aps.catemplateapp.common.framework.presentation.views.DialogState
import com.aps.catemplateapp.common.framework.presentation.views.ProgressIndicatorState
import com.aps.catemplateapp.common.framework.presentation.views.ProgressIndicatorType
import com.aps.catemplateapp.common.framework.presentation.views.SnackBarState
import com.aps.catemplateapp.common.framework.presentation.views.SwipeableFragmentWithBottomSheetAndFAB
import com.aps.catemplateapp.common.framework.presentation.views.ToastState
import com.aps.catemplateapp.common.util.ALog
import com.aps.catemplateapp.fah.business.domain.model.entities.FieldSurvey
import com.aps.catemplateapp.fah.business.domain.model.factories.FieldSurveyFactory
import com.aps.catemplateapp.fah.framework.presentation.activity01.composables.impl.values.Dimens
import com.aps.catemplateapp.fah.framework.presentation.activity01.composables.impl.values.HomeScreenTheme
import com.aps.catemplateapp.fah.framework.presentation.activity01.state.HomeScreenStateEvent

private const val TAG = "HomeScreenCompDetailsScreen1"
private const val LOG_ME = true
const val HomeScreenCompDetailsScreen1TestTag = TAG
const val HomeScreenCompoDetailsScreen1CityTestTag = "HomeScreenCompDetailsScreen1CityTestTag"
const val HomeScreenCompDetailsScreen1ActionButtonTestTag = "HomeScreenCompDetailsScreen1ActionButtonTestTag"

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun HomeScreenCompDetailsScreen1(
    launchStateEvent: (HomeScreenStateEvent) -> Unit = {},

    activity: Activity? = null,
    permissionHandlingData: PermissionHandlingData = PermissionHandlingData(),

    stateEventTracker: StateEventTracker,

    snackBarState: SnackBarState?,
    toastState: ToastState?,
    dialogState: DialogState?,
    progressIndicatorState: ProgressIndicatorState?,

    entity: FieldSurvey?,
    actionOnEntity: (FieldSurvey?, ()-> Unit) -> Unit,
    navigateToProfileScreen: () -> Unit,
    isPreview: Boolean = false,
) {
    val permissionsRequiredInFragment = mutableSetOf<String>()

    val scope = rememberCoroutineScope()            // For bottom sheet if implemented.
    val sheetState = rememberBottomSheetState(
        initialValue = BottomSheetValue.Collapsed,
//        initialValue = BottomSheetValue.Expanded,
        animationSpec = spring(dampingRatio = Spring.DampingRatioHighBouncy)
    )
    val launchInitStateEvent: () -> Unit = {
        if(activity != null) {
            if(LOG_ME) ALog.d(TAG, "launchInitStateEvent(): " +
                    "activity != null, launching state event in view model.")
            // Nothing needed yet.
//            launchStateEvent(
//                HomeScreenStateEvent.GetAdditionalEntity1Details(entity)
//            )
        } else {
            if(LOG_ME) ALog.w(TAG, "launchInitStateEvent(): " +
                    "activity == null, couldn't set initial state event!")
        }
    }

    SwipeableFragmentWithBottomSheetAndFAB(
        launchInitStateEvent = launchInitStateEvent,
        activity = activity,
        permissionHandlingData = permissionHandlingData,
        permissionsRequiredInFragment = permissionsRequiredInFragment,
        backgroundDrawableId = R.drawable.default_background,
        onSwipeLeft = {},
        onSwipeRight = {},
        onSwipeUp = {},
        onSwipeDown = {},
        onSwipeRightFromComposableSide = {},
        onSwipeLeftFromComposableSide = {},
        onSwipeDownFromComposableSide = {},
        onSwipeUpFromComposableSide = {},
        leftSideThreshold = Dimens.fragmentsSwipingLeftSideThresholdFraction,
        floatingActionButtonDrawableId = null,
        floatingActionButtonOnClick = {},
        floatingActionButtonContentDescription = null,
        snackBarState = snackBarState,
        toastState = toastState,
        dialogState = dialogState,
        progressIndicatorState = progressIndicatorState,
        sheetState = sheetState,
        sheetContent = {},
        content = {
            HomeScreenComposableDetailsScreen1Content(
                entity = entity,
                actionOnEntity = actionOnEntity,
                navigateToProfileScreen = navigateToProfileScreen,
                isPreview = isPreview,
            )
        },
        isPreview = isPreview,
    )
}

@Composable
fun HomeScreenComposableDetailsScreen1Content(
    entity: FieldSurvey?,
    actionOnEntity: (FieldSurvey?, ()-> Unit) -> Unit,
    navigateToProfileScreen: () -> Unit,
    isPreview: Boolean = false,
) {
    BoxWithBackground(
        modifier = Modifier
            .fillMaxSize()
            .testTag(HomeScreenCompDetailsScreen1TestTag),
        contentAlignment = Alignment.Center
    ) {
        val cityContentDescription = stringResource(id = R.string.city)
        Column(
            modifier = Modifier
                .fillMaxSize()
                .verticalScroll(rememberScrollState()),
        ) {
            Text(text = "HomeScreenComposableDetailsScreen1")
            Text(text = "Placeholder")
            Row {
                Text(
                    text = "City:",
                    Modifier.semantics {
                        contentDescription = cityContentDescription
                    }
                )
                Text(
                    text = entity?.city ?: "",
                    Modifier
                        .semantics {
                            contentDescription = entity?.city ?: ""
                        }
                        .testTag(HomeScreenCompoDetailsScreen1CityTestTag)
                )
            }
            Button(
                modifier = Modifier
                    .testTag(HomeScreenCompDetailsScreen1ActionButtonTestTag)
                    .padding(
                        start = dimensionResource(R.dimen.button_horizontal_margin),
                        top = dimensionResource(R.dimen.button_vertical_margin),
                        end = dimensionResource(R.dimen.button_horizontal_margin),
                        bottom = dimensionResource(R.dimen.button_vertical_margin),
                    ),
                onClick = { actionOnEntity(entity, navigateToProfileScreen) },
            ) {
                Text(
                    text = stringResource(R.string.action),
                    color = MaterialTheme.colors.onPrimary,
                    fontSize = 14.sp
                )
            }

        }
    }
}

//========================================================================================
@Preview
@Composable
fun HomeScreenComposableDetailsScreen1Preview() {
    val previewEntity = FieldSurveyFactory.createPreviewEntitiesList()[0]
    val navigateToProfileScreen = {}
    val actionOnEntity: (FieldSurvey?, ()-> Unit) -> Unit = { _, _ -> }
    HomeScreenTheme {
        // Start Interactive Mode to actually see SnackBar
        val snackBarState = remember{
            SnackBarState(
                show = true,
                message = "Snackbar",
                actionLabel = "action",
                iconDrawableRes = R.drawable.ic_launcher,
                onClickAction = {},
                onDismissAction = {},
                duration = SnackbarDuration.Indefinite,
                updateSnackBarInViewModel = {},
            )
        }
        snackBarState.onDismissAction = { snackBarState.show = false }
        snackBarState.onClickAction = { snackBarState.show = false }

        val toastState = ToastState(
            show = true,
            message = "This is a Toast from Preview!",
            duration = Toast.LENGTH_LONG,
            updateToastInViewModel = {},
        )

        val dialogState = DialogState(
            show = false,
            onDismissRequest = {},
            title = "Dialog's Title",
            text = "Dialog's text.",
            leftButtonText = "Dismiss",
            leftButtonOnClick = {},
            rightButtonText = "Confirm",
            rightButtonOnClick = {},
            updateDialogInViewModel = {},
        )

        val progressIndicatorState = ProgressIndicatorState(
            show = false,
            progress = 0.7f,
            type = ProgressIndicatorType.DeterminateCircularProgressIndicator,
            updateProgressIndicatorStateInViewModel = {},
        )

        HomeScreenCompDetailsScreen1(
            stateEventTracker = StateEventTracker(),
            snackBarState = snackBarState,
            toastState = toastState,
            dialogState = dialogState,
            progressIndicatorState = progressIndicatorState,

            entity = previewEntity,
            actionOnEntity = actionOnEntity,
            navigateToProfileScreen = navigateToProfileScreen,
        )
    }
}