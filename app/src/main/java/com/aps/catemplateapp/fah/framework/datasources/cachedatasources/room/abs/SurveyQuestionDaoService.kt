package com.aps.catemplateapp.fah.framework.datasources.cachedatasources.room.abs


import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.fah.business.domain.model.entities.SurveyQuestion

interface SurveyQuestionDaoService {
    suspend fun insertOrUpdateEntity(entity: SurveyQuestion): SurveyQuestion

    suspend fun insertEntity(entity: SurveyQuestion): Long

    suspend fun insertEntities(Entities: List<SurveyQuestion>): LongArray

    suspend fun getEntityById(id: UniqueID?): SurveyQuestion?

    suspend fun updateEntity(
        id : UniqueID?,
        updated_at: String?,
        created_at: String?,

        picture1URI: String?,

        question: String?,
        answer: String?,

        fieldSurveyID: UniqueID?,
    ): Int

    suspend fun deleteEntity(primaryKey: UniqueID?): Int

    suspend fun deleteEntities(Entities: List<SurveyQuestion>): Int

    suspend fun searchEntities(): List<SurveyQuestion>

    suspend fun getAllEntities(): List<SurveyQuestion>

    suspend fun getNumEntities(): Int
}