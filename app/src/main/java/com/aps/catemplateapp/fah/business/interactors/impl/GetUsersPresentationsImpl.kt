package com.aps.catemplateapp.fah.business.interactors.impl

import com.aps.catemplateapp.R
import com.aps.catemplateapp.common.business.data.network.ApiResponseHandler
import com.aps.catemplateapp.common.business.data.util.safeApiCall
import com.aps.catemplateapp.common.business.domain.state.*
import com.aps.catemplateapp.common.util.ALog
import com.aps.catemplateapp.fah.business.data.cache.abstraction.UsersPresentationCacheDataSource
import com.aps.catemplateapp.fah.business.data.network.abs.UsersPresentationNetworkDataSource
import com.aps.catemplateapp.fah.business.domain.model.entities.UsersPresentation
import com.aps.catemplateapp.core.business.domain.model.entities.ProjectUser
import com.aps.catemplateapp.fah.business.domain.model.factories.UsersPresentationFactory
import com.aps.catemplateapp.fah.business.interactors.abs.GetUsersPresentations
import com.aps.catemplateapp.fah.framework.presentation.activity01.state.HomeScreenViewState
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

private const val TAG = "GetEntities2Impl"
private const val LOG_ME = true

class GetUsersPresentationsImpl
@Inject
constructor(
    private val cacheDataSource: UsersPresentationCacheDataSource,
    private val networkDataSource: UsersPresentationNetworkDataSource,
    private val entityFactory: UsersPresentationFactory
): GetUsersPresentations {
    override fun getUsersPresentations(
        stateEvent: StateEvent,
        returnViewState : HomeScreenViewState<ProjectUser>,
        updateReturnViewState : (HomeScreenViewState<ProjectUser>, List<UsersPresentation>?) -> HomeScreenViewState<ProjectUser>,
        onErrorAction: () -> Unit,
    ): Flow<DataState<HomeScreenViewState<ProjectUser>>?> = flow {
        val userTrips = safeApiCall(
            dispatcher = Dispatchers.IO,
            onErrorAction = onErrorAction,
            apiCall = {
                returnViewState.mainEntity?.id?.let { networkDataSource.getUsersEntities2(it) }
            }
        )

        val response = object : ApiResponseHandler<HomeScreenViewState<ProjectUser>, List<UsersPresentation>>(
            response = userTrips,
            stateEvent = stateEvent,
        ) {
            override suspend fun handleSuccess(resultObj: List<UsersPresentation>): DataState<HomeScreenViewState<ProjectUser>>? {
                var message: String? = GetUsersTripsImplConstants.GET_ENTITY_SUCCESS
                var uiComponentType: UIComponentType? = UIComponentType.None()
                ALog.d(TAG, "getEntities2().handleSuccess(): ")


                if(resultObj == null){
                    ALog.d(TAG, "getEntities2(): resultObj == null")
                    message = GetUsersTripsImplConstants.GET_ENTITY_NO_MATCHING_RESULTS
                    uiComponentType = UIComponentType.Toast()
                } else {
                    ALog.d(TAG, "getEntities2(): resultObj != null")
                    returnViewState.entities2List = ArrayList(resultObj)
                    updateReturnViewState(returnViewState, resultObj)
                }
                return DataState.data(
                    response = Response(
                        messageId = R.string.error,
                        message = message,
                        uiComponentType = uiComponentType as UIComponentType,
                        messageType = MessageType.Success()
                    ),
                    data = returnViewState,
                    stateEvent = stateEvent
                )
            }
        }.getResult()

        emit(response)
    }

    object GetUsersTripsImplConstants{
        const val GET_ENTITY_SUCCESS = "Successfully entities 2."
        const val GET_ENTITY_NO_MATCHING_RESULTS = "There are no entities 2 in database."
        const val SEARCH_ENTITIES_FAILED = "Failed to retrieve the list of entities 2."
    }
}
