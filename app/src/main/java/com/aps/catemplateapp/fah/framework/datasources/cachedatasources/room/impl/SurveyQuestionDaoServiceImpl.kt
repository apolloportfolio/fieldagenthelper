package com.aps.catemplateapp.fah.framework.datasources.cachedatasources.room.impl

import com.aps.catemplateapp.common.util.DateUtil
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.fah.business.domain.model.entities.SurveyQuestion
import com.aps.catemplateapp.fah.framework.datasources.cachedatasources.room.abs.SurveyQuestionDaoService
import com.aps.catemplateapp.fah.framework.datasources.cachedatasources.room.daos.SurveyQuestionDao
import com.aps.catemplateapp.fah.framework.datasources.cachedatasources.room.mappers.SurveyQuestionCacheMapper
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SurveyQuestionDaoServiceImpl
@Inject
constructor(
    private val dao: SurveyQuestionDao,
    private val mapper: SurveyQuestionCacheMapper,
    private val dateUtil: DateUtil
): SurveyQuestionDaoService {

    override suspend fun insertOrUpdateEntity(entity: SurveyQuestion): SurveyQuestion {
        val databaseEntity = dao.getEntityById(entity.id)
        if(databaseEntity != null) {
            dao.insert(mapper.mapToEntity(entity))
            return mapper.mapFromEntity(databaseEntity)
        } else {
            this.updateEntity(
                entity.id,
                entity.updated_at,
                entity.created_at,

                entity.picture1URI,

                entity.question,
                entity.answer,

                entity.fieldSurveyID,
            )
            return mapper.mapFromEntity(dao.getEntityById(entity.id)!!)
        }
    }


    override suspend fun insertEntity(entity: SurveyQuestion): Long {
        return dao.insert(mapper.mapToEntity(entity))
    }

    override suspend fun insertEntities(entities: List<SurveyQuestion>): LongArray {
        return dao.insertEntities(
            mapper.mapToEntityList(entities)
        )
    }

    override suspend fun getEntityById(id: UniqueID?): SurveyQuestion? {
        return dao.getEntityById(id)?.let { entity ->
            mapper.mapFromEntity(entity)
        }
    }

    override suspend fun updateEntity(
        id : UniqueID?,
        updated_at: String?,
        created_at: String?,

        picture1URI: String?,

        question: String?,
        answer: String?,

        fieldSurveyID: UniqueID?,
    ): Int {
        return if(updated_at != null){
            dao.updateEntity(
                id,
                updated_at,
                created_at,

                picture1URI,

                question,
                answer,

                fieldSurveyID,
            )
        } else {
            dao.updateEntity(
                id,
                dateUtil.getCurrentTimestamp(),
                created_at,

                picture1URI,

                question,
                answer,

                fieldSurveyID,
            )
        }

    }

    override suspend fun deleteEntity(primaryKey: UniqueID?): Int {
        return dao.deleteEntity(primaryKey)
    }

    override suspend fun deleteEntities(entities: List<SurveyQuestion>): Int {
        val ids = entities.mapIndexed {index, value -> value.id}
        return dao.deleteEntities(ids)
    }

    override suspend fun searchEntities(): List<SurveyQuestion> {
        return mapper.mapFromEntityList(
            dao.searchEntities()
        )
    }

    override suspend fun getAllEntities(): List<SurveyQuestion> {
        return mapper.mapFromEntityList(dao.getAllEntities())
    }

    override suspend fun getNumEntities(): Int {
        return dao.getNumEntities()
    }
}