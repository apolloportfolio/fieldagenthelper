package com.aps.catemplateapp.fah.framework.presentation.activity01.composables.impl.views

import android.app.Activity
import android.content.Intent
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Settings
import androidx.compose.material.icons.filled.Share
import androidx.compose.runtime.Composable
import androidx.compose.ui.tooling.preview.Preview
import com.aps.catemplateapp.R
import com.aps.catemplateapp.common.framework.presentation.views.appbar.AppBarItem
import com.aps.catemplateapp.common.framework.presentation.views.appbar.ShowAppBarItemAsAction
import com.aps.catemplateapp.common.framework.presentation.views.appbar.StandardAppBar
import com.aps.catemplateapp.common.util.ALog
import com.aps.catemplateapp.fah.framework.presentation.activity01.composables.impl.values.HomeScreenTheme

private const val TAG = "HomeScreenAppBar"
private const val LOG_ME = true

@Composable
fun HomeScreenAppBar(
    activity: Activity?,
    onBackButtonPress: () -> Unit,
    navigateToSettingsScreen: (() -> Unit)?,
) {

    val appBarItems: MutableList<AppBarItem> = mutableListOf()

    appBarItems.add(
        AppBarItem(
            nameRes = R.string.share_app,
            icon = Icons.Filled.Share,
            showAppBarItemAsAction = ShowAppBarItemAsAction.NEVER_OVERFLOW,
            doAction = { shareAppLink(activity) },
        )
    )

    if(navigateToSettingsScreen != null) {
        appBarItems.add(
            AppBarItem(
                nameRes = R.string.settings,
                icon = Icons.Filled.Settings,
                showAppBarItemAsAction = ShowAppBarItemAsAction.IF_NECESSARY,
                doAction = navigateToSettingsScreen,
            )
        )
    }

    StandardAppBar(
        R.string.homescreen_title,
        navigateBack = onBackButtonPress,
        appBarItems = appBarItems,
    )
}

internal fun shareAppLink(activity: Activity?) {
    if (activity == null) {
        if(LOG_ME) ALog.e(TAG, "shareAppWithOthers(): " +
                "activity == null")

    } else {
        val appPackageName = activity.packageName
        val playStoreUrl = "https://play.google.com/store/apps/details?id=$appPackageName"

        val intent = Intent(Intent.ACTION_SEND)
        intent.type = "text/plain"
        intent.putExtra(Intent.EXTRA_TEXT, playStoreUrl)

        activity.startActivity(Intent.createChooser(intent, "Share via"))
    }
}

// Preview =========================================================================================
@Preview
@Composable
fun HomeScreenAppBarPreview() {
    HomeScreenTheme {
        HomeScreenAppBar(
            null,
            {},
            {},
        )
    }
}