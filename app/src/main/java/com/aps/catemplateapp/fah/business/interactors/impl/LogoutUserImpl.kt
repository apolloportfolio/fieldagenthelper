package com.aps.catemplateapp.fah.business.interactors.impl


import com.aps.catemplateapp.R
import com.aps.catemplateapp.common.business.data.cache.CacheResponseHandler
import com.aps.catemplateapp.common.business.data.util.safeCacheCall
import com.aps.catemplateapp.common.business.domain.state.*
import com.aps.catemplateapp.common.util.ALog
import com.aps.catemplateapp.core.business.data.cache.abstraction.UserCacheDataSource
import com.aps.catemplateapp.core.business.data.network.abstraction.UserNetworkDataSource
import com.aps.catemplateapp.core.business.domain.model.entities.ProjectUser
import com.aps.catemplateapp.fah.business.interactors.abs.LogoutUser
import com.aps.catemplateapp.fah.business.interactors.impl.LogoutUserImpl.LogoutUserImplConstants.LOGOUT_SUCCESS
import com.aps.catemplateapp.fah.framework.presentation.activity01.state.HomeScreenViewState
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

private const val TAG = "LogoutUserImpl"
private const val LOG_ME = true

class LogoutUserImpl
@Inject
constructor(
    private val userCacheDataSource: UserCacheDataSource,
    private val userNetworkDataSource: UserNetworkDataSource,
): LogoutUser {

    override fun logout(
        user: ProjectUser,
        stateEvent: StateEvent,
        returnViewState: HomeScreenViewState<ProjectUser>,
        updateReturnViewState: (HomeScreenViewState<ProjectUser>) -> HomeScreenViewState<ProjectUser>,
    ): Flow<DataState<HomeScreenViewState<ProjectUser>>?> = flow {
        val logoutSuccessful = safeCacheCall(Dispatchers.IO) {
            userNetworkDataSource.logoutUser(user)
            userCacheDataSource.deleteAllEntities()
            true
        }

        val response = object : CacheResponseHandler<HomeScreenViewState<ProjectUser>, Boolean>(
            response = logoutSuccessful,
            stateEvent = stateEvent
        ) {
            override suspend fun handleSuccess(resultObj : Boolean): DataState<HomeScreenViewState<ProjectUser>>? {
                val methodName: String = "handleSuccess()"
                if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
                try {
                    val message: String? = LOGOUT_SUCCESS
                    val uiComponentType: UIComponentType? = UIComponentType.None()

                    updateReturnViewState(returnViewState)

                    return DataState.data(
                        response = Response(
                            messageId = R.string.error,
                            message = message,
                            uiComponentType = uiComponentType as UIComponentType,
                            messageType = MessageType.Success()
                        ),
                        data = returnViewState,
                        stateEvent = stateEvent
                    )
                } catch (e: Exception) {
                    ALog.e(TAG, methodName, e)
                    return null
                } finally {
                    if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
                }
            }
        }.getResult()

        emit(response)
    }

    object LogoutUserImplConstants{
        const val LOGOUT_SUCCESS = "Successfully logged out."
    }
}