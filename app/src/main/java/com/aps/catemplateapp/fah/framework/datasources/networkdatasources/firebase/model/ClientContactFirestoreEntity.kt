package com.aps.catemplateapp.fah.framework.datasources.networkdatasources.firebase.model

import androidx.room.Entity
import com.aps.catemplateapp.common.util.UniqueID
import com.google.firebase.firestore.GeoPoint
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.*

@Entity(tableName = "entities4")
data class ClientContactFirestoreEntity(
    @SerializedName("id")
    @Expose
    var id : UniqueID?,

    @SerializedName("created_at")
    @Expose
    val created_at: String?,

    @SerializedName("updated_at")
    @Expose
    var updated_at: String?,

    @SerializedName("surname")
    @Expose
    var surname: String?,

    @SerializedName("title")
    @Expose
    var title: String?,

    @SerializedName("phoneNumber")
    @Expose
    var phoneNumber: String?,

    @SerializedName("emailAddress")
    @Expose
    var emailAddress: String?,

    @SerializedName("lattitude")
    @Expose
    var latitude : Double?,

    @SerializedName("longitude")
    @Expose
    var longitude: Double?,

    @SerializedName("geoLocation")
    @Expose
    var geoLocation: GeoPoint?,

    @SerializedName("firestoreGeoLocation")
    @Expose
    var firestoreGeoLocation: Double?,

    @SerializedName("picture1URI")
    @Expose
    var picture1URI: String?,

    @SerializedName("name")
    @Expose
    var name : String?,

    @SerializedName("description")
    @Expose
    var description : String?,

    ) {
    constructor() : this(
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
    )
}