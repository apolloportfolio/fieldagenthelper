package com.aps.catemplateapp.fah.di

import com.aps.catemplateapp.core.framework.datasources.networkdatasources.firebase.abstraction.*
import com.aps.catemplateapp.core.framework.datasources.networkdatasources.firebase.implementation.*
import com.aps.catemplateapp.fah.framework.datasources.networkdatasources.firebase.abs.FieldSurveyFirestoreService
import com.aps.catemplateapp.fah.framework.datasources.networkdatasources.firebase.abs.UsersPresentationFirestoreService
import com.aps.catemplateapp.fah.framework.datasources.networkdatasources.firebase.abs.SurveyQuestionFirestoreService
import com.aps.catemplateapp.fah.framework.datasources.networkdatasources.firebase.abs.ClientContactFirestoreService
import com.aps.catemplateapp.fah.framework.datasources.networkdatasources.firebase.impl.FieldSurveyFirestoreServiceImpl
import com.aps.catemplateapp.fah.framework.datasources.networkdatasources.firebase.impl.UsersPresentationFirestoreServiceImpl
import com.aps.catemplateapp.fah.framework.datasources.networkdatasources.firebase.impl.SurveyQuestionFirestoreServiceImpl
import com.aps.catemplateapp.fah.framework.datasources.networkdatasources.firebase.impl.ClientContactFirestoreServiceImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
abstract class FirestoreDAOsModule {

    @Binds
    abstract fun bindFieldSurveyFirestoreService(implementation: FieldSurveyFirestoreServiceImpl): FieldSurveyFirestoreService

    @Binds
    abstract fun bindUsersPresentationFirestoreService(implementation: UsersPresentationFirestoreServiceImpl): UsersPresentationFirestoreService

    @Binds
    abstract fun bindSurveyQuestionFirestoreService(implementation: SurveyQuestionFirestoreServiceImpl): SurveyQuestionFirestoreService

    @Binds
    abstract fun bindClientContactFirestoreService(implementation: ClientContactFirestoreServiceImpl): ClientContactFirestoreService


}