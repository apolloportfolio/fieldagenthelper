package com.aps.catemplateapp.fah.business.data.cache.abstraction

import com.aps.catemplateapp.common.business.data.cache.StandardCacheDataSource
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.fah.business.domain.model.entities.SurveyQuestion

interface SurveyQuestionCacheDataSource: StandardCacheDataSource<SurveyQuestion> {
    override suspend fun insertOrUpdateEntity(entity: SurveyQuestion): SurveyQuestion?

    override suspend fun insertEntity(entity: SurveyQuestion): Long

    override suspend fun deleteEntity(primaryKey: UniqueID?): Int

    override suspend fun deleteEntities(entities: List<SurveyQuestion>): Int

    suspend fun updateEntity(
        id: UniqueID?,
        updated_at: String?,
        created_at: String?,

        picture1URI: String?,

        question: String?,
        answer: String?,

        fieldSurveyID: UniqueID?,
    ): Int

    override suspend fun searchEntities(
        query: String,
        filterAndOrder: String,
        page: Int
    ): List<SurveyQuestion>

    override suspend fun getAllEntities(): List<SurveyQuestion>

    override suspend fun getEntityById(id: UniqueID?): SurveyQuestion?

    override suspend fun getNumEntities(): Int

    override suspend fun insertEntities(entities: List<SurveyQuestion>): LongArray
}