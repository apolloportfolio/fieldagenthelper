package com.aps.catemplateapp.fah.framework.datasources.networkdatasources.firebase.abs

import android.net.Uri
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.core.framework.datasources.networkdatasources.firebase.abstraction.BasicFirestoreService
import com.aps.catemplateapp.fah.business.domain.model.entities.FieldSurvey
import com.aps.catemplateapp.fah.business.interactors.impl.FirestoreEntity1SearchParameters
import com.aps.catemplateapp.fah.framework.datasources.networkdatasources.firebase.mappers.FieldSurveyFirestoreMapper
import com.aps.catemplateapp.fah.framework.datasources.networkdatasources.firebase.model.FieldSurveyFirestoreEntity

interface FieldSurveyFirestoreService: BasicFirestoreService<
        FieldSurvey,
        FieldSurveyFirestoreEntity,
        FieldSurveyFirestoreMapper
        > {
    suspend fun searchEntities(
        searchParameters : FirestoreEntity1SearchParameters
    ) : List<FieldSurvey>?

    suspend fun getUsersEntities1(userID: UserUniqueID): List<FieldSurvey>?

    suspend fun uploadEntity1PhotoToFirestore(
        entity : FieldSurvey,
        entitysPhotoUri : Uri,
        entitysPhotoNumber: Int,
    ) : String?
}