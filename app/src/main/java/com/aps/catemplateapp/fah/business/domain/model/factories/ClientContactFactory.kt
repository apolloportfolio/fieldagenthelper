package com.aps.catemplateapp.fah.business.domain.model.factories

import com.aps.catemplateapp.common.util.DateUtil
import com.aps.catemplateapp.common.util.ParcelableGeoPoint
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.fah.business.domain.model.entities.ClientContact
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ClientContactFactory
@Inject
constructor(
    private val dateUtil: DateUtil
) {
    fun createSingleEntity(
        id: UniqueID?,

        surname: String?,
        title: String?,
        phoneNumber: String?,
        emailAddress: String?,

        latitude : Double?,
        longitude: Double?,
        geoLocation: ParcelableGeoPoint?,
        firestoreGeoLocation: Double?,

        picture1URI: String?,
        name: String?,
        description : String?,
    ): ClientContact {
        return ClientContact(
            id,
            dateUtil.getCurrentTimestamp(),
            dateUtil.getCurrentTimestamp(),

            surname,
            title,
            phoneNumber,
            emailAddress,

            latitude,
            longitude,
            geoLocation,
            firestoreGeoLocation,

            picture1URI,
            name,
            description,
        )
    }

    fun createEntitiesList(numEntities: Int): List<ClientContact> {
        val list: ArrayList<ClientContact> = ArrayList()
        for(i in 0 until numEntities){ // exclusive on upper bound
            list.add(
                createSingleEntity(
                    UniqueID.generateFakeRandomID(),
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                )
            )
        }
        return list
    }


    fun generateEmpty(): ClientContact {
        return createSingleEntity(
            UniqueID.generateFakeRandomID(),
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
        )
    }

    companion object {
        fun createPreviewEntitiesList(): List<ClientContact> {
            return arrayListOf(
                ClientContact(
                    id = UniqueID("client01"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    name = "Joanna",
                    surname = "Tracer",
                    title = "Ms",
                    phoneNumber = "789 567 432",
                    emailAddress = "john.doe@example.com",
                    latitude = 54.23154,
                    longitude = 21.614117,
                    geoLocation = null,
                    firestoreGeoLocation = null,
                    picture1URI = "1",
                    description = "Joanna Tracer is a potential client interested in urban planning services. He has shown keen interest in sustainable development projects and seeks consultation on implementing eco-friendly initiatives in his community."
                ),
                ClientContact(
                    id = UniqueID("client02"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    name = "Alice",
                    surname = "Smith",
                    title = "Ms",
                    phoneNumber = "789 567 432",
                    emailAddress = "alice.smith@example.com",
                    latitude = 54.23154,
                    longitude = 21.614117,
                    geoLocation = null,
                    firestoreGeoLocation = null,
                    picture1URI = "1",
                    description = "Alice Smith is a business owner seeking consultation on urban development for her commercial property. She aims to enhance the aesthetic appeal and functionality of her business premises to attract more customers."
                ),
                ClientContact(
                    id = UniqueID("client03"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    name = "David",
                    surname = "Brown",
                    title = "Mr",
                    phoneNumber = "789 567 432",
                    emailAddress = "david.brown@example.com",
                    latitude = 54.23154,
                    longitude = 21.614117,
                    geoLocation = null,
                    firestoreGeoLocation = null,
                    picture1URI = "1",
                    description = "David Brown is a community leader advocating for better infrastructure in his neighborhood. He seeks assistance in urban planning to address issues such as traffic congestion and public space utilization."
                ),
                ClientContact(
                    id = UniqueID("client04"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    name = "Emily",
                    surname = "Johnson",
                    title = "Ms",
                    phoneNumber = "789 567 432",
                    emailAddress = "emily.johnson@example.com",
                    latitude = 51.23164,
                    longitude = 22.614167,
                    geoLocation = null,
                    firestoreGeoLocation = null,
                    picture1URI = "1",
                    description = "Emily Johnson is a real estate developer planning a new residential project. She seeks expert advice on urban design and zoning regulations to ensure compliance and maximize the project's potential."
                ),
                ClientContact(
                    id = UniqueID("client05"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    name = "Michael",
                    surname = "Williams",
                    title = "Mr",
                    phoneNumber = "789 567 432",
                    emailAddress = "michael.williams@example.com",
                    latitude = 51.23254,
                    longitude = 22.615117,
                    geoLocation = null,
                    firestoreGeoLocation = null,
                    picture1URI = "1",
                    description = "Michael Williams is a government official responsible for urban development projects. He seeks collaboration with experienced urban planners to implement sustainable initiatives and improve quality of life for residents."
                ),
                ClientContact(
                    id = UniqueID("client06"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    name = "Sophia",
                    surname = "Miller",
                    title = "Ms",
                    phoneNumber = "789 567 432",
                    emailAddress = "sophia.miller@example.com",
                    latitude = 51.23354,
                    longitude = 22.614717,
                    geoLocation = null,
                    firestoreGeoLocation = null,
                    picture1URI = "1",
                    description = "Sophia Miller is an environmental activist advocating for green spaces in urban areas. She seeks guidance on community engagement strategies to mobilize support for conservation efforts and sustainable development projects."
                ),
                ClientContact(
                    id = UniqueID("client07"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    name = "Daniel",
                    surname = "Taylor",
                    title = "Mr",
                    phoneNumber = "789 567 432",
                    emailAddress = "daniel.taylor@example.com",
                    latitude = 51.23154,
                    longitude = 22.624117,
                    geoLocation = null,
                    firestoreGeoLocation = null,
                    picture1URI = "1",
                    description = "Daniel Taylor is a property developer interested in urban revitalization projects. He seeks expertise in adaptive reuse and heritage conservation to preserve historical landmarks while repurposing them for modern uses."
                ),
                ClientContact(
                    id = UniqueID("client08"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    name = "Olivia",
                    surname = "Anderson",
                    title = "Ms",
                    phoneNumber = "789 567 432",
                    emailAddress = "olivia.anderson@example.com",
                    latitude = 51.23154,
                    longitude = 22.664117,
                    geoLocation = null,
                    firestoreGeoLocation = null,
                    picture1URI = "1",
                    description = "Olivia Anderson is a city council member advocating for smart city initiatives. She seeks innovative solutions for urban challenges such as traffic management, waste disposal, and energy efficiency."
                ),
                ClientContact(
                    id = UniqueID("client09"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    name = "Ethan",
                    surname = "Thomas",
                    title = "Mr",
                    phoneNumber = "789 567 432",
                    emailAddress = "ethan.thomas@example.com",
                    latitude = 51.23154,
                    longitude = 22.6147,
                    geoLocation = null,
                    firestoreGeoLocation = null,
                    picture1URI = "1",
                    description = "Ethan Thomas is a business owner seeking expansion opportunities in urban areas. He seeks consultation on market analysis and location strategies to identify lucrative investment opportunities."
                ),
                ClientContact(
                    id = UniqueID("client10"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    name = "Isabella",
                    surname = "Martinez",
                    title = "Ms",
                    phoneNumber = "789 567 432",
                    emailAddress = "isabella.martinez@example.com",
                    latitude = 51.23154,
                    longitude = 22.714117,
                    geoLocation = null,
                    firestoreGeoLocation = null,
                    picture1URI = "1",
                    description = "Isabella Martinez is a community organizer advocating for affordable housing solutions. She seeks collaboration with urban planners to develop inclusive housing policies and create opportunities for marginalized communities."
                )
            )
        }
    }
}