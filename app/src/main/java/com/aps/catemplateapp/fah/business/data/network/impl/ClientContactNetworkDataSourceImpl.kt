package com.aps.catemplateapp.fah.business.data.network.impl

import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.fah.business.data.network.abs.ClientContactNetworkDataSource
import com.aps.catemplateapp.fah.business.domain.model.entities.ClientContact
import com.aps.catemplateapp.fah.framework.datasources.networkdatasources.firebase.abs.ClientContactFirestoreService
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ClientContactNetworkDataSourceImpl
@Inject
constructor(
    private val firestoreService: ClientContactFirestoreService
): ClientContactNetworkDataSource {
    override suspend fun insertOrUpdateEntity(entity: ClientContact): ClientContact? {
        return firestoreService.insertOrUpdateEntity(entity)
    }

    override suspend fun deleteEntity(primaryKey: UniqueID?) {
        return firestoreService.deleteEntity(primaryKey)
    }

    override suspend fun insertDeletedEntity(entity: ClientContact) {
        return firestoreService.insertDeletedEntity(entity)
    }

    override suspend fun insertDeletedEntities(entities: List<ClientContact>) {
        return firestoreService.insertDeletedEntities(entities)
    }

    override suspend fun deleteDeletedEntity(entity: ClientContact) {
        return firestoreService.deleteDeletedEntity(entity)
    }

    override suspend fun getDeletedEntities(): List<ClientContact> {
        return firestoreService.getAllDeletedEntities()
    }

    override suspend fun deleteAllEntities() {
        firestoreService.deleteAllEntities()
    }

    override suspend fun searchEntity(entity: ClientContact): ClientContact? {
        return firestoreService.searchEntity(entity)
    }

    override suspend fun getAllEntities(): List<ClientContact> {
        return firestoreService.getAllEntities()
    }

    override suspend fun insertOrUpdateEntities(entities: List<ClientContact>): List<ClientContact>? {
        return firestoreService.insertOrUpdateEntities(entities)
    }

    override suspend fun getEntityById(id: UniqueID): ClientContact? {
        return firestoreService.getEntityById(id)
    }
}