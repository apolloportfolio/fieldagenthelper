package com.aps.catemplateapp.fah.business.domain.model.factories

import com.aps.catemplateapp.common.util.DateUtil
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.fah.business.domain.model.entities.UsersPresentation
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class UsersPresentationFactory
@Inject
constructor(
    private val dateUtil: DateUtil
) {
    fun createSingleEntity(
        id: UniqueID?,

        presentationFileURI: String?,

        picture1URI: String?,

        name: String?,
        description : String?,

        ownerID: UserUniqueID?,
    ): UsersPresentation {
        return UsersPresentation(
            id,
            dateUtil.getCurrentTimestamp(),
            dateUtil.getCurrentTimestamp(),

            presentationFileURI,

            picture1URI,
            name,
            description,
            ownerID,
        )
    }

    fun createEntitiesList(numEntities: Int): List<UsersPresentation> {
        val list: ArrayList<UsersPresentation> = ArrayList()
        for(i in 0 until numEntities){ // exclusive on upper bound
            list.add(
                createSingleEntity(
                    UniqueID.generateFakeRandomID(),
                    null,
                    null,
                    null,
                    null,
                    null,
                )
            )
        }
        return list
    }


    fun generateEmpty(): UsersPresentation {
        return createSingleEntity(
            UniqueID.generateFakeRandomID(),
            null,
            null,
            null,
            null,
            null,
        )
    }

    companion object {
        fun createPreviewEntitiesList(): List<UsersPresentation> {
            return arrayListOf(
                UsersPresentation(
                    id = UniqueID("presentation01"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    presentationFileURI = null,
                    picture1URI = "1",
                    name = "Introduction to Urban Planning",
                    description = "This presentation provides an overview of urban planning principles and practices. It covers topics such as city zoning, transportation planning, and sustainable development. Whether you're a student or a professional in the field, this presentation offers valuable insights into shaping the cities of tomorrow.",
                    ownerID = UserUniqueID("666", "666")
                ),
                UsersPresentation(
                    id = UniqueID("presentation02"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    presentationFileURI = null,
                    picture1URI = "1",
                    name = "Environmental Sustainability Strategies",
                    description = "This presentation explores strategies for promoting environmental sustainability in urban environments. It discusses green infrastructure, energy efficiency, and waste management practices. Whether you're an environmental advocate or a policymaker, this presentation offers actionable solutions for creating greener cities.",
                    ownerID = UserUniqueID("666", "666")
                ),
                UsersPresentation(
                    id = UniqueID("presentation03"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    presentationFileURI = null,
                    picture1URI = "1",
                    name = "Smart Cities Technology",
                    description = "This presentation explores the role of technology in building smart cities of the future. It covers topics such as Internet of Things (IoT), data analytics, and urban mobility solutions. Whether you're a technologist or a city planner, this presentation offers insights into harnessing technology for urban development.",
                    ownerID = UserUniqueID("666", "666")
                ),
                UsersPresentation(
                    id = UniqueID("presentation04"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    presentationFileURI = null,
                    picture1URI = "1",
                    name = "Community Engagement Strategies",
                    description = "This presentation discusses strategies for engaging communities in the urban planning process. It covers methods such as participatory budgeting, citizen workshops, and online platforms for feedback. Whether you're a community organizer or a city official, this presentation offers practical approaches to fostering inclusive decision-making.",
                    ownerID = UserUniqueID("666", "666")
                ),
                UsersPresentation(
                    id = UniqueID("presentation05"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    presentationFileURI = null,
                    picture1URI = "1",
                    name = "Urban Design Principles",
                    description = "This presentation explores fundamental principles of urban design. It covers topics such as walkability, mixed land use, and placemaking strategies. Whether you're an architect or a city planner, this presentation offers design principles to create vibrant and livable urban spaces.",
                    ownerID = UserUniqueID("666", "666")
                ),
                UsersPresentation(
                    id = UniqueID("presentation06"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    presentationFileURI = null,
                    picture1URI = "1",
                    name = "Public Transit Planning",
                    description = "This presentation delves into the planning and design of public transportation systems. It covers topics such as transit-oriented development, bus rapid transit, and last-mile connectivity solutions. Whether you're a transportation engineer or a policymaker, this presentation offers insights into creating efficient and accessible transit networks.",
                    ownerID = UserUniqueID("666", "666")
                ),
                UsersPresentation(
                    id = UniqueID("presentation07"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    presentationFileURI = null,
                    picture1URI = "1",
                    name = "Green Building Technologies",
                    description = "This presentation explores sustainable building technologies and practices. It covers topics such as passive design, renewable energy integration, and green materials. Whether you're an architect or a developer, this presentation offers innovative solutions for constructing eco-friendly buildings.",
                    ownerID = UserUniqueID("666", "666")
                ),
                UsersPresentation(
                    id = UniqueID("presentation08"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    presentationFileURI = null,
                    picture1URI = "1",
                    name = "Resilient Cities Planning",
                    description = "This presentation discusses strategies for building resilient cities that can withstand and recover from various shocks and stresses. It covers topics such as climate adaptation, disaster preparedness, and community resilience-building efforts. Whether you're a disaster planner or a resilience advocate, this presentation offers strategies for creating more resilient urban environments.",
                    ownerID = UserUniqueID("666", "666")
                ),
                UsersPresentation(
                    id = UniqueID("presentation09"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    presentationFileURI = null,
                    picture1URI = "1",
                    name = "Urban Renewal Strategies",
                    description = "This presentation explores strategies for revitalizing urban areas and promoting equitable development. It covers topics such as brownfield redevelopment, affordable housing initiatives, and community-driven revitalization efforts. Whether you're a developer or an urban planner, this presentation offers approaches to fostering inclusive and sustainable urban renewal.",
                    ownerID = UserUniqueID("666", "666")
                ),
                UsersPresentation(
                    id = UniqueID("presentation10"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    presentationFileURI = null,
                    picture1URI = "1",
                    name = "Inclusive Urban Design",
                    description = "This presentation examines the principles and practices of inclusive urban design. It covers topics such as universal design, accessibility standards, and inclusive public spaces. Whether you're an advocate for disability rights or a city planner, this presentation offers strategies for creating cities that are accessible and welcoming for all.",
                    ownerID = UserUniqueID("666", "666")
                )
            )
        }

    }
}