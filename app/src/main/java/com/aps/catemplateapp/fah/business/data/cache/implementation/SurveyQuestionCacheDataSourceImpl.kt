package com.aps.catemplateapp.fah.business.data.cache.implementation


import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.fah.business.data.cache.abstraction.SurveyQuestionCacheDataSource
import com.aps.catemplateapp.fah.business.domain.model.entities.SurveyQuestion
import com.aps.catemplateapp.fah.framework.datasources.cachedatasources.room.abs.SurveyQuestionDaoService
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SurveyQuestionCacheDataSourceImpl
@Inject
constructor(
    private val entityDaoService: SurveyQuestionDaoService
): SurveyQuestionCacheDataSource {

    override suspend fun insertOrUpdateEntity(entity: SurveyQuestion): SurveyQuestion? {
        return entityDaoService.insertOrUpdateEntity(entity)
    }

    override suspend fun insertEntity(entity: SurveyQuestion): Long {
        return entityDaoService.insertEntity(entity)
    }

    override suspend fun deleteEntity(primaryKey: UniqueID?): Int {
        return entityDaoService.deleteEntity(primaryKey)
    }

    override suspend fun deleteEntities(entities: List<SurveyQuestion>): Int {
        return entityDaoService.deleteEntities(entities)
    }

    override suspend fun updateEntity(
        id: UniqueID?,
        updated_at: String?,
        created_at: String?,

        picture1URI: String?,

        question: String?,
        answer: String?,

        fieldSurveyID: UniqueID?,
    ): Int {
        return entityDaoService.updateEntity(
            id,
            updated_at,
            created_at,

            picture1URI,

            question,
            answer,

            fieldSurveyID,
        )
    }

    override suspend fun searchEntities(
        query: String,
        filterAndOrder: String,
        page: Int
    ): List<SurveyQuestion> {
        return entityDaoService.searchEntities()
    }

    override suspend fun getAllEntities(): List<SurveyQuestion> {
        return entityDaoService.getAllEntities()
    }

    override suspend fun getEntityById(id: UniqueID?): SurveyQuestion? {
        return entityDaoService.getEntityById(id)
    }

    override suspend fun getNumEntities(): Int {
        return entityDaoService.getNumEntities()
    }

    override suspend fun insertEntities(entities: List<SurveyQuestion>): LongArray{
        return entityDaoService.insertEntities(entities)
    }
}