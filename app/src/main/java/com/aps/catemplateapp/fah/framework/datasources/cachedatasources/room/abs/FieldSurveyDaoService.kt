package com.aps.catemplateapp.fah.framework.datasources.cachedatasources.room.abs

import com.aps.catemplateapp.common.util.ParcelableGeoPoint
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.fah.business.domain.model.entities.FieldSurvey

interface FieldSurveyDaoService {
    suspend fun insertOrUpdateEntity(entity: FieldSurvey): FieldSurvey

    suspend fun insertEntity(entity: FieldSurvey): Long

    suspend fun insertEntities(Entities: List<FieldSurvey>): LongArray

    suspend fun getEntityById(id: UniqueID?): FieldSurvey?

    suspend fun updateEntity(
        id: UniqueID?,
        created_at: String?,
        updated_at: String?,

        creatorId: UserUniqueID?,
        surveyedClientContactID: UniqueID?,
        fillingTime: String?,
        isTemplate: Boolean,

        latitude : Double?,
        longitude: Double?,
        geoLocation: ParcelableGeoPoint?,
        firestoreGeoLocation: Double?,

        picture1URI: String?,
        description : String?,

        city: String?,

        ownerID: UserUniqueID?,

        name: String?,

        switch1: Boolean?,
        switch2: Boolean?,
        switch3: Boolean?,
        switch4: Boolean?,
        switch5: Boolean?,
        switch6: Boolean?,
        switch7: Boolean?,
    ): Int

    suspend fun deleteEntity(primaryKey: UniqueID?): Int

    suspend fun deleteEntities(Entities: List<FieldSurvey>): Int

    suspend fun searchEntities(): List<FieldSurvey>

    suspend fun getAllEntities(): List<FieldSurvey>

    suspend fun getNumEntities(): Int
}