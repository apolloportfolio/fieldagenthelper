package com.aps.catemplateapp.fah.framework.datasources.networkdatasources.firebase.model

import androidx.room.Entity
import com.aps.catemplateapp.common.util.UniqueID
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.*

@Entity(tableName = "entities3")
data class SurveyQuestionFirestoreEntity(
    @SerializedName("id")
    @Expose
    var id: UniqueID?,

    @SerializedName("created_at")
    @Expose
    val created_at: String?,

    @SerializedName("updated_at")
    @Expose
    var updated_at: String?,

    @SerializedName("picture1URI")
    @Expose
    var picture1URI: String?,

    @SerializedName("question")
    @Expose
    var question: String?,

    @SerializedName("answer")
    @Expose
    var answer: String?,

    @SerializedName("fieldSurveyID")
    @Expose
    var fieldSurveyID: UniqueID?,

    ) {
    constructor() : this(
        null,
        null,
        null,
        null,
        null,
        null,
        null,
    )
}