package com.aps.catemplateapp.fah.framework.presentation.activity01.composables.impl.fragments

import android.annotation.SuppressLint
import androidx.compose.animation.core.Spring
import androidx.compose.animation.core.spring
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.constraintlayout.compose.ConstraintLayout
import com.aps.catemplateapp.R
import com.aps.catemplateapp.common.framework.presentation.views.SegmentedControl
import com.aps.catemplateapp.common.util.ALog
import com.aps.catemplateapp.fah.framework.presentation.activity01.composables.impl.values.ColorTransparent
import com.aps.catemplateapp.fah.framework.presentation.activity01.composables.impl.values.HomeScreenTheme
import kotlinx.coroutines.CoroutineScope

private const val TAG = "HomeScreenCompFragment3BottomSheet"
private const val LOG_ME = true

@SuppressLint("ComposableNaming")
@OptIn(ExperimentalMaterialApi::class)
@Composable
fun HomeScreenCompFragment3BottomSheet(
    scope: CoroutineScope,
    sheetState: BottomSheetState,
    toggleBottomSheetState: () -> Unit =
        remember { standardToggleBottomSheet(scope, sheetState) },
    leftButtonOnClick: () -> Unit,
    rightButtonOnClick: () -> Unit,
): @Composable ColumnScope.() -> Unit {
    return {
        HomeScreenComposableFragment3BottomSheetContent(
            scope,
            sheetState,
            toggleBottomSheetState,
            leftButtonOnClick,
            rightButtonOnClick,
        )
    }
}

@OptIn(ExperimentalMaterialApi::class)
@Composable
internal fun HomeScreenComposableFragment3BottomSheetContent(
    scope: CoroutineScope,
    sheetState: BottomSheetState,
    toggleBottomSheetState: () -> Unit =
        remember { standardToggleBottomSheet(scope, sheetState) },
    leftButtonOnClick: () -> Unit,
    rightButtonOnClick: () -> Unit,
) {
    Box(modifier = Modifier
        .fillMaxWidth()
    ) {
        Image(
            painterResource(id = R.drawable.example_background_1),
            contentDescription = "",
            contentScale = ContentScale.FillBounds,
            modifier = Modifier.matchParentSize()
        )

        ConstraintLayout(
            modifier = Modifier
                .fillMaxWidth()
                .background(ColorTransparent)
        ) {
            val (
                bottomSheetCloseButton,
                leftButton,
                rightButton,
                inputsBackground,
            ) = createRefs()

            IconButton(
                onClick = toggleBottomSheetState,
                modifier = Modifier
                    .constrainAs(bottomSheetCloseButton) {
                        top.linkTo(parent.top)
                        end.linkTo(parent.end)
                    }
                    .fillMaxWidth(.15f)
                    .aspectRatio(1f)
                    .padding(
                        start = dimensionResource(id = R.dimen.rounded_corner_margin),
                        top = dimensionResource(id = R.dimen.rounded_corner_margin),
                        end = dimensionResource(id = R.dimen.rounded_corner_margin),
                        bottom = dimensionResource(id = R.dimen.rounded_corner_margin),
                    )
            ) {
                Icon(
                    painter = painterResource(R.drawable.ic_x_to_close),
                    contentDescription = stringResource(id = R.string.toggle_bottom_sheet_button),
                    tint = MaterialTheme.colors.secondary,
                )
            }

            Button(
                onClick = leftButtonOnClick,
                modifier = Modifier
                    .constrainAs(leftButton) {
                        top.linkTo(bottomSheetCloseButton.top)
                        bottom.linkTo(bottomSheetCloseButton.bottom)
                        start.linkTo(parent.start)
                        end.linkTo(rightButton.start)
                    }
                    .padding(
                        start = dimensionResource(R.dimen.button_horizontal_margin),
                        top = dimensionResource(R.dimen.button_vertical_margin),
                        end = dimensionResource(R.dimen.button_horizontal_margin),
                        bottom = dimensionResource(R.dimen.button_vertical_margin),
                    ),
            ) {
                Icon(
                    painter = painterResource(R.drawable.ic_filters),
                    contentDescription = stringResource(R.string.left_button),
                    modifier = Modifier.padding(end = 8.dp),
                )
                Text(
                    text = stringResource(R.string.left_button),
                    color = MaterialTheme.colors.onPrimary,
                    fontSize = 14.sp
                )
            }

            Button(
                onClick = rightButtonOnClick,
                modifier = Modifier
                    .constrainAs(rightButton) {
                        top.linkTo(bottomSheetCloseButton.top)
                        bottom.linkTo(bottomSheetCloseButton.bottom)
//                        start.linkTo(leftButton.start)
                        end.linkTo(bottomSheetCloseButton.start)
                    }
                    .padding(
                        start = dimensionResource(R.dimen.button_horizontal_margin),
                        top = dimensionResource(R.dimen.button_vertical_margin),
                        end = dimensionResource(R.dimen.button_horizontal_margin),
                        bottom = dimensionResource(R.dimen.button_vertical_margin),
                    ),
            ) {
                Text(
                    text = stringResource(R.string.right_button),
                    color = MaterialTheme.colors.onPrimary,
                    fontSize = 14.sp
                )
            }


            LazyColumn(
                modifier = Modifier
                    .fillMaxWidth()
                    .constrainAs(inputsBackground) {
                        top.linkTo(leftButton.bottom)
                        bottom.linkTo(parent.bottom)
                        start.linkTo(parent.start)
                        end.linkTo(parent.end)
                    }
                    .padding(
                        start = dimensionResource(R.dimen.button_horizontal_margin),
                        top = dimensionResource(R.dimen.button_vertical_margin),
                        end = dimensionResource(R.dimen.button_horizontal_margin),
                        bottom = dimensionResource(R.dimen.button_vertical_margin),
                    ),
            ) {
                item {
                    Text(text = stringResource(R.string.display_option))
                    Spacer(modifier = Modifier.height(5.dp))
                    val items = listOf("One", "Two", "Three", "Four")
                    SegmentedControl(
                        items = items,
                        defaultSelectedItemIndex = 0,
                        color = R.color.purple_200,
                        cornerRadius = 50
                    ) {
                        if(LOG_ME) ALog.d(TAG, ": " +
                                "Selected item: ${items[it]}")
                    }
                }
            }
        }
    }
}

@OptIn(ExperimentalMaterialApi::class)
@Preview
@Composable
fun HomeScreenComposableFragment3BottomSheetPreview() {
    HomeScreenTheme {
//        HelloWorldComposable()
        HomeScreenComposableFragment3BottomSheetContent(
            scope = rememberCoroutineScope(),
            sheetState = rememberBottomSheetState(
                initialValue = BottomSheetValue.Expanded,
                animationSpec = spring(dampingRatio = Spring.DampingRatioHighBouncy)
            ),
            leftButtonOnClick = { /* Left button click handler */ },
            rightButtonOnClick = { /* Right button click handler */ }
        )
    }
}

data class HomeScreenComposableFragment3BottomSheetActions(
    val leftButtonOnClick: () -> Unit,
    val rightButtonOnClick: () -> Unit,
)