package com.aps.catemplateapp.fah.framework.datasources.cachedatasources.room.mappers

import com.aps.catemplateapp.common.business.domain.model.EntityMapper
import com.aps.catemplateapp.fah.business.domain.model.entities.FieldSurvey
import com.aps.catemplateapp.fah.framework.datasources.cachedatasources.room.model.FieldSurveyCacheEntity
import javax.inject.Inject

class FieldSurveyCacheMapper
@Inject
constructor() : EntityMapper<FieldSurveyCacheEntity, FieldSurvey> {
    override fun mapFromEntity(entity: FieldSurveyCacheEntity): FieldSurvey {
        return FieldSurvey(
            entity.id,
            entity.created_at,
            entity.updated_at,

            entity.creatorId,
            entity.surveyedClientContactID,
            entity.fillingTime,
            entity.isTemplate ?: false,

            entity.latitude ,
            entity.longitude,
            entity.geoLocation,
            entity.firestoreGeoLocation,

            entity.picture1URI,
            entity.description,

            entity.city,

            entity.ownerID,

            entity.name,

            entity.switch1,
            entity.switch2,
            entity.switch3,
            entity.switch4,
            entity.switch5,
            entity.switch6,
            entity.switch7,
        )
    }

    override fun mapToEntity(domainModel: FieldSurvey): FieldSurveyCacheEntity {
        return FieldSurveyCacheEntity(
            domainModel.id!!,
            domainModel.created_at,
            domainModel.updated_at,

            domainModel.creatorId,
            domainModel.surveyedClientContactID,
            domainModel.fillingTime,
            domainModel.isTemplate ?: false,

            domainModel.latitude ,
            domainModel.longitude,
            domainModel.geoLocation,
            domainModel.firestoreGeoLocation,

            domainModel.picture1URI,
            domainModel.description,

            domainModel.city,

            domainModel.ownerID,

            domainModel.name,

            domainModel.switch1,
            domainModel.switch2,
            domainModel.switch3,
            domainModel.switch4,
            domainModel.switch5,
            domainModel.switch6,
            domainModel.switch7,
        )
    }

    override fun mapFromEntityList(entities : List<FieldSurveyCacheEntity>) : List<FieldSurvey> {
        return entities.map{mapFromEntity(it)}
    }

    override fun mapToEntityList(entities: List<FieldSurvey>): List<FieldSurveyCacheEntity> {
        return entities.map{mapToEntity(it)}
    }
}