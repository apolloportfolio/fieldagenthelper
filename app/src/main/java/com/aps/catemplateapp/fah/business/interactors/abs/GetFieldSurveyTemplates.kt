package com.aps.catemplateapp.fah.business.interactors.abs

import com.aps.catemplateapp.common.business.domain.state.DataState
import com.aps.catemplateapp.common.business.domain.state.StateEvent
import com.aps.catemplateapp.core.business.domain.model.entities.ProjectUser
import com.aps.catemplateapp.fah.business.domain.model.entities.FieldSurvey
import com.aps.catemplateapp.fah.framework.presentation.activity01.state.HomeScreenViewState
import kotlinx.coroutines.flow.Flow

interface GetFieldSurveyTemplates {
    fun getFieldSurveyTemplates(
        stateEvent: StateEvent,
        returnViewState : HomeScreenViewState<ProjectUser>,
        updateReturnViewState : (HomeScreenViewState<ProjectUser>, List<FieldSurvey>?) -> HomeScreenViewState<ProjectUser>,
    ): Flow<DataState<HomeScreenViewState<ProjectUser>>?>
}