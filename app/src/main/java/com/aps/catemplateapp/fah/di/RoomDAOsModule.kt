package com.aps.catemplateapp.fah.di

import com.aps.catemplateapp.core.framework.datasources.cachedatasources.room.abstraction.*
import com.aps.catemplateapp.core.framework.datasources.cachedatasources.room.implementation.*
import com.aps.catemplateapp.fah.framework.datasources.cachedatasources.room.abs.FieldSurveyDaoService
import com.aps.catemplateapp.fah.framework.datasources.cachedatasources.room.abs.UsersPresentationDaoService
import com.aps.catemplateapp.fah.framework.datasources.cachedatasources.room.abs.SurveyQuestionDaoService
import com.aps.catemplateapp.fah.framework.datasources.cachedatasources.room.abs.ClientContactDaoService
import com.aps.catemplateapp.fah.framework.datasources.cachedatasources.room.impl.FieldSurveyDaoServiceImpl
import com.aps.catemplateapp.fah.framework.datasources.cachedatasources.room.impl.UsersPresentationDaoServiceImpl
import com.aps.catemplateapp.fah.framework.datasources.cachedatasources.room.impl.SurveyQuestionDaoServiceImpl
import com.aps.catemplateapp.fah.framework.datasources.cachedatasources.room.impl.ClientContactDaoServiceImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
abstract class RoomDAOsModule {

    @Binds
    abstract fun bindFieldSurveyDaoService(implementation: FieldSurveyDaoServiceImpl): FieldSurveyDaoService

    @Binds
    abstract fun bindUsersPresentationDaoService(implementation: UsersPresentationDaoServiceImpl): UsersPresentationDaoService

    @Binds
    abstract fun bindSurveyQuestionDaoService(implementation: SurveyQuestionDaoServiceImpl): SurveyQuestionDaoService

    @Binds
    abstract fun bindClientContactDaoService(implementation: ClientContactDaoServiceImpl): ClientContactDaoService

}