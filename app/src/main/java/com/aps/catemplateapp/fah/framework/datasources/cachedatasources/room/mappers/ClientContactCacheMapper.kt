package com.aps.catemplateapp.fah.framework.datasources.cachedatasources.room.mappers

import com.aps.catemplateapp.common.business.domain.model.EntityMapper
import com.aps.catemplateapp.fah.business.domain.model.entities.ClientContact
import com.aps.catemplateapp.fah.framework.datasources.cachedatasources.room.model.ClientContactCacheEntity
import javax.inject.Inject

class ClientContactCacheMapper
@Inject
constructor() : EntityMapper<ClientContactCacheEntity, ClientContact> {
    override fun mapFromEntity(entity: ClientContactCacheEntity): ClientContact {
        return ClientContact(
            entity.id,
            entity.created_at,
            entity.updated_at,

            entity.surname,
            entity.title,
            entity.phoneNumber,
            entity.emailAddress,

            entity.latitude ,
            entity.longitude,
            entity.geoLocation,
            entity.firestoreGeoLocation,

            entity.picture1URI,
            entity.name,
            entity.description,
        )
    }

    override fun mapToEntity(domainModel: ClientContact): ClientContactCacheEntity {
        return ClientContactCacheEntity(
            domainModel.id!!,
            domainModel.created_at,
            domainModel.updated_at,

            domainModel.surname,
            domainModel.title,
            domainModel.phoneNumber,
            domainModel.emailAddress,

            domainModel.latitude ,
            domainModel.longitude,
            domainModel.geoLocation,
            domainModel.firestoreGeoLocation,

            domainModel.picture1URI,
            domainModel.name,
            domainModel.description,
        )
    }

    override fun mapFromEntityList(entities : List<ClientContactCacheEntity>) : List<ClientContact> {
        return entities.map{mapFromEntity(it)}
    }

    override fun mapToEntityList(entities: List<ClientContact>): List<ClientContactCacheEntity> {
        return entities.map{mapToEntity(it)}
    }
}