package com.aps.catemplateapp.fah.business.data.network.impl

import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.fah.business.data.network.abs.SurveyQuestionNetworkDataSource
import com.aps.catemplateapp.fah.business.domain.model.entities.SurveyQuestion
import com.aps.catemplateapp.fah.framework.datasources.networkdatasources.firebase.abs.SurveyQuestionFirestoreService
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SurveyQuestionNetworkDataSourceImpl
@Inject
constructor(
    private val firestoreService: SurveyQuestionFirestoreService
): SurveyQuestionNetworkDataSource {
    override suspend fun insertOrUpdateEntity(entity: SurveyQuestion): SurveyQuestion? {
        return firestoreService.insertOrUpdateEntity(entity)
    }

    override suspend fun deleteEntity(primaryKey: UniqueID?) {
        return firestoreService.deleteEntity(primaryKey)
    }

    override suspend fun insertDeletedEntity(entity: SurveyQuestion) {
        return firestoreService.insertDeletedEntity(entity)
    }

    override suspend fun insertDeletedEntities(entities: List<SurveyQuestion>) {
        return firestoreService.insertDeletedEntities(entities)
    }

    override suspend fun deleteDeletedEntity(entity: SurveyQuestion) {
        return firestoreService.deleteDeletedEntity(entity)
    }

    override suspend fun getDeletedEntities(): List<SurveyQuestion> {
        return firestoreService.getAllDeletedEntities()
    }

    override suspend fun deleteAllEntities() {
        firestoreService.deleteAllEntities()
    }

    override suspend fun searchEntity(entity: SurveyQuestion): SurveyQuestion? {
        return firestoreService.searchEntity(entity)
    }

    override suspend fun getAllEntities(): List<SurveyQuestion> {
        return firestoreService.getAllEntities()
    }

    override suspend fun insertOrUpdateEntities(entities: List<SurveyQuestion>): List<SurveyQuestion>? {
        return firestoreService.insertOrUpdateEntities(entities)
    }

    override suspend fun getEntityById(id: UniqueID): SurveyQuestion? {
        return firestoreService.getEntityById(id)
    }

    override suspend fun getUsersEntities3(userId: UserUniqueID): List<SurveyQuestion>? {
        return firestoreService.getUsersEntities3(userId)
    }
}