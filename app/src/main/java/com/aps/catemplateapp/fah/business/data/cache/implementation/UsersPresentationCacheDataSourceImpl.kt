package com.aps.catemplateapp.fah.business.data.cache.implementation


import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.fah.business.data.cache.abstraction.UsersPresentationCacheDataSource
import com.aps.catemplateapp.fah.business.domain.model.entities.UsersPresentation
import com.aps.catemplateapp.fah.framework.datasources.cachedatasources.room.abs.UsersPresentationDaoService
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class UsersPresentationCacheDataSourceImpl
@Inject
constructor(
    private val entityDaoService: UsersPresentationDaoService
): UsersPresentationCacheDataSource {

    override suspend fun insertOrUpdateEntity(entity: UsersPresentation): UsersPresentation? {
        return entityDaoService.insertOrUpdateEntity(entity)
    }

    override suspend fun insertEntity(entity: UsersPresentation): Long {
        return entityDaoService.insertEntity(entity)
    }

    override suspend fun deleteEntity(primaryKey: UniqueID?): Int {
        return entityDaoService.deleteEntity(primaryKey)
    }

    override suspend fun deleteEntities(entities: List<UsersPresentation>): Int {
        return entityDaoService.deleteEntities(entities)
    }

    override suspend fun updateEntity(
        id: UniqueID?,
        updated_at: String?,
        created_at: String?,

        presentationFileURI: String?,

        picture1URI: String?,

        name: String?,
        description: String?,

        ownerID: UserUniqueID?,
    ): Int {
        return entityDaoService.updateEntity(
            id,
            updated_at,
            created_at,

            presentationFileURI,

            picture1URI,

            name,
            description,

            ownerID,
        )
    }

    override suspend fun searchEntities(
        query: String,
        filterAndOrder: String,
        page: Int
    ): List<UsersPresentation> {
        return entityDaoService.searchEntities()
    }

    override suspend fun getAllEntities(): List<UsersPresentation> {
        return entityDaoService.getAllEntities()
    }

    override suspend fun getEntityById(id: UniqueID?): UsersPresentation? {
        return entityDaoService.getEntityById(id)
    }

    override suspend fun getNumEntities(): Int {
        return entityDaoService.getNumEntities()
    }

    override suspend fun insertEntities(entities: List<UsersPresentation>): LongArray{
        return entityDaoService.insertEntities(entities)
    }
}