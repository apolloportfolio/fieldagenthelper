package com.aps.catemplateapp.fah.business.data.network.impl

import android.net.Uri
import com.aps.catemplateapp.common.util.ALog
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.fah.business.data.network.abs.FieldSurveyNetworkDataSource
import com.aps.catemplateapp.fah.business.domain.model.entities.FieldSurvey
import com.aps.catemplateapp.fah.business.interactors.impl.FirestoreEntity1SearchParameters
import com.aps.catemplateapp.fah.framework.datasources.networkdatasources.firebase.abs.FieldSurveyFirestoreService
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class FieldSurveyNetworkDataSourceImpl
@Inject
constructor(
    private val firestoreService: FieldSurveyFirestoreService
): FieldSurveyNetworkDataSource {
    override suspend fun insertOrUpdateEntity(entity: FieldSurvey): FieldSurvey? {
        return firestoreService.insertOrUpdateEntity(entity)
    }

    override suspend fun deleteEntity(primaryKey: UniqueID?) {
        return firestoreService.deleteEntity(primaryKey)
    }

    override suspend fun insertDeletedEntity(entity: FieldSurvey) {
        return firestoreService.insertDeletedEntity(entity)
    }

    override suspend fun insertDeletedEntities(entities: List<FieldSurvey>) {
        return firestoreService.insertDeletedEntities(entities)
    }

    override suspend fun deleteDeletedEntity(entity: FieldSurvey) {
        return firestoreService.deleteDeletedEntity(entity)
    }

    override suspend fun getDeletedEntities(): List<FieldSurvey> {
        return firestoreService.getAllDeletedEntities()
    }

    override suspend fun deleteAllEntities() {
        firestoreService.deleteAllEntities()
    }

    override suspend fun searchEntity(entity: FieldSurvey): FieldSurvey? {
        return firestoreService.searchEntity(entity)
    }

    override suspend fun getAllEntities(): List<FieldSurvey> {
        return firestoreService.getAllEntities()
    }

    override suspend fun insertOrUpdateEntities(entities: List<FieldSurvey>): List<FieldSurvey>? {
        return firestoreService.insertOrUpdateEntities(entities)
    }

    override suspend fun getEntityById(id: UniqueID): FieldSurvey? {
        return firestoreService.getEntityById(id)
    }

    override suspend fun searchEntities(
        searchParameters : FirestoreEntity1SearchParameters
    ) : List<FieldSurvey>? {
        if(LOG_ME) ALog.d(TAG, ".searchEntities(): searchParameters:\n$searchParameters")
        return firestoreService.searchEntities(searchParameters)
    }

    override suspend fun getUsersFieldSurveyTemplates(userID: UserUniqueID) : List<FieldSurvey>? {
        return firestoreService.getUsersEntities1(userID)
    }

    override suspend fun uploadEntity1PhotoToFirestore(
        entity : FieldSurvey,
        entitysPhotoUri : Uri,
        entitysPhotoNumber: Int,
    ) : String? {
        return firestoreService.uploadEntity1PhotoToFirestore(
            entity,
            entitysPhotoUri,
            entitysPhotoNumber
        )
    }

    companion object {
        const val TAG = "Entity1NetworkDataSourceImpl"
        const val LOG_ME = true
        const val COLLECTION_NAME = "entity1"
        const val DELETES_COLLECTION_NAME = "entity1_d"
    }
}
