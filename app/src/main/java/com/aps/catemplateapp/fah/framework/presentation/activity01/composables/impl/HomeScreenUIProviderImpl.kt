package com.aps.catemplateapp.fah.framework.presentation.activity01.composables.impl

import android.app.Activity
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.res.stringResource
import com.aps.catemplateapp.R
import com.aps.catemplateapp.common.business.domain.state.StateEvent
import com.aps.catemplateapp.common.util.ALog
import com.aps.catemplateapp.fah.business.domain.model.entities.FieldSurvey
import com.aps.catemplateapp.fah.business.domain.model.entities.UsersPresentation
import com.aps.catemplateapp.fah.business.domain.model.entities.ClientContact
import com.aps.catemplateapp.fah.framework.presentation.activity01.HomeScreenViewModelCompose
import com.aps.catemplateapp.fah.framework.presentation.activity01.composables.HomeScreenDestination
import com.aps.catemplateapp.fah.framework.presentation.activity01.composables.abs.HomeScreenUIProvider
import com.aps.catemplateapp.fah.framework.presentation.activity01.composables.impl.fragments.HomeScreenComposableFragment1BottomSheetActions
import com.aps.catemplateapp.fah.framework.presentation.activity01.composables.impl.fragments.HomeScreenComposableFragment2BottomSheetActions
import com.aps.catemplateapp.fah.framework.presentation.activity01.composables.impl.fragments.HomeScreenComposableFragment3BottomSheetActions
import com.aps.catemplateapp.fah.framework.presentation.activity01.composables.impl.fragments.HomeScreenComposableFragment4BottomSheetActions
import com.aps.catemplateapp.fah.framework.presentation.activity01.composables.impl.fragments.HomeScreenComposableFragment5BottomSheetActions
import com.aps.catemplateapp.fah.framework.presentation.activity01.state.HomeScreenCard1SearchFilters
import com.aps.catemplateapp.fah.framework.presentation.activity01.state.HomeScreenStateEvent
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
// For debug purposes only:
//import android.widget.Toast
//import androidx.compose.animation.core.Spring
//import androidx.compose.animation.core.spring
//import androidx.compose.foundation.layout.Arrangement
//import androidx.compose.foundation.layout.Box
//import androidx.compose.foundation.layout.Column
//import androidx.compose.foundation.layout.fillMaxSize
//import androidx.compose.foundation.layout.fillMaxWidth
//import androidx.compose.foundation.layout.height
//import androidx.compose.foundation.layout.padding
//import androidx.compose.foundation.layout.wrapContentHeight
//import androidx.compose.material.BottomSheetScaffold
//import androidx.compose.material.BottomSheetValue
//import androidx.compose.material.Button
//import androidx.compose.material.ExperimentalMaterialApi
//import androidx.compose.material.SnackbarDuration
//import androidx.compose.material.SnackbarHost
//import androidx.compose.material.SnackbarHostState
//import androidx.compose.material.Text
//import androidx.compose.material.rememberBottomSheetScaffoldState
//import androidx.compose.material.rememberBottomSheetState
//import androidx.compose.runtime.LaunchedEffect
//import androidx.compose.runtime.movableContentOf
//import androidx.compose.runtime.mutableStateOf
//import androidx.compose.runtime.remember
//import androidx.compose.runtime.saveable.rememberSaveable
//import androidx.compose.runtime.setValue
//import androidx.compose.ui.Alignment
//import androidx.compose.ui.Modifier
//import androidx.compose.ui.platform.LocalContext
//import androidx.compose.ui.res.dimensionResource
//import androidx.compose.ui.unit.dp
//import androidx.compose.ui.zIndex
//import androidx.lifecycle.viewmodel.compose.viewModel
//import com.aps.catemplateapp.common.framework.presentation.PermissionHandlingData
//import com.aps.catemplateapp.common.framework.presentation.views.CustomSnackBar
//import com.aps.catemplateapp.common.framework.presentation.views.DialogState
//import com.aps.catemplateapp.common.framework.presentation.views.HelloWorldComposable
//import com.aps.catemplateapp.common.framework.presentation.views.ProgressIndicatorState
//import com.aps.catemplateapp.common.framework.presentation.views.ProgressIndicatorType
//import com.aps.catemplateapp.common.framework.presentation.views.ShowDialogIfNecessary
//import com.aps.catemplateapp.common.framework.presentation.views.SnackBarState
//import com.aps.catemplateapp.common.framework.presentation.views.ToastState
//import com.aps.catemplateapp.common.util.ARandomGenerator
//import com.aps.catemplateapp.core.business.domain.model.entities.ProjectUser
//import com.aps.catemplateapp.fah.framework.presentation.activity01.composables.implementation.fragments.HomeScreenComposableFragment1
//import com.aps.catemplateapp.fah.framework.presentation.activity01.state.HomeScreenViewState
// =================================================================================================

private const val TAG = "HomeScreenUIProviderImpl"
private const val LOG_ME = true
class HomeScreenUIProviderImpl: HomeScreenUIProvider {
    @OptIn(ExperimentalCoroutinesApi::class, FlowPreview::class)
    override fun getComposable(
        viewModel: HomeScreenViewModelCompose,
        activity: Activity,
    ): @Composable () -> Unit {
        return {
            val currentViewState by viewModel.getCurrentViewStateOrNewAsLiveData().observeAsState()
            if(LOG_ME) ALog.d(TAG, "getComposable(): " +
                    "Recomposition start. "
            )
            val launchStateEvent: (StateEvent) -> Unit = { stateEvent ->
                viewModel.setStateEvent(stateEvent)
            }

            val onUsersPresentationsSearchQueryUpdate: (String) -> Unit = { searchQuery ->
                viewModel.setUsersPresentationsSearchQuery(searchQuery)
                viewModel.setStateEvent(HomeScreenStateEvent.SearchUsersPresentationsStateEvent)
            }
            val onSearchQueryUpdateFieldSurveyTemplates: (String) -> Unit = { searchQuery ->
                viewModel.setFieldSurveysTemplatesSearchQuery(searchQuery)
                viewModel.setStateEvent(HomeScreenStateEvent.SearchFieldSurveyTemplatesStateEvent)
            }
            val onClientContactSearchQueryUpdate: (String) -> Unit = { searchQuery ->
                viewModel.setClientContactsSearchQuery(searchQuery)
                viewModel.setStateEvent(HomeScreenStateEvent.SearchClientContactsStateEvent)
            }

            // Permissions
            val permissionHandlingData = currentViewState!!.permissionHandlingData

            // Navigation & configuration change
            val stateEventTracker = currentViewState!!.stateEventTracker
            val deviceLocation = currentViewState!!.location
            val snackBarState = currentViewState!!.snackBarState
            val toastState = currentViewState!!.toastState
            val dialogStatus = currentViewState!!.dialogState
            val progressIndicatorState = currentViewState!!.progressIndicatorState
            val isDrawerOpen = currentViewState!!.isDrawerOpen
            val updateOrientation: (Boolean) -> Unit = { viewModel.updateIsDualPane(it) }
            val leftPaneStartDestination: String = HomeScreenDestination.Card1.route
            val rightPaneStartDestination: String = HomeScreenDestination.DetailsPlaceholderScreen.route

            // Card 1
            val showProfileStatusBar: Boolean = !(currentViewState!!.mainEntity?.fullyVerified ?: false)
            if(LOG_ME)ALog.d(TAG, "(): " +
                                    "showProfileStatusBar = $showProfileStatusBar")
            val card1SearchQuery = currentViewState!!.entity1SearchQuery ?: ""
            val card1OnSearchQueryUpdate: (String) -> Unit = { searchQuery ->
                viewModel.setEntity1SearchQuery(searchQuery)
                viewModel.setStateEvent(HomeScreenStateEvent.SearchFieldSurveysStateEvent)
            }
            val card1EntitiesList = currentViewState!!.searchedEntities1List
            val card1OnListItemClick: (FieldSurvey) -> Unit = { entity ->
                if(LOG_ME)ALog.d(TAG, "(): " +
                                        "Calling card1OnListItemClick.")
                viewModel.setCard1CurrentlyShownEntity(entity)
            }
            val searchFilters = currentViewState!!.getHomeScreenCard1SearchFilters()
            val onSearchFiltersUpdated: (HomeScreenCard1SearchFilters) -> Unit = { filters ->
                viewModel.setEntity1SearchFilters(filters)
            }
            val card1CurrentlyShownEntity = currentViewState!!.card1CurrentlyShownEntity
            val card1ActionOnEntity: (FieldSurvey?, ()-> Unit) -> Unit = { entity, callbackOnProfileIncomplete ->
                launchStateEvent(
                    HomeScreenStateEvent.ActOnEntity1(
                        entity,
                        callbackOnProfileIncomplete,
                    )
                )
            }

            // Card 2
            val floatingActionButtonDrawableIdCard2 = R.drawable.ic_search_yellow_24
            val floatingActionButtonOnClickCard2 = null
            val floatingActionButtonContentDescriptionCard2 = stringResource(
                id = R.string.home_screen_floating_action_button_content_description_card_2
            )
            val card2EntitiesList = currentViewState!!.entities2List
            val card2OnListItemClick: (UsersPresentation) -> Unit = { entity ->
                viewModel.setCard2CurrentlyShownEntity(entity)
            }

            // Card 3
            val floatingActionButtonDrawableIdCard3 = R.drawable.ic_baseline_add_24
            val floatingActionButtonOnClickCard3 = null
            val floatingActionButtonContentDescriptionCard3 = stringResource(
                id = R.string.home_screen_floating_action_button_content_description_card_3
            )
            val card3EntitiesList = currentViewState!!.usersSurveyTemplates
            val card3OnListItemClick: (FieldSurvey) -> Unit = { entity ->
                viewModel.setCard1CurrentlyShownEntity(entity)
            }

            // Card 4
            val floatingActionButtonDrawableIdCard4 = R.drawable.ic_baseline_add_24
            val floatingActionButtonOnClickCard4 = null
            val floatingActionButtonContentDescriptionCard4 = stringResource(
                id = R.string.home_screen_floating_action_button_content_description_card_4
            )
            val card4EntitiesList = currentViewState!!.entities4List
            val card4OnListItemClick: (ClientContact) -> Unit = { entity ->
                viewModel.setCard4CurrentlyShownEntity(entity)
            }

            // For debug purposes only:
//            val testShowToast = {
//                if(LOG_ME)ALog.d(TAG, "(): " +
//                        "Left button clicked")
//                val randomString = ARandomGenerator.generateRandomString(10)
//                if(LOG_ME)ALog.d(TAG, "HomeScreenUIComposableTest1(): " +
//                        "randomString == $randomString")
//                viewModel.setToastState(
//                    ToastState(
//                        true,
//                        "$TAG $randomString",
//                        updateToastInViewModel = { viewModel.setToastState(it) },
//                    )
//                )
//            }

            // For debug purposes only:
//            val testShowSnackBar = {
//                if(LOG_ME)ALog.d(TAG, "(): " +
//                        "Left button clicked")
//                val randomString = ARandomGenerator.generateRandomString(5)
//                if(LOG_ME)ALog.d(TAG, "HomeScreenUIComposableTest1(): " +
//                        "randomString == $randomString")
//                viewModel.setSnackBarState(
//                    SnackBarState(
//                        show = true,
//                        message = "Snackbar $randomString",
//                        actionLabel = randomString,
//                        iconDrawableRes = R.drawable.ic_launcher,
//                        onClickAction = {},
//                        onDismissAction = {},
//                        duration = SnackbarDuration.Indefinite,
//                        updateSnackBarInViewModel = { viewModel.setSnackBarState(it) },
//                    )
//                )
//            }

            // For debug purposes only:
//            val testShowDialog = {
//                if(LOG_ME)ALog.d(TAG, "(): " +
//                        "Left button clicked")
//                val randomString = ARandomGenerator.generateRandomString(5)
//                if(LOG_ME)ALog.d(TAG, "HomeScreenUIComposableTest1(): " +
//                        "randomString == $randomString")
//                viewModel.setDialogState(
//                    DialogState(
//                        show = true,
//                        onDismissRequest = {
//                            if(LOG_ME)ALog.d(TAG, "onDismissRequest(): " +
//                                    "Dialog's onDismissRequest.")
//                        },
//                        title = "Dialog $randomString",
//                        text = "Dialog's text.",
//                        leftButtonText = "Dismiss",
//                        leftButtonOnClick = {
//                            if(LOG_ME)ALog.d(TAG, "leftButtonOnClick(): " +
//                                    "Dialog's right button clicked.")
//                        },
//                        rightButtonText = "Confirm",
//                        rightButtonOnClick = {
//                            if(LOG_ME)ALog.d(TAG, "rightButtonOnClick(): " +
//                                    "Dialog's right button clicked.")
//                        },
//                        updateDialogInViewModel = {
//                            if(LOG_ME)ALog.d(TAG, "updateDialogInViewModel(): " +
//                                    "Updating dialog in viewModel.")
//                            viewModel.setDialogState(it)
//                        },
//                    )
//                )
//            }

            // For debug purposes only:
//            val testShowProgressIndicator: () -> Unit = {
//                if(LOG_ME)ALog.d(TAG, "(): " +
//                        "Left button clicked")
//                val randomFloat = ARandomGenerator.getRandomInteger(0, 10) / 10f
//                if(LOG_ME)ALog.d(TAG, "HomeScreenUIComposableTest1(): " +
//                        "randomFloat == $randomFloat")
//                viewModel.setProgressIndicatorState(
//                    ProgressIndicatorState(
//                        show = true,
//                        progress = randomFloat,
//                        type = ProgressIndicatorType.IndeterminateCircularProgressIndicator,
//                        updateProgressIndicatorStateInViewModel = {
//                            if(LOG_ME)ALog.d(TAG, "updateProgressIndicatorStateInViewModel(): " +
//                                    "Updating ProgressIndicatorState in viewModel.")
//                            viewModel.setProgressIndicatorState(it)
//                        },
//                    )
//                )
//            }
            //==================================

            HomeScreenUI(
                initialUsersPresentationsSearchQuery = "",
                onUsersPresentationsSearchQueryUpdate = onUsersPresentationsSearchQueryUpdate,
                initialDoneFieldSurveysSearchQuery = "",
                onSearchQueryUpdateDoneFieldSurveys = onSearchQueryUpdateFieldSurveyTemplates,
                initialSearchQueryClientContacts = "",
                onClientContactSearchQueryUpdate = onClientContactSearchQueryUpdate,
                launchStateEvent = launchStateEvent,

                activity = activity,
                permissionHandlingData = permissionHandlingData,

                stateEventTracker = stateEventTracker,

                deviceLocation = deviceLocation,
                snackBarState = snackBarState,
                toastState = toastState,
                dialogState = dialogStatus,
                progressIndicatorState = progressIndicatorState,
                isDrawerOpen = isDrawerOpen,

                updateOrientation = updateOrientation,
                leftPaneStartDestination = leftPaneStartDestination,
                rightPaneStartDestination = rightPaneStartDestination,

                showProfileStatusBar = showProfileStatusBar,
                card1SearchQuery = card1SearchQuery,
                card1OnSearchQueryUpdate = card1OnSearchQueryUpdate,
                searchFilters = searchFilters,
                onSearchFiltersUpdated = onSearchFiltersUpdated,
                floatingActionButtonDrawableIdCard1 = null,
                floatingActionButtonOnClickCard1 = null,
                floatingActionButtonContentDescriptionCard1 = null,
                card1EntitiesList = card1EntitiesList,
                card1OnListItemClick = card1OnListItemClick,
                card1BottomSheetActions = HomeScreenComposableFragment1BottomSheetActions(
                    leftButtonOnClick = {},
                    rightButtonOnClick = {},
                ),
                card1CurrentlyShownEntity = card1CurrentlyShownEntity,
                card1ActionOnEntity = card1ActionOnEntity,

                floatingActionButtonDrawableIdCard2 = floatingActionButtonDrawableIdCard2,
                floatingActionButtonOnClickCard2 = floatingActionButtonOnClickCard2,
                floatingActionButtonContentDescriptionCard2 = floatingActionButtonContentDescriptionCard2,
                card2EntitiesList = card2EntitiesList,
                card2OnListItemClick = card2OnListItemClick,
                card2BottomSheetActions = HomeScreenComposableFragment2BottomSheetActions(
                    leftButtonOnClick = {},
                    rightButtonOnClick = {},
                ),

                floatingActionButtonDrawableIdCard3 = floatingActionButtonDrawableIdCard3,
                floatingActionButtonOnClickCard3 = floatingActionButtonOnClickCard3,
                floatingActionButtonContentDescriptionCard3 = floatingActionButtonContentDescriptionCard3,
                card3EntitiesList = card3EntitiesList,
                card3OnListItemClick = card3OnListItemClick,
                card3BottomSheetActions = HomeScreenComposableFragment3BottomSheetActions(
                    leftButtonOnClick = {},
                    rightButtonOnClick = {},
                ),

                floatingActionButtonDrawableIdCard4 = floatingActionButtonDrawableIdCard4,
                floatingActionButtonOnClickCard4 = floatingActionButtonOnClickCard4,
                floatingActionButtonContentDescriptionCard4 = floatingActionButtonContentDescriptionCard4,
                card4EntitiesList = card4EntitiesList,
                card4OnListItemClick = card4OnListItemClick,
                card4BottomSheetActions = HomeScreenComposableFragment4BottomSheetActions(
                    leftButtonOnClick = {},
                    rightButtonOnClick = {},
                ),

                card5BottomSheetActions = HomeScreenComposableFragment5BottomSheetActions(
                    leftButtonOnClick = {},
                    rightButtonOnClick = {},
                ),
            )

            if(LOG_ME) ALog.d(TAG, "getComposable(): " +
                    "Recomposition end."
            )
        }
    }
}