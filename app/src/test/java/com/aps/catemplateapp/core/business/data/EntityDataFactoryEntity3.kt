package com.aps.catemplateapp.core.business.data

import com.aps.catemplateapp.core.util.Entity3
import com.google.common.reflect.TypeToken
import com.google.gson.Gson

class EntityDataFactoryEntity3(
    override val testClassLoader: ClassLoader,
    override val fileNameWithTestData: String): EntityDataFactory<Entity3>(testClassLoader, fileNameWithTestData) {


    override fun produceListOfEntities(): List<Entity3>{
        val entities: List<Entity3> = Gson()
            .fromJson(
                getEntitiesFromFile(fileNameWithTestData),
                object: TypeToken<List<Entity3>>() {}.type
            )
        return entities
    }
}