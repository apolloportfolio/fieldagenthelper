package com.aps.catemplateapp.core.di


import com.aps.catemplateapp.common.util.DateUtil
import com.aps.catemplateapp.core.business.data.EntityDataFactoryEntity1
import com.aps.catemplateapp.core.business.data.EntityDataFactoryEntity2
import com.aps.catemplateapp.core.business.data.EntityDataFactoryEntity3
import com.aps.catemplateapp.core.business.data.EntityDataFactoryUser
import com.aps.catemplateapp.core.business.data.cache.abstraction.UserCacheDataSource
import com.aps.catemplateapp.core.business.data.cache.implementation.FakeEntity1CacheDataSourceImpl
import com.aps.catemplateapp.core.business.data.cache.implementation.FakeEntity2CacheDataSourceImpl
import com.aps.catemplateapp.core.business.data.cache.implementation.FakeEntity3CacheDataSourceImpl
import com.aps.catemplateapp.core.business.data.cache.implementation.FakeUserCacheDataSourceImpl
import com.aps.catemplateapp.core.business.data.cache.implementation.UserCacheDataSourceImpl
import com.aps.catemplateapp.core.business.data.network.abstraction.UserNetworkDataSource
import com.aps.catemplateapp.core.business.data.network.implementation.FakeEntity1NetworkDataSourceImpl
import com.aps.catemplateapp.core.business.data.network.implementation.FakeEntity2NetworkDataSourceImpl
import com.aps.catemplateapp.core.business.data.network.implementation.FakeEntity3NetworkDataSourceImpl
import com.aps.catemplateapp.core.business.data.network.implementation.FakeUserNetworkDataSourceImpl
import com.aps.catemplateapp.core.business.data.util.FakeFirestoreIDGenerator
import com.aps.catemplateapp.core.business.domain.model.entities.ProjectUser
import com.aps.catemplateapp.core.business.domain.model.factories.UserFactory
import com.aps.catemplateapp.core.util.Entity1CacheDataSource
import com.aps.catemplateapp.core.util.Entity1Factory
import com.aps.catemplateapp.core.util.Entity1NetworkDataSource
import com.aps.catemplateapp.core.util.Entity2CacheDataSource
import com.aps.catemplateapp.core.util.Entity2Factory
import com.aps.catemplateapp.core.util.Entity2NetworkDataSource
import com.aps.catemplateapp.core.util.Entity3CacheDataSource
import com.aps.catemplateapp.core.util.Entity3Factory
import com.aps.catemplateapp.core.util.Entity3NetworkDataSource
import com.aps.catemplateapp.feature01.business.interactors.registerlogin.RegisterLoginInteractors
import com.aps.catemplateapp.feature01.framework.presentation.activity01.state.RegisterLoginActivityViewState
import java.text.SimpleDateFormat
import java.util.*

class DependencyContainer {
    private val dateFormatForFirestore = SimpleDateFormat("yyyy-MM-dd hh:mm:ss a", Locale.ENGLISH)
    private val dateFormatForUser = SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH)
    private val dateFormatJustYear = SimpleDateFormat("yyyy", Locale.ENGLISH)
    val dateUtil = DateUtil(dateFormatForFirestore, dateFormatForUser, dateFormatJustYear)
    lateinit var entity1Factory: Entity1Factory
    lateinit var entity1DataFactory: EntityDataFactoryEntity1
    lateinit var entity1CacheDataSource: Entity1CacheDataSource
    lateinit var entity1NetworkDataSource: Entity1NetworkDataSource

    lateinit var entity3Factory: Entity3Factory
    lateinit var entity3DataFactory: EntityDataFactoryEntity3
    lateinit var entity3CacheDataSource: Entity3CacheDataSource
    lateinit var entity3NetworkDataSource: Entity3NetworkDataSource

    lateinit var entity2Factory: Entity2Factory
    lateinit var entity2DataFactory: EntityDataFactoryEntity2
    lateinit var entity2CacheDataSource: Entity2CacheDataSource
    lateinit var entity2NetworkDataSource: Entity2NetworkDataSource

    lateinit var userFactory: UserFactory
    lateinit var userDataFactory: EntityDataFactoryUser
    lateinit var userCacheDataSource: UserCacheDataSource
    lateinit var userNetworkDataSource: UserNetworkDataSource

    lateinit var interactorsStart: RegisterLoginInteractors<
            ProjectUser,
            UserCacheDataSourceImpl,
            UserNetworkDataSource,
            RegisterLoginActivityViewState<ProjectUser>
            >

    
    init {
    }

    fun build() {
        entity1Factory = Entity1Factory(dateUtil)
        this.javaClass.classLoader?.let { classLoader ->
            entity1DataFactory = EntityDataFactoryEntity1(classLoader, "entity1_test_list.json")
        }
        entity1CacheDataSource = FakeEntity1CacheDataSourceImpl(
            entitiesData = entity1DataFactory.produceHashMapOfEntities(
                entity1DataFactory.produceListOfEntities(),
                { it.id!! }
            ),
            dateUtil = dateUtil
        )
        entity1NetworkDataSource = FakeEntity1NetworkDataSourceImpl(
            entitiesData = entity1DataFactory.produceHashMapOfEntities(
                entity1DataFactory.produceListOfEntities(),
                { it.id!! }
            ),
            deletedEntitiesData = HashMap(),
            FakeFirestoreIDGenerator(),
            dateUtil = dateUtil
        )

        entity2Factory = Entity2Factory(dateUtil)
        this.javaClass.classLoader?.let { classLoader ->
            entity2DataFactory = EntityDataFactoryEntity2(classLoader, "entity2_test_list.json")
        }
        entity2CacheDataSource = FakeEntity2CacheDataSourceImpl(
            entitiesData = entity2DataFactory.produceHashMapOfEntities(
                entity2DataFactory.produceListOfEntities(),
                { it.id!! }
            ),
            dateUtil = dateUtil
        )
        entity2NetworkDataSource = FakeEntity2NetworkDataSourceImpl(
            data = entity2DataFactory.produceHashMapOfEntities(
                entity2DataFactory.produceListOfEntities(),
                { it.id!! }
            ),
            deletedData = HashMap(),
            FakeFirestoreIDGenerator(),
            dateUtil = dateUtil
        )

        entity3Factory = Entity3Factory(dateUtil)
        this.javaClass.classLoader?.let { classLoader ->
            entity3DataFactory = EntityDataFactoryEntity3(classLoader, "entity3_test_list.json")
        }
        entity3CacheDataSource = FakeEntity3CacheDataSourceImpl(
            entitiesData = entity3DataFactory.produceHashMapOfEntities(
                entity3DataFactory.produceListOfEntities(),
                { it.id!! }
            ),
            dateUtil = dateUtil
        )
        entity3NetworkDataSource = FakeEntity3NetworkDataSourceImpl(
            data = entity3DataFactory.produceHashMapOfEntities(
                entity3DataFactory.produceListOfEntities(),
                { it.id!! }
            ),
            deletedData = HashMap(),
            FakeFirestoreIDGenerator(),
            dateUtil = dateUtil
        )

        userFactory = UserFactory(dateUtil)
        this.javaClass.classLoader?.let { classLoader ->
            userDataFactory = EntityDataFactoryUser(classLoader, "user_test_list.json")
        }
        userCacheDataSource = FakeUserCacheDataSourceImpl(
            entitiesData = userDataFactory.produceHashMapOfUsers(
                userDataFactory.produceListOfEntities(),
                { it.id!!.firestoreDocumentID }
            ),
            dateUtil = dateUtil
        )
        val hashMapOfUsers = userDataFactory.produceHashMapOfUsers(
            userDataFactory.produceListOfEntities(),
            { it.id!!.firestoreDocumentID }
        )
        userNetworkDataSource = FakeUserNetworkDataSourceImpl(
            data = hashMapOfUsers,
            deletedData = HashMap(),
            ratings = userDataFactory.produceHashMapOfUsersRatings(
                hashMapOfUsers,
                { it.id!! },
            ),
            fakeFirestoreIDGenerator = FakeFirestoreIDGenerator(),
            dateUtil = dateUtil
        )
    }
}