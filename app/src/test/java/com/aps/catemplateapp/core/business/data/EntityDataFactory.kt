package com.aps.catemplateapp.core.business.data

import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.core.business.domain.model.entities.ProjectUsersRating
import com.google.common.reflect.TypeToken
import com.google.gson.Gson
import kotlin.math.roundToInt

abstract class EntityDataFactory<Entity>(
    open val testClassLoader: ClassLoader,
    open val fileNameWithTestData: String) {


    open fun produceListOfEntities(): List<Entity>{
        val entities: List<Entity> = Gson()
            .fromJson(
                getEntitiesFromFile(fileNameWithTestData),
                object: TypeToken<List<Entity>>() {}.type
            )
        return entities
    }


    fun produceHashMapOfEntities(entityList: List<Entity>, getId: (Entity)-> UniqueID): HashMap<UniqueID, Entity>{
        val map = HashMap<UniqueID, Entity>()
        for(entity in entityList){
            map[getId(entity)] = entity
        }
        return map
    }


    fun produceHashMapOfUsers(entityList: List<Entity>, getId: (Entity)-> String): HashMap<String, Entity>{
        val map = HashMap<String, Entity>()
        for(entity in entityList){
            map[getId(entity)] = entity
        }
        return map
    }


    fun produceHashMapOfUsersRatings(
        entityHashMap: HashMap<String, Entity>,
        getId: (Entity)-> UserUniqueID,
    ): HashMap<String, ProjectUsersRating>{
        val map = HashMap<String, ProjectUsersRating>()
        for(entity in entityHashMap.values){
            val usersRatingEntity = HashMap<String, String?>()
            val rnds1 = (0..4).random()
            val rnds2 = (0..9).random()
            val rnds3 = (0..9).random()
            val rnds4 = (0..9999).random()
            usersRatingEntity[ProjectUsersRating.FIELD_NAME_AVERAGE_RATING] =
                (((rnds1 + rnds2.toFloat()/10 + rnds3.toFloat()/100)*100).roundToInt()/100).toString()
            usersRatingEntity[ProjectUsersRating.FIELD_NAME_NUMBER_OF_RATINGS] = rnds4.toString()

            map[getId(entity).firestoreDocumentID] = ProjectUsersRating(
                getId(entity),
                usersRatingEntity,
            )
        }
        return map
    }

    fun produceEmptyListOfEntities(): List<Entity>{
        return ArrayList()
    }

    fun getEntitiesFromFile(fileName: String): String {
        return testClassLoader.getResource(fileName).readText()
    }
}